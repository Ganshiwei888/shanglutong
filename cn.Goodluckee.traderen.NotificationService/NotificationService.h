//
//  NotificationService.h
//  cn.Goodluckee.traderen.NotificationService
//
//  Created by KeWen on 2017/8/16.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end

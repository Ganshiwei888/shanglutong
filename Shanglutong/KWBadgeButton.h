//
//  KWBadgeButton.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWBadgeButton : UIButton

/**
 提醒数字
 */
@property (nonatomic, copy) NSString *badgeValue;

@end

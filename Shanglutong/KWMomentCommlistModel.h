//
//  KWMomentCommlistModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

    //                                 Bak = 66666;
    //                                 CreateTm = "2017-08-21T16:36:51.677";
    //                                 Creater = admin;
    //                                 CreaterName = "\U7ba1\U7406\U5458";
    //                                 Id = 191;
    //                                 KeyId = 51;
    //                                 MenuNo = AT;
    //                                 ShareUID = ",1,121,126,147,";
    //                                 ShareUID1 = "";
    //                                 TableName = "WorkDynamic_T";
    //                                 TitleHtml = 6666;
    //                                 WdId = 51;
    //                             },

@interface KWMomentCommlistModel : NSObject

@property (nonatomic, copy) NSString *Bak;
@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *KeyId;
@property (nonatomic, copy) NSString *MenuNo;
@property (nonatomic, copy) NSString *ShareUID;
@property (nonatomic, copy) NSString *ShareUID1;
@property (nonatomic, copy) NSString *TableName;
@property (nonatomic, copy) NSString *TitleHtml;
@property (nonatomic, copy) NSString *WdId;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
















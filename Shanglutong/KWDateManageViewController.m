//
//  KWDateManageViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDateManageViewController.h"
#import "KWDateManageModel.h"
#import "KWDateManageFrame.h"
#import "KWDateManageViewCell.h"
#import "KWDateManage.h"
#import "KWDateAddManageParam.h"
#import "KWDateAddViewController.h"
#import "KWNavigationController.h"
#import "KWHttpTool.h"
#import "Daysquare.h"
#import "Utility.h"
#import <MJRefresh.h>
#import <MJExtension.h>


@interface KWDateManageViewController ()<UITableViewDataSource, UITableViewDelegate> {
    CGFloat contentOffsetY;
    CGFloat oldContentOffsetY;
    CGFloat newContentOffsetY;
}

@property (weak, nonatomic) IBOutlet DAYCalendarView *calendarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *listArray;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;


@end

@implementation KWDateManageViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    self.title = @"代办日程";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backBtn:)];
    [self setupDateView];
    [self setupTableView];
    // Do any additional setup after loading the view.
    [self setupDateManage];
}

- (void)setupNav {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addDate:)];
}

- (void)addDate:(UIButton *)sender {
    KWDateAddViewController *vc = [[KWDateAddViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)setupDateView {
    
//    DAYCalendarView *calendarView = [[DAYCalendarView alloc] initWithFrame:CGRectMake(0, 65, KWSCREEN_WIDTH, 240)];
//    calendarView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:calendarView];
//    self.calendarView = calendarView;
    [self.calendarView addTarget:self action:@selector(datePickerDidChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)setupTableView {
    
}

- (void)setupDateManage {
    
    KWDateManage *param = [KWDateManage param];
    param.Act = @"all";
//    param.date = @"2017-12-30";
//    param.fh = @"=";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getpeding",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSArray *getAll = json[@"back"][@"getall"];
        if (getAll) {
            NSMutableArray *temp = [NSMutableArray array];
            for (NSDictionary *dict in getAll) {
                KWDateManageModel *model = [[KWDateManageModel alloc] initWithDict:dict];
                KWDateManageFrame *frame = [[KWDateManageFrame alloc] init];
                frame.manageModel = model;
                [temp addObject:frame];
            }
            self.listArray = temp;
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - UIScrollViewdelegate - 

// 开始拖拽
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"++开始拖拽");
    contentOffsetY = scrollView.contentOffset.y;
    NSLog(@"%f",contentOffsetY);
    
//    if (contentOffsetY > 0) {
//        
//        BOOL flag = YES;
//        self.calendarHeightConstraint.constant = flag ? 100 : 250;
//        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.72 initialSpringVelocity:0 options:kNilOptions animations:^{
//            [self.view layoutIfNeeded];
//            self.calendarView.singleRowMode = flag;
//        } completion:nil];
//        
//    }
//    
//    if (contentOffsetY < 0) {
//        BOOL flag = NO;
//        self.calendarHeightConstraint.constant = flag ? 100 : 250;
//        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.72 initialSpringVelocity:0 options:kNilOptions animations:^{
//            [self.view layoutIfNeeded];
//            self.calendarView.singleRowMode = flag;
//        } completion:nil];
//    }
    
}

// 滚动时调用此方法(手指离开屏幕后)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //NSLog(@"scrollView.contentOffset:%f, %f", scrollView.contentOffset.x, scrollView.contentOffset.y);
    newContentOffsetY = scrollView.contentOffset.y;
    if (newContentOffsetY > oldContentOffsetY && oldContentOffsetY > contentOffsetY) { // 向上滚动
        NSLog(@"up");
    } else if (newContentOffsetY < oldContentOffsetY && oldContentOffsetY < contentOffsetY) {// 向下滚动
        NSLog(@"down");
    } else {
        //        NSLog(@"dragging");
    }
    if (scrollView.dragging) { // 拖拽
        //        NSLog(@"scrollView.dragging");
        //        NSLog(@"contentOffsetY: %f", contentOffsetY);
        //        NSLog(@"newContentOffsetY: %f", scrollView.contentOffset.y);
        if ((scrollView.contentOffset.y - contentOffsetY) > 5.0f) {  // 向上拖拽
            NSLog(@"向上拖拽");
            BOOL flag = YES;
            self.calendarHeightConstraint.constant = flag ? 100 : 250;
            [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.72 initialSpringVelocity:0 options:kNilOptions animations:^{
                [self.view layoutIfNeeded];
                self.calendarView.singleRowMode = flag;
            } completion:nil];
       
        }
        else if ((contentOffsetY - scrollView.contentOffset.y) > 5.0f)
        {   // 向下拖拽
            NSLog(@"--向下拖拽");
            BOOL flag = NO;
            self.calendarHeightConstraint.constant = flag ? 100 : 250;
            [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.72 initialSpringVelocity:0 options:kNilOptions animations:^{
                [self.view layoutIfNeeded];
                self.calendarView.singleRowMode = flag;
            } completion:nil];
        } else {
            
        }
    }
}

// 完成拖拽(滚动停止时调用此方法，手指离开屏幕前)
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    oldContentOffsetY = scrollView.contentOffset.y;
    NSLog(@"停止拖拽");
}

#pragma mark - UITableViewDataSource && UITableViewdelegate -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"date";
    KWDateManageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[KWDateManageViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.manageFrame = self.listArray[indexPath.row];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark -

- (void)datePickerDidChange:(id)sender {
    NSLog(@"aaa%@",self.calendarView.selectedDate);

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：zzz表示时区
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    //NSDate转NSString
    NSString *currentDateString = [dateFormatter stringFromDate:self.calendarView.selectedDate];
    NSString *dateStr = [currentDateString substringToIndex:10];
    NSLog(@"%@",dateStr);
    [self setupPickDate: dateStr];
    
}

- (void)setupPickDate:(NSString *)selectDate {
    
    KWDateManage *param = [KWDateManage param];
    param.Act = @"all";
    param.date = selectDate;
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getpeding",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSArray *getAll = json[@"back"][@"getall"];
        if (getAll) {
            NSMutableArray *temp = [NSMutableArray array];
            for (NSDictionary *dict in getAll) {
                KWDateManageModel *model = [[KWDateManageModel alloc] initWithDict:dict];
                KWDateManageFrame *frame = [[KWDateManageFrame alloc] init];
                frame.manageModel = model;
                [temp addObject:frame];
            }
            self.listArray = temp;
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end





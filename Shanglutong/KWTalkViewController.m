//
//  KWTalkViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/9.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWTalkViewController.h"
#import "Utility.h"

@interface KWTalkViewController () <EMChatManagerDelegate, EMClientDelegate>

@end

@implementation KWTalkViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self tableViewDidTriggerHeaderRefresh];
    self.showRefreshHeader = YES;
    
//    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
}

- (void)viewDidLoad {
    [self setupNav];
    [super viewDidLoad];
    self.title = @"会话";
//    [self tableViewDidTriggerHeaderRefresh];
//    self.showRefreshHeader = YES;
    // Do any additional setup after loading the view from its nib.
    
        
    
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    
}


//
//- (id<IConversationModel>)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
//                                    modelForConversation:(EMConversation *)conversation {
//    EaseConversationModel *model = [[EaseConversationModel alloc] initWithConversation:conversation];
//    if (model.conversation.type == EMConversationTypeChat) {
//        
//        NSDictionary *ext = conversation.lastReceivedMessage.ext;
//        if (ext[@"userImage"] == nil || ext[@"userName"] == nil ) {
//            
//            model.title = ext[@"userName"];
//            model.avatarURLPath = @"http://upload-images.jianshu.io/upload_images/3996989-50a0375d45bff8b1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240";
//            
//        } else {
//            
//           
//            
//        }
//        
//    } else if (model.conversation.type == EMConversationTypeGroupChat) {
//        NSString *imageName = @"groupPublicHeader";
//        if (![conversation.ext objectForKey:@"subject"])
//        {
//            NSArray *groupArray = [[EMClient sharedClient].groupManager getJoinedGroups];
//            for (EMGroup *group in groupArray) {
//                if ([group.groupId isEqualToString:conversation.conversationId]) {
//                    NSMutableDictionary *ext = [NSMutableDictionary dictionaryWithDictionary:conversation.ext];
//                    [ext setObject:group.subject forKey:@"subject"];
//                    [ext setObject:[NSNumber numberWithBool:group.isPublic] forKey:@"isPublic"];
//                    conversation.ext = ext;
//                    break;
//                }
//            }
//        }
//        NSDictionary *ext = conversation.ext;
//        model.title = [ext objectForKey:@"subject"];
//        imageName = [[ext objectForKey:@"isPublic"] boolValue] ? @"groupPublicHeader" : @"groupPrivateHeader";
//        model.avatarImage = [UIImage imageNamed:imageName];
//    }
//    return model;
//}

- (void)messagesDidReceive:(NSArray *)aMessages {
    [self refreshAndSortView];
    [self tableViewDidTriggerHeaderRefresh];
    self.showRefreshHeader = YES;
}

- (void)setupNav {
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

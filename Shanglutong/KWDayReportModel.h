//
//  KWDayReportModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWDayReportModel : NSObject

@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *Def;
@property (nonatomic, copy) NSString *DeptIdStr;
@property (nonatomic, copy) NSString *Html;
@property (nonatomic, copy) NSString *Name;
@property (nonatomic, copy) NSString *UpdateTm;
@property (nonatomic, copy) NSString *Updater;
@property (nonatomic, copy) NSString *UpdaterName;
@property (nonatomic, copy) NSString *Type;
@property (nonatomic, copy) NSString *Id;

- (instancetype)initWithDict:(NSDictionary *)dict;


@end
//
//CreateTm = "2017-08-03T10:54:47.37";
//Creater = admin;
//CreaterName = "\U7ba1\U7406\U5458";
//Def = "<null>";
//DeptIdStr = "2,27,29,6,11,25";
//Html = "";
//Name = test2;
//Type = 2;
//UpdateTm = "<null>";
//Updater = "<null>";
//UpdaterName = "<null>";
//id = 40;





//
//  KWWorkBenchViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/9.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWWorkBenchViewController : UIViewController


@property(nonatomic, strong)NSMutableArray *gridListArray;
@property(nonatomic, strong)NSArray *showGridArray; //title
@property(nonatomic, strong)NSArray *showGridImageArray;//image

@property(nonatomic, strong)UIView  *gridListView;
@property(nonatomic, strong)NSMutableArray *showGridIDArray;//gridId



@end

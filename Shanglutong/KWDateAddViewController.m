//
//  KWDateAddViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDateAddViewController.h"
#import "DatePickerView.h"
#import "KWHttpTool.h"
#import "KWDateAddManageParam.h"
#import "Utility.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>
@interface KWDateAddViewController () <UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) UITextView *textView;

@property (weak, nonatomic) UILabel *startTime;
@property (weak, nonatomic) UILabel *endTime;
@property (nonatomic,strong) DatePickerView *datePicker;
@property (nonatomic, weak) UILabel *crashLabel;
@property (weak, nonatomic) UILabel *repeatValueLabel;
@property (nonatomic, weak) UIButton *colorBtn;
@property (weak, nonatomic) UILabel *remindValueLabel;

@property (weak, nonatomic) UIPickerView *repeatPickView;
@property (strong, nonatomic) NSArray *repeatArray;

@property (weak, nonatomic) UIPickerView *remindPickView;
@property (strong, nonatomic) NSArray *remindArray;

@property (copy, nonatomic) NSString *level;




@end

@implementation KWDateAddViewController

- (NSArray *)repeatArray {
    if (!_repeatArray) {
        _repeatArray = @[@"不重复",@"每天",@"每周",@"每月"];
    }
    return _repeatArray;
}

- (NSArray *)remindArray {
    if (!_remindArray) {
        _remindArray = @[@"不提醒",@"事件发生时",@"5分钟前",@"15分钟前",@"30分钟前",@"1小时前",@"2小时前",@"3小时前",@"4小时前",@"1天前",@"1天前",@"2天前",@"3天前",@"4天前",@"1周前",@"2周前",@"3周前",@"4周前"];
    }
    return _remindArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"新建日程";
    self.view.backgroundColor = RGBCOLOR(252, 252, 252);
    [self setupNav];
    [self setupView];
    // Do any additional setup after loading the view.
}

- (void)setupNav {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtn:)];
}

- (void)saveBtn:(UIButton *)sender {
    
    KWDateAddManageParam  *param = [KWDateAddManageParam param];
    param.menuno = @"at";
    param.start_date = self.startTime.text;
    param.end_date = self.endTime.text;
    param.is_repeat = @"0";
    param.clientid = @"0";
    param.note = self.textView.text;
    param.Urgency_level = self.level;
    param.Reminder = @"3";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Addpeding",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json[@"back"]);
        [SVProgressHUD showInfoWithStatus:@"添加成功"];
        [SVProgressHUD dismissWithDelay:0.4];
    } failure:^(NSError *error) {
        
    }];
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)setupView {
    [self setupTextView];
    [self setupDatePick];
    [self setupColorView];
    [self setupRepeatView];
    [self setupRemindView];
}

- (void)setupTextView {
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8, 8, KWSCREEN_WIDTH - 16,  180)];
    textView.delegate = self;
    textView.text = @"添加代办内容: ";
    textView.textColor = [UIColor grayColor];
    [self.view addSubview:textView];
    self.textView = textView;
}

- (void)setupDatePick {
    
    UILabel *startDate = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.textView.frame) + 8, 80, 20)];
    startDate.text = @"起始日期";
    startDate.textColor = [UIColor darkGrayColor];
    startDate.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:startDate];
    
    UILabel *startTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(startDate.frame) + 4, CGRectGetMaxY(self.textView.frame) + 8, 120, 20)];
    startTime.textColor = RGBCOLOR(38, 59, 138);
    startTime.font = [UIFont systemFontOfSize:14];
//    startTime.text = @"2017-08-29";
    [self.view addSubview:startTime];
    self.startTime = startTime;
    
    UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [startBtn setFrame:CGRectMake(CGRectGetMaxX(startTime.frame) + 4, CGRectGetMaxY(self.textView.frame) + 8, 120, 20)];
    [startBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [startBtn setTitle:@"请选择开始日期" forState:UIControlStateNormal];
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:@"请选择开始日期" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    [startBtn setTintColor:[UIColor redColor]];
    [startBtn setAttributedTitle:attStr forState:UIControlStateNormal];
    [startBtn addTarget:self action:@selector(pickupDate:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startBtn];
    
    UILabel *endDate = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(startDate.frame) + 8, 80, 20)];
    endDate.text = @"截止日期";
    endDate.textColor = [UIColor darkGrayColor];
    endDate.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:endDate];
    
    UILabel *endTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(endDate.frame) + 4, CGRectGetMaxY(startTime.frame) + 8, 120, 20)];
    endTime.textColor = RGBCOLOR(38, 59, 138);
    endTime.font = [UIFont systemFontOfSize:14];
//    endTime.text = @"2017-08-29";
    [self.view addSubview:endTime];
    self.endTime = endTime;
    
    UIButton *endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [endBtn setFrame:CGRectMake(CGRectGetMaxX(endTime.frame) + 4, CGRectGetMaxY(startBtn.frame) + 8, 120, 20)];
    [endBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [endBtn setTitle:@"请选择结束日期" forState:UIControlStateNormal];
    NSAttributedString *attStrT = [[NSAttributedString alloc] initWithString:@"请选择结束日期" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    [endBtn setTintColor:[UIColor redColor]];
    [endBtn setAttributedTitle:attStrT forState:UIControlStateNormal];
    [endBtn addTarget:self action:@selector(pickupDateTwo:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:endBtn];
    
}

- (void)pickupDate:(UIButton *)sender {
    
    __weak KWDateAddViewController * weakself=self;
    self.datePicker =[[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:self options:0] lastObject];
    self.datePicker.frame =CGRectMake(0, self.view.frame.size.height-260, self.view.frame.size.width, 260);
    self.datePicker.Datetitle =@"日期选择";
    self.datePicker.cancelBlock = ^{
        [UIView animateWithDuration:5 animations:^{
            [weakself.datePicker removeFromSuperview];
        }];
    };
    self.datePicker.sureBlock = ^(NSString *selectDateStr) {
        [weakself.startTime setText:selectDateStr];
        [UIView animateWithDuration:5 animations:^{
            [weakself.datePicker removeFromSuperview];
        }];
    };
    [self.view addSubview:self.datePicker];
}

- (void)pickupDateTwo:(UIButton *)sender {
    __weak KWDateAddViewController * weakself=self;
    self.datePicker =[[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:self options:0] lastObject];
    self.datePicker.frame =CGRectMake(0, self.view.frame.size.height-260, self.view.frame.size.width, 260);
    self.datePicker.Datetitle =@"日期选择";
    self.datePicker.cancelBlock = ^{
        [UIView animateWithDuration:5 animations:^{
            [weakself.datePicker removeFromSuperview];
        }];
    };
    self.datePicker.sureBlock = ^(NSString *selectDateStr) {
        [weakself.endTime setText:selectDateStr];
        [UIView animateWithDuration:5 animations:^{
            [weakself.datePicker removeFromSuperview];
        }];
    };
    [self.view addSubview:self.datePicker];
}

- (void)setupColorView {
    
    UILabel *crashLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.endTime.frame) + 8, 80, 20)];
    crashLabel.text = @"紧急程度";
    crashLabel.textColor = [UIColor darkGrayColor];
    crashLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:crashLabel];
    self.crashLabel = crashLabel;
    for (int i = 0; i < 6; i ++) {
        [self createBtnWith:i];
    }
}

- (void)createBtnWith:(NSInteger)index {
    
    CGFloat buttonW = (KWSCREEN_WIDTH - 25) / 6;
    CGFloat buttonH = 30;
    CGFloat buttonX = 12.5;
    CGFloat buttonY = CGRectGetMaxY(self.crashLabel.frame) + 8;
    
    UIButton *colorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [colorBtn setFrame:CGRectMake((buttonX + buttonW * index ) , buttonY, buttonW, buttonH)];
    
//    NSString *imageName = [NSString stringWithFormat:@"color%ld",(long)index];
//    NSString *imagedName = [NSString stringWithFormat:@"colored%ld",(long)index];
//    [colorBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
//    [colorBtn setImage:[UIImage imageNamed:imagedName] forState:UIControlStateSelected];
    
    UIColor *btnColor = [[UIColor alloc] init];
    if (index == 0) {
        btnColor = RGBCOLOR(61, 202, 63);
    }
    if (index == 1) {
        btnColor = RGBCOLOR(130, 193, 47);
    }
    if (index == 2) {
        btnColor = RGBCOLOR(255, 253, 56);
    }
    if (index == 3) {
        btnColor = RGBCOLOR(253, 191, 45);
    }
    if (index == 4) {
        btnColor = RGBCOLOR(253, 127, 35);
    }
    if (index == 5) {
        btnColor = RGBCOLOR(251, 13, 27);
    }
    [colorBtn setBackgroundColor:btnColor];
    colorBtn.tag = 200 + index;
    colorBtn.layer.borderWidth = 0.6;
    colorBtn.layer.borderColor = [UIColor blackColor].CGColor;
    if (index == 0) {
        [colorBtn setSelected:YES];
        self.level = @"1";
    }
    NSAttributedString *attStrT = [[NSAttributedString alloc] initWithString:@"已选" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}];
    [colorBtn setAttributedTitle:attStrT forState:UIControlStateSelected];
    [colorBtn addTarget:self action:@selector(colorBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:colorBtn];
    self.colorBtn = colorBtn;
}

- (void)colorBtn:(UIButton *)sender {
    
//    NSInteger selectTag = sender.tag;
    if (sender.selected) {
        
        return;
    } else {
        for (int i = 0; i < 6; i ++) {
            UIButton *button = [self.view viewWithTag:i + 200];
            NSString *levelStr = [NSString stringWithFormat:@"%d",i];
            self.level = levelStr;
            button.selected = NO;
            sender.selected = YES;
            NSAttributedString *attStrT = [[NSAttributedString alloc] initWithString:@"已选" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}];
            [sender setAttributedTitle:attStrT forState:UIControlStateSelected];
        }
    }
    
}

- (void)setupRepeatView {
    
    UILabel *repeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.colorBtn.frame) + 20, 60, 20)];
    repeatLabel.text = @"是否重复";
    repeatLabel.textColor = [UIColor darkGrayColor];
    repeatLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:repeatLabel];
    
    UILabel *repeatValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repeatLabel.frame) + 8, CGRectGetMaxY(self.colorBtn.frame) + 20, 60, 20)];
    repeatValueLabel.text = @"不重复";
    repeatValueLabel.textColor = [UIColor redColor];
    repeatValueLabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:repeatValueLabel];
    self.repeatValueLabel = repeatValueLabel;
    
    UIPickerView *repeatPickView = [[UIPickerView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.repeatValueLabel.frame) + 4, KWSCREEN_WIDTH - 160, 100)];
    repeatPickView.delegate = self;
    repeatPickView.dataSource = self;
    [self.view addSubview:repeatPickView];
    self.repeatPickView = repeatPickView;
}

- (void)setupRemindView {
    UILabel *remindLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.repeatPickView.frame) + 8, 60, 20)];
    remindLabel.text = @"提醒时间";
    remindLabel.textColor = [UIColor darkGrayColor];
    remindLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:remindLabel];
    
    UILabel *remindValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(remindLabel.frame) + 8, CGRectGetMaxY(self.repeatPickView.frame) + 8, 80, 20)];
    remindValueLabel.text = @"不提醒";
    remindValueLabel.textColor = [UIColor redColor];
    remindValueLabel.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:remindValueLabel];
    self.remindValueLabel = remindValueLabel;
    
    UIPickerView *remindPickView = [[UIPickerView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(self.remindValueLabel.frame) + 4, KWSCREEN_WIDTH - 160, 100)];
    remindPickView.delegate = self;
    remindPickView.dataSource = self;
    [self.view addSubview:remindPickView];
    self.remindPickView = remindPickView;
}

#pragma mark - UIPickViewDelegate - 

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (pickerView == self.repeatPickView) {
        return 1;
    } else {
        return 1;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.repeatPickView) {
        if (component == 0) {
            return self.repeatArray.count;
        } else {
            return 0;
        }
    } else {
        if (component == 0) {
            return self.remindArray.count;
        } else {
            return 0;
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == self.repeatPickView) {
        if (component == 0) {
            return self.repeatArray[row];
        } else {
            return 0;
        }
    } else {
        if (component == 0) {
            return self.remindArray[row];
        } else {
            return 0;
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == self.repeatPickView) {
        [pickerView reloadComponent:0];
        NSInteger repeatRow = [pickerView selectedRowInComponent:0];
        [self.repeatValueLabel setText:[NSString stringWithFormat:@"%@",self.repeatArray[repeatRow]]];
    } else {
        [pickerView reloadComponent:0];
        NSInteger repeatRow = [pickerView selectedRowInComponent:0];
        [self.remindValueLabel setText:[NSString stringWithFormat:@"%@",self.remindArray[repeatRow]]];
    }
}

#pragma mark - TextViewDelegate -
- (void)textViewDidEndEditing:(UITextView *)textView {
    if(self.textView.text.length < 1){
        self.textView.text = @"添加代办内容: ";
        self.textView.textColor = [UIColor grayColor];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if([self.textView.text isEqualToString:@"添加代办内容: "]){
        self.textView.text=@"";
        self.textView.textColor=[UIColor blackColor];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

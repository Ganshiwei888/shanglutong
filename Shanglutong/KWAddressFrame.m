//
//  KWAddressFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressFrame.h"
#import "KWAddress.h"
#import "Utility.h"

@implementation KWAddressFrame

- (void)setAddress:(KWAddress *)address {
    
    _address = address;
    //width heoght
//    CGFloat cellWidth = [UIScreen mainScreen].bounds.size.width - KWAddressTableBorder * 2;
    
    //headView image
    CGFloat headimageViewW = 65;
    CGFloat headimageViewH = 65;
    CGFloat headimageViewX = 16;
    CGFloat headimageViewY = 16;
    _headimageView = CGRectMake(headimageViewX, headimageViewY, headimageViewW, headimageViewH);
    
    //addressName
    CGFloat addressNameW = 239;
    CGFloat addressNameH = 20;
    CGFloat addressNameX = headimageViewW + headimageViewX + 16;
    CGFloat addressNameY = CGRectGetMinY(_headimageView);
    _addresNameView = CGRectMake(addressNameX, addressNameY, addressNameW, addressNameH);
    
    //sex and age
    CGFloat sexAndageViewW = 55;
    CGFloat sexAndageViewH = 20;
    CGFloat sexAndageViewX = CGRectGetMinX(_addresNameView);
    CGFloat sexAndageViewY = CGRectGetMaxY(_addresNameView) + 5;
    _sexAndageView = CGRectMake(sexAndageViewX, sexAndageViewY, sexAndageViewW, sexAndageViewH);
    
    //workNameView
    CGFloat workNameViewW = 239;
    CGFloat workNameViewH = 20;
    CGFloat workNameViewX = CGRectGetMinX(_addresNameView);
    CGFloat workNameViewY = CGRectGetMaxY(_sexAndageView) + 5;
    _workNameView = CGRectMake(workNameViewX, workNameViewY, workNameViewW, workNameViewH);
    
//    //dividingView
    CGFloat dividingViewW = KWSCREEN_WIDTH - 16;
    CGFloat dividingViewH = 1;
    CGFloat dividingViewX = 0;
    CGFloat dividingViewY = 90;
    _dividingView = CGRectMake(dividingViewX, dividingViewY, dividingViewW, dividingViewH);
    
    //phoneImage
    CGFloat phoneImageW = 15;
    CGFloat phoneImageH = 15;
    CGFloat phoneImageX = 16;
    CGFloat phoneImageY = 99;
    _phoneImage = CGRectMake(phoneImageX, phoneImageY, phoneImageW, phoneImageH);

    //phoneNumView
    CGFloat phoneNumViewW = 110;
    CGFloat phoneNumViewH = 21;
    CGFloat phoneNumViewX = 97;
    CGFloat phoneNumViewY = 93;
    _phoneNumView = CGRectMake(phoneNumViewX, phoneNumViewY, phoneNumViewW, phoneNumViewH);
    
    //faceBookImage
    _faceBookImage = CGRectMake(16, CGRectGetMaxY(_phoneImage) + 8, 15, 15);
    //faceBookView
    _faceBookView = CGRectMake(97, CGRectGetMaxY(_phoneNumView) + 4, 180, 21);
    
    //emailImage
    _emailImage = CGRectMake(16, CGRectGetMaxY(_faceBookImage) + 8, 15, 15);
    //emailView
    _emailView = CGRectMake(97, CGRectGetMaxY(_faceBookView) + 4, 320, 21);
    
}

@end



















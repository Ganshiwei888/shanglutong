//
//  ZoomHeaderView.m
//  JXTableViewZoomHeaderImageView
//
//  Created by jiaxin on 15/12/17.
//  Copyright © 2015年 jiaxin. All rights reserved.
//

#import "ZoomHeaderView.h"
#import "KWMineViewController.h"
#import "Masonry.h"
#import "Utility.h"
#import "KWBaseParam.h"
#import "KWHttpTool.h"
#import <UIImageView+WebCache.h>
#import <MJExtension.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface ZoomHeaderView ()

@property (nonatomic, assign) ZoomHeaderViewType type;
@property (nonatomic, weak) UIImageView *headerImageView;
//没有约束
@property (nonatomic, assign) CGRect originalHeaderImageViewFrame;
//代码约束
@property (nonatomic, strong) MASConstraint *codeConstraintHeight;

@property (nonatomic, assign) CGFloat originalHeaderImageViewHeight;
//xib约束

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightOfHeaderImageView;
@property (copy, nonatomic) NSString *imageName;



@end

@implementation ZoomHeaderView

- (instancetype)initWithFrame:(CGRect)frame type:(ZoomHeaderViewType)type
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isNeedNarrow = YES;
        self.type = type;
        [self initUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.isNeedNarrow = YES;
        [self initUIXibConstraint];
    }
    return self;
}

- (void)initUI
{
    switch (self.type) {
        case ZoomHeaderViewTypeNoConstraint:
            [self initUINoConstraint];
            break;
        case ZoomHeaderViewTypeCodeConstraint:
            [self initUICodeConstraint];
            break;
        default:
            break;
    }
}

- (void)initUINoConstraint
{
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    headerImageView.clipsToBounds = YES;
    headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    headerImageView.image = [UIImage imageNamed:@"lufei.jpg"];
    [self addSubview:headerImageView];
    self.headerImageView = headerImageView;
    self.originalHeaderImageViewFrame = self.bounds;
}

- (void)initUICodeConstraint
{
    UIImageView *headerImageView = [[UIImageView alloc] init];
    headerImageView.clipsToBounds = YES;
    headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    headerImageView.image = [UIImage imageNamed:@"lufei.jpg"];
    [self addSubview:headerImageView];
    self.headerImageView = headerImageView;
    //约束设置为：跟父视图左、下、右贴紧，再约束高度，所以更新高度约束的时候会向上增加，xib约束同理
    [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.and.bottom.equalTo(self).offset(0);
        self.codeConstraintHeight = make.height.equalTo(@(self.bounds.size.height));
    }];
    self.originalHeaderImageViewHeight = self.bounds.size.height;
}

- (void)initUIXibConstraint
{
    self.type = ZoomHeaderViewTypeXibConstraint;
    self.originalHeaderImageViewHeight = self.bounds.size.height;

    [self setupUserImage];
    
    [self setupUserName];
    
    [self setupUserDuty];
    
    [self setupSubImage];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:ISSUBACCOUNT]) {
        [self reloadSubImage];
    } else {
        self.switchToSelf.hidden = YES;
        self.subImageView.hidden = YES;
    }
    [self addObserver];
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUI:) name:@"switchAccount" object:nil];
}

- (void)reloadUI:(NSNotification *)notification {
//    NSInteger status = [(NSNumber *)notification.object integerValue];
//    if (status ==1) {
        [self reloadSubImage];
        self.userName.text = USER_NAME;
        self.userDuty.text = USER_DEPART;
//    }
}

/**
 设置用户图
 */
- (void)setupUserImage {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    UIButton *userImageButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    userImageButton.center = CGPointMake(60, 25+75/2);
    userImageButton.layer.cornerRadius = 70/2 ;  //设置按钮的拐角为宽的一半
    userImageButton.layer.borderWidth = 0.5;  // 边框的宽
    userImageButton.layer.borderColor = [UIColor clearColor].CGColor;//边框的颜色
    userImageButton.layer.masksToBounds = YES;// 这个属性很重要，把超出边框的部分去除
//    [userImageButton setBackgroundImage:[UIImage imageNamed:@"userimage"] forState:UIControlStateNormal];
//    [userImageButton setBackgroundColor:[UIColor blackColor]];
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
    [requestInfo setValue:USER_ID forKey:@"picuserid"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:ISSUBACCOUNT]) {
        NSString *owerUserId = [userDefaults objectForKey:OWERUSERID];
        NSString *owerPassword = [userDefaults objectForKey:OWERPASSWORD];
        [requestInfo setValue:owerUserId forKey:@"UserId"];
        [requestInfo setValue:owerPassword forKey:@"Password"];
        [requestInfo setValue:owerUserId forKey:@"picuserid"];
    };
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/getpic",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:requestInfo.mj_keyValues success:^(id json) {
        NSString *str = json[@"back"];
        self.imageName = str;
        NSString *imageStr = [NSString stringWithFormat:@"http://%@/%@",USER_SERVERADDRESS, self.imageName];
        [userImageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:imageStr] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setValue:imageStr forKey:@"userImage"];

    } failure:^(NSError *error) {
        
    }];
    [self addSubview:userImageButton];
    self.userImageButton = userImageButton;
}

- (void)setupSubImage {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    UIImageView *userImageButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    userImageButton.center = CGPointMake(width-60, 25+75/2);
    userImageButton.layer.cornerRadius = 70/2 ;  //设置按钮的拐角为宽的一半
    userImageButton.layer.borderWidth = 0.5;  // 边框的宽
    userImageButton.layer.borderColor = [UIColor clearColor].CGColor;//边框的颜色
    userImageButton.layer.masksToBounds = YES;// 这个属性很重要，把超出边框的部分去除
    //    [userImageButton setBackgroundImage:[UIImage imageNamed:@"userimage"] forState:UIControlStateNormal];
    //    [userImageButton setBackgroundColor:[UIColor blackColor]];

    [self addSubview:userImageButton];
    self.subImageView = userImageButton;
    
    self.switchToSelf = [UIButton new];
    [self.switchToSelf setImage:[UIImage imageNamed:@"switch"] forState:UIControlStateNormal];
    self.switchToSelf.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.switchToSelf];
    [self.switchToSelf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.subImageView.mas_centerY);
        make.height.mas_equalTo(44);
        make.left.equalTo(self.userName.mas_right).offset(6);
        make.right.equalTo(self.subImageView.mas_left).offset(-6);
    }];
}

- (void)reloadSubImage {
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
    [requestInfo setValue:USER_ID forKey:@"picuserid"];
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/getpic",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:requestInfo.mj_keyValues success:^(id json) {
        NSString *str = json[@"back"];
        NSString *imageStr = [NSString stringWithFormat:@"http://%@/%@",USER_SERVERADDRESS, str];
        [self.subImageView sd_setImageWithURL:[NSURL URLWithString:imageStr]];
    } failure:^(NSError *error) {
        
    }];
}


- (void)setupUserName {
    
    UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.frame) + 16, 22, 60, 35)];
    userName.font = [UIFont systemFontOfSize:19];
    userName.textColor = [UIColor whiteColor];
    userName.textAlignment = NSTextAlignmentLeft;
    userName.text = USER_NAME;
    [self addSubview:userName];
    self.userName = userName;
    
}

- (void)setupUserDuty {
    UILabel *userDuty = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.frame) + 16, 66, 60, 35)];
    userDuty.font = [UIFont systemFontOfSize:17];
    userDuty.textColor = [UIColor whiteColor];
    userDuty.textAlignment = NSTextAlignmentLeft;
    userDuty.text = USER_DEPART;
    [self addSubview:userDuty];
    self.userDuty = userDuty;
}

- (void)updateHeaderImageViewFrameWithOffsetY:(CGFloat)offsetY
{
    //用于实现向上滚动的时候，图片不变窄
    if (!self.isNeedNarrow && offsetY > 0) {
        return;
    }
    switch (self.type) {
        case ZoomHeaderViewTypeNoConstraint:
        {
            //防止height小于0
            if (self.originalHeaderImageViewFrame.size.height - offsetY < 0) {
                return;
            }
            //如果不使用约束的话，图片的y值要上移offsetY,同时height也要增加offsetY
            CGFloat x = self.originalHeaderImageViewFrame.origin.x;
            CGFloat y = self.originalHeaderImageViewFrame.origin.y + offsetY;
            CGFloat width = self.originalHeaderImageViewFrame.size.width;
            CGFloat height = self.originalHeaderImageViewFrame.size.height - offsetY;
            self.headerImageView.frame = CGRectMake(x, y, width, height);
        }
            break;
        case ZoomHeaderViewTypeCodeConstraint:
        {
            //防止height小于0
            if (self.originalHeaderImageViewHeight -offsetY < 0) {
                return;
            }
            //第一种方式：获取到这个约束，直接对约束值修改
            //self.codeConstraintHeight.equalTo(@(self.originalHeaderImageViewHeight -offsetY));
            //第二种方式：直接使用masonry提供的更新约束方法，其实原理是一样的
            [self.headerImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(self.originalHeaderImageViewHeight -offsetY));
            }];
        }
            break;
        case ZoomHeaderViewTypeXibConstraint:
        {
            //防止height小于0
            if (self.originalHeaderImageViewHeight -offsetY < 0) {
                return;
            }
            self.layoutHeightOfHeaderImageView.constant = self.originalHeaderImageViewHeight - offsetY;
        }
            break;
        default:
            break;
    }
    

}

@end

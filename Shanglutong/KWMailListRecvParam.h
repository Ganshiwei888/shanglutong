//
//  KWMailListRecvParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailListRecvParam : KWBaseParam

@property (nonatomic, strong) NSString *mailBoxId;

@end

//
//  KWMailLeftViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailLeftViewController.h"
#import "KWMailBoxParam.h"
#import "KWMailBoxResult.h"
#import "KWMailBoxChildList.h"
#import "KWMailBoxChildResult.h"
#import "KWMailBoxTool.h"
#import "Utility.h"
#import "KWUserInfoManager.h"
#import "MBProgressHUD+XQ.h"
#import "UIImageView+EMWebCache.h"
#import "KWHttpTool.h"
#import <MBProgressHUD.h>
#import <AFNetworking.h>
#import <MJExtension.h>
#import <UIViewController+MMDrawerController.h>
#import "KWSamrtLabelView.h"
#import "KWMailLeftLabelCell.h"

static NSString *cellIdentifier = @"KWMailLeftLabelCell";

@interface KWMailLeftViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIView *buttonBackView;
@property (weak, nonatomic) IBOutlet UILabel *mailBoxName;
@property (weak, nonatomic) IBOutlet UITableView *mailListTableView;
@property (weak, nonatomic) IBOutlet UITableView *mailLableTableView;

@property (strong, nonatomic) MBProgressHUD *hud;
@property (assign, nonatomic) NSUInteger btnCount;
@property (strong, nonatomic) NSMutableArray *childListArray;
@property (strong, nonatomic) NSMutableDictionary *boxMailName;
@property (strong, nonatomic) NSMutableArray <KWMailBoxLabelList *>*mailLabelArray;

/**
 智能标签子标签数组
 */
@property (strong, nonatomic) NSMutableArray<KWMailBoxChildListModel *> *smartLabelArray;

@end

@implementation KWMailLeftViewController

- (IBAction)dismiss:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableArray *)mailLabelArray {
    if (!_mailLabelArray) {
        _mailLabelArray = [NSMutableArray new];
    }
    return _mailLabelArray;
}

- (NSMutableDictionary *)boxMailName {
    if (!_boxMailName) {
        _boxMailName = [NSMutableDictionary dictionary];
    }
    return _boxMailName;
}

- (void)viewWillAppear:(BOOL)animated {
//    [self startSmartList];
    [self.navigationController setNavigationBarHidden:YES];
//    self.mailListTableView.estimatedRowHeight = 100;
    self.mailListTableView.rowHeight = UITableViewAutomaticDimension;
    [self.mailListTableView.tableHeaderView setHidden:YES];
    [self.mailListTableView.tableFooterView setHidden:YES];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //获取邮件按钮列表
    [self getOfUserMailBoxList];
    [self getUserSmartLabelList];
    
    [self setup];
    
}

- (void)setup {
    [self.mailLableTableView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.mailLableTableView.rowHeight = 60;
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.mailListTableView reloadData];
}

- (void)startSmartList {
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        [self getUserSmartMailBoxChildList];
//    });
    [self getUserSmartMailBoxChildList];
}

- (void)getOfUserMailBoxList {
    [self getUserMailBoxFromNetAndCreateMailBoxButton];
}

- (void)getUserSmartMailBoxChildList {
    self.mailBoxName.text = @"智能文件夹";
    KWMailBoxParam *param = [KWMailBoxParam param];
    param.parentid = @"all";
    NSLog(@"%@",param);
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mailbox/Getmenu",USER_SERVERADDRESS];
    
    __weak KWMailLeftViewController *weakSelf = self;
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
//        NSLog(@"%@",json);
        
        NSArray *backArray = json[@"back"];
        
        NSArray *next = backArray[0][@"next"];
        
        NSLog(@"%@",next);
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in next) {
            
            weakSelf.childListArray = [NSMutableArray array];
            KWMailBoxChildListModel *model = [[KWMailBoxChildListModel alloc] initWithDict:dict];
            [temp addObject:model];
            
        }
        
        weakSelf.childListArray = temp;
        [weakSelf.mailListTableView reloadData];

    } failure:^(NSError *error) {
        
    }];
}

- (void)getUserSmartLabelList {
    KWBaseParam *param = [KWBaseParam param];
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/getlabel",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSArray *backArray = json[@"back"];
        NSArray <KWMailBoxChildListModel *> *temp = [KWMailBoxChildListModel mj_objectArrayWithKeyValuesArray:backArray];
        self.smartLabelArray = [NSMutableArray new];
        KWMailBoxChildListModel *model = [[KWMailBoxChildListModel alloc] init];
        model.name = @"全部标签邮件";
        model.Class = @"LBL";
        [self.smartLabelArray addObject:model];
        [self.smartLabelArray addObjectsFromArray:temp];

    } failure:^(NSError *error) {

    }];
}

- (void)getUserOtherMailBoxChildListByParentID:(NSString *)parentID {
    
    KWMailBoxParam *param = [KWMailBoxParam param];
    param.parentid = parentID;
    __weak KWMailLeftViewController *weakSelf = self;
    

    [KWMailBoxTool postGetUserMailBoxChild:param success:^(KWMailBoxChildResult *resultData) {
        
        weakSelf.childListArray = [NSMutableArray array];
        
        weakSelf.childListArray = resultData.childResult;
        
        [weakSelf.mailListTableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - LeftButton -

- (void)getUserMailBoxFromNetAndCreateMailBoxButton {
    
    KWMailBoxParam *param = [KWMailBoxParam param];
    
    MBProgressHUD *hud = [MBProgressHUD showMessage:@"" toView:self.view];
    
    [KWMailBoxTool postGetUserMailBox:param success:^(KWMailBoxResult *resultData) {
        [hud hideAnimated:YES];
        KWMailBoxLabelList *list = [[KWMailBoxLabelList alloc] init];
        list.id = @"all";
        list.name = @"智";
        list.email = @"智能文件夹";
        self.mailBoxName.text = list.email;
        [self.mailLabelArray addObject:list];
        
        [resultData.back enumerateObjectsUsingBlock:^(KWMailBoxLabelList * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.name.length >= 1) {
                [self.mailLabelArray addObject:obj];
            }
        }];
        
        [self.mailLableTableView reloadData];
        [self.mailLableTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self getUserSmartMailBoxChildList];
    } failure:^(NSError *error) {
         [hud hideAnimated:YES];
    }];
    
}

- (UIButton *)createBoxMailButtonsWithTitle:(NSString *)title Index:(NSInteger)index {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont monospacedDigitSystemFontOfSize:28.0 weight:1.5];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0] forState:UIControlStateSelected];
    if (index == 0) {
        button.selected = YES;
        if (button.selected) {
            [self getUserSmartMailBoxChildList];
        }
        button.layer.borderWidth = 2.0;
        button.tag = 100;
    }
    button.layer.borderColor = [UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0].CGColor;
    button.layer.cornerRadius = 8.0;
    [button setBackgroundColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(mailBoxButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(BOXMAIL_BUTTON_BETWEEN_SPACE, 60 + BOXMAIL_BUTTON_BETWEEN_SPACE * (index + 1) + BOXMAIL_BUTTONWIDTH * index, BOXMAIL_BUTTONWIDTH, BOXMAIL_BUTTONWIDTH);
    
    return button;
    
}

- (void)mailBoxButtonClick:(UIButton *)sender {
    if (sender.selected) {
        return;
    } else {
        NSArray *buttonArray = [self.buttonBackView subviews];
        for (UIButton *button in buttonArray) {
            if (button.selected) {
                button.selected = NO;
                button.layer.borderWidth = 0;
                break;
            }
        }
        sender.selected = YES;
        sender.layer.borderWidth = 2;
        for (NSInteger i = 0; i < buttonArray.count; i++) {
            UIButton *button = buttonArray[i];
            if (button.selected) {
                if (button.tag == 100) {
                    self.mailBoxName.text = @"智能文件夹";
                } else {
                    
                }
                break;
            }
        }
        for (NSInteger i = 0; i < buttonArray.count; i++) {
            if (sender.tag == 100) {
                switch ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus) {
                    case AFNetworkReachabilityStatusUnknown: {
                        //[self getUserSmartMailBoxChildListFromDataBaseAndReloadTableView];
                        [MBProgressHUD showError:@"网络状态未知" toView:nil];
                        break;
                    }
                        
                    case AFNetworkReachabilityStatusNotReachable: {
                       // [self getUserSmartMailBoxChildListFromDataBaseAndReloadTableView];
                        [MBProgressHUD showError:@"无网络" toView:nil];
                        break;
                    }
                        
                    default: {
                        [self getUserSmartMailBoxChildList];
                    }
                }
            } else {
                switch ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus) {
                    case AFNetworkReachabilityStatusUnknown:
                        [MBProgressHUD showError:@"网络状态未知" toView:nil];
                        break;
                    case AFNetworkReachabilityStatusNotReachable:
                        [MBProgressHUD showError:@"无网络" toView:nil];
                        break;
                    default: {
                        NSInteger tag = sender.tag - 100;
                        NSString *parentID = [NSString stringWithFormat:@"m_%ld",(long)tag];
                        [self getUserOtherMailBoxChildListByParentID:parentID];
                    }
                }
            }
        }
        
    }
}

- (void)backButtonClick:(UIButton *)sender {
    
    NSLog(@"chick out");
    
}
- (IBAction)popToRootView:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

#pragma mark - UITableViewDataSource && UITableViewDelegate -



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mailLableTableView) {
        return self.mailLabelArray.count;
    } else {
        return self.childListArray.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.mailLableTableView) {
        return 70;
    } else {
        return 45;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.mailLableTableView) {
        KWMailLeftLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        KWMailBoxLabelList *list = self.mailLabelArray[indexPath.row];
        NSString *str = [list.name substringToIndex:1];
        cell.label.text = str;
        return cell;
        } else {
        static NSString *cellID = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
        }
        
        KWMailBoxChildListModel *listModel = self.childListArray[indexPath.row];
        
        UIImage *image = [UIImage imageNamed:listModel.name];
        NSString *string = [NSString stringWithFormat:@"%@",listModel.mailId];
        NSArray *array = [string componentsSeparatedByString:@"_"];
        NSString *stringTwo = [NSString stringWithFormat:@"%@",array[array.count-1]];
        // 加入正则表达式
        NSString *regex = @"[0-9]*";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        
        if ([pred evaluateWithObject:stringTwo]) {
            image = [UIImage imageNamed:@"文件夹"];
        }
        cell.imageView.image = image;
        cell.textLabel.text = listModel.name;
        
        return cell;
    }
    return nil;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.mailLableTableView) {
        KWMailBoxLabelList *list = self.mailLabelArray[indexPath.row];
        self.mailBoxName.text = list.email;
        if (indexPath.row == 0) {
            [self getUserSmartMailBoxChildList];
        } else {
            [self getUserOtherMailBoxChildListByParentID:[NSString stringWithFormat:@"m_%@",list.id]];
        }
    } else {
        KWMailBoxChildListModel *listModel = self.childListArray[indexPath.row];
        
        NSString *mailAct = listModel.act;
        
        NSString *boxId = listModel.mailId;
        
        NSLog(@"%@",boxId);
        
        
        
        NSString *mailName = listModel.name;
        NSLog(@"%@",mailAct);
        
        if ([mailAct isEqualToString:@"BQYJ"]) {
            KWSamrtLabelView *samrtLabelView = [[NSBundle mainBundle] loadNibNamed:@"KWSamrtLabelView" owner:self options:nil][0];
            samrtLabelView.dataArray = self.smartLabelArray;
            [samrtLabelView setClickBlock:^(NSIndexPath *indexPath) {
                NSString *mailName = self.smartLabelArray[indexPath.row].name;
                NSString *mailAct;
                if (indexPath.row == 0) {
                    mailAct = self.smartLabelArray[indexPath.row].Class;
                } else {
                    mailAct = [NSString stringWithFormat:@"LBL%@",self.smartLabelArray[indexPath.row].Class];
                }
                NSString *boxId = @"0";
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMailList" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:boxId, @"MailID" , mailName, @"name",mailAct,@"ACT", nil]];
                [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
            }];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMailList" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:boxId, @"MailID" , mailName, @"name",mailAct,@"ACT", nil]];
            [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

@end

//
//  KWMailBoxChildList.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailBoxChildList : NSObject

@property (nonatomic, strong) NSArray *childListArray;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *timestamp;

@end

//email = "<null>";
//id = "b_8_SJX";
//name = "\U6536\U4ef6\U7bb1";
//next = "<null>";

@interface KWMailBoxChildListModel : NSObject

@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSString *mailId;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *next;

@property (nonatomic, copy) NSString *act;

@property (nonatomic, copy) NSString *boxid;

@property (nonatomic, copy) NSString *Class;

/**
 图片
 */
@property (nonatomic, copy) NSString *pic;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

- (instancetype)initWithDict:(NSDictionary *)dict;


@end







//
//  KWUserTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWUserInfoParam.h"
#import "KWUserInfoResult.h"

@interface KWUserTool : NSObject

//@property (nonatomic, strong) KWUserInfoResult *model;

/**
 post验证用户信息

 @param param post参数
 @param success 请求成功后的回调
 @param failure 请求失败后的回调
 */
+ (void)userInfoWithParam:(KWUserInfoParam *)param success:(void(^)(KWUserInfoResult *resultData))success failure:(void(^)(NSError *error))failure;

@end








//
//  KWPbCustomerLeftViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWPbCustomerLeftViewController.h"
#import "KWHttpTool.h"
#import "KWBaseParam.h"
#import "Utility.h"
#import <MJExtension.h>
#import <UIViewController+MMDrawerController.h>

#import "Utility.h"

@interface KWPbCustomerLeftViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listArray;

@end

@implementation KWPbCustomerLeftViewController

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNav];
    [self setupTopView];
    [self setupData];
    [self setupTableView];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)setupNav {
    
 
}

- (void)setupTopView {
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 64)];
    topView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:topView];
    
    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 30, 120, 20)];
    topLabel.text = @"公海分类";
    topLabel.font = [UIFont systemFontOfSize:19 weight:2];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.textColor = RGBCOLOR(28, 59, 138);
    [topView addSubview:topLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame:CGRectMake(16, 26, 28, 28)];
    [backBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:backBtn];
}

- (void)setupData {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/getpublimit",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        //        NSLog(@"%@",json);
        
        NSString *str1 = @"全部公海客户";
        NSString *str2 = @"无分类客户";
        [self.listArray addObject:str1];
        [self.listArray addObject:str2];
        

        NSArray *backArray = json[@"back"];
        
        NSMutableArray *temp = [NSMutableArray array];
        
        for (int i=0; i<backArray.count; i++) {
            NSString *str = backArray[i];
            [temp addObject:str];
        }
        [self.listArray addObjectsFromArray:temp];
        NSLog(@"%@",self.listArray);
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
    
    
}

- (void)backBtnClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)setupTableView {
    
    UITableView *leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(8, 70, 184, KWSCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    leftTableView.dataSource = self;
    leftTableView.delegate = self;
    [self.view addSubview:leftTableView];
    self.tableView = leftTableView;
}

#pragma mark - UITableViewDataSource && UITableViewDelegate -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"pbcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    cell.textLabel.text = self.listArray[indexPath.row];
    NSLog(@"%@",self.listArray[indexPath.row]);
    cell.imageView.image = [UIImage imageNamed:@"客户"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSArray *listArray = @[@"全部公海客户",@"有价值客户",@"无价值客户",@"美国客人",@"无分类客户"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"getPbCustomer" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:self.listArray[indexPath.row], @"pubClass", nil]];
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

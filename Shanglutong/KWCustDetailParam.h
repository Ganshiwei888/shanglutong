//
//  KWCustDetailParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/13.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWCustDetailParam : KWBaseParam

@property (nonatomic, copy) NSString *Clientid;

@end

//
//  KWMailImageAttachmentViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMailImageAttachmentViewController : UIViewController
- (void)recieveTheImage:(NSData *)image;
@end

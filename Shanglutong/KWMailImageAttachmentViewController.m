//
//  KWMailImageAttachmentViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailImageAttachmentViewController.h"
#import "TBActionSheet.h"
#import "TBAlertController.h"
#import <Photos/Photos.h>
#import <SVProgressHUD.h>
#import "Utility.h"

@interface KWMailImageAttachmentViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (nonatomic, strong) NSData *imageData;
@end

@implementation KWMailImageAttachmentViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.attachmentImageView.image = [UIImage imageWithData:self.imageData];
    self.attachmentImageView.userInteractionEnabled = YES;
    
    UIButton *btnSave = [[UIButton alloc]initWithFrame:CGRectMake(KWSCREEN_WIDTH-66-10, KWSCREEN_HEIGHT-64-66, 66, 66)];
    [btnSave setImage:[UIImage imageNamed:@"白省略"] forState:UIControlStateNormal];
    [btnSave addTarget:self action:@selector(saveTo) forControlEvents:UIControlEventTouchUpInside];
    [self.attachmentImageView addSubview:btnSave];

}

//点击省略号
- (void)saveTo
{
    TBAlertController *controller = [TBAlertController alertControllerWithTitle:@"TBAlertController" message:@"AlertStyle" preferredStyle:TBAlertControllerStyleActionSheet];
    TBAlertAction *clickme = [TBAlertAction actionWithTitle:@"保存到相册" style: TBAlertActionStyleDefault handler:^(TBAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"保存中"];
        //保存到相册
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            
            //写入图片到相册
            PHAssetChangeRequest *req = [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageWithData:self.imageData]];
            
            NSLog(@"%@",req);
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            
            if (success){
                [SVProgressHUD showSuccessWithStatus:@"保存成功"];
            }else{
                [SVProgressHUD showSuccessWithStatus:@"保存失败"];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
        }];
        
    }];
    TBAlertAction *cancel = [TBAlertAction actionWithTitle:@"取消" style: TBAlertActionStyleCancel handler:^(TBAlertAction * _Nonnull action) {
        NSLog(@"%@ ",action.title);
    }];
    [controller addAction:clickme];
    [controller addAction:cancel];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)recieveTheImage:(NSData *)imageData {
    self.imageData = imageData;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

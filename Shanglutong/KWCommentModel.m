//
//  KWCommentModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCommentModel.h"
#import "Utility.h"

@implementation KWCommentModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _Bak = checkStringNull(dict[@"Bak"]);
        _CreateTm = checkStringNull(dict[@"CreateTm"]);
        _Creater = checkStringNull(dict[@"Creater"]);
        _Id = checkStringNull(dict[@"Id"]);
        _KeyId = checkStringNull(dict[@"KeyId"]);
        _MenuNo = checkStringNull(dict[@"MenuNo"]);
        _ShareUID = checkStringNull(dict[@"ShareUID"]);
        _ShareUID1 = checkStringNull(dict[@"ShareUID1"]);
        _TableName = checkStringNull(dict[@"TableName"]);
        _TitleHtml = checkStringNull(dict[@"TitleHtml"]);
        _WdId = checkStringNull(dict[@"WdId"]);
        
    }
    return self;
}

//
//Bak = 111;
//CreateTm = "2017-08-19T21:04:07.927";
//Creater = admin;
//CreaterName = "\U7ba1\U7406\U5458";
//Id = 189;
//KeyId = 29229;
//MenuNo = AB;
//ShareUID = ",1,121,126,147,";
//ShareUID1 = "-1";
//TableName = "Client_T";
//TitleHtml = "[DZ17070006]4444we";
//WdId = 0;

@end






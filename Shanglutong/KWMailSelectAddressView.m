//
//  KWMailSelectAddressView.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailSelectAddressView.h"
#import "KWMailSelectAddCell.h"
#import "KWAddress.h"
#import "KWHttpTool.h"
#import "Utility.h"

@interface KWMailSelectAddressView () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) UIWindow *alphaWindow;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *listArray;

@property (nonatomic, strong) NSMutableArray *selectArray;

@property (nonatomic, weak) UISearchBar *searchBar;

@end

@implementation KWMailSelectAddressView

- (NSMutableArray *)selectArray {
    if (!_selectArray) {
        _selectArray = [NSMutableArray array];
    }
    return _selectArray;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    [self setupTopView];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 48, self.frame.size.width, 44)];
    searchBar.showsCancelButton = YES;
    searchBar.delegate = self;
    [self addSubview:searchBar];
    self.searchBar = searchBar;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(8, 94, self.frame.size.width, self.frame.size.height - 94) style:UITableViewStylePlain];
    
    tableView.dataSource = self;
    tableView.delegate = self;
    [self addSubview:tableView];
    self.tableView = tableView;
    
    [self setupAddress];
}

- (void)setupTopView {
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 49)];
    topView.backgroundColor = RGBCOLOR(22, 59, 138);
    [self addSubview:topView];
    
    UILabel *topLable = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width / 2) - 60 , 15, 120, 20)];
    topLable.text = @"选择联系人";
    topLable.textColor = [UIColor whiteColor];
    topLable.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:topLable];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setFrame:CGRectMake(16, 14, 40, 30)];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(disappearFromBottomAnimated) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [topView addSubview:cancelBtn];
    
    UIButton *deterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [deterBtn setFrame:CGRectMake(self.frame.size.width - 56, 14, 40, 30)];
    [deterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deterBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [deterBtn setTitle:@"确定" forState:UIControlStateNormal];
    [deterBtn addTarget:self action:@selector(selectTheAddress:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:deterBtn];
    
}

- (void)setupAddress {
    
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
    [requestInfo setValue:@"1" forKey:@"pageindex"];
    [requestInfo setValue:@"50" forKey:@"pagemax"];
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/contact/Get",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in json[@"back"][@"list"]) {
            KWAddress *model = [[KWAddress alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.listArray = temp;
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

- (void)selectTheAddress:(UIButton *)sender {
    
    
    NSLog(@"%lu",(unsigned long)self.selectArray.count);
    NSMutableArray *temp = [NSMutableArray array];
    for (KWAddress *address in self.selectArray) {
        NSString *email = address.Email;
        [temp addObject:email];
    }
    NSString *email = [temp componentsJoinedByString:@","];
    
    self.selectAdd(email);
    
    [self disappearFromBottomAnimated];
}

#pragma mark - UISearchdelegate - 

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {

    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    UIButton *cancelBtn = [searchBar valueForKeyPath:@"cancelButton"]; //首先取出cancelBtn
    cancelBtn.enabled = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    NSLog(@"%@",self.searchBar.text);
    
    [self.searchBar resignFirstResponder];
    UIButton *cancelBtn = [searchBar valueForKeyPath:@"cancelButton"]; //首先取出cancelBtn
    cancelBtn.enabled = YES;

}
#pragma mark - UITableViewDataSource && UITableViewDelegate -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"select";
    
    KWMailSelectAddCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[KWMailSelectAddCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    KWAddress *model = self.listArray[indexPath.row];
    cell.address = self.listArray[indexPath.row];
    cell.selectBtn.selected = model.isSelect;
    
    
    cell.selectBtn.tag = indexPath.row + 500;
    [cell.selectBtn addTarget:self action:@selector(addSelect:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)addSelect:(UIButton *)sender {
    
    NSInteger index = sender.tag - 500;
    
    if (sender.selected) {
        sender.selected = NO;
        KWAddress *model = self.listArray[index];
        model.isSelect = NO;
        [self.selectArray removeObject:model];
    } else {
        sender.selected = YES;
        KWAddress *model = self.listArray[index];
        model.isSelect = YES;
        [self.selectArray addObject:self.listArray[index]];
    }
    
//    [self.tableView reloadData];
}

- (UIWindow *)alphaWindow {
    if (!_alphaWindow) {
        _alphaWindow = [[UIWindow alloc] init];
        _alphaWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _alphaWindow.windowLevel = 100;
    }
    return _alphaWindow;
}

- (void)showAnimated {
    
    self.alphaWindow.frame = [UIScreen mainScreen].bounds;
    [self.alphaWindow makeKeyAndVisible];
    [self.alphaWindow addSubview:self];
    
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.25;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [self.layer addAnimation:animation forKey:nil];
}

- (void)disappearFromBottomAnimated {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = self.frame;
        self.frame = CGRectMake(rect.origin.x, rect.origin.y + 652, rect.size.width, rect.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alphaWindow.hidden = YES;
    }];
}

- (void)disappearFromTopAnimated {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = self.frame;
        self.frame = CGRectMake(rect.origin.x, rect.origin.y - 652, rect.size.width, rect.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alphaWindow.hidden = YES;
    }];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

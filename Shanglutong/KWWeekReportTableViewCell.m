//
//  KWWeekReportTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWWeekReportTableViewCell.h"
#import "KWWeekReportModel.h"
#import "Utility.h"

@interface KWWeekReportTableViewCell ()


@property (nonatomic, weak) UIImageView *readImageView;

@property (nonatomic, weak) UILabel *nameLabel;
@property (nonatomic, weak) UILabel *typeLabel;
@property (nonatomic, weak) UILabel *timeLabel;

@end

@implementation KWWeekReportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setupCellView];
        
    }
    return self;
}

- (void)setupCellView {
    
    UIImageView *readImageView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 35, 10, 10)];
    [self.contentView addSubview:readImageView];
    self.readImageView = readImageView;
    
    UILabel *nameLable = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(readImageView.frame) + 4, 31, 60, 18)];
    nameLable.textColor = RGBCOLOR(28, 59, 138);
    nameLable.font = [UIFont systemFontOfSize:16];
    nameLable.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:nameLable];
    self.nameLabel = nameLable;
    
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(KWSCREEN_WIDTH - 80, self.frame.size.height - 9, 40, 18)];
    typeLabel.textColor = [UIColor darkGrayColor];
    typeLabel.font = [UIFont systemFontOfSize:15];
    typeLabel.textAlignment = NSTextAlignmentCenter;
    typeLabel.text = @"周报";
    [self.contentView addSubview:typeLabel];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(KWSCREEN_WIDTH - 200, CGRectGetMaxY(typeLabel.frame) + 4, 30, 16)];
    time.textColor = [UIColor darkGrayColor];
    time.font = [UIFont systemFontOfSize:12];
    time.textAlignment = NSTextAlignmentCenter;
    time.text = @"时间";
    [self.contentView addSubview:time];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(time.frame) + 4, CGRectGetMaxY(typeLabel.frame) + 4, 140, 16)];
    timeLabel.textColor = [UIColor darkGrayColor];
    timeLabel.font = [UIFont systemFontOfSize:11];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
}

- (void)setWeekModel:(KWWeekReportModel *)weekModel {
    
    _weekModel = weekModel;
    
    _readImageView.image = [UIImage imageNamed:@"未读"];
    
    NSString *timeOne = [weekModel.CreateTm substringToIndex:19];
    NSString *strUrl = [timeOne stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    self.timeLabel.text = strUrl;
    
    self.nameLabel.text = weekModel.CreaterName;
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

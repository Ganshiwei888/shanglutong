//
//  KWMailBox.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailBox : NSObject

@property (nonatomic, strong) NSArray *mailBoxArray;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *timestamp;

@end


@interface KWMailBoxModel : NSObject

@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSString *mailID;

@property (nonatomic, copy) NSString *name;

+ (instancetype)modelWithDict:(NSDictionary *)dict;
- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//
//back =     (
//            {
//                email = "inquiry@foodchem.cn";
//                id = 11;
//                name = "\U63a8\U5e7f\U544a";
//            },
//            {
//                email = "info@18518.net";
//                id = 8;
//                name = "\U4e1a\U52a1";
//            }
//            );
//code = 0000;
//timestamp = 1501643698;




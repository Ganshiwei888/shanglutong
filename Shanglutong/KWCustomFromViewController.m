//
//  KWCustomFromViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/13.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomFromViewController.h"
#import "KWMailSendViewController.h"
#import "KWNavigationController.h"
#import "KWHttpTool.h"
#import "KWCustDetailParam.h"
#import "KWCustomerResult.h"
#import <SVProgressHUD.h>
#import <MJExtension.h>
#import "Utility.h"

@interface KWCustomFromViewController ()

@property (nonatomic, copy) NSString *mailaddress;

@property (nonatomic, copy) NSString *clientid;

@property (nonatomic, copy) NSString *svpStr;

@end

@implementation KWCustomFromViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"客户详情";

}

- (void)setupView {
    
    UILabel *fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 80, KWSCREEN_WIDTH - 16, 22)];
    fromLabel.text = self.fromName;
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentCenter;
    fromLabel.font = [UIFont boldSystemFontOfSize:20];
    fromLabel.font = [UIFont fontWithName:@ "Arial Rounded MT Bold"  size:(18.0)];
    [self.view addSubview:fromLabel];
    
    UIImageView *dividing1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 110, KWSCREEN_WIDTH, 1)];
    dividing1.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:dividing1];
    
    UIImageView *dividing2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 140, KWSCREEN_WIDTH, 1)];
    dividing2.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:dividing2];
    
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 114, 80, 22)];
    emailLabel.textColor = [UIColor darkGrayColor];
    emailLabel.font = [UIFont systemFontOfSize:14];
    emailLabel.text = @"电子邮件";
    [self.view addSubview:emailLabel];
    
    UILabel *email = [[UILabel alloc] initWithFrame:CGRectMake(100, 114, 240, 22)];
    email.textColor = [UIColor blackColor];
    email.font = [UIFont systemFontOfSize:14];
    email.text = self.mailaddress;
    [self.view addSubview:email];
    
    UIButton *replyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [replyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [replyBtn setFrame:CGRectMake(0, CGRectGetMaxY(email.frame) + 16, KWSCREEN_WIDTH, 30)];
    [replyBtn setTitle:@"编写邮件" forState:UIControlStateNormal];
    replyBtn.layer.borderWidth = 1;
    replyBtn.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    replyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [replyBtn addTarget:self action:@selector(replyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:replyBtn];
    
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [customBtn setFrame:CGRectMake(0, CGRectGetMaxY(replyBtn.frame) + 16, KWSCREEN_WIDTH, 30)];
    [customBtn setTitle:@"客户详情" forState:UIControlStateNormal];
    customBtn.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    customBtn.layer.borderWidth = 1;
    customBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [customBtn addTarget:self action:@selector(customBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:customBtn];
    
}

- (void)receiveTheMail:(NSString *)mailaddress andclientId:(NSString *)clientid {
    self.mailaddress = mailaddress;
    
    self.clientid = clientid;
    
    [self setupView];
    [self downTheClient];
}

- (void)downTheClient {
    
    KWCustDetailParam *param = [KWCustDetailParam param];
    param.Clientid = self.clientid;
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/client/get",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json);
        NSString *code = json[@"code"];
        if ([code isEqualToString:@"0006"]) {
            self.svpStr = @"你不能获取该客户资料";
        }
        if ([code isEqualToString:@"0000"]) {
            KWlistCustonModel *model = [KWlistCustonModel mj_objectWithKeyValues:json[@"back"]];
            NSLog(@"%@",model.Bak);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

- (void)replyBtnClick:(UIButton *)sender {
    
    KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
    [sendVc setMailSendViewText:self.mailaddress AndReplyTo:nil AndSubject:nil];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:sendVc];
    
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (void)customBtnClick:(UIButton *)sender {
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  KWAddressBackView.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressBackView.h"
#import "KWAddressFrame.h"
#import "KWAddress.h"
#import "Utility.h"

@interface KWAddressBackView ()

/**
 头像名图
 */
@property (nonatomic, weak) UILabel *headimageLabel;
/**
 name
 */
@property (nonatomic, weak) UILabel *nameLabel;
/**
 sex and age
 */
@property (nonatomic, weak) UILabel *sexAndageLabel;
/**
 work
 */
@property (nonatomic, weak) UILabel *workLabel;
/**
 dividingView
 */
@property (nonatomic, weak) UIImageView *dividingView;

@property (nonatomic, weak) UIImageView *phoneImage;
/**
 phone label
 */
@property (nonatomic, weak) UILabel *phoneLabel;
/**
 faceBook
 */
@property (nonatomic, weak) UIImageView *faceBookImage;
/**
 faceBook Label
 */
@property (nonatomic, weak) UILabel *faceBookLabel;
/**
 email Image
 */
@property (nonatomic, weak) UIImageView *emailImage;
/**
 email Label
 */
@property (nonatomic, weak) UILabel *emailLabel;


@end

@implementation KWAddressBackView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self  = [super initWithFrame:frame]) {
    
    
        self.backgroundColor = RGBCOLOR(252, 252, 252);
        self.layer.cornerRadius = 3.0;
        self.layer.masksToBounds = YES;

        //头像大
        UILabel *headimageLabel = [[UILabel alloc] init];
        headimageLabel.font = [UIFont systemFontOfSize:24];
        headimageLabel.textColor = [UIColor whiteColor];
        headimageLabel.textAlignment = NSTextAlignmentCenter;
        CGFloat color1 = random()%254;
        CGFloat color2 = random()%254;
        CGFloat color3 = random()%254;
        headimageLabel.backgroundColor =RGBCOLOR(color1, color2, color3);
        headimageLabel.layer.cornerRadius = 32.5;
//        headimageLabel.layer.borderColor = RGBCOLOR(21, 126, 251).CGColor;
//        headimageLabel.layer.borderWidth = 2;
        headimageLabel.clipsToBounds = YES;
//        [headimageLabel sizeToFit];
        [self addSubview:headimageLabel];
        self.headimageLabel = headimageLabel;
        //头像名
        UILabel *nameLabel = [[UILabel alloc] init];
        nameLabel.font = [UIFont systemFontOfSize:16];
        nameLabel.textColor = RGBCOLOR(22, 57, 138);
        nameLabel.backgroundColor = [UIColor clearColor];
        [nameLabel sizeToFit];
        [self addSubview:nameLabel];
        self.nameLabel = nameLabel;
        //sex and age
        UILabel *sexAndageLabel = [[UILabel alloc] init];
        sexAndageLabel.font = [UIFont systemFontOfSize:14];
        sexAndageLabel.textColor = RGBCOLOR(41, 36, 33);
        sexAndageLabel.backgroundColor = [UIColor clearColor];
        [sexAndageLabel sizeToFit];
        [self addSubview:sexAndageLabel];
        self.sexAndageLabel = sexAndageLabel;
        //work label
        
        UILabel *workLabel = [[UILabel alloc] init];
        workLabel.font = [UIFont systemFontOfSize:14];
        workLabel.textColor = RGBCOLOR(41, 36, 33);
        workLabel.backgroundColor = [UIColor clearColor];
        [workLabel sizeToFit];
        [self addSubview:workLabel];
        self.workLabel = workLabel;
        
        //dividing View
        UIImageView *dividingView = [[UIImageView alloc] init];
        [self addSubview:dividingView];
        self.dividingView = dividingView;
        
        //phoneImage
        UIImageView *phoneImage = [[UIImageView alloc] init];
//        [self addSubview:phoneImage];
        self.phoneImage = phoneImage;
        //phoneLabel
        UILabel *phoneLabel = [[UILabel alloc] init];
        phoneLabel.font = [UIFont systemFontOfSize:14];
        phoneLabel.textColor = RGBCOLOR(41, 36, 33);
        phoneLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:phoneLabel];
        self.phoneLabel = phoneLabel;
        
        //facebookImage
        UIImageView *faceBookImage = [[UIImageView alloc] init];
//        [self addSubview:faceBookImage];
        self.faceBookImage = faceBookImage;
        //faceBookLabel
        UILabel *faceBookLabel = [[UILabel alloc] init];
        faceBookLabel.font = [UIFont systemFontOfSize:14];
        faceBookLabel.textColor = RGBCOLOR(41, 36, 33);
        faceBookLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:faceBookLabel];
        self.faceBookLabel = faceBookLabel;
        
        //emailImage
        UIImageView *emailImage = [[UIImageView alloc] init];
//        [self addSubview:emailImage];
        self.emailImage = emailImage;
        //emailLabel
        UILabel *emailLabel = [[UILabel alloc] init];
        emailLabel.font = [UIFont systemFontOfSize:14];
        emailLabel.textColor = RGBCOLOR(41, 36, 33);
        emailLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:emailLabel];
        self.emailLabel = emailLabel;
    }
    
    return self;
}

-(BOOL)IsChinese:(NSString *)str {
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff)
        {
            return YES;
        }
        
    }
    return NO;
}

- (void)setAddressFrame:(KWAddressFrame *)addressFrame {

    _addressFrame = addressFrame;

    //取出数据模型
    KWAddress *address = addressFrame.address;
    
    //头像图
    if (address.Name == nil) {
        address.Name = @"Customer";
    }
    NSString *head = address.Name;
    if (head.length >= 2){
        NSString *headTwo = [head substringToIndex:2];
        BOOL haveChinese = [self IsChinese:headTwo];
        if (haveChinese){
            self.headimageLabel.text = [head substringToIndex:1];
        }else {
            self.headimageLabel.text = headTwo;
        }
    }
    
    CGRect frame = addressFrame.headimageView;
    [self.headimageLabel setFrame:frame];
    
    //头像名
    [self.nameLabel setText:address.Name];
    [self.nameLabel setFrame:addressFrame.addresNameView];
    //sex && age
//    NSString *sex = address.addressSex;s
    self.sexAndageLabel.text = @"男";
    [self.sexAndageLabel setFrame:addressFrame.sexAndageView];
    
    NSString *dept = @"";
    NSString *job = @"";
    NSString *add = address.MainAdd;
    NSString *contact = @"";
    //普通联系人 主联系人 超级联系人
    int mianAdd = [add intValue];
    if (mianAdd == 0) {
        contact = @"普通联系人";
    } else if (mianAdd == 1) {
        contact = @" 主联系人";
    } else if (mianAdd == 2) {
        contact = @" 超级联系人";
    }
    
    if (address.Dept || ![address.Dept isEqualToString:@""]) {
        dept = address.Dept;
    }
    
    if (address.Job || ![address.Job isEqualToString:@""]) {
        job = [NSString stringWithFormat:@"%@",address.Job];
    }
    NSString *work = [NSString stringWithFormat:@"%@%@%@", dept, job, contact];
    NSLog(@"%@",work);
    self.workLabel.text = work;
    [self.workLabel setFrame:addressFrame.workNameView];
    
    self.dividingView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.dividingView.frame = addressFrame.dividingView;
    
    self.phoneImage.image = [UIImage imageNamed:@"icon_phone"];
    self.phoneImage.frame = addressFrame.phoneImage;
    
    //phone label
    [self.phoneLabel setText:address.Mobile];
    self.phoneLabel.frame = addressFrame.phoneNumView;
    //faceBook
    self.faceBookImage.image = [UIImage imageNamed:@"faceBook"];
    self.faceBookImage.frame = addressFrame.faceBookImage;
    //facebook label
    self.faceBookLabel.text = address.Facebook;
    self.faceBookLabel.frame = addressFrame.faceBookView;
    //email image
    self.emailImage.image = [UIImage imageNamed:@"mail"];
    self.emailImage.frame = addressFrame.emailImage;
    //email label
    self.emailLabel.text = address.Email;
    self.emailLabel.frame = addressFrame.emailView;
    
}

@end







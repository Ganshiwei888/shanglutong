//
//  KWMailAttachmentViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWMailAttachmentResult.h"
#import "KWMailListResult.h"

@interface KWMailAttachmentViewController : UIViewController

- (void)getAttachmentCellModel:(KWMailAttachmentResult *)mailDetailFileModel andMailHomeMailListCell:(KWMailListModel *)mailListCell AndFilePath:(NSString *)filePath;

+ (void)downloadAttachmentWithUserAccount:(NSString *)userAccount UserPassword:(NSString *)userPassword AttachmentFileId:(NSInteger)fileId AndPosition:(NSInteger)position;


@end

//
//  KWHomeViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWTabBar;
@interface KWHomeViewController : UITabBarController
@property (nonatomic, weak) KWTabBar *customTabBar;

@end

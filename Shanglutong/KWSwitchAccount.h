//
//  KWSwitchAccount.h
//  Shanglutong
//
//  Created by ganshiwei on 2017/11/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWSwitchAccount : KWBaseParam

@property (nonatomic, copy) NSString *parentid;
@property (nonatomic, copy) NSString *userid;

@end

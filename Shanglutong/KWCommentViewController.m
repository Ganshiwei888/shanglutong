//
//  KWCommentViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCommentViewController.h"
#import "KWCommentModel.h"
#import "KWCommentFrame.h"
#import "KWCommentTableViewCell.h"
#import "KWHttpTool.h"
#import "KWComDetailViewController.h"
#import "KWCommentParam.h"
#import "Utility.h"
#import <MJExtension.h>
#import <MJRefresh.h>

@interface KWCommentViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *commentListArray;

@end

@implementation KWCommentViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评论中心";
    [self setupTableView];
    // Do any additional setup after loading the view.
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(backBtn:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backBtn:)];
}

- (void)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setCommentList {
    
    KWCommentParam *param = [KWCommentParam param];
    param.Pagemax = @"100";
    param.Pageindex = @"1";
    param.Act = @"ALL";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getcommlist",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSArray *backArray = json[@"back"][@"commlistss"];
        NSMutableArray *temp = [NSMutableArray array];
        
        for (NSDictionary *dict in backArray) {
            
            KWCommentModel *model = [[KWCommentModel alloc] initWithDict:dict];
            KWCommentFrame *frameModel = [[KWCommentFrame alloc] init];
            frameModel.commentModel = model;
            [temp addObject:frameModel];
        
        }
        self.commentListArray = temp;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
   
}

- (void)setupTableView {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerViewRefreshed)];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerViewRefresh)];
    [self setCommentList];

}

- (void)headerViewRefreshed {
    [self setCommentList];
    [self.tableView.mj_header endRefreshing];
}

- (void)footerViewRefresh {
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *cellId = @"comment";
    KWCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[KWCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.commentFrame = self.commentListArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 134;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    KWCommentFrame *model = self.commentListArray[indexPath.row];
    KWCommentModel *com = model.commentModel;
    KWComDetailViewController *vc = [[KWComDetailViewController alloc] init];
    [vc recTheBakStr:com.TitleHtml];
    [vc recTheKeyId:com.KeyId];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end














//
//  KWComDetailTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWComDetailFrame;
@interface KWComDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) KWComDetailFrame *comDetailFrame;

@end

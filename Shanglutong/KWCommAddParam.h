//
//  KWCommAddParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWCommAddParam : KWBaseParam

@property (copy, nonatomic) NSString *KeyId;
@property (copy, nonatomic) NSString *BAK;
@property (copy, nonatomic) NSString *MenuNo;

@end

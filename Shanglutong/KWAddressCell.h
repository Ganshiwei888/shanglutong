//
//  KWAddressCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWAddressFrame;
@interface KWAddressCell : UITableViewCell
/**
 frame
 */
@property (nonatomic, strong) KWAddressFrame *addressFrame;
/**
 创建cell
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

//
//  KWMailToolView.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/7.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMailToolView : UIView
@property (weak, nonatomic) IBOutlet UIButton *topButton;
@property (weak, nonatomic) IBOutlet UIButton *redFlag;

@end

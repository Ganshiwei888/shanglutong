//
//  KWNewsListModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWNewsListModel.h"
#import "Utility.h"

@implementation KWNewsListModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _IsRead = dict[@"IsRead"];
        _Html = dict[@"Html"];
        _CreateTm = dict[@"CreateTm"];
        _MenuNo = dict[@"MenuNo"];
    }
    return self;
}

@end

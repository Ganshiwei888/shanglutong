//
//  UIImage+KWImageSize.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KWImageSize)

//高还原截图
+ (UIImage *)saveImageOfView:(UIView *)view;

//图片压缩到指定大小
+ (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize SourceImage:(UIImage *)image;

@end

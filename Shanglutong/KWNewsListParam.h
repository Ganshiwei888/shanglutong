//
//  KWNewsListParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWNewsListParam : KWBaseParam

@property (nonatomic, copy) NSString *Pageindex;
@property (nonatomic, copy) NSString *Pagemax;
@property (nonatomic, copy) NSString *Act;

@end

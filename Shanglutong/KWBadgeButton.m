//
//  KWBadgeButton.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBadgeButton.h"
#import "UIImage+KW.h"

@implementation KWBadgeButton

- (id)initWithFrame:(CGRect)frame  {
    if (self = [super initWithFrame:frame]) {
        self.hidden = YES;
        self.userInteractionEnabled = NO;
        [self setBackgroundImage:[UIImage resizedImageWithName:@"main_badge"] forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont systemFontOfSize:11];
    }
    return self;
}

- (void)setBadgeValue:(NSString *)badgeValue {
    
    _badgeValue = badgeValue;
    if (badgeValue) {
        self.hidden = NO;
        [self setTitle:badgeValue forState:UIControlStateNormal];
        
        //setFrame
        
        CGRect frame = self.frame;
        
        CGFloat badgeH = self.currentBackgroundImage.size.height;
        CGFloat badgeW = self.currentBackgroundImage.size.width;
        NSDictionary *attr = @{NSFontAttributeName : self.titleLabel.font};
        if (badgeValue.length > 1) {
            CGSize badgeSize = [badgeValue sizeWithAttributes:attr];
            badgeW = badgeSize.width + 10;
        }
        frame.size.width = badgeW;
        frame.size.height = badgeH;
        self.frame = frame;
        
    } else {
        self.hidden = YES;
    }
    
}

@end














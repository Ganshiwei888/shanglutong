//
//  KWTalkMessageViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/5.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "EaseMessageViewController.h"

@interface KWTalkMessageViewController : EaseMessageViewController

- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController
                           modelForMessage:(EMMessage *)message;

@end

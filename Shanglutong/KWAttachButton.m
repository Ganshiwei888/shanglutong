//
//  KWAttachButton.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/12.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAttachButton.h"
#import "Utility.h"

@implementation KWAttachButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // 设置图片的其他属性
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    return self;
}

/**
 内部图片的frame
 
 @return 返回图片的frame
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGFloat imageW = contentRect.size.width * 0.5;
    CGFloat imageH = contentRect.size.height * KWTabBarButtonImageRatio;
    
    return CGRectMake(20, 0, imageW, imageH);
    
}

/**
 内部文字的frame
 
 @param contentRect 文字的contentRect
 @return 文字的frame
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
    CGFloat titleX = 0;
    CGFloat titleY = contentRect.size.height * KWTabBarButtonImageRatio;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

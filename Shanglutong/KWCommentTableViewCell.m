//
//  KWCommentTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCommentTableViewCell.h"
#import "KWCommentModel.h"
#import "KWCommentFrame.h"
#import "NSString+KWCreateTime.h"
#import "Utility.h"
@interface KWCommentTableViewCell ()

@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) UIImageView *comImageView;
@property (weak, nonatomic) UIImageView *divView;
@property (weak, nonatomic) UILabel *detailLabel;
@property (weak, nonatomic) UILabel *nameLabel;
@property (weak, nonatomic) UILabel *timeLabel;

@end

@implementation KWCommentTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, KWSCREEN_HEIGHT, 124)];
        view.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:view];
        [self setupCellView];
    }
    return self;
}

- (void)setupCellView {
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:11];
    titleLabel.textColor = RGBCOLOR(22, 58, 138);
    [self.contentView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UIImageView *comImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:comImageView];
    self.comImageView = comImageView;
    
    UIImageView *divView = [[UIImageView alloc] init];
    divView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.contentView addSubview:divView];
    self.divView = divView;
    
    UILabel *detailLabel = [[UILabel alloc] init];
    detailLabel.font = [UIFont systemFontOfSize:14];
    detailLabel.textColor = [UIColor blackColor];
    detailLabel.numberOfLines = 0;
    [self.contentView addSubview:detailLabel];
    self.detailLabel = detailLabel;
    
    UIImageView *userImageView = [[UIImageView alloc] init];
//    [self.contentView addSubview:userImageView];
    self.userImageView = userImageView;
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.textColor = [UIColor blackColor];
    nameLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.textColor = [UIColor lightGrayColor];
    timeLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
}

- (void)setCommentFrame:(KWCommentFrame *)commentFrame {
    _commentFrame = commentFrame;
    
    KWCommentModel *commentModel = commentFrame.commentModel;
    
    self.titleLabel.text = commentModel.TitleHtml;
    self.titleLabel.frame = commentFrame.comTitleF;
    
    self.comImageView.image = [UIImage imageNamed:@""];
    self.comImageView.frame = commentFrame.comImageF;
    
    self.divView.frame = commentFrame.divImageF;
    
    self.detailLabel.text = commentModel.Bak;
    self.detailLabel.frame = commentFrame.comDetailF;
    
    self.userImageView.image = [UIImage imageNamed:@"user"];
    self.userImageView.frame = commentFrame.userImageF;
    
    self.nameLabel.text = commentModel.Creater;
    self.nameLabel.frame = commentFrame.userNameF;
    
    NSString *timeOne = [commentModel.CreateTm substringToIndex:19];
    //    替换字符
    NSString *strUrl = [timeOne stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    self.timeLabel.text = strUrl;
    self.timeLabel.frame = commentFrame.timeF;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end











//
//  KWMailDetailResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWMailDetailResultModel;
@interface KWMailDetailResult : NSObject

@property (nonatomic, strong) KWMailDetailResultModel *resultModel;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *timestamp;

@end

@interface KWMailDetailResultModel : NSObject

@property (nonatomic, strong) NSString *bcc;

@property (nonatomic, strong) NSString *cc;

@property (nonatomic, strong) NSString *file;

@property (nonatomic, strong) NSString *htmlbody;

@property (nonatomic, strong) NSString *ID;

@property (nonatomic, strong) NSString *mailtype;

@property (nonatomic, strong) NSString *replyto;

@property (nonatomic, strong) NSString *textbody;

@property (nonatomic, strong) NSString *FromName;

@property (nonatomic, strong) NSString *Subject;

@property (nonatomic, strong) NSString *CreateTime;



@end

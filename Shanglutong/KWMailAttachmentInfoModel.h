//
//  KWMailAttachmentInfoModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailAttachmentInfoModel : NSObject
@property (nonatomic, assign) NSUInteger size;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *sizeString;
@property (nonatomic, strong) NSData *smallImageData;
@property (nonatomic, strong) NSData *imageData;
@end

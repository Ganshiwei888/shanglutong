//
//  KWWorkBenchCellView.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWUserMenuno;
@interface KWWorkBenchCellView : UICollectionViewCell

@property (weak, nonatomic) UIImageView *workBenchImage;
@property (weak, nonatomic) UILabel *workNameLabel;
@property (weak, nonatomic) UIImageView *backImageView;

@property (strong, nonatomic) KWUserMenuno *userMenuno;

@end

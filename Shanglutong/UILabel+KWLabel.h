//
//  UILabel+KWLabel.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/6.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (KWLabel)

+ (CGFloat)getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont*)font;

+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font;

@end

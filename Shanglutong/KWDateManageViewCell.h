//
//  KWDateManageViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWDateManageFrame;
@interface KWDateManageViewCell : UITableViewCell

@property (strong, nonatomic) KWDateManageFrame *manageFrame;

@property (weak, nonatomic) UIImageView *doneWellView;
@property (weak, nonatomic) UILabel *doneLabel;

@end

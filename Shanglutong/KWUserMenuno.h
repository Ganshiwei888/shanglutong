//
//  KWUserMenuno.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/11.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWUserMenuno : NSObject

@property (nonatomic, strong) NSString *menuno;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *next;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//
//  TMHTopInfoModel.h
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/19.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMHTopInfoModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *unit;

- (instancetype)initWithName:(NSString *)name content:(double)content timestamp:(double)timestamp;

@end

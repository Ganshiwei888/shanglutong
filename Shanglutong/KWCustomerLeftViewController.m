//
//  KWCustomerLeftViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerLeftViewController.h"
#import "KWCustomListModel.h"
#import "KWNavigationController.h"
#import "NSString+MD5.h"
#import "Utility.h"
#import <UIViewController+MMDrawerController.h>
#import <AFNetworking.h>

@interface KWCustomerLeftViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *sortCustomArray;
@property (strong, nonatomic) NSArray *sortEnglishCustomArray;
@property (strong, nonatomic) NSMutableArray *detailArray;
@property (strong, nonatomic) KWCustomListModel *model;
@property (nonatomic,strong) NSString *sortEnglishCustonString;  //是定死的

@end

@implementation KWCustomerLeftViewController

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[KWCustomListModel alloc] init];
    
    [self initTableView];
    
//    [self getNetworkOfPeopleWithType:@"客户类型"];
    
    [self getUserCustomBoxListFromDataBaseAndCreateMailBoxButtons];
    // Do any additional setup after loading the view.
}

- (void)initTableView {
    
}


#pragma mark - 获取客户 -

- (void)getNetworkOfPeopleWithType:(NSString *)type {
    
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
    [requestInfo setValue:@"客户管理" forKey:@"class"];
    [requestInfo setValue:type forKey:@"type"];
    
//    NSString *netPath = @"http://116.62.232.164:9898/api/system/dictionary/get";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/system/dictionary/get", USER_SERVERADDRESS];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    __weak KWCustomerLeftViewController *weakSelf = self;
    [manager POST:netPath parameters:requestInfo progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
        NSArray *mutArray = [responseObject valueForKey:@"back"];
        //刷新
        self.detailArray = [NSMutableArray array];
        for (NSDictionary *dict in mutArray) {
            KWCustomListModel *model = [[KWCustomListModel alloc] initWithDict:dict];
            [weakSelf.detailArray addObject:model];
        }
        
        [weakSelf.tableView reloadData];
    
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail");
    }];
}



- (void)getUserCustomBoxListFromDataBaseAndCreateMailBoxButtons {
    
    NSArray *array = [NSArray arrayWithObjects:@"类",@"来",@"分",@"星",@"状",@"国",nil];
    self.sortCustomArray = [NSArray arrayWithObjects:@"客户类型",@"客户来源",@"客户分类",@"客户星级",@"客户状态",@"国家", nil];
    self.sortEnglishCustomArray = [NSArray arrayWithObjects:@"ClientType",@"ClientSource",@"ClientClass",@"ClientLevel",@"ClientState",@"Country", nil];
    
    for (int index = 0; index < array.count; index++) {
        UIButton *btn = [self createBoxMailButtonsWithTitle:array[index] Index:index];
        btn.tag = index + 100;
        [self.buttonView addSubview:btn];
    }
    
    [self.view layoutIfNeeded];
    //开始先调用第一个的数据
    [self getNetworkOfPeopleWithType:self.sortCustomArray[0]];
    
}

- (UIButton *)createBoxMailButtonsWithTitle:(NSString *)title Index:(NSInteger)index{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont monospacedDigitSystemFontOfSize:28.0 weight:1.5];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0] forState:UIControlStateSelected];
    if (index == 0) {
        button.selected = YES;
        button.layer.borderWidth = 2.0;
        button.tag = 100;
    }
    button.layer.borderColor = [UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0].CGColor;
    button.layer.cornerRadius = 8.0;
    [button setBackgroundColor:[UIColor whiteColor]];
    [button addTarget:self action:@selector(mailBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(BOXMAIL_BUTTON_BETWEEN_SPACE, 60 + BOXMAIL_BUTTON_BETWEEN_SPACE * (index + 1) + BOXMAIL_BUTTONWIDTH * index, BOXMAIL_BUTTONWIDTH, BOXMAIL_BUTTONWIDTH);
    return button;
}

- (void)mailBoxButtonClicked:(UIButton*)sender
{
    if (sender.selected){
        return;
    } else {
        //改变其他按钮的选择状态
        for (int i = 0; i < self.sortCustomArray.count; i++) {
            UIButton *button = [self.view viewWithTag:i+100];
            button.selected = NO;
            button.layer.borderWidth = 0;
            sender.selected = YES;
            sender.layer.borderWidth =2;
        }
        //网络请求数据
        [self getNetworkOfPeopleWithType:self.sortCustomArray[sender.tag-100]];
    }
}

#pragma mark - UITableView -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detailArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    self.model = self.detailArray[indexPath.row];
    cell.textLabel.text = self.model.DataText;
    cell.imageView.image = [UIImage imageNamed:@"客户"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.model = self.detailArray[indexPath.row];
    if (!self.model.Id){
        return;
    }
    NSString *str = self.model.DataType;
    NSString *sub;
    if ([str isEqualToString:@"客户类型"]){
        sub = @"ClientType";
    }else if ([str isEqualToString:@"客户来源"]){
        sub = @"ClientSource";
    }else if ([str isEqualToString:@"客户来源"]){
        sub = @"ClientSource";
    }else if ([str isEqualToString:@"客户分类"]){
        sub = @"ClientClass";
    }else if ([str isEqualToString:@"客户星级"]){
        sub = @"ClientLevel";
    }else if ([str isEqualToString:@"客户状态"]){
        sub = @"ClientState";
    }else if ([str isEqualToString:@"国家"]){
        sub = @"Country";
    }
    // 回到中心界面
    //  请求数据 刷新界面
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reladateCustomDetail" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:sub,@"DataType",self.model.DataText, @"DataText", nil]];
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];

    
}
- (IBAction)BackButtonClick:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

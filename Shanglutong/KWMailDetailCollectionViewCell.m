//
//  KWMailDetailCollectionViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/12.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailDetailCollectionViewCell.h"

@implementation KWMailDetailCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViewCell];
    }
    return self;
}

- (void)setupViewCell {
    
    UIImageView *attachmentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12.5, 10.5, 35, 35)];
    attachmentImageView.image = [UIImage imageNamed:@"附件asd"];
    [self.contentView addSubview:attachmentImageView];
    self.attachmentImageView = attachmentImageView;
    
    UILabel *attachmentNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 45.5, 60, 38)];
    attachmentNameLabel.font = [UIFont systemFontOfSize:8];
    attachmentNameLabel.textColor = [UIColor blackColor];
    attachmentNameLabel.textAlignment = NSTextAlignmentCenter;
    attachmentNameLabel.numberOfLines = 0;
    [self.contentView addSubview:attachmentNameLabel];
    self.attachmentNameLabel = attachmentNameLabel;
    
}

- (void)setResult:(KWMailAttachmentResult *)result {
    _result = result;
    
    self.attachmentNameLabel.text = result.name;
    
}


@end

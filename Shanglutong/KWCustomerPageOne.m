//
//  KWCustomerPageOne.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerPageOne.h"
#import "KWCutomPageOneView.h"
#import "Utility.h"
#import "LXSpiderView.h"
#import "KWHttpTool.h"
#import <MJExtension.h>
#import "KWCustomDetailModel.h"
#import <SVProgressHUD.h>

@interface KWCustomerPageOne () <UITableViewDataSource, UITableViewDelegate>{
    UIScrollView *_scrollerView;
    UITableView *_tableView;
    NSArray *_imageArray;   //tableView的头图
    NSArray *_titleArray;   //tableView的cell的文字
    KWCutomPageOneView *_ThisMoth;  //加入一堆子试图
    UIButton *_btnMonth;
    UIButton *_btnFourMonth;
    UIButton *_btnYear;
}

@property (nonatomic, strong) NSDictionary *leida;

@end

@implementation KWCustomerPageOne

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _scrollerView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT)];
    _scrollerView.contentSize = CGSizeMake(KWSCREEN_WIDTH, KWSCREEN_HEIGHT);
    _scrollerView.bounces = YES;
    [self.view addSubview:_scrollerView];
    
    [self addModel];
//    创建数据
    [self createDate];
}

- (void)addModel {
    
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomIDNow"] forKey:@"clientid"];
    
//    NSLog(@"%@",requestInfo);
    NSString *netPath = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/client/Getdetai"];
    NSMutableDictionary *leida = [NSMutableDictionary dictionary];
    self.leida = leida;
    __weak KWCustomerPageOne *weakSelf = self;
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        self.leida = [[json valueForKey:@"back"] valueForKey:@"listpf"][0];
        NSLog(@"%@",self.leida);
        [SVProgressHUD dismiss];
        
        [weakSelf createLeiDa];

        [weakSelf createTableView];
//
//        [weakSelf createThreeButtom];

    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"网络未连接"];

    }];
    
}

- (void)createLeiDa {
    
    double one = [[self.leida objectForKey:@"pf3"]integerValue]/100.0;
    double Two = [[self.leida objectForKey:@"pf2"]integerValue]/100.0;
    double Three = [[self.leida objectForKey:@"pf4"]integerValue]/100.0;
    double Four = [[self.leida objectForKey:@"pf1"]integerValue]/100.0;
    double Five = [[self.leida objectForKey:@"pf5"]integerValue]/100.0;
    
    LXSpiderView * spiderView  = [[LXSpiderView alloc]initWithFrame:CGRectMake((KWSCREEN_WIDTH-200)/2, 50, 200, 200) radius: 100];
    NSArray *value = [NSArray array];
    spiderView.valueArray = value;
    spiderView.valueArray = @[@(one),@(Two),@(Three),@(Four),@(Five)];
    spiderView.titleArray = @[@"数据完整率",@"商机率",@"紧密度",@"活跃度",@"星级"];
    [_scrollerView addSubview:spiderView];
    
}

- (void)createTableView {
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 300, KWSCREEN_WIDTH, 240)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_scrollerView addSubview:_tableView];
}

//三个不同时期的按钮
- (void)createThreeButtom
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_tableView.frame)+20, KWSCREEN_WIDTH, 55)];
    view.layer.borderWidth =2;
    view.layer.borderColor = RGBACOLOR(239, 239, 239, 1).CGColor;
    [_scrollerView addSubview:view];
    
    CGFloat with = (KWSCREEN_WIDTH-95*3)/4;
    
    //每月
    _btnMonth = [[UIButton alloc]initWithFrame:CGRectMake(with, 12.5, 95, 30)];
    _btnMonth.layer.cornerRadius = 15;
    [_btnMonth setTitle:@"本月" forState:UIControlStateNormal];
    [_btnMonth setTitleColor:RGBACOLOR(130, 130, 130, 1) forState:UIControlStateNormal];
    [_btnMonth setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btnMonth addTarget:self action:@selector(addMoreSubView:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_btnMonth];
    
    //本季度
    _btnFourMonth = [[UIButton alloc]initWithFrame:CGRectMake(with*2+95, 12.5, 95, 30)];
    _btnFourMonth.layer.cornerRadius = 15;
    [_btnFourMonth setTitle:@"本季度" forState:UIControlStateNormal];
    [_btnFourMonth setTitleColor:RGBACOLOR(130, 130, 130, 1) forState:UIControlStateNormal];
    [_btnFourMonth setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btnFourMonth addTarget:self action:@selector(addMoreSubView:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_btnFourMonth];
    
    
    //本季度
    _btnYear = [[UIButton alloc]initWithFrame:CGRectMake(with*3+95*2, 12.5, 95, 30)];
    _btnYear.layer.cornerRadius = 15;
    [_btnYear setTitle:@"年度" forState:UIControlStateNormal];
    [_btnYear setTitleColor:RGBACOLOR(130, 130, 130, 1) forState:UIControlStateNormal];
    [_btnYear setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btnYear addTarget:self action:@selector(addMoreSubView:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_btnYear];
    
    [self addMoreSubView:_btnMonth];
    
}

- (void)addMoreSubView:(UIButton*)sender
{
    //首先所有的btn全部变成不选中
    _btnMonth.selected = NO;
    _btnFourMonth.selected =NO;
    _btnYear.selected = NO;
    _btnMonth.backgroundColor = [UIColor whiteColor];
    _btnFourMonth.backgroundColor = [UIColor whiteColor];
    _btnYear.backgroundColor = [UIColor whiteColor];
    
    sender.selected = YES;
    sender.backgroundColor = RGBACOLOR(24, 148, 218, 1);
    
    
    _ThisMoth = [[KWCutomPageOneView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_tableView.frame)+75, KWSCREEN_WIDTH, 676)];
    [_scrollerView addSubview:_ThisMoth];
}

#pragma  mark - uitableViewDelegate uitableViewDateSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    cell.imageView.image = [UIImage imageNamed:_imageArray[indexPath.row]];
    cell.textLabel.text =_titleArray[indexPath.row];
    return cell;
}


- (void)createDate {
    _imageArray = [NSArray arrayWithObjects:@"预警1",@"供应商1",@"商机管理1", nil];
    _titleArray = [NSArray arrayWithObjects:@"90天没有订单",@"20天没有往来邮件",@"20天没有商机", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

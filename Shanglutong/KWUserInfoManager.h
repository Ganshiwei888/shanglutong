//
//  KWUserInfoManager.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"

@interface KWUserInfoManager : NSObject

@property (nonatomic, strong) NSString *userAccount;
@property (nonatomic, strong) NSString *userMD5Password;
@property (nonatomic, strong) NSString *userServerAddress;
singleton_interface(KWUserInfoManager)

@end

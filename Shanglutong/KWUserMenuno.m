//
//  KWUserMenuno.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/11.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWUserMenuno.h"
#import "Utility.h"

@implementation KWUserMenuno

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _menuno = checkStringNull(dict[@"menuno"]);
        _name = checkStringNull(dict[@"name"]);
        _next = dict[@"next"];
    }
    return self;
}

@end

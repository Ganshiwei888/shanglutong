//
//  KWMailRemove.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailRemove.h"

@implementation KWMailRemove

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _folderName = dict[@"name"];
        _Id = dict[@"id"];
    }
    return self;
}

@end

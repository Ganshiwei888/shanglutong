//
//  KWAddress.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWAddress : NSObject

///**
// 联系人姓名
// */
//@property (nonatomic, copy) NSString *addressName;
///**
// 生日
// */
//@property (nonatomic, copy) NSString *addressBirthday;
///**
// 邮箱1
// */
//@property (nonatomic, copy) NSString *addressEmailone;
///**
// 邮箱2
// */
//@property (nonatomic, copy) NSString *addressEmailtwo;
///**
// 邮箱3
// */
//@property (nonatomic, copy) NSString *addressEmailthree;
//
///**
// facebook
// */
//@property (nonatomic, copy) NSString *faceBook;
///**
// 性别
// */
//@property (nonatomic, copy) NSString *addressSex;
//
///**
// 手机号码
// */
//@property (nonatomic, copy) NSString *addressPhone;
//
///**
// ID
// */
//@property (nonatomic, copy) NSString *addressID;
//
///**
// 部门
// */
//@property (nonatomic, copy) NSString *addressDept;
//
///**
// 工作
// */
//@property (nonatomic, copy) NSString *addressJob;
//
///**
// 联系人类型
// */
//@property (nonatomic, copy) NSString *mianAdd;

@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *ClientId;
@property (nonatomic, strong) NSString *Name;
@property (nonatomic, strong) NSString *Sex;
@property (nonatomic, strong) NSString *Email;
@property (nonatomic, strong) NSString *Email1;
@property (nonatomic, strong) NSString *Email2;
@property (nonatomic, strong) NSString *Mobile;
@property (nonatomic, strong) NSString *Tel;
@property (nonatomic, strong) NSString *Dept;
@property (nonatomic, strong) NSString *Job;
@property (nonatomic, strong) NSString *Birthday;
@property (nonatomic, strong) NSString *Bak;
@property (nonatomic, strong) NSString *QQ;
@property (nonatomic, strong) NSString *Msn;
@property (nonatomic, strong) NSString *WeChat;
@property (nonatomic, strong) NSString *SKYPE;
@property (nonatomic, strong) NSString *Facebook;
@property (nonatomic, strong) NSString *Twitter;
@property (nonatomic, strong) NSString *Interest;
@property (nonatomic, strong) NSString *MainAdd;
@property (nonatomic, strong) NSString *UpdaterName;
@property (nonatomic, strong) NSString *UpdateTm;

@property (nonatomic, assign) BOOL isSelect;

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)addressWithDict:(NSDictionary *)dict;

@end

/*
 Bak = "";
 Birthday = "<null>";
 ClientId = 29230;
 Dept = "";
 Email = "support@bestofsender.com";
 Email1 = "";
 Email2 = "";
 Facebook = "";
 Id = 29529;
 Interest = "";
 Job = "";
 MainAdd = 0;
 Mobile = "";
 Msn = "";
 Name = BitcoinNEWS;
 QQ = "";
 SKYPE = "";
 Sex = "\U7537";
 Tel = "";
 Twitter = "";
 UpdateTm = "<null>";
 UpdaterName = "<null>";
 WeChat = "";
 
*/












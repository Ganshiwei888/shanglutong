//
//  KWMailList.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailList : NSObject

@property (nonatomic, copy) NSString *AccCount;
@property (nonatomic, copy) NSString *Area;
@property (nonatomic, copy) NSString *Box;
@property (nonatomic, copy) NSString *BoxBase;
@property (nonatomic, copy) NSString *CreateTime;
@property (nonatomic, copy) NSString *FromEmail;
@property (nonatomic, copy) NSString *FromName;
@property (nonatomic, copy) NSString *IP;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *MailBoxId;
@property (nonatomic, copy) NSString *MailDate;
@property (nonatomic, copy) NSString *MailLabel;
@property (nonatomic, copy) NSString *Read;
@property (nonatomic, copy) NSString *ReadMode;
@property (nonatomic, copy) NSString *MailType;
@property (nonatomic, copy) NSString *RecDate;
@property (nonatomic, copy) NSString *ReplyTo;
@property (nonatomic, copy) NSString *SendDate;
@property (nonatomic, copy) NSString *Subject;
@property (nonatomic, copy) NSString *TopTime;
@property (nonatomic, copy) NSString *bcc;
@property (nonatomic, copy) NSString *clientid;
@property (nonatomic, copy) NSString *itFrom;
@property (nonatomic, copy) NSString *itTo;
@property (nonatomic, copy) NSString *priority;
@property (nonatomic, copy) NSString *redflag;
@property (nonatomic, copy) NSString *star;

@end

//{
//    AccCount = 0;
//    Area = "\U672c\U673a\U5730\U5740";
//    Bak = "<null>";
//    Box = SJX;
//    BoxBase = SJX;
//    CreateTime = "2017-08-02T10:16:54.257";
//    FromEmail = "xx@yy.NULL.NULL";
//    FromName = "Ricky Lee ";
//    IP = "127.0.0.1";
//    Id = 17804;
//    MailBoxId = 11;
//    MailDate = "2017/8/2 10:16:16";
//    MailLabel = "<null>";
//    MailSize = 98972;
//    MailType = "multipart/alternative";
//    RE = "<null>";
//    Read = "\U662f";
//    ReadMode = 0;
//    RecDate = "2017-08-02T10:16:17";
//    ReplyTo = "";
//    RevUserId = "<null>";
//    RevUserName = "<null>";
//    RootId = "<null>";
//    SendDate = "2017/8/2 10:16:17";
//    Subject = "Ricky Lee Would Like To Do Business With You inquiry@foodchem.cn Via Linkedin";
//    TopTime = "<null>";
//    bcc = "";
//    cc = "";
//    clientid = "<null>";
//    itFrom = "Ricky Lee <xx@yy.NULL.NULL>";
//    itTo = "inquiry@foodchem.cn";
//    priority = "";
//    redflag = "<null>";
//    star = "<null>";
//}





//
//  KWBusinessTableViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBusinessTableViewController.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import "NSObject+KWModel.h"
#import "KWBusinseeParam.h"
#import "KWBusinessBackResult.h"
#import <MJExtension.h>


@interface KWBusinessTableViewController ()

@end

@implementation KWBusinessTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商机中心";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getmsg",USER_SERVERADDRESS];
    
    KWBusinseeParam *param = [KWBusinseeParam param];
    
    param.Pagemax = @"5";
    param.Pageindex = @"1";
    param.Act = @"all";
    
    KWBaseParam *paramP = [param copy];
    
    NSLog(@"%@",paramP);
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json[@"back"]);
        
        KWBusinessBackResult *result = [KWBusinessBackResult modelInModelAndArrayWithDict:json[@"back"]];
        
        NSLog(@"%ld",result.msg.count);
        
    } failure:^(NSError *error) {
        
    }];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"busCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  KWMailDetailParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailDetailParam : KWBaseParam
/**
 请求邮件的Id
 */
@property (nonatomic, copy) NSString *MailId;
@property (nonatomic, copy) NSString *is;

@end

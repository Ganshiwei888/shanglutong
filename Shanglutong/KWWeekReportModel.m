//
//  KWWeekReportModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWWeekReportModel.h"
#import "Utility.h"

//@property (nonatomic, copy) NSString *BranchId;
//@property (nonatomic, copy) NSString *CommCount;
//@property (nonatomic, copy) NSString *CommReadUID;
//@property (nonatomic, copy) NSString *CreateTm;
//@property (nonatomic, copy) NSString *Creater;
//@property (nonatomic, copy) NSString *CreaterName;
//@property (nonatomic, copy) NSString *DeptId;
//@property (nonatomic, copy) NSString *Html;
//@property (nonatomic, copy) NSString *RepEndDate;
//@property (nonatomic, copy) NSString *RepStartDate;
//@property (nonatomic, copy) NSString *Type;
//@property (nonatomic, copy) NSString *UpdateTm;
//@property (nonatomic, copy) NSString *Id;
//@property (nonatomic, copy) NSString *Updater;
//@property (nonatomic, copy) NSString *UpdaterName;

@implementation KWWeekReportModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _BranchId = checkStringNull(dict[@"BranchId"]);
        _CommCount = checkStringNull(dict[@"CommCount"]);
        _CommReadUID = checkStringNull(dict[@"CommReadUID"]);
        _CreateTm = checkStringNull(dict[@"CreateTm"]);
        _Creater = checkStringNull(dict[@"Creater"]);
        _CreaterName = checkStringNull(dict[@"CreaterName"]);
        _Html = checkStringNull(dict[@"Html"]);
        _RepEndDate = checkStringNull(dict[@"RepEndDate"]);
        _RepStartDate = checkStringNull(dict[@"RepStartDate"]);
        _Id = checkStringNull(dict[@"Id"]);
        
    }
    return self;
}


@end

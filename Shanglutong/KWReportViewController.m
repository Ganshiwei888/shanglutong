//
//  KWReportViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWReportViewController.h"
#import "KWDayReportModel.h"
#import "KWDayReportTableViewCell.h"
#import "KWWeekReportModel.h"
#import "KWWeekReportTableViewCell.h"
#import "KWHttpTool.h"
#import "KWBaseParam.h"
#import "Utility.h"
#import "KWReportDetailViewController.h"
#import <MJRefresh.h>
#import <MJExtension.h>
#import <SVProgressHUD.h>


@interface KWReportViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UISegmentedControl *segment;

@property (nonatomic, weak) UITableView *dayTableView;
@property (nonatomic, weak) UITableView *weekTableView;

@property (nonatomic, strong) NSArray *dayListArray;
@property (nonatomic, strong) NSArray *weekListArray;

@end

@implementation KWReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    // Do any additional setup after loading the view.
    
    [self setupTableView];
    [self setupDayReport];
    [self setupWeekReport];
}

- (void)setupNav {
    
    NSArray *array = [NSArray arrayWithObjects:@"工作日报",@"工作周报", nil];
    //初始化UISegmentedControl
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:array];
    //设置frame
    segment.frame =CGRectMake(60, 100, 160, 30);
    self.navigationItem.titleView = segment;
    //添加事件
    segment.selectedSegmentIndex = 0;
    [segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    self.segment = segment;
    
}

- (void)setupTableView {
    
    UITableView *dayTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    dayTableView.dataSource = self;
    dayTableView.delegate = self;
    [self.view addSubview:dayTableView];
    self.dayTableView = dayTableView;
    
    UITableView *weekTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    weekTableView.dataSource = self;
    weekTableView.delegate = self;
    [self.view addSubview:weekTableView];
    self.weekTableView = weekTableView;
    self.weekTableView.hidden = YES;
    
}

- (void)setupDayReport {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getdayrepot",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSArray *BGLarray = json[@"back"][@"BGL"];
        if (BGLarray) {
            
            NSMutableArray *temp = [[NSMutableArray array] init];
            for (NSDictionary *dict in BGLarray) {
                KWDayReportModel *model = [[KWDayReportModel alloc] initWithDict:dict];
                [temp addObject:model];
            }
            self.dayListArray = temp;
            [self.dayTableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];

}

- (void)setupWeekReport {
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/GetWeekrepot",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json);
        NSArray *BGLarray = json[@"back"][@"BGL"];
        if (BGLarray) {
            
            NSMutableArray *temp = [[NSMutableArray array] init];
            for (NSDictionary *dict in BGLarray) {
                KWWeekReportModel *model = [[KWWeekReportModel alloc] initWithDict:dict];
                [temp addObject:model];
            }
            self.weekListArray = temp;
            [self.weekTableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)change:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        NSLog(@"1");
        [self.dayTableView setHidden:NO];
        [self.weekTableView setHidden:YES];
    }else if (sender.selectedSegmentIndex == 1){
        NSLog(@"2");
        [self.dayTableView setHidden:YES];
        [self.weekTableView setHidden:NO];
    }
    
}

#pragma mark - UITableView -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.dayTableView) {
        return self.dayListArray.count;
    } else {
        return self.weekListArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.dayTableView) {
        static NSString *cellID = @"rcell";
        KWDayReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[KWDayReportTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.dayModel = self.dayListArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    } else {
        static NSString *cellid = @"dcell";
        KWWeekReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) {
            cell = [[KWWeekReportTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        }
        cell.weekModel = self.weekListArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.dayTableView) {
        KWReportDetailViewController *vc = [[KWReportDetailViewController alloc] init];
        [vc receiveTheDayModel:self.dayListArray[indexPath.row] type:@"day"];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        KWReportDetailViewController *vc = [[KWReportDetailViewController alloc] init];
        [vc receiveTheWeekModel:self.weekListArray[indexPath.row] type:@"week"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

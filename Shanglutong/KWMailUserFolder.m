//
//  KWMailUserFolder.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/11.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailUserFolder.h"
#import "Utility.h"

@implementation KWMailUserFolder
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"ID": @"id"};
}

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _ID = checkStringNull(dict[@"id"]);
        _mailboxId = checkStringNull(dict[@"mailboxId"]);
        _name = checkStringNull(dict[@"name"]);
        _parentid = checkStringNull(dict[@"parentid"]);
        _sort = checkStringNull(dict[@"sort"]);
    }
    return self;
}

@end





//
//  KWMailListTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWMailList.h"
#import "KWMailListParam.h"
#import "KWMailListResult.h"
#import "KWMailDetailParam.h"

@interface KWMailListTool : NSObject

/**
 请求邮件列表

 @param param 请求参数
 @param success 请求成功回调
 @param failure 请求失败回调
 */
+ (void)postGetMailListWith:(KWMailListParam *)param success:(void(^)(KWMailListResult *resultData))success failure:(void(^)(NSError *error))failure;

/**
 redFlagMail

 @param param   请求参数
 @param success 请求成功
 @param failure 请求失败
 */
+ (void)postFlagTheMailWith:(KWMailDetailParam *)param success:(void(^)(NSString *resultData))success failure:(void(^)(NSError *erroe))failure;

/**
 starMail
 
 @param param   请求参数
 @param success 请求成功
 @param failure 请求失败
 */
+ (void)postStarTheMailWith:(KWMailDetailParam *)param success:(void(^)(NSString *resultData))success failure:(void(^)(NSError *erroe))failure;

/**
 topMail
 
 @param param   请求参数
 @param success 请求成功
 @param failure 请求失败
 */
+ (void)postTopTheMailWith:(KWMailDetailParam *)param success:(void(^)(NSString *resultData))success failure:(void(^)(NSError *erroe))failure;

+ (void)postReadTheMailWith:(KWMailDetailParam *)param success:(void(^)(NSString *resultData))success failure:(void(^)(NSError *erroe))failure;


@end





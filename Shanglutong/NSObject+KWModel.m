
//
//  NSObject+KWModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "NSObject+KWModel.h"
#import <objc/message.h>
@implementation NSObject (KWModel)

+ (instancetype)modelWithDict:(NSDictionary *)dict {
    
    //创建对应的对象
    id objc = [[self alloc] init];
    
    //利用runtime给对象中的属性赋值
    
    unsigned int count = 0;
    
    //获取类中所有成员变量
    Ivar *ivarList = class_copyIvarList(self, &count);
    
    for (int i = 0; i < count; i++) {
        
        Ivar ivar = ivarList[i];
        
        NSString *ivarName = [NSString stringWithUTF8String:ivar_getName(ivar)];
        
        NSString *key = [ivarName substringFromIndex:1];
        
        id value = dict[key];
        
        if (value) {
            [objc setValue:value forKey:key];
        }
        
    }
    return objc;
}

+ (instancetype)modelInModelWithDict:(NSDictionary *)dict {
    
    id objc = [[self alloc] init];
    
    unsigned int count = 0;
    
    Ivar *ivarList = class_copyIvarList(self, &count);
    
    for (int i=0; i<count; i++) {
        Ivar ivar = ivarList[i];
        //获取成员变量的名字
        NSString *ivarName = [NSString stringWithUTF8String:ivar_getName(ivar)];
        //获取成员变量的类型
        NSString *ivarType = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
        
        //替换
        ivarType = [ivarType stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        ivarType = [ivarType stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        NSString *key = [ivarName substringFromIndex:1];
        
        //根据成员的属性名去字典中查找对应的value
        
        id value = dict[key];
        
        //字典中还有字典，也需要把对应的字典转换成模型
        // 判断下value是否是字典,并且是自定义对象才需要转换
        if ([value isKindOfClass:[NSDictionary class]] && ![ivarType hasPrefix:@"NS"]) {
            Class modelClass = NSClassFromString(ivarType);
            if (modelClass) {
                value = [modelClass modelInModelWithDict:value];
            }
        }
        if (value) {
            [objc setValue:value forKey:key];
        }
    }
    return objc;
}

+ (instancetype)modelInModelAndArrayWithDict:(NSDictionary *)dict {
    id objc = [[self alloc] init];
    
    unsigned int count = 0;
    
    //返回的是一个Ivar类型的数组，由count index。
    //由Ivar指针指向这个数组
    Ivar *ivarList = class_copyIvarList(self, &count);
    
    for (int i=0; i<count; i++) {
        
        Ivar ivar = ivarList[i];
        
        NSString *ivarName = [NSString stringWithUTF8String:ivar_getName(ivar)];
        
        NSString *key = [ivarName substringFromIndex:1];
        
        id value = dict[key];
        
//      三级转换：NSArray中也是字典，把数组中的字典转换成模型.
        if ([value isKindOfClass:[NSArray class]]) {
            // 判断对应类有没有实现字典数组转模型数组的协议
            // arrayContainModelClass 提供一个协议，只要遵守这个协议的类，都能把数组中的字典转模型
            if ([self respondsToSelector:@selector(arrayContainModelClass)]) {
                
                id idSelf = self;
                // 获取数组中字典对应的模型
                NSString *type = [idSelf arrayContainModelClass][key];
                
                // 生成模型
                Class classModel = NSClassFromString(type);
                
                NSMutableArray *arrM = [NSMutableArray array];
                
                for (NSDictionary *dict in value) {
                    id model = [classModel modelInModelAndArrayWithDict:dict];
                    [arrM addObject:model];
                }
                
                // 把模型数组赋值给value
                value = arrM;
            }
        }
        
        if (value) {
            [objc setValue:value forKey:key];
        }
        
    }
    return objc;
}

@end










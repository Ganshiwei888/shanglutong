//
//  KWMailReMoveView.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMailReMoveView : UIView

- (void)showAnimated;

- (void)receTheMailId:(NSString *)mailId;

@end

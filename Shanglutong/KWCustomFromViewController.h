//
//  KWCustomFromViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/13.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWCustomFromViewController : UIViewController

@property (nonatomic, copy) NSString *fromName;

- (void)receiveTheMail:(NSString *)mailaddress andclientId:(NSString *)clientid;

@end

//
//  KWMailRemove.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailRemove : NSObject

//@property (weak, nonatomic) UIButton *selectBtn;
@property (weak, nonatomic) NSString *folderName;
@property (weak, nonatomic) NSString *Id;

@property (assign, nonatomic) BOOL isSelect;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

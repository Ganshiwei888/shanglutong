//
//  AppDelegate.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/19.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <GTSDK/GeTuiSdk.h>     // GetuiSdk头文件应用

// iOS10 及以上需导入 UserNotifications.framework
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#import <UserNotifications/UserNotifications.h>
#endif

#define kGtAppId           @"Xg1sgtTNZJA03Ij2IEkwy3"
#define kGtAppKey          @"EdW5PcIdHyAnRPcU6SJMQ4"
#define kGtAppSecret       @"X9FZPpk7nT81tJ0PTxR8Y6"
#define kBuglyAppId        @"cd0e69efe6"
#define kBuglyAppKey       @"b72eb462-122e-454b-94e7-a776f6a251dc"

@class KWLoginViewController;
@class KWNavigationController;
@interface AppDelegate : UIResponder <UIApplicationDelegate, GeTuiSdkDelegate, UNUserNotificationCenterDelegate, EMClientDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KWLoginViewController *loginViewController;
@property (strong, nonatomic) KWNavigationController *nav;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (copy, nonatomic) NSString *clientId;

- (void)saveContext;


@end


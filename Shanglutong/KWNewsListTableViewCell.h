//
//  KWNewsListTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell.h>

@class KWNewsListFrame;
@interface KWNewsListTableViewCell : MGSwipeTableCell

@property (strong, nonatomic) KWNewsListFrame *listFrame;

@property (weak, nonatomic) UIImageView *readImageView;

@property (weak, nonatomic) UIImageView *typeImageView;

@end

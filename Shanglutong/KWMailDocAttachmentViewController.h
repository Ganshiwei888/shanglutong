//
//  KWMailDocAttachmentViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMailDocAttachmentViewController : UIViewController
- (void)recieveFilePath:(NSString *)filePath;
@end

//
//  KWMailSendViewController.m
//  
//
//  Created by KeWen on 2017/8/8.
//
//

#import "KWMailSendViewController.h"
#import "MBProgressHUD+XQ.h"
#import "KWUserMailBoxListCell.h"
#import "KWBaseParam.h"
#import "KWHttpTool.h"
#import "KWMailList.h"
#import "KWMailAttachmentInfoModel.h"
#import "UIImage+KWImageSize.h"
#import "KWMailSendParam.h"
#import <LPDQuoteImagesView.h>
#import "KWUserInfoManager.h"
#import "Utility.h"
#import "Base64.h"
#import "NSData+Base64.h"
#import "LWActiveIncator.h"
#import "KWMailSelectAddressView.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>
#import <IQKeyboardManager.h>

#define TableViewHeight 40
#define TopView_TotalHeight 225
#define CollectionImageViewSize CGSizeMake(60, 60)

@interface KWMailSendViewController () <UITextFieldDelegate, UITextViewDelegate, LPDQuoteImagesViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    
}

@property (nonatomic, assign) MailSendType type;
@property (nonatomic, strong) NSString *mailBoxId;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) KWMailDetailResultModel *mailDetailResultModel;
@property (weak, nonatomic) UIButton *userSendMailButton;
@property (weak, nonatomic) IBOutlet UITextField *recipientTextField;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;
@property (weak, nonatomic) IBOutlet UITextField *mailCcTextField;
@property (strong, nonatomic) UITextView *mailDetailText;
@property (nonatomic, strong) NSArray <KWUserMailBoxListCell *> *mailBoxArray;
@property (nonatomic, strong) NSArray<KWMailDetailFileModel *> *attachmentArray;
@property (nonatomic, strong) NSMutableArray <KWMailAttachmentInfoModel *> *uploadAttachmentArray;

@property (strong, nonatomic) UITableView *mailBoxArrayTableView;

@property (weak, nonatomic) LPDQuoteImagesView *imagePick;
@property (strong,nonatomic) NSMutableArray *fieldArray;
@property (nonatomic,strong) NSMutableDictionary *htmLInfoDic; //html的高度宽度
@property (nonatomic, strong) NSString *mailId;
@property (nonatomic, strong) UIScrollView *pScrollView;


@end

@implementation KWMailSendViewController

#pragma mark - Lazy Laod -

- (NSMutableArray<KWMailAttachmentInfoModel *> *)uploadAttachmentArray {
    if (!_uploadAttachmentArray) {
        _uploadAttachmentArray = [NSMutableArray array];
    }
    return _uploadAttachmentArray;
}

- (UITableView *)mailBoxArrayTableView {
    if (!_mailBoxArrayTableView) {
        CGRect tableViewframe;
        if (self.mailBoxArray.count > 4) {
            tableViewframe = CGRectMake(0, 64, KWSCREEN_WIDTH, TableViewHeight * 4);
        } else {
            tableViewframe = CGRectMake(0, 64, KWSCREEN_WIDTH, TableViewHeight * self.mailBoxArray.count);
        }
        _mailBoxArrayTableView = [[UITableView alloc] initWithFrame:tableViewframe style:UITableViewStylePlain];
        _mailBoxArrayTableView.hidden = YES;
        _mailBoxArrayTableView.delegate = self;
        _mailBoxArrayTableView.dataSource = self;
        _mailBoxArrayTableView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
        [self.view addSubview:_mailBoxArrayTableView];
    }
    return _mailBoxArrayTableView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupMailBoxArray];
    
    [self setupNav];
    
    self.recipientTextField.delegate = self;
    self.subjectTextField.delegate = self;
    self.mailCcTextField.delegate = self;
    // Do any additional setup after loading the view from its nib.
    
    [self setupReplyMail];

    [self setupImagePick];
    
    if (_sendName){
        self.userSendMailButton.titleLabel.text = _sendName;
        
    }
    if (_replyTo){
        self.recipientTextField.text = _replyTo;
    }
    if (_subject){
        //     收件人：recipientTextField 发件人：userSendMailButton 主题：subjectTextField
        if (self.type == MailSendTypeReply){
            self.subjectTextField.text = [NSString stringWithFormat:@"Re: %@",_subject];
        }else if (self.type == MailSendTypeForward){
            self.subjectTextField.text = [NSString stringWithFormat:@"For: %@",_subject];
        }
        
    }
    
    self.mailDetailText.layer.borderColor = [UIColor clearColor].CGColor;
    
}

- (void)setupMailBoxArray {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mailbox/Get"];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        NSArray *backArray = [json valueForKey:@"back"];
        NSMutableArray *mailarray = [NSMutableArray array];
        for (NSDictionary *dict in backArray) {
            KWUserMailBoxListCell *model = [[KWUserMailBoxListCell alloc] initWithDict:dict];
            [mailarray addObject:model];
        }
        self.mailBoxArray = mailarray;
        KWUserMailBoxListCell *model = self.mailBoxArray[0];
        [self.userSendMailButton setTitle:model.email forState:UIControlStateNormal];
        self.mailId = model.ID;
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"没有发件人" toView:self.view];
    }];
    
}
- (IBAction)getAttachment:(UIButton *)sender {
    
    self.imagePick.hidden = NO;
    self.pScrollView.frame = CGRectMake(8, TopView_TotalHeight + 8, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - TopView_TotalHeight - 100);
    [self.view layoutIfNeeded];
    
}

- (void)setupReplyMail {
    
       if (!(self.type == MailSendTypeNew)){
           //textView的长度 宽度
           NSInteger with = [[self.htmLInfoDic objectForKey:@"htmlWith"]integerValue];
           NSInteger heigth = [[self.htmLInfoDic objectForKey:@"htmlHeight"]integerValue];
        
           UITextView *mailDetailText = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, with, heigth)];
           mailDetailText.userInteractionEnabled = YES;
           mailDetailText.clipsToBounds = NO;
           mailDetailText.scrollEnabled = YES;
           mailDetailText.delegate = self;
           self.mailDetailText = mailDetailText;
           
       } else {
           UITextView *mailDetailText = [[UITextView alloc] initWithFrame:CGRectMake(8, TopView_TotalHeight + 8, (KWSCREEN_WIDTH - 16), KWSCREEN_HEIGHT - TopView_TotalHeight - 160)];
           mailDetailText.userInteractionEnabled = YES;
           mailDetailText.clipsToBounds = NO;
           mailDetailText.scrollEnabled = YES;
           mailDetailText.delegate = self;
           self.mailDetailText = mailDetailText;
    }
    
    NSString *normalStr = nil;
    NSMutableAttributedString *contentText = nil;
    
    if (self.type == MailSendTypeNew) {
        normalStr = @"  ";
        contentText = [[NSMutableAttributedString alloc] initWithString:normalStr];
    } else if(self.type == MailSendTypeForward){
        
        normalStr = @" \n\n\n---   Original Message   ---\n<br>";
        NSString *str = nil;
        
        if (self.mailDetailResultModel.htmlbody.length >0) {
            
            str = [self.mailDetailResultModel.htmlbody stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\\"];
        } else {
            str = [self.mailDetailResultModel.textbody stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\\"];
        }
        NSLog(@"%@", str);
        if (str) {
            NSString *newStr = [normalStr stringByAppendingString:str];
            if (![_replyTo containsString:@"facebookmail.com"]) {
                contentText = [[NSMutableAttributedString alloc] initWithData:[newStr dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            }
        }
        
    }else if (self.type == MailSendTypeReply){
        
        normalStr = @" <br> <br> <br> <br> ---   Original Message   ---\n<br>";
        NSString *str = nil;
        if (self.mailDetailResultModel.htmlbody.length >0) {
            
            str = [self.mailDetailResultModel.htmlbody stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\\"];
        } else {
            str = [self.mailDetailResultModel.textbody stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\\"];
        }
        if (str) {
            NSString *newStr = [normalStr stringByAppendingString:str];
            if (![_replyTo containsString:@"facebookmail.com"]) {
                contentText = [[NSMutableAttributedString alloc] initWithData:[newStr dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            }
        }
    }
    self.mailDetailText.textAlignment = NSTextAlignmentLeft;
    self.mailDetailText.attributedText = contentText;
    self.mailDetailText.returnKeyType = UIReturnKeyDefault;
    
    if (!(self.type ==MailSendTypeNew)){
        UIScrollView *scrtext = [[UIScrollView alloc]initWithFrame:CGRectMake(8, TopView_TotalHeight + 8, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - TopView_TotalHeight)];
        scrtext.contentSize = self.mailDetailText.frame.size;
        self.pScrollView = scrtext;
        [self.view addSubview:self.pScrollView];
        [self.pScrollView addSubview: self.mailDetailText];
    }else {
        [self.view addSubview: self.mailDetailText];
    }
    
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    //当前输入字数
//    if ([text isEqualToString:@"\n"]) {
//        [self.mailDetailText resignFirstResponder];
//        return NO;
//    }
//    return YES;
//}

- (void)setupNav {
    UIButton *userSendMailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    userSendMailButton.frame = CGRectMake(0, 0, 280, 40);
    self.navigationItem.titleView = userSendMailButton;
    [userSendMailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [userSendMailButton addTarget:self action:@selector(selectSendMail:) forControlEvents:UIControlEventTouchUpInside];
    self.userSendMailButton = userSendMailButton;
    
    UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelBarButtonItemClick:)];
    cancelBarButtonItem.tintColor = RGBCOLOR(11, 23, 70);
    self.navigationItem.leftBarButtonItem = cancelBarButtonItem;
    UIBarButtonItem *mailSendBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStylePlain target:self action:@selector(mailSendBarButtonClick:)];
    mailSendBarButtonItem.tintColor = RGBCOLOR(11, 23, 70);
    self.navigationItem.rightBarButtonItem = mailSendBarButtonItem;
    
}

- (void)selectSendMail:(UIButton *)sender {
    
    NSLog(@"button click");
    
    self.mailBoxArrayTableView.hidden = NO;
    
}
- (IBAction)selectRec:(UIButton *)sender {
    
    KWMailSelectAddressView *selectView = [[KWMailSelectAddressView alloc] initWithFrame:CGRectMake(25, 45, KWContentViewWidth, KWContentViewHeight)];
    __weak KWMailSendViewController *weakSelf = self;
    selectView.selectAdd = ^(NSString *Email) {
        weakSelf.recipientTextField.text = Email;
    };
    
    [selectView showAnimated];
    
}
- (IBAction)selectCc:(UIButton *)sender {
    
    
    
}
- (IBAction)selectBcc:(UIButton *)sender {
    
    
    
}

- (void)setupImagePick {
    
    LPDQuoteImagesView *imagePick = [[LPDQuoteImagesView alloc] initWithFrame:CGRectMake(30, KWSCREEN_HEIGHT - 90, KWSCREEN_WIDTH - 60, 80) withCountPerRowInView:5 cellMargin:8];
    imagePick.collectionView.scrollEnabled = YES;
    imagePick.navcDelegate = self;
    imagePick.maxSelectedCount = 9;
    imagePick.hidden = YES;
    [self.view addSubview:imagePick];
    self.imagePick = imagePick;
    
}

- (void)setupAttachmentArray {
    
    NSArray <UIImage *> *imageArray = [NSArray arrayWithArray:self.imagePick.selectedPhotos];
    
    for (UIImage *image in imageArray) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.2);
        KWMailAttachmentInfoModel *model = [[KWMailAttachmentInfoModel alloc] init];
        model.imageData = imageData;
        model.size = imageData.length;
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateForMatter = [[NSDateFormatter alloc] init];
        dateForMatter.dateFormat = @"yyyyMMdd";
        NSString *dateString = [dateForMatter stringFromDate:date];
        
        model.name = [NSString stringWithFormat:@"%@_%zd.png",dateString,self.uploadAttachmentArray.count];
        NSString *sizeString = [self dataSizeToString:imageData.length];
        model.sizeString = sizeString;
        
        UIImage *smallImage = [UIImage imageByScalingAndCroppingForSize:CollectionImageViewSize SourceImage:image];
        NSData *smallImageData = UIImagePNGRepresentation(smallImage);
        model.smallImageData = smallImageData;
        [self.uploadAttachmentArray addObject:model];
    }
    
}

- (NSString *)dataSizeToString:(NSUInteger)size {
    NSString *sizeString;
    double aSize = size / 1024.0;
    if (aSize > 1024) {
        double bSize = size / 1024.0 / 1024.0;
        if (bSize > 1024) {
            double cSize = size / 1024.0 / 1024.0 / 1024.0;
            sizeString = [NSString stringWithFormat:@"%.2fG",cSize];
        } else {
            sizeString = [NSString stringWithFormat:@"%.2fM",bSize];
        }
    } else {
        sizeString = [NSString stringWithFormat:@"%.2fKB",aSize];
    }
    return sizeString;
}

- (void)cancelBarButtonItemClick:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)mailSendBarButtonClick:(UIButton *)sender {
    
    [self setupAttachmentArray];
    
    if (!self.recipientTextField.text || [self.recipientTextField.text isEqualToString:@""] || self.recipientTextField.text.length < 1) {
        [MBProgressHUD showError:@"收件人不能为空" toView:self.view];
        return;
    }
    
    if (!self.subjectTextField.text || [self.subjectTextField.text isEqualToString:@""] || self.subjectTextField.text.length < 1) {
        [MBProgressHUD showError:@"主题不能为空" toView:self.view];
        return;
    }
    
    if ([self.userSendMailButton.titleLabel.text isEqualToString:@"选择发送邮箱"]) {
        [MBProgressHUD showError:@"请选择发送邮箱" toView:self.view];
        return;
    }
    
    // 创建队列组，可以使多个网络请求异步执行，执行完之后再进行操作
    dispatch_group_t group = dispatch_group_create();
     //创建全局队列
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    
    dispatch_group_async(group, queue, ^{
        //创建数组保存fieldid
        self.fieldArray = [NSMutableArray array];
        
        for (int i = 0;  i < self.uploadAttachmentArray.count; i++) {
            
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            
            KWMailSendParam *param = [KWMailSendParam param];
            
            NSData *dataupOne = self.uploadAttachmentArray[i].imageData;
            NSString *strupOne = [NSString stringWithFormat:@"%ld",(unsigned long)dataupOne.length];
            NSNumber *numStr = @([strupOne intValue]);
            param.size = numStr;
            param.name = self.uploadAttachmentArray[i].name;
            
            NSString *urlStr = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/file/Submit"];
            
            [KWHttpTool postWithURL:urlStr params:param.mj_keyValues success:^(id json) {
                
                if ([[json objectForKey:@"code"] isEqualToString:@"0000"]) {
                    
                    NSDictionary *back = [json valueForKey:@"back"];
                    NSString *fieldId = [back valueForKey:@"fileid"];
                    int fileInter = [fieldId intValue];
                    NSString *fileInterStr = [NSString stringWithFormat:@"%d",fileInter];
                    [self.fieldArray addObject:fileInterStr];
                    
                    //上传Data
                    NSData *dataup = self.uploadAttachmentArray[i].imageData;
                    //上传的长度
                    NSString *lenth = [NSString stringWithFormat:@"%d",10240];
                    //上传部分的数据
                    NSData *uptoData = [[NSData alloc] init];
                    if (dataup.length>10240){
                        NSRange rang = NSMakeRange(0,[lenth integerValue]);
                        uptoData = [dataup subdataWithRange:rang];
                    }else {
                        uptoData = dataup;
                    }
                    
                    NSString *strup = [uptoData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                    NSMutableDictionary *requestInfoup = [[NSMutableDictionary alloc]init];
                    [requestInfoup setValue:USER_ID forKey:@"UserId"];
                    [requestInfoup setValue:USER_PASSWORD forKey:@"Password"];
                    [requestInfoup setValue:RAN forKey:@"Ran"];
                    [requestInfoup setValue:SIGN forKey:@"Sign"];
                    [requestInfoup setValue:strup forKey:@"data"];
                    [requestInfoup setValue:fileInterStr forKey:@"fileid"];
                    [requestInfoup setValue:@"0" forKey:@"position"];
                    NSString *urlStrup = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/file/Upload"];
                    [self uploadfiled:requestInfoup portPath:urlStrup sign:semaphore str:dataup];
                
                } else {
                    dispatch_semaphore_signal(semaphore);
                }
            } failure:^(NSError *error) {
                NSLog(@"error");
                dispatch_semaphore_signal(semaphore);
            }];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
    });
    // 当所有队列执行完成之后
    
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *userAccount = [KWUserInfoManager sharedKWUserInfoManager].userAccount;
            NSString *userPassword = [KWUserInfoManager sharedKWUserInfoManager].userMD5Password;
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970] * 1000;
            NSString *dateString = [NSString stringWithFormat:@"%f",interval];
            NSString *itFrom = self.userSendMailButton.titleLabel.text;
            NSString *htmlString = [[NSString alloc]init];
            
            NSData *htmlData = [self.mailDetailText.attributedText dataFromRange:NSMakeRange(0, self.mailDetailText.attributedText.length) documentAttributes:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} error:nil];
            NSLog(@"%@",self.mailDetailText.attributedText);
            
            htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
            NSRange range = [htmlString rangeOfString:@"<html>"];//匹配得到的下标
            htmlString = [htmlString substringFromIndex:range.location];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"'" withString:@""];

            
//            self.hud = [MBProgressHUD showIndicatorWithText:@"发送中" ToView:self.view];
            
            [LWActiveIncator showInView:self.view];
            
            NSString *urlStr = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/mail/Add"];
            NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
            [requestInfo setValue:userAccount forKey:@"UserId"];
            [requestInfo setValue:userPassword forKey:@"Password"];
            [requestInfo setValue:RAN forKey:@"Ran"];
            [requestInfo setValue:SIGN forKey:@"Sign"];
            requestInfo[@"check"] = @"1";
            requestInfo[@"mailid"] = @"0";
            requestInfo[@"MailBoxId"] = self.mailId;
            NSLog(@"________%@",self.mailId);
            requestInfo[@"FromName"] = @"LEO";
            requestInfo[@"Subject"] = self.subjectTextField.text;
            requestInfo[@"SendDate"] = dateString;
            requestInfo[@"MailType"] = @"text/html";
            requestInfo[@"itFrom"] = itFrom;
            requestInfo[@"itTo"] = [NSString stringWithFormat:@"<%@>",self.recipientTextField.text];
            if (self.mailCcTextField.text && ![self.mailCcTextField.text isEqualToString:@""] && self.mailCcTextField.text.length > 0) {
                requestInfo[@"cc"] = [NSString stringWithFormat:@"<%@>",self.mailCcTextField.text];
            }else {
                requestInfo[@"cc"] = @"";
            }
//            if (self.bindCarbonCopyTextField.text && ![self.bindCarbonCopyTextField.text isEqualToString:@""] && self.bindCarbonCopyTextField.text.length > 0) {
//                requestInfo[@"Bcc"] = [NSString stringWithFormat:@"<%@>",self.bindCarbonCopyTextField.text];
//            }else {
//                requestInfo[@"Bcc"] = @"";
//            }
            requestInfo[@"HtmlBody"] =htmlString;
            requestInfo[@"BoxBase"] = @"YFS";
            requestInfo[@"Box"] = @"CGX";
            requestInfo[@"MailDate"] = dateString;
            
            requestInfo[@"MailLabel"] = @"";
            
            //fieldId  数组
            NSString *fieldIdString = [self.fieldArray componentsJoinedByString:@","];
            if (self.fieldArray.count){
                requestInfo[@"fileid"] = fieldIdString;
            }
            
            __weak KWMailSendViewController *weakSelf = self;
            
            [KWHttpTool postWithURL:urlStr params:requestInfo success:^(id json) {
                
                NSLog(@"send a mail");
                
//                [weakSelf.hud removeFromSuperview];
                
                NSLog(@"%@",json[@"back"][@"msg"]);
                //增加发送处理
                NSString *MailIDSend = json[@"back"][@"id"];
                if (MailIDSend){
                    [weakSelf sendMail:MailIDSend];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
                
                [LWActiveIncator hideInViwe:self.view];
                
            } failure:^(NSError *error) {
                [LWActiveIncator hideInViwe:self.view];
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"创建失败"]];
            }];
        });
        
    });
    
}
/*
 分段上传
 增加信号量
 */
- (void)uploadfiled:(NSMutableDictionary *)requestInfoup portPath:(NSString *)urlStrup sign:(dispatch_semaphore_t)semaphore str:(NSData *)data{
    
    [KWHttpTool postWithURL:urlStrup params:requestInfoup success:^(id json) {
        
        NSString *code = [json valueForKey:@"code"];
        
        if ([code isEqualToString:@"0011"]) {
            NSLog(@"上传附件data成功");
            dispatch_semaphore_signal(semaphore);
        } else if([code isEqualToString:@"0000"]) {
            //继续的起点
            NSString *next = [[json objectForKey:@"back"]objectForKey:@"length"];
            
            //上传的长度
            NSString *lenth = [NSString stringWithFormat:@"%d",10*1024];
            //上传部分的数据
            NSRange rang = NSMakeRange([next intValue],[lenth integerValue]);
            NSData *uptoData = [[NSData alloc]init];
            NSString *strup = [[NSString alloc]init];
            if (rang.location +rang.length >data.length){
                rang = NSMakeRange([next intValue], data.length - [next intValue]);
                uptoData = [data subdataWithRange:rang];
                strup = [uptoData base64EncodedString];
            }else {
                uptoData = [data subdataWithRange:rang];
                strup = [uptoData base64EncodedString];
            }
            //修改上传参数
            [requestInfoup setValue:strup forKey:@"data"];
            [requestInfoup setValue:next forKey:@"position"];
            [self uploadfiled:requestInfoup portPath:urlStrup sign:semaphore str:data];
        }
    } failure:^(NSError *error) {
        NSLog(@"上传附件data失败");
        dispatch_semaphore_signal(semaphore);
    }];
    
}

#pragma mark -发送邮件
- (void)sendMail:(NSString*)mailId
{
    NSString *urlStr = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/mail/send"];
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:mailId forKey:@"mailid"];
    
    [KWHttpTool postWithURL:urlStr params:requestInfo success:^(id json) {
        
        if ([json[@"code"] isEqualToString:@"0000"]){
            
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            [SVProgressHUD dismissWithDelay:0.5];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}


#pragma mark - UITableViewDataSource && UITableViewDelegate - 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.mailBoxArray.count;
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const cellIdentifier = @"MailSendTabelCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    KWUserMailBoxListCell *cellModel = self.mailBoxArray[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@<%@>", cellModel.name, cellModel.email];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //    self.userSendMailButton.titleLabel.text = ;
    [self.userSendMailButton setTitle:cell.textLabel.text forState:UIControlStateNormal];
    //    self.maskView.hidden = !self.maskView.hidden;
    self.mailBoxArrayTableView.hidden = !self.mailBoxArrayTableView.hidden;
    KWUserMailBoxListCell *cellModel = self.mailBoxArray[indexPath.row];
    self.mailId = cellModel.ID;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)recieveTheMailDetailModel:(KWMailDetailResultModel *)mailDetailModel AndAttachmentArray:(NSArray<KWMailDetailFileModel *> *)attachmentArray AndType:(MailSendType)type AndMailBoxId:(NSString *)mailBoxId AndHtmlInfo:(NSMutableDictionary *)htmlInfo {
    
    self.type = type;
    self.attachmentArray = attachmentArray;
    self.mailDetailResultModel = mailDetailModel;
    self.htmLInfoDic = htmlInfo;
    self.mailBoxId = mailBoxId;
    
}

- (void)setMailSendViewText:(NSString *)fromEmail AndReplyTo:(NSString *)replyTo AndSubject:(NSString *)subject {
    
    NSLog(@"%@,%@,%@",fromEmail,replyTo,subject);
    self.subject = subject;
    self.sendName = replyTo;
    self.replyTo = fromEmail;
    
}

- (void)receTheSendMail:(NSString *)sendMail andMailId:(NSString *)mailId {
    [self.userSendMailButton setTitle:sendMail forState:UIControlStateNormal];
    self.mailId = mailId;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

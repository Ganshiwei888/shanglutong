//
//  KWUserMailBoxListCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWUserMailBoxListCell : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

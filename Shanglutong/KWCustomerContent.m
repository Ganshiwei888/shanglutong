//
//  KWCustomerContent.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerContent.h"
#import "KWCustomer.h"
#import "KWCustomerFrame.h"
#import "Utility.h"

@interface KWCustomerContent ()

@property (nonatomic, weak) UILabel *noLabel;
@property (nonatomic, weak) UILabel *noValueLabel;
@property (nonatomic, weak) UILabel *saleLabel;
@property (nonatomic, weak) UILabel *saleValueLabel;
@property (nonatomic, weak) UILabel *customerLabel;
@property (nonatomic, weak) UIImageView *dividingImageView;
@property (nonatomic, weak) UIImageView *typeImageView;
@property (nonatomic, weak) UILabel *typeLabel;
@property (nonatomic, weak) UILabel *typeValueLabel;
@property (nonatomic, weak) UIImageView *timeImageView;
@property (nonatomic, weak) UILabel *timeLabel;
@property (nonatomic, weak) UILabel *timeValueLabel;
@property (nonatomic, weak) UIImageView *warnImageView;
@property (nonatomic, weak) UILabel *warnLabel;
@property (nonatomic, weak) UILabel *warnValueLabel;

@end


@implementation KWCustomerContent

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = RGBCOLOR(252, 252, 252);
        self.layer.cornerRadius = 3.0;
        self.layer.masksToBounds = YES;
        
        UILabel *noLabel = [[UILabel alloc] init];
        noLabel.font = [UIFont systemFontOfSize:10];
        noLabel.textColor = RGBCOLOR(192, 192, 192);
        noLabel.backgroundColor = [UIColor clearColor];
        noLabel.text = @"编号:";
        [self addSubview:noLabel];
        self.noLabel = noLabel;
        
        UILabel *noValueLabel = [[UILabel alloc] init];
        noValueLabel.font = [UIFont systemFontOfSize:10];
        noValueLabel.textColor = RGBCOLOR(192, 192, 192);
        noValueLabel.backgroundColor = [UIColor clearColor];
        [noValueLabel sizeToFit];
        [self addSubview:noValueLabel];
        self.noValueLabel = noValueLabel;
        
        UILabel *saleLabel = [[UILabel alloc] init];
        saleLabel.font = [UIFont systemFontOfSize:10];
        saleLabel.textColor = RGBCOLOR(192, 192, 192);
        saleLabel.backgroundColor = [UIColor clearColor];
        saleLabel.text = @"业务员:";
        [saleLabel sizeToFit];
        [self addSubview:saleLabel];
        self.saleLabel = saleLabel;
        
        UILabel *saleValueLabel = [[UILabel alloc] init];
        saleValueLabel.font = [UIFont systemFontOfSize:11];
        saleValueLabel.textColor = RGBCOLOR(192, 192, 192);
        saleValueLabel.backgroundColor = [UIColor clearColor];
        [saleValueLabel sizeToFit];
        [self addSubview:saleValueLabel];
        self.saleValueLabel = saleValueLabel;
        
        UILabel *customerLabel = [[UILabel alloc] init];
        customerLabel.font = [UIFont systemFontOfSize:20];
        customerLabel.textColor = RGBCOLOR(22, 57, 138);
        customerLabel.textAlignment = NSTextAlignmentLeft;
        customerLabel.backgroundColor = [UIColor clearColor];
        [customerLabel sizeToFit];
        [self addSubview:customerLabel];
        self.customerLabel = customerLabel;
        
        UIImageView *dividingImageView = [[UIImageView alloc] init];
        dividingImageView.backgroundColor = RGBCOLOR(233, 233, 233);
        [self addSubview:dividingImageView];
        self.dividingImageView = dividingImageView;
        
        UIImageView *typeImageView = [[UIImageView alloc] init];
        typeImageView.image = [UIImage imageNamed:@"type"];
        [self addSubview:typeImageView];
        self.typeImageView = typeImageView;
        
        UILabel *typeLabel = [[UILabel alloc] init];
        typeLabel.font = [UIFont systemFontOfSize:14];
        typeLabel.textColor = [UIColor blackColor];
        typeLabel.backgroundColor = [UIColor clearColor];
        typeLabel.text = @"类型:";
        [typeLabel sizeToFit];
        [self addSubview:typeLabel];
        self.typeLabel = typeLabel;
        
        UILabel *typeValueLabel = [[UILabel alloc] init];
        typeValueLabel.font = [UIFont systemFontOfSize:14];
        typeValueLabel.textColor = [UIColor blackColor];
        typeValueLabel.backgroundColor = [UIColor clearColor];
        [typeValueLabel sizeToFit];
        [self addSubview:typeValueLabel];
        self.typeValueLabel = typeValueLabel;
        
        UIImageView *timeImageView = [[UIImageView alloc] init];
        timeImageView.image = [UIImage imageNamed:@"time"];
        [self addSubview:timeImageView];
        self.timeImageView = timeImageView;
        
        UILabel *timeLabel = [[UILabel alloc] init];
        timeLabel.font = [UIFont systemFontOfSize:14];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.text = @"上次联系时间:";
        [timeLabel sizeToFit];
        [self addSubview: timeLabel];
        self.timeLabel = timeLabel;
        
        UILabel *timeValueLabel = [[UILabel alloc] init];
        timeValueLabel.font = [UIFont systemFontOfSize:14];
        timeValueLabel.textColor = [UIColor blackColor];
        timeValueLabel.backgroundColor = [UIColor clearColor];
        [timeValueLabel sizeToFit];
        [self addSubview:timeValueLabel];
        self.timeValueLabel = timeValueLabel;
        
        UIImageView *warnImageView = [[UIImageView alloc] init];
        warnImageView.image = [UIImage imageNamed:@"warn"];
        [self addSubview:warnImageView];
        self.warnImageView = warnImageView;
        
        UILabel *warnLabel = [[UILabel alloc] init];
        warnLabel.font = [UIFont systemFontOfSize:14];
        warnLabel.textColor = [UIColor blackColor];
        warnLabel.backgroundColor = [UIColor clearColor];
        warnLabel.text = @"销售预警:";
        [self addSubview:warnLabel];
        self.warnLabel = warnLabel;
        
        UILabel *warnValueLabel = [[UILabel alloc] init];
        warnValueLabel.font = [UIFont systemFontOfSize:14];
        warnValueLabel.textColor = [UIColor blackColor];
        warnValueLabel.backgroundColor = [UIColor clearColor];
        [warnValueLabel sizeToFit];
        [self addSubview:warnValueLabel];
        self.warnValueLabel = warnValueLabel;
    }
    return self;
}

- (void)setCustomerFrame:(KWCustomerFrame *)customerFrame {
    
    _customerFrame = customerFrame;
    //取出模型数据
    KWCustomer *customer = customerFrame.customer;
    
    self.noLabel.frame = customerFrame.noLabelView;
    
    self.noValueLabel.frame = customerFrame.noValueView;
    self.noValueLabel.text = customer.customerNO;
    
    self.saleLabel.frame = customerFrame.saleLabelView;
    
    self.saleValueLabel.frame = customerFrame.saleValueView;
    self.saleValueLabel.text = customer.customerSale;
    
    self.customerLabel.frame = customerFrame.customerNameView;
    self.customerLabel.text = customer.customerSName;
    
    self.dividingImageView.frame = customerFrame.dividingImageView;
    
    self.typeImageView.frame = customerFrame.typeImageView;
    
    self.typeLabel.frame = customerFrame.typeLabelView;
    
    self.typeValueLabel.frame = customerFrame.typeValueView;
    self.typeValueLabel.text = customer.customerType;
    
    self.timeImageView.frame = customerFrame.timeImageView;
    
    self.timeLabel.frame = customerFrame.timeLabelView;
    
    self.timeValueLabel.frame = customerFrame.timeValueView;
    self.timeValueLabel.text = customer.customerTime;
    
    self.warnImageView.frame = customerFrame.warnImageView;
    
    self.warnLabel.frame = customerFrame.warnLabelView;
    
    self.warnValueLabel.frame = customerFrame.warnValueView;
    self.warnValueLabel.text = customer.customerWarn;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end















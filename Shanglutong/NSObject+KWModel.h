//
//  NSObject+KWModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KWModelDelegate <NSObject>

@optional

+ (NSDictionary *)arrayContainModelClass;

@end

@interface NSObject (KWModel)

/**
 字典转模型

 @param dict 字典
 @return 模型
 */
+ (instancetype)modelWithDict:(NSDictionary *)dict;

/**
 模型中嵌套模型

 @param dict 字典
 @return 模型
 */
+ (instancetype)modelInModelWithDict:(NSDictionary *)dict;

/**
 数组中装着模型,模型的属性是一个数组，数组中是字典模型对象

 @param dict 字典
 @return 模型
 */
+ (instancetype)modelInModelAndArrayWithDict:(NSDictionary *)dict;

@end








//
//  TMHWorkBenchTopView.m
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/18.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "TMHWorkBenchTopView.h"

@implementation TMHWorkBenchTopView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.infoSrollView.backgroundColor = [UIColor clearColor];
    self.infoSrollView.delegate = self;
    self.infoSrollView.showsVerticalScrollIndicator = NO;
    self.infoSrollView.showsHorizontalScrollIndicator = NO;
//    self.timer = [NSTimer timerWithTimeInterval:4 target:self selector:@selector(autoScrollInfoView) userInfo:nil repeats:YES];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(autoScrollInfoView) userInfo:nil repeats:YES];
    
    
}

- (void)autoScrollInfoView {
    int maxPage = self.infoSrollView.contentSize.width / KWSCREEN_WIDTH;
    int page = self.infoSrollView.contentOffset.x / KWSCREEN_WIDTH;
    if(page == maxPage-1) {
        [self.infoSrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        self.pageControl.currentPage = 0;
    }else {
        CGFloat x = KWSCREEN_WIDTH * (page + 1);
        [self.infoSrollView setContentOffset:CGPointMake(x, 0) animated:YES];
        self.pageControl.currentPage = page+1;
    }
}

#pragma mark - scrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"didEndDecelerating");
    int page = scrollView.contentOffset.x / KWSCREEN_WIDTH;
    self.pageControl.currentPage = page;
    
    [self.timer setFireDate:[NSDate dateWithTimeInterval:4 sinceDate:[NSDate date]]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

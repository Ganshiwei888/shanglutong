//
//  KWMomentCommlistModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMomentCommlistModel.h"
#import "Utility.h"

@implementation KWMomentCommlistModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _Bak = checkStringNull(dict[@"Bak"]);
        _Creater = checkStringNull(dict[@"Creater"]);
        _WdId = checkStringNull(dict[@"WdId"]);
    }
    return self;
}

@end

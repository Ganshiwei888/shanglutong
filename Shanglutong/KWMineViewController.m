//
//  KWMineViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMineViewController.h"
#import "KWMineLoginViewController.h"
#import "KWMineMailSetViewController.h"
#import "KWMineRemaindViewController.h"
#import "KWMineSetViewController.h"
#import "KWLoginViewController.h"
#import "KWNavigationController.h"
#import "ZoomHeaderView.h"
#import "Utility.h"
#import "KWBaseParam.h"
#import "KWHttpTool.h"
#import <MJExtension.h>
#import <UIImageView+WebCache.h>
#import "KWSubViewController.h"
#import "MBProgressHUD+XQ.h"
@interface KWMineViewController () <UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate, UIViewControllerTransitioningDelegate> {

    NSArray *_tableViewSet;
    
}

@property (nonatomic, weak) UITableView *tableView;

@property (weak, nonatomic) ZoomHeaderView *zoomHeadView;
@property (strong,nonatomic) NSString *pickType;


@end

@implementation KWMineViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.delegate = self;
    [self addObserver];
    [self createTableView];
    //添加退出按钮
    [self addLogOut];
    
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUI:) name:@"switchAccount" object:nil];
}

- (void)reloadUI:(NSNotification *)notification {
    NSInteger status = [(NSNumber *)notification.object integerValue];
    if (status ==1) {
        self.zoomHeadView.switchToSelf.hidden = NO;
        self.zoomHeadView.subImageView.hidden = NO;
    } else {
        NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
        [userD setBool:NO forKey:ISSUBACCOUNT];
        [userD synchronize];
        self.zoomHeadView.switchToSelf.hidden = YES;
        self.zoomHeadView.subImageView.hidden = YES;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 创建tableView
 */
- (void)createTableView {
    
    _tableViewSet = [NSArray arrayWithObjects:@"账号与安全",@"查看下属",@"清除缓存",@"提醒设置",@"使用帮助",@"设置", nil];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT)];
    tableView.delegate = self;
    tableView.dataSource = self;
//    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    //添加headView
    ZoomHeaderView *zoomHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"ZoomHeaderView" owner:nil options:nil] lastObject];
    tableView.tableHeaderView = zoomHeaderView;
    [zoomHeaderView.switchToSelf addTarget:self action:@selector(switchToSelf) forControlEvents:UIControlEventTouchUpInside];
    self.zoomHeadView = zoomHeaderView;
}

- (void)switchToSelf {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:ISSUBACCOUNT]) {
        NSString *owerUserId = [userDefaults objectForKey:OWERUSERID];
        NSString *owerPassword = [userDefaults objectForKey:OWERPASSWORD];
        NSString *owerName = [userDefaults objectForKey:OWERNAME];
        NSString *owerDePart = [userDefaults objectForKey:OWERDEPART];
        [userDefaults setObject:owerUserId forKey:USERID];
        [userDefaults setObject:owerPassword forKey:PASSWORD];
        [userDefaults setObject:owerName forKey:NAME];
        [userDefaults setObject:owerDePart forKey:DEPART];
        [userDefaults synchronize];
        self.zoomHeadView.switchToSelf.hidden = YES;
        self.zoomHeadView.subImageView.hidden = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"switchAccount" object:@0];
    };
}
    
     
/*
 换头像
 */
-(void)ChangeIcon
{
    UIActionSheet *action  =[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册选取",@"拍照", nil];
    [action showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        DMLog(@"从相册获取");
        self.pickType=@"2";
        UIImagePickerController *img=[[UIImagePickerController alloc]init];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            img.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
            img.allowsEditing=YES;
            img.delegate=self;
            [self presentViewController:img animated:YES completion:nil];
        }
    }
    else if(buttonIndex==1)
    {
        DMLog(@"拍照");
        self.pickType=@"1";
        UIImagePickerControllerSourceType sourceType=UIImagePickerControllerSourceTypeCamera;
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"相机不可用" message:nil delegate:nil cancelButtonTitle:@"取消" otherButtonTitles: nil];
            [alert show];
            return;
        }
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.delegate=self;
        picker.allowsEditing=YES;
        picker.sourceType=sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        DMLog(@"取消");
    }
    
}

- (void)addLogOut {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 80)];
    [_tableView setTableFooterView:view];
//    view.backgroundColor = [UIColor redColor];
    UIButton *logOutButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 10, KWSCREEN_WIDTH - 50 * 2, 40)];
    [logOutButton setBackgroundColor:[UIColor redColor]];
    [logOutButton setTitle:@"退出登录" forState:UIControlStateNormal];
    logOutButton.layer.cornerRadius = 5;
    [logOutButton addTarget:self action:@selector(logOutButton) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview: logOutButton];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([self.pickType isEqualToString:@"1"]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
        UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
        [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
    }
    else if ([self.pickType isEqualToString:@"2"])
    {
        [picker dismissViewControllerAnimated:YES completion:nil];
        UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
        [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
    }
    
}

- (void)saveImage:(UIImage *)image {
    
    [self.zoomHeadView.userImageButton setImage:image forState:UIControlStateNormal];
    
    NSData *dateImage = UIImageJPEGRepresentation(image, 0.3);
    NSString *strup = [dateImage base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    //设置常用参数
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:@"jpg" forKey:@"Pictype"];
    [requestInfo setValue:strup forKey:@"picdata"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:ISSUBACCOUNT]) {
        NSString *owerUserId = [userDefaults objectForKey:OWERUSERID];
        NSString *owerPassword = [userDefaults objectForKey:OWERPASSWORD];
        [requestInfo setValue:owerUserId forKey:@"UserId"];
        [requestInfo setValue:owerPassword forKey:@"Password"];
    };
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/user/setpic"];
    
//    __weak KWMineViewController *weakSelf = self;
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        NSLog(@"%@",json);
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
    
}

- (void)logOutButton {

    NSLog(@"Click logOut");
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"USER_PASSWORD"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"CLIENTID"];
    //退出登陆不请空ip地址
    //    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userServerAddress"];
    KWLoginViewController *login = [[KWLoginViewController alloc] init];
    login.transitioningDelegate = self;
    login.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    CGFloat sectionHeaderHeight = 80;//设置footer高度
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
    CGFloat offsetY = scrollView.contentOffset.y;
    [self.zoomHeadView updateHeaderImageViewFrameWithOffsetY:offsetY];
}

- (void)ChangeIcon:(UIButton *)sender {
    
}

#pragma mark - UINavigationControllerDelegate
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
    
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
}

#pragma mark - UITableViewDataSource && UITableViewDelegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _tableViewSet.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 53.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    if (indexPath.section == 2) {
//        cell.detailTextLabel.text = @"20 MB";
    }
    cell.textLabel.text = _tableViewSet[indexPath.section];
    cell.textLabel.textColor = RGBCOLOR(139, 139, 139);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 10)];
    view.backgroundColor = RGBACOLOR(240, 240, 240, 1);
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        self.tabBarController.tabBar.hidden = YES;
        KWMineLoginViewController *mineLogin = [[KWMineLoginViewController alloc] init];
        [self.navigationController pushViewController:mineLogin animated:YES];
        self.tabBarController.tabBar.hidden = NO;
    }
//    else if (indexPath.section == 1) {
//        self.hidesBottomBarWhenPushed = YES;
//        KWMineMailSetViewController *mailSet = [[KWMineMailSetViewController alloc]init];
//        [self.navigationController pushViewController:mailSet animated:YES];
//        self.hidesBottomBarWhenPushed = NO;
//    }
    else if (indexPath.section == 1) {
        NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
        BOOL isSub = [userD boolForKey:ISSUBACCOUNT];
        if (isSub) {
            [MBProgressHUD showResponseMessage:@"不能查看下属的下属"];
        } else {
            self.hidesBottomBarWhenPushed = YES;
            KWSubViewController *mailSet = [[KWSubViewController alloc]init];
            [self.navigationController pushViewController:mailSet animated:YES];
            self.hidesBottomBarWhenPushed = NO;
        }
    }
    else if (indexPath.section == 2){
        
        [self putBufferBtnClicked];
        
    } else if (indexPath.section == 3) {
        self.hidesBottomBarWhenPushed = YES;
        KWMineRemaindViewController *remainedView= [[KWMineRemaindViewController alloc] init];
        [self.navigationController pushViewController:remainedView animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    } else if (indexPath.section == 5) {
        self.hidesBottomBarWhenPushed = YES;
        KWMineSetViewController *setView = [[KWMineSetViewController alloc] init];
        [self.navigationController pushViewController: setView animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
    
}

//清除缓存按钮的点击事件
- (void)putBufferBtnClicked{
    
    CGFloat size = [self folderSizeAtPath:NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject] + [self folderSizeAtPath:NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES).lastObject] + [self folderSizeAtPath:NSTemporaryDirectory()];
    NSString *message = size > 1 ? [NSString stringWithFormat:@"缓存%.2fM, 删除缓存", size] : [NSString stringWithFormat:@"缓存%.2fK, 删除缓存", size * 1024.0];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {                [self cleanCaches];
        [_tableView reloadData];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    [alert addAction:action];
    [alert addAction:cancel];
    [self showDetailViewController:alert sender:nil];
}
// 计算目录大小
- (CGFloat)folderSizeAtPath:(NSString *)path{
    // 利用NSFileManager实现对文件的管理
    NSFileManager *manager = [NSFileManager defaultManager];
    CGFloat size = 0;
    if ([manager fileExistsAtPath:path]) {
        // 获取该目录下的文件，计算其大小
        NSArray *childrenFile = [manager subpathsAtPath:path];
        for (NSString *fileName in childrenFile) {
            NSString *absolutePath = [path stringByAppendingPathComponent:fileName];
            size += [manager attributesOfItemAtPath:absolutePath error:nil].fileSize;
        }
        // 将大小转化为M
        return size / 1024.0 / 1024.0;
    }
    return 0;
}

- (void)cleanCaches{
    [self cleanCaches:NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject];
    [self cleanCaches:NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES).lastObject];
    [self cleanCaches:NSTemporaryDirectory()];
}

// 根据路径删除文件
- (void)cleanCaches:(NSString *)path{
    // 利用NSFileManager实现对文件的管理
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        // 获取该路径下面的文件名
        NSArray *childrenFiles = [fileManager subpathsAtPath:path];
        for (NSString *fileName in childrenFiles) {
            // 拼接路径
            NSString *absolutePath = [path stringByAppendingPathComponent:fileName];
            // 将文件删除
            [fileManager removeItemAtPath:absolutePath error:nil];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

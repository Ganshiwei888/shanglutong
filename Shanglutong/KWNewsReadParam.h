//
//  KWNewsReadParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWNewsReadParam : KWBaseParam

@property (nonatomic, copy) NSString *Ydwd;

@end

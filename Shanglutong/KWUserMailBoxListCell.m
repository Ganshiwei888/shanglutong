//
//  KWUserMailBoxListCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWUserMailBoxListCell.h"
#import "Utility.h"

@implementation KWUserMailBoxListCell

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _ID = checkStringNull(dict[@"id"]);
        _name = checkStringNull(dict[@"name"]);
        _email = checkStringNull(dict[@"email"]);
    }
    return self;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"ID": @"id"};
}

@end

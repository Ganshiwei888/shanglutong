//
//  KWMailBoxResult.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailBoxResult.h"
#import "Utility.h"
#import <MJExtension.h>

@implementation KWMailBoxResult

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"back":@"KWMailBoxLabelList"};
}

@end

@implementation KWMailBoxLabelList

@end

//
//  KWMailListTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailListTool.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import <MJExtension.h>

@implementation KWMailListTool

+ (void)postGetMailListWith:(KWMailListParam *)param success:(void (^)(KWMailListResult *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Getlist"];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
       
        if (success) {
            
            KWMailListResult *listResult = [KWMailListResult mj_objectWithKeyValues:[json valueForKey:@"back"]];
            success(listResult);
        }
        
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)postFlagTheMailWith:(KWMailDetailParam *)param success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Redflag"];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
            NSString *result = [json valueForKey:@"back"];
            success(result);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}

+ (void)postStarTheMailWith:(KWMailDetailParam *)param success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Star"];
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
            NSString *result = [json valueForKey:@"back"];
            success(result);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}
    
+ (void)postTopTheMailWith:(KWMailDetailParam *)param success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure {
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Top"];
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
            NSString *result = [json valueForKey:@"back"];
            success(result);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}

+ (void)postReadTheMailWith:(KWMailDetailParam *)param success:(void (^)(NSString *))success failure:(void (^)(NSError *))failure {
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Read"];
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
            NSString *result = [json valueForKey:@"back"];
            success(result);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end







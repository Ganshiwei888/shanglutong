//
//  KWLoginViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/26.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWLoginViewController.h"
#import "MBProgressHUD+XQ.h"
#import "KWHomeViewController.h"
#import "KWCustomerViewController.h"
#import "KWAddressViewController.h"
#import "KWMineViewController.h"
#import "KWNavigationController.h"
#import "KWUserInfoManager.h"
#import "KWUserDefaultsManager.h"
#import "KWUserInfoParam.h"
#import "KWUserTool.h"
#import "NSString+MD5.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "TabBarVC.h"
#import <MJExtension/MJExtension.h>
#import <SVProgressHUD.h>

@interface KWLoginViewController () <GeTuiSdkDelegate>
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *rememberPasswordButton;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation KWLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.accountTextField.layer.borderColor = [UIColor blackColor].CGColor;
    self.passwordTextField.layer.borderColor = [UIColor blackColor].CGColor;
    // Do any additional setup after loading the view from its nib.
}

- (void)transitionToTabBarViewController {
    TabBarVC *tabVC = [[TabBarVC alloc] init];
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    window.rootViewController = tabVC;
    self.view = nil;
    [window makeKeyAndVisible];
    
//    [UIView transitionFromView:self.view
//                        toView:tabVC.view
//                      duration:0
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    completion:^(BOOL finished)
//     {
////         [self.navigationController setViewControllers:@[tabVC]];
//     }];
}

- (IBAction)loginButtonClick:(UIButton *)sender {
    if (!self.accountTextField.text || self.accountTextField.text.length == 0 || [self.accountTextField.text isEqualToString:@""]) {
        [MBProgressHUD showError:@"未填写用户账号" toView:self.view];
        return;
    }
    if (!self.passwordTextField.text || self.passwordTextField.text.length == 0 || [self.passwordTextField.text isEqualToString:@""]) {
        [MBProgressHUD showError:@"未填写用户密码" toView:self.view];
        return;
    }
    //显示等待
    self.hud = [MBProgressHUD showIndicatorWithText:@"登录中" ToView:self.view];
    
    //设置参数
    NSString *MD5UpperString = [NSString stringTo32BitMD5UpperStringWithString:self.passwordTextField.text];
    [[NSUserDefaults standardUserDefaults] setValue:self.accountTextField.text forKey:@"USER_ID"];
    
    [[NSUserDefaults standardUserDefaults] setValue:MD5UpperString forKey:@"USER_PASSWORD"];
    
    KWUserInfoParam *param = [KWUserInfoParam param];
    param.appid = CLIENTID;
    __weak KWLoginViewController *weakSelf = self;
    
    [KWUserTool userInfoWithParam:param success:^(KWUserInfoResult *resultData) {
    
        [weakSelf.hud hideAnimated:YES];
        
        NSLog(@"%@",resultData.code);
        
        NSString *code = resultData.code;
        if ([code isEqualToString:@"已注册用户"] ) {
            //保存账号密码
            [[NSUserDefaults standardUserDefaults]setValue:self.accountTextField.text forKey:@"USER_ID"];
            [[NSUserDefaults standardUserDefaults]setValue:MD5UpperString forKey:@"USER_PASSWORD"];
            [[NSUserDefaults standardUserDefaults]setValue:resultData.resultModel.name forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:resultData.resultModel.department forKey:@"USER_DEPART"];
            [self getUserInfoSuccess:self.rememberPasswordButton.isSelected UserIfId:resultData.resultModel.id];
        } else if ([code isEqualToString:@"新注册用户"]){
            
            [[NSUserDefaults standardUserDefaults]setValue:self.accountTextField.text forKey:@"USER_ID"];
            [[NSUserDefaults standardUserDefaults]setValue:MD5UpperString forKey:@"USER_PASSWORD"];
            [[NSUserDefaults standardUserDefaults]setValue:resultData.resultModel.name forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] setObject:resultData.resultModel.department forKey:@"USER_DEPART"];
            [self getUserInfoSuccess:self.rememberPasswordButton.isSelected UserIfId:resultData.resultModel.id];
            
        } else {
            [SVProgressHUD showErrorWithStatus:resultData.info];
            NSLog(@"%@",resultData.info);
            [self.hud hideAnimated:YES];
        }
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:NO forKey:ISSUBACCOUNT];
        [userDefaults synchronize];
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"账号密码错误"];
        [SVProgressHUD dismissWithDelay:1.0];
        [self.hud hideAnimated:YES];
    }];
    
}

#pragma mark - Notifications
- (void)getUserInfoSuccess:(BOOL)isRemember UserIfId:(NSString*)userIfId {
    [self.hud hideAnimated:NO];
    [self transitionToTabBarViewController];
    [KWUserDefaultsManager setUserId:userIfId];
    [KWUserInfoManager sharedKWUserInfoManager].userAccount = self.accountTextField.text;
    [KWUserDefaultsManager setUserAccount:self.accountTextField.text];
    NSString *MD5Password = [NSString stringTo32BitMD5UpperStringWithString:self.passwordTextField.text];
    [KWUserInfoManager sharedKWUserInfoManager].userMD5Password = MD5Password;
    [KWUserDefaultsManager setUserPassword:MD5Password];
//    BOOL rememberPassword = isRemember;
    if (!isRemember) {
//       [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"USER_PASSWORD"];
        NSLog(@"没有保存密码");
    }
    [KWUserInfoManager sharedKWUserInfoManager].userServerAddress = [KWUserDefaultsManager getUserServerAddress];

}


- (IBAction)settingButtonClick:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"设置" message:@"设置服务器地址\n例:116.62.232.164:9898" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"请正确填写服务器地址";
    }];
    UIAlertAction *actionYes = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField * textField = alert.textFields.firstObject;
        if (textField.text && textField.text.length > 0 && ![textField.text isEqualToString:@""]) {
            [KWUserDefaultsManager setUserServerAddress:textField.text];
            [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:@"userServerAddress"];
        }
    }];
    UIAlertAction *actionNO = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:actionNO];
    [alert addAction:actionYes];
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)rememberButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    
}

#pragma mark - keyBoard
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 0) {
        [textField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    } else {
        [self.view endEditing:YES];
        [self loginButtonClick:nil];
    }
    return YES;
}


@end

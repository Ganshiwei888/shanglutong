//
//  KWTabBar.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWTabBar;
@protocol KWTabBarDelegate <NSObject>

- (void)tabBar:(KWTabBar *)tabBar didSelectedButtonFrom:(long int)from to:(long int)to;

@end

@interface KWTabBar : UIView

/**
 添加自定义按钮
 
 @param item UITabBarItem
 */
- (void)addTabBarWithItem:(UITabBarItem *)item;

@property (nonatomic, weak) UIButton *plusButton;
@property (nonatomic, weak) id<KWTabBarDelegate> delegate;

@end

////
////  TMHMineViewSubordinateViewController.m
////  Shanglutong
////
////  Created by Ming Tian on 2017/10/21.
////  Copyright © 2017年 KeWen. All rights reserved.
////
//
//#import "TMHMineViewSubordinateViewController.h"
//#import "KWAddress.h"
//#import "KWAddressFrame.h"
//#import "KWAddressBackView.h"
//#import "KWAddressCell.h"
//#import "NSString+MD5.h"
//#import "Utility.h"
//#import "UIBarButtonItem+KW.h"
//#import "KWUserAllMenu.h"
//#import "KWAddressNumViewController.h"
//#import <AFNetworking.h>
//#import "MBProgressHUD+XQ.h"
//#import "KWAddressNumTool.h"
//#import "KWAddressNumCell.h"
//#import "KWBaseParam.h"
//#import "KWMailSendViewController.h"
//#import <MJRefresh/MJRefresh.h>
//#import <UIImageView+WebCache.h>
//#import "ChineseString.h"
//#import "LWActiveIncator.h"
//#import "pinyin.h"
//#import "KWTalkViewController.h"
//#import "KWTalkMessageViewController.h"
//
//@interface TMHMineViewSubordinateViewController ()<UITableViewDataSource, UITableViewDelegate>{
//
//NSMutableArray *_indexArray; //索引数组
//}
//
//@property (nonatomic, strong) NSMutableArray *addressFrame;
//@property (nonatomic, strong) NSArray<KWAddress*> *addressDetail;
//@property (nonatomic, assign) NSInteger pageIndex; //设置当前的pageIndex
//@property (nonatomic, weak) UITableView *tableView;
//@property (nonatomic, weak) UITableView *rightTableView;
////@property (nonatomic, strong) NSArray *userAddress;
//@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
//@property (nonatomic, retain) NSMutableArray *dataArr;
//@property (nonatomic, retain) NSMutableArray *sortedArrForArrays;
//
//@end
//
//@implementation TMHMineViewSubordinateViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//}
//
//
//- (void)setupAddressData {
//    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    
//    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
//    [requestInfo setValue:USER_ID forKey:@"UserId"];
//    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
//    [requestInfo setValue:@"123" forKey:@"Ran"];
//    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
//    [requestInfo setValue:@"1" forKey:@"pageindex"];
//    [requestInfo setValue:@"50" forKey:@"pagemax"];
//    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/Get",USER_SERVERADDRESS];
//    
//    __weak TMHMineViewSubordinateViewController *weakSelf = self;
//    
//    [manager POST:netPath parameters:requestInfo progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        //取出字典模型
//        NSDictionary *dictArray = responseObject[@"back"];
//        //取出list数组
//        NSArray *arrayRes = dictArray[@"list"];
//        //创建address数组
//        NSMutableArray *addressMutableArray = [NSMutableArray array];
//        for (NSDictionary *dict in arrayRes) {
//            KWAddress *address = [KWAddress addressWithDict:dict];
//            [addressMutableArray addObject:address];
//        }
//        self.addressDetail = addressMutableArray;
//        NSArray *addressData = addressMutableArray;
//        
//        // 创建frame模型对象
//        NSMutableArray *addressFrameArray = [NSMutableArray array];
//        for (KWAddress *address in addressData) {
//            KWAddressFrame *addressFrame = [[KWAddressFrame alloc] init];
//            // 传递微博模型数据
//            addressFrame.address = address;
//            [addressFrameArray addObject:addressFrame];
//        }
//        
//        // 赋值
//        self.addressFrame = addressFrameArray;
//        
//        [self.tableView reloadData];
//        [weakSelf endRefrshing];
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"fail");
//    }];
//    
//}
//
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//@end


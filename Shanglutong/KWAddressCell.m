//
//  KWAddressCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressCell.h"
#import "KWAddress.h"
#import "KWAddressBackView.h"
#import "KWAddressFrame.h"
#import "KWAddressBackView.h"
#import "Utility.h"

@interface KWAddressCell ()

/**
 cell backgroundView
 */
@property (nonatomic, weak) KWAddressBackView *addressBackView;

@end

@implementation KWAddressCell

/**
 初始化
 */
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    
    static NSString *cellID = @"cell";
    KWAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[KWAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = RGBCOLOR(225, 225, 225);
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setupBackView];
    }
    return self;
}

- (void)setupBackView {
    
   // self.selectedBackgroundView = [[UIView alloc] init];
    
    KWAddressBackView *addressBackView = [[KWAddressBackView alloc] initWithFrame:CGRectMake(8, 5, KWSCREEN_WIDTH - 16, 170)];
    [self.contentView addSubview:addressBackView];
    
    self.addressBackView = addressBackView;

}

//- (void)setFrame:(CGRect)frame {
//    frame.origin.x = KWAddressTableBorder;
//    frame.origin.y += KWAddressTableBorder;
//    frame.size.width -= 2 * KWAddressTableBorder;
//    frame.size.height -= KWAddressTableBorder;
//    [super setFrame:frame];
//}

#pragma mark - 设置数据模型 -

- (void)setAddressFrame:(KWAddressFrame *)addressFrame {
    _addressFrame = addressFrame;
    
    self.addressBackView.addressFrame = self.addressFrame;

}

//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end

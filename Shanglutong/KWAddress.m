//
//  KWAddress.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddress.h"
#import "Utility.h"
@implementation KWAddress

+ (instancetype)addressWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

- (instancetype)initWithDict:(NSDictionary *)dict {

    if (self = [super init]) {

        _Name = checkStringNull(dict[@"Name"]);
        
        _Email = checkStringNull(dict[@"Email"]);
        
        _Email1 = checkStringNull(dict[@"Email1"]);
        
        _Email2 = checkStringNull(dict[@"Email2"]);
        
        _QQ = checkStringNull(dict[@"QQ"]);
        
        _Twitter = checkStringNull(dict[@"Twitter"]);
        
        _Mobile = checkStringNull(dict[@"Mobile"]);
        
        _Tel = checkStringNull(dict[@"Tel"]);

        _Dept = checkStringNull(dict[@"Dept"]);
        
        _Job = checkStringNull(dict[@"Job"]);
        
        _MainAdd = checkStringNull(dict[@"MainAdd"]);
        
        _Facebook = checkStringNull(dict[@"Facebook"]);
        
        _WeChat = checkStringNull(dict[@"WeChat"]);
        
        _Sex = checkStringNull(dict[@"Sex"]);
        
        _Birthday = checkStringNull(dict[@"Birthday"]);
        
    }
    
    return self;
}

@end

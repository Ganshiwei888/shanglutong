//
//  KWTalkMessageViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/5.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWTalkMessageViewController.h"
#import "TMHTalkTool.h"

@interface KWTalkMessageViewController ()<EaseMessageViewControllerDelegate, EaseMessageViewControllerDataSource>

@end

@implementation KWTalkMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    // Do any additional setup after loading the view.
}

- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController
                           modelForMessage:(EMMessage *)message {
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    
    id<IMessageModel> model = nil;
    
    model = [[EaseMessageModel alloc] initWithMessage:message];
    model.avatarURLPath = [defs objectForKey:@"userImage"];
    
    if (model.isSender) {
        model.avatarURLPath = [defs objectForKey:@"userImage"];
        model.nickname = [defs objectForKey:@"userName"];
    } else {
        KWUserAllMenu *menu = [TMHTalkTool getUserMenuWithHxid:message.conversationId];
//        model.avatarURLPath = message.ext [@"userImage"];
//        model.nickname = message.ext[@"USER_ID"];
        model.avatarURLPath = [NSString stringWithFormat:@"http://116.62.232.164:9898/picimage/%@", menu.PicName];
        model.nickname = menu.username;
        
    }
    return model;
}

- (void)userAccountDidLoginFromOtherDevice {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

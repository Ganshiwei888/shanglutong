//
//  KWComDetailFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWComDetailFrame.h"
#import "KWComDetailModel.h"

//
//@property (assign, readonly, nonatomic) CGRect detailF;
//
//@property (assign, readonly, nonatomic) CGRect userImageF;
//
//@property (assign, readonly, nonatomic) CGRect nameF;
//
//@property (assign, readonly, nonatomic) CGRect timeF;


@implementation KWComDetailFrame

- (void)setComDetailModel:(KWComDetailModel *)comDetailModel {
    _comDetailModel = comDetailModel;
    
    CGFloat detailX = 26;
    CGFloat detailY = 4;
    CGFloat detailW = KWSCREEN_WIDTH - 32;
    CGFloat detailH = 56;
    _detailF = CGRectMake(detailX, detailY, detailW, detailH);
    
    CGFloat userImageX = 20;
    CGFloat userImageY = CGRectGetMaxY(_detailF) + 2;
    CGFloat userImageW = 18;
    CGFloat userImageH = 18;
    _userImageF = CGRectMake(userImageX, userImageY, userImageW, userImageH);
    
    CGFloat nameX = CGRectGetMaxX(_userImageF) + 8;
    CGFloat nameY = userImageY;
    CGFloat nameW = 45;
    CGFloat nameH = 20;
    _nameF = CGRectMake(nameX, nameY, nameW, nameH);
    
    CGFloat timeX = CGRectGetMaxX(_nameF) + 20;
    CGFloat timeY = nameY;
    CGFloat timeW = 170;
    CGFloat timeH = 20;
    _timeF = CGRectMake(timeX, timeY, timeW, timeH);
    
}

@end

















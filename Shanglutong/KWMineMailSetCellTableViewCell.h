//
//  KWMineMailSetCellTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMineMailSetCellTableViewCell : UITableViewCell

//邮箱名称
@property (weak, nonatomic) IBOutlet UILabel *mialName;
//邮箱账号
@property (weak, nonatomic) IBOutlet UILabel *mailAdress;
//缓存数量
@property (weak, nonatomic) IBOutlet UITextField *mailNum;
//switch Btn
@property (weak, nonatomic) IBOutlet UISwitch *switchStart;

@end

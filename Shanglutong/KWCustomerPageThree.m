//
//  KWCustomerPageThree.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerPageThree.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import "KWMailListResult.h"
#import "KWMailListTableViewCell.h"
#import "KWMailListFrame.h"
#import <MJRefresh.h>
#import <MJExtension.h>


@interface KWCustomerPageThree ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *mailListArray;

@property (nonatomic, strong) NSArray *mailArray;

@end

@implementation KWCustomerPageThree

- (void)viewDidLoad {
    [super viewDidLoad];
    //    创建tableView
    [self createTableView];
    //    //请求参数
    [self createNetwork];
    // Do any additional setup after loading the view.
}

#pragma mark - initView
- (void)createTableView {
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 190)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.estimatedRowHeight = 100;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerViewRefresh)];
    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerViewRefresh)];
    [_tableView registerNib:[UINib nibWithNibName:@"SLTMailNewTableViewCell" bundle:nil] forCellReuseIdentifier:@"MailNewCenterViewCell"];
}

#pragma mark - MJRefresh

- (void)endRefreshing {
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
}

- (void)headerViewRefresh {
    [self createNetwork];
    [self endRefreshing];
}

- (void)footerViewRefresh {
    [self createNetwork];
    [self endRefreshing];
}

- (void)endRefrshing {
    
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
}


#pragma mark netWork
- (void)createNetwork {
    //设置常用参数
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:@"1" forKey:@"pageindex"];
    [requestInfo setValue:@"50" forKey:@"pagemax"];
    [requestInfo setValue:@"CLIENT" forKey:@"act"];
    [requestInfo setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomIDNow"] forKey:@"clientid"];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/mail/getlist"]; 
    NSLog(@"%@",netPath);
    
    __weak KWCustomerPageThree *weakSelf = self;
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        
        NSLog(@"%@",json);
        KWMailListResult *listResult = [KWMailListResult mj_objectWithKeyValues:[json valueForKey:@"back"]];
        weakSelf.mailListArray = listResult.list;
        
        NSMutableArray *modelArray = [NSMutableArray array];
        for (int i = 0; i < listResult.list.count; i++) {
            KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
            listFrame.isSelect = @"no";
            listFrame.mailModelList = listResult.list[i];
            [modelArray addObject:listFrame];
        }
        
        weakSelf.mailArray = modelArray;
        [weakSelf.tableView reloadData];
        
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);

    }];
    
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mailArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"mailcell";
    
 
    KWMailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[KWMailListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    cell.mailListFrame = listFrame;
    
    if ([listFrame.mailModelList.Read isEqualToString:@"否"]) {
        cell.readView.hidden = NO;
    } else {
        cell.readView.hidden = YES;
    }
    
    if ([listFrame.isEdit isEqualToString:@"0"]) {
        cell.selectButton.hidden = YES;
    }
    
    if ([listFrame.isEdit isEqualToString:@"1"]) {
        cell.selectButton.hidden = NO;
    }
    
    if ([listFrame.isSelect  isEqualToString: @"yes"]) {
        cell.selectButton.selected = YES;
    } else if ([listFrame.isSelect isEqualToString:@"no"]) {
        cell.selectButton.selected = NO;
    }
    cell.selectButton.tag = indexPath.row + 1000;
    
    if (listFrame.mailModelList.TopTime) {
        cell.topView.hidden = NO;
    } else {
        cell.topView.hidden = YES;
    }
    
    if (listFrame.mailModelList.star) {
        cell.starView.hidden = NO;
    } else {
        cell.starView.hidden = YES;
    }
    
    if (listFrame.mailModelList.redflag) {
        cell.flagView.hidden = NO;
    } else {
        cell.flagView.hidden = YES;
    }
    
    if ([listFrame.mailModelList.AccCount isEqualToString:@"0"]) {
        cell.attachmentView.hidden = YES;
    } else {
        cell.attachmentView.hidden = NO;
    }
    
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

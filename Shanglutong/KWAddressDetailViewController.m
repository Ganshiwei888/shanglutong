//
//  KWAddressDetailViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressDetailViewController.h"
#import "KWMailSendViewController.h"
#import "Utility.h"


@interface KWAddressDetailViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *titleArray;
@end

@implementation KWAddressDetailViewController

- (NSArray *)titleArray {
    if (!_titleArray){
        _titleArray = [NSArray arrayWithObjects:@"手机:",@"电话:",@"电子邮件:",@"电子邮件2:",@"电子邮件3:",@"微信:",@"QQ:",@"FaceBook:",@"Twitter:",@"性别:",@"年龄:",@"生日:",@"职位:",@"部门:",@"备注:", nil];
        
    }
    return _titleArray;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"详情";
}

- (void)loadTableView {
    [self tableView];
    [self footView];
}

- (void)footView {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 150)];
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(53, 15, 100, 14)];
    lable.textColor = [UIColor grayColor];
    lable.text = @"备注";
    lable.font = [UIFont systemFontOfSize:14];
    [view addSubview:lable];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 0, KWSCREEN_WIDTH-20, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    line.alpha = 0.3;
    [view addSubview:line];
    
    UILabel *textLable = [[UILabel alloc]initWithFrame:CGRectMake(53, CGRectGetMaxY(lable.frame)+10, KWSCREEN_WIDTH-106, 75)];
    textLable.layer.borderWidth = 1;
    textLable.layer.borderColor = [UIColor grayColor].CGColor;
    textLable.numberOfLines = 0;
    textLable.text = self.detailArray[14];
    textLable.textAlignment = NSTextAlignmentCenter;
    textLable.font = [UIFont systemFontOfSize:12];
    [view addSubview:textLable];
    
    self.tableView.tableFooterView = view;
}

#pragma mark - UITableViewDataSource && UITabledelegate -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 14;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identfire =@"ContactPeopleDetail";
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identfire];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identfire];
    }
    
    cell.textLabel.text = self.titleArray[indexPath.row];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    NSString *detail = [NSString stringWithFormat:@"%@",self.detailArray[indexPath.row]];
    cell.detailTextLabel.textColor = [UIColor blackColor];
    if (detail){
        cell.detailTextLabel.text = detail;
    }else{
        cell.detailTextLabel.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        KWMailSendViewController *mailSendVC = [[KWMailSendViewController alloc] init];
        [mailSendVC recieveTheMailDetailModel:nil AndAttachmentArray:nil AndType:MailSendTypeNew AndMailBoxId:@"11" AndHtmlInfo:nil];
        mailSendVC.replyTo = self.detailArray[2];
        UINavigationController *mailSendNavi = [[UINavigationController alloc] initWithRootViewController:mailSendVC];
        
        [self presentViewController:mailSendNavi animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

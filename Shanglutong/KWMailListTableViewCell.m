//
//  KWMailListTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailListTableViewCell.h"
#import "KWMailList.h"
#import "KWMailListFrame.h"
#import "NSString+KWCreateTime.h"
#import "Utility.h"
#import "UILabel+KWLabel.h"

@implementation MailIndicatorView

-(void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(ctx, rect);
    CGContextSetFillColor(ctx, CGColorGetComponents(_indicatorColor.CGColor));
    CGContextFillPath(ctx);
    
    if (_innerColor) {
        CGFloat innerSize = rect.size.width * 0.5;
        CGRect innerRect = CGRectMake(rect.origin.x + rect.size.width * 0.5 - innerSize * 0.5,
                                      rect.origin.y + rect.size.height * 0.5 - innerSize * 0.5,
                                      innerSize, innerSize);
        CGContextAddEllipseInRect(ctx, innerRect);
        CGContextSetFillColor(ctx, CGColorGetComponents(_innerColor.CGColor));
        CGContextFillPath(ctx);
    }
}

-(void) setIndicatorColor:(UIColor *)indicatorColor
{
    _indicatorColor = indicatorColor;
    [self setNeedsDisplay];
}

-(void) setInnerColor:(UIColor *)innerColor
{
    _innerColor = innerColor;
    [self setNeedsDisplay];
}

@end


@implementation KWMailListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupContentView];
    }
    return self;
}

- (void)setupContentView {
    
    UILabel *fromNameLabel = [[UILabel alloc] init];
    fromNameLabel.textColor = RGBCOLOR(8, 46, 85);
    [fromNameLabel setFont:[UIFont systemFontOfSize:17.0]];
    [self.contentView addSubview:fromNameLabel];
    self.fromNameLabel = fromNameLabel;
    
    UILabel *subjectLabel = [[UILabel alloc] init];
    subjectLabel.textColor = [UIColor darkGrayColor];
    subjectLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    [self.contentView addSubview:subjectLabel];
    self.subjectLabel = subjectLabel;
    
    UILabel *recDateLabel = [[UILabel alloc] init];
    recDateLabel.textColor = RGBCOLOR(41, 36, 33);
    recDateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.0f];
    [self.contentView addSubview:recDateLabel];
    self.recDateLabel = recDateLabel;
    
    UILabel *mailDateilLabel = [[UILabel alloc] init];
    mailDateilLabel.textColor = RGBCOLOR(128, 138, 135);
    mailDateilLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f];
    mailDateilLabel.numberOfLines = 0;
    [self.contentView addSubview:mailDateilLabel];
    self.mailDateilLabel = mailDateilLabel;
    
    UIImageView *topView = [[UIImageView alloc] init];
    topView.image = [UIImage imageNamed:@"置顶邮件"];
    [topView setHidden:YES];
    [self.contentView addSubview: topView];
    self.topView = topView;
    
    UIImageView *starView = [[UIImageView alloc] init];
    starView.image = [UIImage imageNamed:@"星标邮件"];
    [starView setHidden:YES];
    [self.contentView addSubview:starView];
    self.starView = starView;
    
    UIImageView *flagView = [[UIImageView alloc] init];
    flagView.image = [UIImage imageNamed:@"红旗邮件"];
    [flagView setHidden:YES];
    [self.contentView addSubview:flagView];
    self.flagView = flagView;
    
    UIImageView *attachmentView = [[UIImageView alloc] init];
    attachmentView.image = [UIImage imageNamed:@"附件"];
    [attachmentView setHidden:YES];
    [self.contentView addSubview:attachmentView];
    self.attachmentView = attachmentView;
    
    UIButton *selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectButton setImage:[UIImage imageNamed:@"选中灰色"] forState:UIControlStateNormal];
    [selectButton setImage:[UIImage imageNamed:@"选中蓝色"] forState:UIControlStateSelected];
    [selectButton setHidden:NO];
    [self.contentView addSubview:selectButton];
    self.selectButton = selectButton;
    
    UIImageView *readView = [[UIImageView alloc] init];
    readView.image = [UIImage imageNamed:@"未读"];
    [readView setHidden:YES];
    [self.contentView addSubview:readView];
    self.readView = readView;
    
    CGFloat leftPadding = 25.0;
//    CGFloat topPadding = 3.0;
//    CGFloat textWidth = self.contentView.bounds.size.width - leftPadding * 2;
//    CGFloat dateWidth = 40;
    
    _indicatorView.center = CGPointMake(leftPadding * 0.5, fromNameLabel.frame.origin.y + fromNameLabel.frame.size.height * 0.5);
}

- (void)setMailListFrame:(KWMailListFrame *)mailListFrame {
    _mailListFrame = mailListFrame;
    KWMailListModel *mailList = mailListFrame.mailModelList;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:17],};

    NSString *fromStr = mailList.FromName;
    CGSize textSize = [fromStr boundingRectWithSize:CGSizeMake(260, 21) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;;

    
    self.fromNameLabel.text = fromStr;
    self.fromNameLabel.frame = CGRectMake(36, 8, textSize.width, textSize.height);
    
    if (mailList.clientid.length > 0) {
        NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:fromStr attributes:attribtDic];
        self.fromNameLabel.attributedText = attribtStr;
    }
    
    self.subjectLabel.text = [NSString stringWithFormat:@"%@", mailList.Subject];
    self.subjectLabel.frame = mailListFrame.subjectFrame;
    
    NSString *timeText = [NSString createDateStringToCreateTimeFromNow:mailList.MailDate];
    self.recDateLabel.text = timeText;
    self.recDateLabel.frame = mailListFrame.recDateFrame;
    
    self.mailDateilLabel.text = mailList.textbody;
    self.mailDateilLabel.frame = mailListFrame.mailDateilFrame;
    
    self.topView.frame = CGRectMake(CGRectGetMaxX(self.fromNameLabel.frame) + 4, 11, 14, 14);
    self.starView.frame = CGRectMake(CGRectGetMaxX(self.topView.frame) + 4, 11, 14, 14);
    self.flagView.frame = CGRectMake(CGRectGetMaxX(self.starView.frame) + 4, 11, 14, 14);
    self.attachmentView.frame = CGRectMake(CGRectGetMaxX(self.flagView.frame) + 4, 11, 14, 14);
    
    self.selectButton.frame = mailListFrame.selectViewFrame;
    
    self.readView.frame = mailListFrame.readViewFrame;

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

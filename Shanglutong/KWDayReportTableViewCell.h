//
//  KWDayReportTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWDayReportModel;
@interface KWDayReportTableViewCell : UITableViewCell

@property (nonatomic, strong) KWDayReportModel *dayModel;

@end

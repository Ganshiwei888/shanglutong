//
//  KWCustomerViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/26.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#define maxPageNum @"80"   //一页最大条数

#import "KWCustomerViewController.h"
#import "KWCustomer.h"
#import "KWCustomerFrame.h"
#import "KWCustomerContent.h"
#import "KWCustomerTableViewCell.h"
#import "Utility.h"
#import "NSString+MD5.h"
#import "KWCustomerLeftViewController.h"
#import "KWHomeViewController.h"
#import "KWCustomerLeftViewController.h"
#import "RESideMenu.h"
#import "KWHomeViewController.h"
#import "KWCustomerDetailViewController.h"
#import <UIViewController+MMDrawerController.h>
#import <AFNetworking.h>
#import <MJExtension.h>

@interface KWCustomerViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *_indexArray; //索引数组
}

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSArray *customerFrameS;

@property (nonatomic,strong)NSString *DataType; // 请求参数  刷新列表
@property (nonatomic,strong)NSString *DataText;  // 请求参数  刷新列表
@property (nonatomic,assign)NSInteger pageIndex; //设置当前的pageIndex

@end

@implementation KWCustomerViewController


//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor = RGBCOLOR(225, 225, 225);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menud"] style:UIBarButtonItemStylePlain target:self action:@selector(presentLeftMenu)];
    
    self.title = @"客户管理";
    self.pageIndex = 1;
    self.view.backgroundColor = RGBCOLOR(225, 225, 225);
    
    [self setupTableView];
    
    [self getNetWorkOfPeople:1 DataText:maxPageNum];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reladateCustom:) name:@"reladateCustomDetail" object:nil];
}

- (void)reladateCustom:(NSNotification *)sender
{
    //传通知的时候判断过了  此处安全
    NSString *DataType = sender.userInfo[@"DataType"];
    NSString *DataText = sender.userInfo[@"DataText"];
    // 保存参数用于刷新列表
    self.DataType = DataType;
    self.DataText = DataText;
    
    [self getNetWorkOfPeople:self.pageIndex DataText:maxPageNum];
}


- (void)presentLeftMenu {
    if ([self.title isEqualToString:@"客户"]) {
        NSLog(@"弹出客户抽屉");
    }
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)setupTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    [tableView.tableHeaderView setHidden:YES];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    self.tableView = tableView;
}



- (void)getNetWorkOfPeople:(NSInteger)pageindex DataText:(NSString*)pagemax {
    
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];

    [requestInfo setValue:[NSString stringWithFormat:@"%ld",(long)pageindex] forKey:@"pageindex"];
    [requestInfo setValue:pagemax forKey:@"pagemax"];
    if (self.DataType && self.DataText){
        
        NSString *where = [NSString stringWithFormat:@"%@='%@'",self.DataType,self.DataText];
        [requestInfo setValue:where forKey:@"where"];
    }
    
//    NSString *netPath = @"http://116.62.232.164:9898/api/client/getlist";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/client/getlist",USER_SERVERADDRESS];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    __weak KWCustomerViewController *weakSelf = self;

    [manager POST:netPath parameters:requestInfo progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        KWCustomerResult *customerResult =[KWCustomerResult mj_objectWithKeyValues:[responseObject valueForKey:@"back"]];
        weakSelf.customerResult = customerResult;
        
        NSDictionary *dict = responseObject[@"back"];
        NSArray *arrayRes = dict[@"list"];
        NSMutableArray *customerArray = [NSMutableArray array];
        for (NSDictionary *dict in arrayRes) {
            KWCustomer *customer = [[KWCustomer alloc] initWithDict:dict];
            [customerArray addObject:customer];
        }
        NSArray *customerModel = [NSArray array];
        customerModel = customerArray;
        
        NSMutableArray *customerFrameArray = [NSMutableArray array];
        for (KWCustomer *customer in customerModel) {
            KWCustomerFrame *customerFrame = [[KWCustomerFrame alloc] init];
            customerFrame.customer = customer;
            [customerFrameArray addObject:customerFrame];
        }
        self.customerFrameS = customerFrameArray;
                
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail");
    }];
    
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  160;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.customerFrameS.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KWCustomerTableViewCell *cell = [KWCustomerTableViewCell cellWithCustomer: tableView];
    
    cell.customerFrame = self.customerFrameS[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSUserDefaults standardUserDefaults]setObject:self.customerResult.list[indexPath.row].Id forKey:@"CustomIDNow"];
    
    NSLog(@"%@",self.customerResult.list[indexPath.row].Id);
    
    KWCustomerDetailViewController *detailVc =[[KWCustomerDetailViewController alloc]init];
    detailVc.model = self.customerResult;
    detailVc.num = indexPath.row;
    [detailVc initSetView];
    [self.navigationController pushViewController:detailVc animated:YES];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

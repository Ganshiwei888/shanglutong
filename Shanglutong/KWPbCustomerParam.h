//
//  KWPbCustomerParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWPbCustomerParam : KWBaseParam

@property (nonatomic, copy) NSString *pageIndex;

@property (nonatomic, copy) NSString *pagemax;

@property (nonatomic, copy) NSString *pubclass;

@end

//
//  KWUserAllMenu.m
//  
//
//  Created by KeWen on 2017/8/14.
//
//

#import "KWUserAllMenu.h"
#import "Utility.h"

@implementation KWUserAllMenu

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _BranchId = checkStringNull(dict[@"BranchId"]);
        _DeptId = checkStringNull(dict[@"DeptId"]);
        _PicName = checkStringNull(dict[@"PicName"]);
        _duty = checkStringNull(dict[@"duty"]);
        _email = checkStringNull(dict[@"email"]);
        _mobile = checkStringNull(dict[@"mobile"]);
        _sex = checkStringNull(dict[@"sex"]);
        _userid = checkStringNull(dict[@"userid"]);
        _username = checkStringNull(dict[@"username"]);
        _Hxid = checkStringNull(dict[@"Hxid"]);
        
    }
    return self;
}

- (NSString *)decryptionSubPassword:(NSString *)password {
    NSString *str = @"";
    int count =(int)password.length;
    for (int i = 0; i<count; i++) {
        if (i%2 == 0 && i<15) {
            int tempASCII = [password characterAtIndex:i] + 15 - i;
            NSString *tempStr = [NSString stringWithFormat:@"%c",tempASCII];
            str = [str stringByAppendingString:tempStr];
        } else {
            NSString *temp = [password substringWithRange:NSMakeRange(i, 1)];
            str = [str stringByAppendingString:temp];
        }
    }
    return str;
}

@end

//
//  KWUserHttpTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWHttpTool.h"
#import <AFNetworking.h>
#import "MBProgressHUD+XQ.h"

@implementation KWHttpTool

//Block 的声明与赋值只是保存了一段代码块，只有调用才能执行内部代码。
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error = [NSError new];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *temp = (NSDictionary *)responseObject;
            if (temp[@"back"]) {
                if ([temp[@"back"] isKindOfClass:[NSDictionary class]]) {
                    if (success) {
                        success(responseObject);
                    }
                } else if ([temp[@"back"] isKindOfClass:[NSString class]] &&([url containsString:@"UPMSG"] ||[url containsString:@"gethx"] || [url containsString:@"move"]|| [url containsString:@"Read"]|| [url containsString:@"Redflag"]||[url containsString:@"getpic"])) {//UPMSG
                    if (success) {
                        success(responseObject);
                    }
                } else {
                    if (success) {
                        success(responseObject);
                    }
                }
            } else {
                if (success) {
                    success(responseObject);
                }
            }
        } else {
            
            if (failure) {
                failure(error);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end

//
//  KWMailAttachmentViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//



#import "KWMailAttachmentViewController.h"
#import "KWUserInfoManager.h"
#import "MBProgressHUD+XQ.h"
#import "KWUserDefaultsManager.h"
#import "Base64.h"
#import <WebKit/WebKit.h>
#import "KWMailImageAttachmentViewController.h"
#import "KWMailDocAttachmentViewController.h"
#import "Utility.h"
#import <AFNetworking.h>

NSString * const DownloadAttachmentSuccess = @"downloadAttachmentSuccess";
NSString * const DownloadAttachmentFailed = @"downloadAttachmentFailed";
NSString * const DownloadAttachmentError = @"downloadAttachmentError";


@interface KWMailAttachmentViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet UILabel *attachmentNameLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *attachmentProgressView;
@property (weak, nonatomic) IBOutlet UILabel *attachmentProgressLabel;

@property (nonatomic, strong) KWMailAttachmentResult *attachmentModel;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, assign) NSInteger attachmentSize;
@property (nonatomic, strong) NSString *attachmentName;
@property (nonatomic, assign) NSString* attachmentId;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) KWMailListModel *mailHomeMailListCellModel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;

@end

@implementation KWMailAttachmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.position = 0;
    self.attachmentNameLabel.text = self.attachmentModel.name;
    self.attachmentSize = [self.attachmentModel.size integerValue];
    self.attachmentName = self.attachmentModel.name;
    self.attachmentId = self.attachmentModel.fileid;
    self.attachmentProgressLabel.text = @"mmmmm";
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:self.filePath]) {
        NSData *fileData = [NSData dataWithContentsOfFile:self.filePath];
        if (fileData.length == self.attachmentSize) {
            [MBProgressHUD showSuccess:@"文件已存在直接打开" toView:self.view];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            [self.view addSubview:imageView];
            NSData *data = [NSData dataWithContentsOfFile:self.filePath];
            imageView.image = [UIImage imageWithData:data];
        } else {
            [fileManager removeItemAtPath:self.filePath error:nil];
            NSString *userAccount = [KWUserInfoManager sharedKWUserInfoManager].userAccount;
            NSString *userPassword = [KWUserInfoManager sharedKWUserInfoManager].userMD5Password;
            [KWMailAttachmentViewController downloadAttachmentWithUserAccount:userAccount UserPassword:userPassword AttachmentFileId:[self.attachmentId integerValue] AndPosition:self.position];
            [self addNotificationsOfDownloadAttachment];
        }
    } else {
        NSString *userAccount = [KWUserInfoManager sharedKWUserInfoManager].userAccount;
        NSString *userPassword = [KWUserInfoManager sharedKWUserInfoManager].userMD5Password;
        [KWMailAttachmentViewController downloadAttachmentWithUserAccount:userAccount UserPassword:userPassword AttachmentFileId:[self.attachmentId integerValue] AndPosition:self.position];
        [self addNotificationsOfDownloadAttachment];
    }

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)getAttachmentCellModel:(KWMailAttachmentResult *)mailDetailFileModel andMailHomeMailListCell:(KWMailListModel *)mailListCell AndFilePath:(NSString *)filePath {
    self.attachmentModel = mailDetailFileModel;
    self.mailHomeMailListCellModel = mailListCell;
    self.filePath = filePath;
}

#pragma mark - NSNotifications Of DownloadAttachment
- (void)downloadAttachmentSuccess:(NSNotification *)sender {
    
    NSString *base64DataString = sender.userInfo[@"data"];
    NSData *base64Data = [base64DataString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [[NSData alloc] initWithBase64EncodedData:base64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSUInteger subSize = data.length;
    self.position += subSize;
    if (![fileManager fileExistsAtPath:self.filePath]) {
        BOOL isSuccess = [fileManager createFileAtPath:self.filePath contents:data attributes:nil];
        if (!isSuccess) {
            [MBProgressHUD showError:@"文件写入失败" toView:self.view];
            [fileManager removeItemAtPath:self.filePath error:nil];
            [self removeNotificationsOfDownloadAttachment];
            return;
        } else {
            NSInteger percent = self.position/self.attachmentSize * 100 * 1.0;
            self.attachmentProgressLabel.text = [NSString stringWithFormat:@"%.1ld%%", (long)percent];
            [self.attachmentProgressView setProgress:self.position/self.attachmentSize*1.0 animated:YES];
            NSLog(@"text = %@, progress = %f", self.attachmentProgressLabel.text, self.attachmentProgressView.progress);
            if (self.position < self.attachmentSize) {
                NSString *userAccount = [KWUserInfoManager sharedKWUserInfoManager].userAccount;
                NSString *userPassword = [KWUserInfoManager sharedKWUserInfoManager].userMD5Password;
                [KWMailAttachmentViewController downloadAttachmentWithUserAccount:userAccount UserPassword:userPassword AttachmentFileId:[self.attachmentId integerValue] AndPosition:self.position];
            } else {
                
                [self checkFile];
                [self removeNotificationsOfDownloadAttachment];
                self.checkButton.hidden = NO;
            }
        }
    } else {
        NSInteger percent = self.position/self.attachmentSize * 100 * 1.0;
        self.attachmentProgressLabel.text = [NSString stringWithFormat:@"%.1ld%%", (long)percent];
        [self.attachmentProgressView setProgress:self.position/self.attachmentSize*1.0 animated:YES];
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:self.filePath];
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
        NSLog(@"text = %@, progress = %f", self.attachmentProgressLabel.text, self.attachmentProgressView.progress);
        if (self.position < self.attachmentSize) {
            NSString *userAccount = [KWUserInfoManager sharedKWUserInfoManager].userAccount;
            NSString *userPassword = [KWUserInfoManager sharedKWUserInfoManager].userMD5Password;
            [KWMailAttachmentViewController downloadAttachmentWithUserAccount:userAccount UserPassword:userPassword AttachmentFileId:[self.attachmentId integerValue] AndPosition:self.position];
        } else {
            
            [self checkFile];
            
            [self removeNotificationsOfDownloadAttachment];
            self.checkButton.hidden = NO;
        }
    }
}

- (void)checkFile {
    NSData *fileData = [NSData dataWithContentsOfFile:self.filePath];
    NSString *lastString = [self.attachmentModel.name componentsSeparatedByString:@"."].lastObject;
    NSString *lowerString = [lastString lowercaseString];
    if ([lowerString isEqualToString:@"png"] || [lowerString isEqualToString:@"jpg"] || [lowerString isEqualToString:@"jpeg"] || [lowerString isEqualToString:@"gif"]) {
        KWMailImageAttachmentViewController *imageAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MailImageAttachmentViewController"];
        [imageAttachmentVC recieveTheImage:fileData];
        [self.navigationController pushViewController:imageAttachmentVC animated:YES];
        //pdf、doc、docx、xls、xlsx、ppt、pptx、txt
    } else if ([lowerString isEqualToString:@"pdf"] || [lowerString isEqualToString:@"doc"] || [lowerString isEqualToString:@"docx"] || [lowerString isEqualToString:@"xls"] || [lowerString isEqualToString:@"xlsx"] || [lowerString isEqualToString:@"ppt"] || [lowerString isEqualToString:@"pptx"] || [lowerString isEqualToString:@"txt"]) {
        KWMailDocAttachmentViewController *docAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MailDocAttachmentViewController"];
        [docAttachmentVC recieveFilePath:self.filePath];
        [self.navigationController pushViewController:docAttachmentVC animated:YES];
    } else {
        NSString *msg = [NSString stringWithFormat:@"此文件已下载\n但无法打开查看%@格式的文件",lowerString];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)downloadAttachmentFailed:(NSNotification *)sender {
    NSString *back = sender.userInfo[@"back"];
    NSLog(@"%@",back);
    [MBProgressHUD showError:back toView:self.view];
    [self removeNotificationsOfDownloadAttachment];
}

- (void)downloadAttachmentError:(NSNotification *)sender {
    NSString *back = sender.userInfo[@"userInfo"];
    [MBProgressHUD showError:back toView:self.view];
    [self removeNotificationsOfDownloadAttachment];
}

- (void)addNotificationsOfDownloadAttachment {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadAttachmentSuccess:) name:DownloadAttachmentSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadAttachmentFailed:) name:DownloadAttachmentFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadAttachmentError:) name:DownloadAttachmentError object:nil];
}

- (void)removeNotificationsOfDownloadAttachment {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DownloadAttachmentSuccess object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DownloadAttachmentFailed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DownloadAttachmentError object:nil];
}

#pragma mark - CheckButtonClick
- (IBAction)checkButtonClick:(UIButton *)sender {
    [self checkFile];
}


+ (void)downloadAttachmentWithUserAccount:(NSString *)userAccount UserPassword:(NSString *)userPassword AttachmentFileId:(NSInteger)fileId AndPosition:(NSInteger)position {
    NSLog(@"当前线程为%@",[NSThread currentThread]);
    NSString *userServerAddress = [KWUserInfoManager sharedKWUserInfoManager].userServerAddress;
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/api/file/Download", userServerAddress];
    NSDictionary *parameters = @{
                                 @"UserId": userAccount,
                                 @"Password": userPassword,
                                 @"Ran": RAN,
                                 @"Sign": SIGN,
                                 @"fileid": @(fileId),
                                 @"position": @(position)
                                 };
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    [sessionManager POST:urlStr parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *code = responseObject[@"code"];
        if ([code isEqualToString:@"0000"]) {
            //            LRLog(@"%@",responseObject);
            NSString *data = responseObject[@"back"][@"data"];
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadAttachmentSuccess object:nil userInfo:@{@"data": data}];
        } else {
            NSString *back = responseObject[@"back"];
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadAttachmentFailed object:nil userInfo:@{@"back": back}];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *userInfo = error.userInfo[@"NSLocalizedDescription"];
        [[NSNotificationCenter defaultCenter] postNotificationName:DownloadAttachmentError object:nil userInfo:@{@"userInfo": userInfo}];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  KWWeekReportTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWWeekReportModel;
@interface KWWeekReportTableViewCell : UITableViewCell

@property (nonatomic, strong) KWWeekReportModel *weekModel;

@end

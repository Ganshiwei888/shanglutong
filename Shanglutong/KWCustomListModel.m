//
//  KWCustomListModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomListModel.h"

//@property (nonatomic,strong) NSString *Id;
//@property (nonatomic,strong) NSString *DataClass;
//@property (nonatomic,strong) NSString *DataType;
//@property (nonatomic,strong) NSString *DataText;
//@property (nonatomic,strong) NSString *DataValue1;
//@property (nonatomic,strong) NSString *DataValue2;
//@property (nonatomic,strong) NSString *DataValue3;
//@property (nonatomic,strong) NSString *DataValue4;
//@property (nonatomic,strong) NSString *DataValue5;
//@property (nonatomic,strong) NSString *Sort;

@implementation KWCustomListModel

+ (instancetype)listWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _Id = checkStringNull(dict[@"Id"]);
        _DataClass = checkStringNull(dict[@"DataClass"]);
        _DataType = checkStringNull(dict[@"DataType"]);
        _DataText = checkStringNull(dict[@"DataText"]);
        _DataValue1 = checkStringNull(dict[@"DataValue1"]);
        _DataValue2 = checkStringNull(dict[@"DataValue2"]);
        _DataValue3 = checkStringNull(dict[@"DataValue4"]);
        _DataValue4 = checkStringNull(dict[@"DataValue5"]);
        _DataValue5 = checkStringNull(dict[@"Sort"]);
    
    }
    
    return self;
    
}

@end

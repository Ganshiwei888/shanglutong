//
//  KWDateManageParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWDateAddManageParam : KWBaseParam

@property (nonatomic, copy) NSString *menuno;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSString *is_repeat;
@property (nonatomic, copy) NSString *clientid;
@property (nonatomic, copy) NSString *note;
@property (nonatomic, copy) NSString *Urgency_level;
@property (nonatomic, copy) NSString *Reminder;


@end

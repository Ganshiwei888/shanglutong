//
//  KWBusinessBackResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWBusinessMsg.h"

@interface KWBusinessBackResult : NSObject

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, copy) NSString *list;

@property (nonatomic, strong) NSArray <KWBusinessMsg *> *msg;

@end

//
//  KWMailHomeViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailHomeViewController.h"
#import "KWMailDetailViewController.h"
#import "KWMailListTableViewCell.h"
#import "KWMailList.h"
#import "KWMailListFrame.h"
#import "Utility.h"
#import "KWMailListTool.h"
#import "KWMailListParam.h"
#import "LWActiveIncator.h"
#import "KWMailDetailParam.h"
#import "KWMailSendViewController.h"
#import "KWNavigationController.h"
#import "KWMailListRecvParam.h"
#import "KWHttpTool.h"
#import "KWBaseParam.h"
#import "KWUserMailBoxListCell.h"
#import "KWMailUserFolder.h"
#import "JSBadgeView.h"
#import "KWMailReMoveView.h"
#import "YCXMenu.h"
#import "KWMailRemoveParam.h"
#import "KWMailSearchViewController.h"
#import <SVProgressHUD.h>
#import <MBProgressHUD.h>
#import <UIViewController+MMDrawerController.h>
#import <MGSwipeTableCell.h>
#import <MJRefresh.h>
#import <MJExtension.h>
#import <SVProgressHUD.h>
#import "MBProgressHUD+XQ.h"
#import <AFNetworking.h>

@interface KWMailHomeViewController ()<UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UITableView *mailTableView;
@property (nonatomic, strong) NSMutableArray *mailListArray;
@property (nonatomic, strong) NSMutableArray *mailArray;
@property (nonatomic, strong) NSString *mailBaseID;
@property (nonatomic, copy) NSString *act;
@property (nonatomic, copy) NSString *boxId;
@property (nonatomic, copy) NSString *boxact;
@property (nonatomic, copy) NSString *parentid;
@property (nonatomic, weak) UIButton *sortButton;
@property (nonatomic, weak) UIButton *moreBarButton;
@property (nonatomic, weak) UIButton *writeBarButton;
@property (nonatomic, strong) NSArray *mailBoxArray;
@property (nonatomic, strong) NSString *mailBox;
@property (nonatomic, weak) UIView *redView;
@property (nonatomic, weak) UIView *blueView;
@property (nonatomic, strong) NSArray *UserFolder;
@property (nonatomic, strong) NSMutableArray *showFolder;
@property (nonatomic, weak) UIView *MailFolderSameLevelBackView;
@property (nonatomic, weak) UIView *MailFolderPreviousBackView;

@property (nonatomic, strong) NSMutableArray *tempFolderArray;
@property (nonatomic, strong) NSMutableArray *tempMailArray;

@property (nonatomic, weak) UIButton *folderButton;
@property (nonatomic, weak) UIButton *hideButton;

@property (nonatomic, copy) NSString *folderBoxid;

@property (nonatomic,assign) BOOL headFreshOrFootFresh; //判断时上拉刷新 还是下啦刷新
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) NSInteger mailCount;

@property (nonatomic, copy) NSString *boxParentId;

@property (nonatomic, weak) UIView *toolView;
@property (nonatomic, strong) NSMutableArray *items;
//@property (weak, nonatomic)  NSLayoutConstraint* imageviewWidth;
@property (nonatomic, weak) NSLayoutConstraint *redViewCon;
@property (nonatomic, strong) JSBadgeView *badgeView;
@property (nonatomic, weak) UIButton *menuButton;
@property (nonatomic, weak) UIImageView *readView;

@property (nonatomic, strong) NSMutableArray *readArray;
@property (nonatomic, strong) NSMutableArray *manageArray;

@property (nonatomic, strong) NSMutableArray *selectBtnState;
@property (nonatomic, copy) NSString *mailACT;

@end

@implementation KWMailHomeViewController
@synthesize items = _items;

- (NSMutableArray *)manageArray {
    if (!_manageArray) {
        _manageArray = [NSMutableArray array];
    }
    return _manageArray;
}

- (NSMutableArray *)readArray {
    if (!_readArray) {
        _readArray = [NSMutableArray array];
    }
    return _readArray;
}

- (NSMutableArray *)tempMailArray {
    if (!_tempMailArray) {
        _tempMailArray = [NSMutableArray array];
    }
    return _tempMailArray;
}

- (NSMutableArray *)tempFolderArray {
    if (!_tempFolderArray) {
        _tempFolderArray = [NSMutableArray array];
    }
    return _tempFolderArray;
}

- (NSMutableArray *)showFolder {
    if (!_showFolder) {
        _showFolder = [NSMutableArray array];
    }
    return _showFolder;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.tabBarController.tabBar setHidden:YES];
    [self.menuButton setHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.menuButton setHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    [self setupTableView];
    [self allMailLsit];
    [self setupToolView];
    self.pageIndex = 1;
    self.parentid = @"ALL";
    self.boxId = @"0";
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadMialList:) name:@"reloadMailList" object:nil];
}

- (void)allMailLsit {
    [_mailTableView.tableHeaderView  setFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 100)];
    [LWActiveIncator showInView:self.view];
    [self getAllMaildata];
    [self getUserFolder];
//    [self setupBadge];
}

- (void)getUserFolder {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *URL = [NSString stringWithFormat:@"http://%@/api/Folder/Get",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json);
        
        NSArray *backArray = [json valueForKey:@"back"];
        NSMutableArray *tempArray = [NSMutableArray array];
        
        for (NSDictionary *dict in backArray) {
            KWMailUserFolder *folder = [[KWMailUserFolder alloc] initWithDict:dict];
            [tempArray addObject:folder];
        }
        self.UserFolder = tempArray;
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"没有网络"];
        [SVProgressHUD dismissWithDelay:1];
    }];
}

- (void)setupTopRedView {

    NSArray* constrains = self.redView.constraints;
    for (NSLayoutConstraint* constraint in constrains) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            constraint.constant = 24.0;
        }
    }
    [self.mailTableView setFrame:CGRectMake(0, 4, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 96)];
//    CGFloat height = self.redView.frame.size.height;
}

//加载所有邮件
- (void)getAllMaildata {
    
    KWMailListParam *param = [KWMailListParam param];
    param.pageindex = @"1";
    param.pagemax = @"50";
    param.Boxid = @"0";
    param.act = @"ALL";
    self.act = param.act;
    __weak KWMailHomeViewController *weakSelf = self;
    
    [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
       
        weakSelf.mailListArray = resultData.list;
        weakSelf.mailCount = [resultData.count integerValue];
        weakSelf.badgeView.badgeText = resultData.unread;
        NSMutableArray *modelArray = [NSMutableArray array];
        for (int i = 0; i < resultData.list.count; i++) {
            KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
            listFrame.isSelect = @"no";
            listFrame.mailModelList = resultData.list[i];
            NSLog(@"%@",listFrame.mailModelList.clientid);
            [modelArray addObject:listFrame];
        }
        weakSelf.mailArray = modelArray;
        [weakSelf.mailTableView reloadData];
        [LWActiveIncator hideInViwe:self.view];
    } failure:^(NSError *error) {
        [LWActiveIncator hideInViwe:self.view];
        [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
        [SVProgressHUD dismissWithDelay:1];
    }];
}

- (void)setupNav {
    
    //规定按钮的位置
    UIButton *menuButton = [[UIButton alloc]initWithFrame:CGRectMake(20.0f, 7.0f, 35.0f, 35.0f)];
    //更改按钮的图片
    [menuButton setImage:[UIImage imageNamed:@"menud"] forState:UIControlStateNormal];
    //设置按钮按下的动作和响应方式
    [menuButton addTarget:self action:@selector(presentLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    //最后就是把创建的按钮添加到navigationbar上
    [self.navigationController.navigationBar addSubview:menuButton];
    self.menuButton = menuButton;
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menud"] style:UIBarButtonItemStylePlain target:self action:@selector(presentLeftMenu)];
    //1、在父控件（parentView）上显示，显示的位置TopRight
    self.badgeView = [[JSBadgeView alloc]initWithParentView:menuButton alignment:JSBadgeViewAlignmentTopRight];
    self.badgeView.badgePositionAdjustment = CGPointMake( 6, 0);
    
    //1、背景色
    self.badgeView.badgeBackgroundColor = [UIColor redColor];
    //2、没有反光面
    self.badgeView.badgeOverlayColor = [UIColor clearColor];
    //3、外圈的颜色，默认是白色
    self.badgeView.badgeStrokeColor = [UIColor redColor];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"全部邮件";
    [titleLabel setFont:[UIFont fontWithName:@"System Medium" size:10]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(0, 0, 100, 40);
    self.navigationItem.titleView = titleLabel;
    self.titleLabel = titleLabel;
    
    //排序
    UIButton *sortBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sortBarButton.frame = CGRectMake(0, 0, 25, 25);
    [sortBarButton setImage:[UIImage imageNamed:@"排序2"] forState:UIControlStateNormal];
    [sortBarButton setBackgroundColor:[UIColor clearColor]];
    [sortBarButton addTarget:self action:@selector(sorting:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *sortRightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortBarButton];
    self.sortButton = sortBarButton;
    
    UIButton *moreBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreBarButton.frame = CGRectMake(0, 0, 25, 25);
    [moreBarButton setImage:[UIImage imageNamed:@"more2"] forState:UIControlStateNormal];
    [moreBarButton setBackgroundColor:[UIColor clearColor]];
    [moreBarButton addTarget:self action:@selector(callOutTheLittleMoreView:) forControlEvents:UIControlEventTouchUpInside];
    self.moreBarButton = moreBarButton;
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:moreBarButton];
    
    UIButton *writeBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    writeBarButton.frame = CGRectMake(0, 0, 25, 25);
    [writeBarButton setImage:[UIImage imageNamed:@"写信2"] forState:UIControlStateNormal];
    [writeBarButton setBackgroundColor:[UIColor clearColor]];
    [writeBarButton addTarget:self action:@selector(writeNewMail:) forControlEvents:UIControlEventTouchUpInside];
    self.writeBarButton = writeBarButton;
    UIBarButtonItem *secondRightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:writeBarButton];
    
    self.navigationItem.rightBarButtonItems = @[rightBarButtonItem, secondRightBarButtonItem,sortRightBarButtonItem];
    
}

#pragma mark - Nav Bar Button -

//- (void)setupBadge {
//    KWMailListParam *param = [KWMailListParam param];
//    param.pagemax = @"50";
//    param.pageindex = @"1";
//    param.Boxid = @"0";
//    param.act = @"ALL";
//    __weak KWMailHomeViewController *weakSelf = self;
//    [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
//        
//        NSLog(@"aaa%@",resultData.unread);
//        weakSelf.badgeView.badgeText = resultData.unread;
//        
//    } failure:^(NSError *error) {
//        NSLog(@"can not load data");
//        [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
//        [SVProgressHUD dismissWithDelay:1];
//    }];
//
//}

- (void)sorting:(UIButton *)sender {
    
    NSMutableArray *temp = self.mailArray;
    temp = (NSMutableArray *)[[temp reverseObjectEnumerator] allObjects];
    
    self.mailArray = temp;
    [SVProgressHUD showWithStatus:nil];
    [self.mailTableView reloadData];
    [SVProgressHUD dismissWithDelay:0.5];
    
}

- (void)callOutTheLittleMoreView:(UIButton *)sender {
    [YCXMenu setTintColor:[UIColor colorWithRed:0.118 green:0.573 blue:0.820 alpha:1]];
//    [YCXMenu setSelectedColor:[UIColor ]];
    if ([YCXMenu isShow]){
        [YCXMenu dismissMenu];
    } else {
        [YCXMenu showMenuInView:self.view fromRect:CGRectMake(self.view.frame.size.width - 50, 0, 50, 0) menuItems:self.items selected:^(NSInteger index, YCXMenuItem *item) {
            NSLog(@"%@",item);
        }];
    }
}

#pragma mark - setter/getter
- (NSMutableArray *)items {
    if (!_items) {
        
        // set title
        YCXMenuItem *menuTitle = [YCXMenuItem menuTitle:@"更多" WithIcon:nil];
        menuTitle.foreColor = [UIColor whiteColor];
        menuTitle.titleFont = [UIFont boldSystemFontOfSize:20.0f];
        
        //set logout button
        
        YCXMenuItem *groupMove = [YCXMenuItem menuItem:@"批量操作" image:nil target:self action:@selector(groupMove:)];
        groupMove.foreColor = [UIColor whiteColor];
        groupMove.alignment = NSTextAlignmentCenter;
        
        YCXMenuItem *searchMail = [YCXMenuItem menuItem:@"搜索" image:nil target:self action:@selector(searchMail:)];
        searchMail.foreColor = [UIColor whiteColor];
        searchMail.alignment = NSTextAlignmentCenter;
        
        YCXMenuItem *newFolder = [YCXMenuItem menuItem:@"建文件夹" image:nil target:self action:@selector(newFolder:)];
        newFolder.foreColor = [UIColor whiteColor];
        newFolder.alignment = NSTextAlignmentCenter;
        
        //set item
        _items = [@[menuTitle,groupMove,searchMail] mutableCopy];
    }
    return _items;
}

- (void)setItems:(NSMutableArray *)items {
    _items = items;
}

- (void)groupMove:(UIButton *)sender {
    
    for (int i = 0; i < self.mailArray.count; i++) {
        KWMailListFrame *model = self.mailArray[i];
        NSLog(@"%@",model.isEdit);
        model.isEdit = @"1";
    }
    
    self.toolView.hidden = NO;
    
    [self.mailTableView reloadData];
    
}

- (void)searchMail:(UIButton *)sender {
    
    KWMailSearchViewController *vc = [[KWMailSearchViewController alloc] init];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)newFolder:(UIButton *)sender {
    
}

- (void)writeNewMail:(UIButton *)sender {
    
    KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
    KWNavigationController *send = [[KWNavigationController alloc] initWithRootViewController:sendVc];
    
    [self presentViewController:send animated:YES completion:^{
        
    }];
    
}

- (void)setupTableView {
    
    CGFloat floderHeight = 64;
    //创建redView
    UIView *redView = [[UIView alloc]init];
    redView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    redView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:redView];
    self.redView = redView;
    
    [self setupRedView];
    NSLayoutConstraint * redLeftLc = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeLeftMargin relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0];
    [self.view addConstraint:redLeftLc];
//    NSLayoutConstraint *redBottomLc = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTopMargin multiplier:1.0f constant: floderHeight];
#pragma mark - 取消邮件列表顶部空白:将redView的Bottom约束设为0  将其颜色设为透明
    _redView.backgroundColor = [UIColor clearColor];
    NSLayoutConstraint *redBottomLc = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTopMargin multiplier:1.0f constant: 0];
    [self.view addConstraint:redBottomLc];

    //这里直接设置自身宽
    NSLayoutConstraint * redWLc = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:kNilOptions multiplier:1.0f constant: self.view.frame.size.width];
    //由于没有参照物，所以约束添加于自身身上
    [redView addConstraint:redWLc];
    //创建最后一个约束，自身的高
    NSLayoutConstraint * redHLc = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:kNilOptions multiplier:1.0f constant: 1];
    //由于没有参照物，所以约束添加于自身身上
    [redView addConstraint:redHLc];
    self.redView = redView;
    UIView *blueView = [[UIView alloc]init];
    blueView.backgroundColor = [UIColor clearColor];
    blueView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:blueView];
    self.blueView = blueView;
    NSLayoutConstraint *blueLeft = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f];
    //与其他控件发生约束，所以约束添加到父控件上
    [self.view addConstraint:blueLeft];
    NSLayoutConstraint *blueBottom = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:redView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f];
    //与其他控件发生约束，所以约束添加到父控件上
    [self.view addConstraint:blueBottom];
    NSLayoutConstraint *blueW = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:redView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f];
    [self.view addConstraint:blueW];
    
    NSLayoutConstraint *blueH = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:kNilOptions multiplier:1.0f constant:KWSCREEN_HEIGHT];
    [self.view addConstraint:blueH];
    
    UITableView *mailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    mailTableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    mailTableView.delegate = self;
    mailTableView.dataSource = self;
    mailTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerViewRefreshed)];
    mailTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerViewRefresh)];
    [mailTableView.tableHeaderView setHidden:YES];
    [mailTableView.tableFooterView setHidden:YES];
    [self.blueView addSubview:mailTableView];
    
    
    
    self.mailTableView = mailTableView;
    
}

- (void)setupToolView {
    
    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, KWSCREEN_HEIGHT - 40, KWSCREEN_WIDTH, 40)];
    toolView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:toolView];
    self.toolView = toolView;
    
    self.toolView.hidden = YES;

    CGFloat buttonW = 60;
    CGFloat buttonH = 30;
    CGFloat buttonX = (KWSCREEN_WIDTH - 300)/5;
    CGFloat buttonY = 5;
    
    UIButton *readBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [readBtn setFrame:CGRectMake(buttonX/2, buttonY, buttonW, buttonH)];
    [readBtn setTitle:@"已读" forState:UIControlStateNormal];
    [readBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [readBtn addTarget:self action:@selector(groupRead:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolView addSubview:readBtn];
    
    UIButton *unReadBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [unReadBtn setFrame:CGRectMake(CGRectGetMaxX(readBtn.frame) + buttonX, buttonY, buttonW, buttonH)];
    [unReadBtn setTitle:@"未读" forState:UIControlStateNormal];
    [unReadBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [unReadBtn addTarget:self action:@selector(groupUnread:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolView addSubview:unReadBtn];
    
    UIButton *moveBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [moveBtn setFrame:CGRectMake(CGRectGetMaxX(unReadBtn.frame) + buttonX, buttonY, buttonW, buttonH)];
    [moveBtn setTitle:@"移动" forState:UIControlStateNormal];
    [moveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [moveBtn addTarget:self action:@selector(moveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolView addSubview:moveBtn];
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [deleteBtn setFrame:CGRectMake(CGRectGetMaxX(moveBtn.frame) + buttonX, buttonY, buttonW, buttonH)];
    [deleteBtn setTitle:@"红旗" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(groupflag:) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.toolView addSubview:deleteBtn];
    
    UIButton *cancalBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancalBtn setFrame:CGRectMake(CGRectGetMaxX(deleteBtn.frame) + buttonX, buttonY, buttonW, buttonH)];
    [cancalBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancalBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancalBtn addTarget:self action:@selector(cancalBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolView addSubview:cancalBtn];
    
}

- (void)groupRead:(UIButton *)sender {
    
    NSString *readStr = [self.readArray componentsJoinedByString:@","];
    NSLog(@"%@",readStr);
    
    for (int i = 0; i < self.manageArray.count; i++) {
        KWMailListFrame *frame = self.manageArray[i];
        KWMailListModel *model = frame.mailModelList;
        frame.isSelect = @"no";
        model.Read = @"是";
    }
    [self.mailTableView reloadData];
    
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = readStr;
    param.is = @"true";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/Read",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json[@"back"]);
        if (json[@"back"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
            [SVProgressHUD dismissWithDelay:1.0];
        }
    } failure:^(NSError *error) {
    
    }];
    
}

- (void)groupUnread:(UIButton *)sender {
    
    NSString *readStr = [self.readArray componentsJoinedByString:@","];
    NSLog(@"%@",readStr);
    
    for (int i = 0; i < self.manageArray.count; i++) {
        KWMailListFrame *frame = self.manageArray[i];
        KWMailListModel *model = frame.mailModelList;
        frame.isSelect = @"no";
        model.Read = @"否";
    }
    [self.mailTableView reloadData];
    
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = readStr;
    param.is = @"false";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/Read",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json[@"back"]);
        if (json[@"back"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
            [SVProgressHUD dismissWithDelay:1.0];
        }
    } failure:^(NSError *error) {
        
    }];

    
}

- (void)moveBtnClick:(UIButton *)sender {
    
    NSString *readStr = [self.readArray componentsJoinedByString:@","];

    KWMailReMoveView *remailView = [[KWMailReMoveView alloc] initWithFrame:CGRectMake(25, 45, KWContentViewWidth, KWContentViewHeight)];
    [remailView receTheMailId:readStr];
    [remailView showAnimated];
    
}

- (void)groupflag:(UIButton *)sender {
    
    NSString *readStr = [self.readArray componentsJoinedByString:@","];
    
    for (int i = 0; i < self.manageArray.count; i++) {
        KWMailListFrame *frame = self.manageArray[i];
        KWMailListModel *model = frame.mailModelList;
        frame.isSelect = @"no";
        model.redflag = @"true";
    }
    
    [self.mailTableView reloadData];
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = readStr;
    param.is = @"true";
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/Redflag",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json[@"back"]);
        if (json[@"back"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
            [SVProgressHUD dismissWithDelay:1.0];
        }
    } failure:^(NSError *error) {
        
    }];

    
}

- (void)cancalBtn:(UIButton *)sender {

    for (int i = 0; i < self.mailArray.count; i++) {
        KWMailListFrame *model = self.mailArray[i];
        model.isEdit = @"0";
    }
    
    self.toolView.hidden = YES;
    
    [self.mailTableView reloadData];
    
    
}

- (void)setupRedView {
    
    UIButton *folderButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [folderButton setFrame:CGRectMake(KWSCREEN_WIDTH/2 - 52, 0, 120, 24)];
//    [folderButton setTitle:@"展开文件夹" forState:UIControlStateNormal];
    NSString *title = @"展开文件夹";
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}];
    [folderButton setAttributedTitle:attStr forState:UIControlStateNormal];
    [folderButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [folderButton addTarget:self action:@selector(getNextLevel:) forControlEvents:UIControlEventTouchUpInside];
    self.folderButton = folderButton;
    [self.redView addSubview:folderButton];
    
    UIButton *hideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [hideButton setFrame:CGRectMake(KWSCREEN_WIDTH/2 - 52, 0, 120, 24)];
    //    [folderButton setTitle:@"展开文件夹" forState:UIControlStateNormal];
    NSString *hideTitle = @"隐藏文件夹";
    NSAttributedString *attStrT = [[NSAttributedString alloc] initWithString:hideTitle attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}];
    [hideButton setAttributedTitle:attStrT forState:UIControlStateNormal];
    [hideButton setHidden:YES];
    [hideButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [hideButton addTarget:self action:@selector(hideFolder:) forControlEvents:UIControlEventTouchUpInside];
    self.hideButton = hideButton;
    [self.redView addSubview:hideButton];
    
//    self.redView.backgroundColor = [UIColor redColor];
    
}

- (void)getNextLevel:(UIButton *)sender {
    
    [self.folderButton setHidden:YES];
    
    [self.hideButton setHidden:NO];
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (KWMailUserFolder *model in self.UserFolder) {
        if ([model.parentid isEqualToString:self.parentid] && [model.mailboxId isEqualToString:self.mailBaseID]) {
            [tempArray addObject:model];
        }
        self.showFolder = tempArray;
    }
    
    self.tempMailArray = [self.mailArray mutableCopy];
    self.tempFolderArray = [self.showFolder mutableCopy];
    
    self.mailArray = [NSMutableArray array] ;
    [self.mailTableView reloadData];
}

- (void)hideFolder:(UIButton *)sender {
    
    [self.folderButton setHidden:NO];
    [self.hideButton setHidden:YES];
    
    self.showFolder = [NSMutableArray array];
    NSLog(@"temp%lu",(unsigned long)self.tempMailArray.count);

    self.mailArray = [self.tempMailArray mutableCopy];
    
    [self.mailTableView reloadData];
    
}

- (void)headerViewRefreshed {
    
    if ([self.boxId isEqualToString:@"0"]) {
        NSLog(@"全部邮箱");
        [self refreshTheSmartBox];
        [self.mailTableView.mj_header endRefreshing];
    } if ([self.parentid rangeOfString:@"_"].location != NSNotFound) {
        NSLog(@"商业邮箱");
        [self refreshTheBoxList];
    } else {
        [self.mailTableView.mj_header endRefreshing];
    }

}

- (void)refreshTheSmartBox {
    
    KWMailListRecvParam *paramed = [KWMailListRecvParam param];
    paramed.mailBoxId = @"0";
    NSLog(@"%@",self.mailBox);
    NSString *URLstr = [NSString stringWithFormat:@"http://%@/api/mail/recv",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:URLstr params:paramed.mj_keyValues success:^(id json) {
        NSLog(@"%@",json);
    } failure:^(NSError *error) {
        
    }];
    
    
}

- (void)footerViewRefresh {
    self.headFreshOrFootFresh = NO;
    switch ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus) {
        case AFNetworkReachabilityStatusUnknown:
        case AFNetworkReachabilityStatusNotReachable:
            [MBProgressHUD showError:@"无网络" toView:self.view];
            [self.mailTableView.mj_footer endRefreshing];
            break;
        default: {
            if ((self.pageIndex) * 50 > self.mailCount) {
                [MBProgressHUD showError:@"没有更多邮件了" toView:self.view];
                [self.mailTableView.mj_footer endRefreshing];
            } else {
                __weak KWMailHomeViewController *weakSelf = self;
                self.pageIndex += 1;
                [SVProgressHUD showWithStatus:nil];
                if ([self.parentid rangeOfString:@"_"].location != NSNotFound) {
                    NSLog(@"用户邮箱");
                    NSArray * mailParams = [self.parentid componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
                    NSString *boxParentId = mailParams[0];
                    self.boxParentId = boxParentId;
                    NSString *boxId = mailParams[1];
                    self.folderBoxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
                    self.mailBaseID = boxId;
                    NSLog(@"%@",self.mailBaseID);
                    NSString *boxact = mailParams[2];
                    
                    if ([boxParentId isEqualToString:@"f"]) {
                        NSLog(@"文件夹");
                        KWMailListParam *param = [KWMailListParam param];
                        param.pagemax = @"50";
                        param.pageindex = [NSString stringWithFormat:@"%ld",self.pageIndex];
                        param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
                        param.act = [NSString stringWithFormat:@"WJJ%@",boxact];
                        
                        [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                            NSMutableArray *mulist = [weakSelf.mailListArray mutableCopy];
                            for (int i = 0; i < resultData.list.count; i++) {
                                [mulist addObject:resultData.list[i]];
                            }
                            NSMutableArray *newlistArray = [mulist copy];
                            weakSelf.mailListArray = newlistArray;
                            NSMutableArray *muarray = [weakSelf.mailArray mutableCopy];
                            for (int i = 0; i < resultData.list.count; i++) {
                                KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                                listFrame.mailModelList = resultData.list[i];
                                [muarray addObject:listFrame];
                            }
                            NSMutableArray *newArray = [muarray copy];
                            weakSelf.mailArray = newArray;
                            NSString *add = [NSString stringWithFormat:@"加载%lu封",(unsigned long)resultData.list.count];
                            [SVProgressHUD dismissWithDelay:1];
                            [SVProgressHUD showInfoWithStatus:add];
                            [SVProgressHUD dismissWithDelay:0.6];
                            [weakSelf.mailTableView reloadData];
                            [SVProgressHUD dismissWithDelay:0.1];
                        } failure:^(NSError *error) {
                            NSLog(@"can not load data");
                            [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                        }];
                        
                    } else {
                        KWMailListParam *param = [KWMailListParam param];
                        param.pageindex = [NSString stringWithFormat:@"%ld",self.pageIndex];
                        param.pagemax = @"50";
                        param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
                        param.act = boxact;
                        self.act = boxact;
                        self.boxId = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
                        [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                            NSMutableArray *mulist = [weakSelf.mailListArray mutableCopy];
                            for (int i = 0; i < resultData.list.count; i++) {
                                [mulist addObject:resultData.list[i]];
                            }
                            NSMutableArray *newlistArray = [mulist copy];
                            weakSelf.mailListArray = newlistArray;
                            
                            NSMutableArray *muarray = [weakSelf.mailArray mutableCopy];
                            for (int i = 0; i < resultData.list.count; i++) {
                                KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                                listFrame.mailModelList = resultData.list[i];
                                [muarray addObject:listFrame];
                            }
                            NSMutableArray *newArray = [muarray copy];
                            weakSelf.mailArray = newArray;
                            NSString *add = [NSString stringWithFormat:@"加载%lu封",(unsigned long)resultData.list.count];
                            [SVProgressHUD dismissWithDelay:0.1];
                            [SVProgressHUD showInfoWithStatus:add];
                            [SVProgressHUD dismissWithDelay:0.6];
                            [weakSelf.mailTableView reloadData];
                        } failure:^(NSError *error) {
                            NSLog(@"can not load data");
                            [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                            [SVProgressHUD dismissWithDelay:1];
                        }];
                    }
                } else {
                    NSLog(@"全部邮箱");
                    KWMailListParam *param = [KWMailListParam param];
                    param.pageindex = [NSString stringWithFormat:@"%ld",self.pageIndex];
                    param.pagemax = @"50";
                    param.Boxid = @"0";
                    param.act = self.parentid;
                    self.showFolder = [NSMutableArray array];
                    [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                        
                        NSMutableArray *mulist = [weakSelf.mailListArray mutableCopy];
                        for (int i = 0; i < resultData.list.count; i++) {
                            [mulist addObject:resultData.list[i]];
                        }
                        NSMutableArray *newlistArray = [mulist copy];
                        weakSelf.mailListArray = newlistArray;
                        NSMutableArray *muarray = [weakSelf.mailArray mutableCopy];
                        for (int i = 0; i < resultData.list.count; i++) {
                            KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                            listFrame.mailModelList = resultData.list[i];
                            [muarray addObject:listFrame];
                        }
                        NSMutableArray *newArray = [muarray copy];
                        weakSelf.mailArray = newArray;
                        NSString *add = [NSString stringWithFormat:@"加载%lu封",(unsigned long)resultData.list.count];
                        [SVProgressHUD showInfoWithStatus:add];
                        [SVProgressHUD dismissWithDelay:0.6];
                        [weakSelf.mailTableView reloadData];
                    } failure:^(NSError *error) {
                        [SVProgressHUD showErrorWithStatus:@"无法加载"];
                        [SVProgressHUD dismissWithDelay:1];
                        NSLog(@"can not load data");
                    }];                 
                }
                [self.mailTableView.mj_footer endRefreshing];
                break;
            }
        }
    }
}


- (void)refreshAllMailList {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mailbox/Get"];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        
        NSArray *backArray = [json valueForKey:@"back"];
        NSMutableArray *mailarray = [NSMutableArray array];
        for (NSDictionary *dict in backArray) {
            KWUserMailBoxListCell *model = [[KWUserMailBoxListCell alloc] initWithDict:dict];
            [mailarray addObject:model];
        }
        self.mailBoxArray = mailarray;
        
    } failure:^(NSError *error) {
    }];
    
    NSLog(@"%@",self.mailBoxArray);
    
    KWMailListRecvParam *paramed = [KWMailListRecvParam param];
    KWUserMailBoxListCell *modelone = self.mailBoxArray[0];
    paramed.mailBoxId = modelone.ID;
    
    NSString *URLstr = [NSString stringWithFormat:@"http://%@/api/mail/recv",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:URLstr params:paramed.mj_keyValues success:^(id json) {
        
        if (checkStringNull(json[@"back"][@"state"])) {
            NSString *state = checkStringNull(json[@"back"][@"state"]);
            NSInteger *stateInt = (long *)[state integerValue];
            if (stateInt > 0) {
                NSString *recv = json[@"back"][@"rec"];
                [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"获取%@条",recv]];
                [SVProgressHUD dismissWithDelay:1.0f];
//                [self getAllMaildata];
            } else {
                [SVProgressHUD showErrorWithStatus:@"没有更多邮件了"];
                [SVProgressHUD dismissWithDelay:2.0f];
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
    [self.mailTableView.mj_header endRefreshing];
    
}

- (void)refreshTheBoxList {
    
    KWMailListRecvParam *paramed = [KWMailListRecvParam param];
    paramed.mailBoxId = self.mailBaseID;
    NSString *URLstr = [NSString stringWithFormat:@"http://%@/api/mail/recv",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:URLstr params:paramed.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json);
        
        if ([json[@"code"] isEqualToString:@"0012"]) {
            NSLog(@"%@",json[@"back"]);
        }
        
        if ([json[@"code"] isEqualToString:@"0000"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"][@"msg"]];
            [SVProgressHUD dismissWithDelay:0.5];
            [self.mailTableView reloadData];
            NSString *recStr = json[@"back"][@"rec"];
            NSInteger rec = [recStr integerValue];
            if (rec > 0) {
                
                
                
            }
        }
//        NSString *state = checkStringNull(json[@"back"][@"state"]);
//        NSInteger stateInt = (long)[state integerValue];
//        if (stateInt > 0) {
//            NSString *rec = json[@"back"][@"rec"];
//            NSInteger recInt = (long)[rec integerValue];
//            if (recInt > 0) {
//                [self refreshTheHeader];
//            } else {
//                [SVProgressHUD showInfoWithStatus:@"获取0封"];
//                [SVProgressHUD dismissWithDelay:0.8];
//            }
//        }
    } failure:^(NSError *error) {
        
    }];
    [self.mailTableView.mj_header endRefreshing];
}

- (void)refreshTheHeader {
    
    NSString *boxAct = self.parentid;
    __weak KWMailHomeViewController *weakSelf = self;
    if([boxAct rangeOfString:@"_"].location != NSNotFound) {
        self.showFolder = [NSMutableArray array];
        NSArray * mailParams = [boxAct componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
        NSString *boxParentId = mailParams[0];
        self.boxParentId = boxParentId;
        NSString *boxId = mailParams[1];
        self.folderBoxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
        self.mailBaseID = boxId;
        NSLog(@"%@",self.mailBaseID);
        NSString *boxact = mailParams[2];
        if ([boxParentId isEqualToString:@"f"]) {
            NSLog(@"文件夹");
            KWMailListParam *param = [KWMailListParam param];
            param.pagemax = @"50";
            param.pageindex = @"1";
            param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            param.act = [NSString stringWithFormat:@"WJJ%@",boxact];
            [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                //刷新邮件列表的数据
                weakSelf.mailListArray = [NSMutableArray array];
                weakSelf.mailListArray = resultData.list;
                //刷新计数
                weakSelf.mailCount = [resultData.count integerValue];
                NSMutableArray *modelArray = [NSMutableArray array];
                //刷新邮件frame
                weakSelf.mailArray = [NSMutableArray array];
                NSLog(@"%@", resultData.count);
                for (int i = 0; i < resultData.list.count; i++) {
                    KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                    listFrame.mailModelList = resultData.list[i];
                    [modelArray addObject:listFrame];
                    //                [LWActiveIncator hideInViwe:self.view];
                }
                weakSelf.mailArray = modelArray;
                [weakSelf.mailTableView reloadData];
            } failure:^(NSError *error) {
                NSLog(@"can not load data");
                [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                [SVProgressHUD dismissWithDelay:1];
            }];
            
        } else {
            KWMailListParam *param = [KWMailListParam param];
            param.pageindex = @"1";
            param.pagemax = @"50";
            param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            param.act = boxact;
            
            self.act = boxact;
            self.boxId = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            
            //        [LWActiveIncator showInView:self.view];
            [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                weakSelf.mailListArray = [NSMutableArray array];
                weakSelf.mailListArray = resultData.list;
                NSMutableArray *modelArray = [NSMutableArray array];
                weakSelf.mailCount = [resultData.count integerValue];
                weakSelf.mailArray = [NSMutableArray array];
                for (int i = 0; i < resultData.list.count; i++) {
                    KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                    listFrame.mailModelList = resultData.list[i];
                    [modelArray addObject:listFrame];
                }
                weakSelf.mailArray = modelArray;
                [weakSelf.mailTableView reloadData];
            } failure:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                [SVProgressHUD dismissWithDelay:1];
            }];
        }
        
    } else {
        KWMailListParam *param = [KWMailListParam param];
        param.pageindex = @"1";
        param.pagemax = @"50";
        param.Boxid = @"0";
        param.act = self.parentid;
        self.boxId = @"0";
        self.showFolder = [NSMutableArray array];
        [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
            //             weakSelf.mailListArray = [NSMutableArray array];
            weakSelf.mailArray = [NSMutableArray array];
            weakSelf.mailListArray = resultData.list;
            NSLog(@"zzzz%ld",weakSelf.mailListArray.count);
            NSMutableArray *modelArray = [NSMutableArray array];
            //刷新数据
            weakSelf.mailCount = [resultData.count integerValue];
            for (int i = 0; i < resultData.list.count; i++) {
                KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                listFrame.mailModelList = resultData.list[i];
                [modelArray addObject:listFrame];
            }
            NSArray *addArray = modelArray;
            NSLog(@"zzzz%ld",weakSelf.mailArray.count);
            [weakSelf.mailArray addObjectsFromArray:addArray];
            [weakSelf.mailTableView reloadData];
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"无法加载"];
            [SVProgressHUD dismissWithDelay:1];
        }];
    }
}

- (void)presentLeftMenu {
    
    for (int i = 0; i < self.mailArray.count; i++) {
        KWMailListFrame *model = self.mailArray[i];
        NSLog(@"%@",model.isEdit);
        model.isEdit = @"0";
    }
    
    self.toolView.hidden = NO;
    
    [self.mailTableView reloadData];
    self.toolView.hidden = YES;
    
    if (self.folderButton.hidden) {
        self.hideButton.selected = YES;
    }
    
    if ([self.title  isEqualToString:@"邮件"]) {
        NSLog(@"弹出邮件");
    }
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - First Load TableView -

- (void)reloadMialList:(NSNotification *)sender {
    
    self.titleLabel.text = sender.userInfo[@"name"];

    //刷新邮件列表数据
    self.mailArray = [NSMutableArray array];
    
    NSString *boxIdAndAct = sender.userInfo[@"MailID"];
    NSLog(@"%@",sender.userInfo[@"name"]);
    NSLog(@"%@",sender.userInfo[@"MailID"]);
    
    self.mailACT = sender.userInfo[@"ACT"];
    
    self.parentid = boxIdAndAct;
    __weak KWMailHomeViewController *weakSelf = self;
    [LWActiveIncator showInView:self.view];
    if([boxIdAndAct rangeOfString:@"_"].location != NSNotFound ) {
        
        [self setupTopRedView];
        NSLog(@"%@",boxIdAndAct);
        self.showFolder = [NSMutableArray array];
        NSArray * mailParams = [boxIdAndAct componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
        NSString *boxParentId = mailParams[0];
        self.boxParentId = boxParentId;
        NSString *boxId = mailParams[1];
        self.folderBoxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
        self.mailBaseID = boxId;
        NSString *boxact = mailParams[2];
        
        if ([boxParentId isEqualToString:@"f"]) {
            NSLog(@"文件夹");
            KWMailListParam *param = [KWMailListParam param];
            param.pagemax = @"50";
            param.pageindex = @"1";
            param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            param.act = [NSString stringWithFormat:@"WJJ%@",boxact];
            
            [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                
                //刷新邮件列表的数据
                weakSelf.mailListArray = [NSMutableArray array];
                weakSelf.mailListArray = resultData.list;
                //刷新计数
                weakSelf.mailCount = [resultData.count integerValue];
                NSMutableArray *modelArray = [NSMutableArray array];
                //刷新邮件frame
                weakSelf.mailArray = [NSMutableArray array];
                
                weakSelf.badgeView.badgeText = resultData.unread;
                for (int i = 0; i < resultData.list.count; i++) {
                    KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                    listFrame.mailModelList = resultData.list[i];
                    [modelArray addObject:listFrame];
                }
                weakSelf.mailArray = modelArray;
                
                [weakSelf.mailTableView reloadData];
                [LWActiveIncator hideInViwe:self.view];
            } failure:^(NSError *error) {
                NSLog(@"can not load data");
                [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                [SVProgressHUD dismissWithDelay:1];
            }];
            
        } else {
            
            KWMailListParam *param = [KWMailListParam param];
            param.pageindex = @"1";
            param.pagemax = @"50";
//            param.Boxid = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            param.Boxid = boxId;
            param.act = boxact;

            NSLog(@"%@",param);
            self.act = boxact;
            self.boxId = [NSString stringWithFormat:@"%@_%@",boxParentId, boxId];
            [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                weakSelf.mailListArray = [NSMutableArray array];
                weakSelf.mailListArray = resultData.list;
                NSMutableArray *modelArray = [NSMutableArray array];
                weakSelf.mailCount = [resultData.count integerValue];
                weakSelf.mailArray = [NSMutableArray array];
                weakSelf.badgeView.badgeText = resultData.unread;
                for (int i = 0; i < resultData.list.count; i++) {
                    KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                    listFrame.mailModelList = resultData.list[i];
                    [modelArray addObject:listFrame];
                }
                weakSelf.mailArray = modelArray;
                [weakSelf.mailTableView reloadData];
                [LWActiveIncator hideInViwe:self.view];
            } failure:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
                [SVProgressHUD dismissWithDelay:1];
            }];
            
        }
        
        } else {
            NSArray* constrains = self.redView.constraints;
            for (NSLayoutConstraint* constraint in constrains) {
                if (constraint.firstAttribute == NSLayoutAttributeHeight) {
                    constraint.constant = 1.0;
                }
            }
            [self.mailTableView setFrame:CGRectMake(0, 4, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 73)];
            
            KWMailListParam *param = [KWMailListParam param];
            param.pageindex = @"1";
            param.pagemax = @"50";
            param.Boxid = @"0";
            param.act = sender.userInfo[@"ACT"];
            self.boxId = @"0";
            self.showFolder = [NSMutableArray array];
            [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
                weakSelf.mailArray = [NSMutableArray array];
                weakSelf.mailListArray = resultData.list;
                NSMutableArray *modelArray = [NSMutableArray array];
                weakSelf.badgeView.badgeText = resultData.unread;
                //刷新数据
                weakSelf.mailCount = [resultData.count integerValue];
                for (int i = 0; i < resultData.list.count; i++) {
                    KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                    listFrame.mailModelList = resultData.list[i];
                    [modelArray addObject:listFrame];
                }
                NSArray *addArray = modelArray;
                [weakSelf.mailArray addObjectsFromArray:addArray];
                [weakSelf.mailTableView reloadData];
                [LWActiveIncator hideInViwe:self.view];
            } failure:^(NSError *error) {
                [SVProgressHUD showErrorWithStatus:@"无法加载"];
                [SVProgressHUD dismissWithDelay:1];
            }];
        }
    
    for (KWMailListFrame *frame in self.mailArray) {
        frame.isSelect = @"no";
    }
    [self.mailTableView reloadData];
}

- (void)refreshTheTableWith:(NSString *)act andBoxId:(NSString *)boxId {
    
    __weak KWMailHomeViewController *weakSelf = self;
    
    if ([act isEqualToString:@"ALL"]) {
        [self getAllMaildata];
    } else {
        KWMailListParam *param = [KWMailListParam param];
        param.pageindex = @"1";
        param.pagemax = @"50";
        param.Boxid = boxId;
        param.act = act;
        [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
            
            weakSelf.mailListArray = resultData.list;
            NSMutableArray *modelArray = [NSMutableArray array];
            //刷新数据
            weakSelf.mailCount = [resultData.count integerValue];
            weakSelf.mailArray = [NSMutableArray array];
            NSLog(@"%@", resultData.count);
            for (int i = 0; i < resultData.list.count; i++) {
                KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
                listFrame.mailModelList = resultData.list[i];
                [modelArray addObject:listFrame];
            }
            weakSelf.mailArray = modelArray;
            [weakSelf.mailTableView reloadData];
            
        } failure:^(NSError *error) {
            NSLog(@"can not load data");
            [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
            [SVProgressHUD dismissWithDelay:1];
    
        }];
    }
    
}

- (void)getMailListWithAct:(NSString *)boxact {
    KWMailListParam *param = [KWMailListParam param];
    param.pagemax = @"50";
    param.pageindex = @"1";
    param.Boxid = self.folderBoxid;
    param.act = [NSString stringWithFormat:@"WJJ%@",boxact];
    __weak KWMailHomeViewController *weakSelf = self;
    [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
        weakSelf.mailListArray = resultData.list;
        NSMutableArray *modelArray = [NSMutableArray array];
        //刷新数据
        weakSelf.mailArray = [NSMutableArray array];
        weakSelf.mailCount = [resultData.count integerValue];
        for (int i = 0; i < resultData.list.count; i++) {
            KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
            listFrame.mailModelList = resultData.list[i];
            [modelArray addObject:listFrame];
        }
        weakSelf.mailArray = modelArray;
        [weakSelf.mailTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"can not load data");
        [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
        [SVProgressHUD dismissWithDelay:1];
    }];

}

#pragma mark - UITableViewDataSource && UITableViewdelegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.showFolder.count;
    } else {
        return self.mailArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 40;
    } else {
        return 90;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"mailcell";
    
    if (indexPath.section == 1) {
        KWMailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[KWMailListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        KWMailListFrame *listFrame = self.mailArray[indexPath.row];
        cell.mailListFrame = listFrame;
        
        if ([listFrame.mailModelList.Read isEqualToString:@"否"]) {
            cell.readView.hidden = NO;
        } else {
            cell.readView.hidden = YES;
        }
        
        if ([listFrame.isEdit isEqualToString:@"0"]) {
            cell.selectButton.hidden = YES;
        }
        
        if ([listFrame.isEdit isEqualToString:@"1"]) {
            cell.selectButton.hidden = NO;
        }
        
        if ([listFrame.isSelect  isEqualToString: @"yes"]) {
            cell.selectButton.selected = YES;
        } else if ([listFrame.isSelect isEqualToString:@"no"]) {
            cell.selectButton.selected = NO;
        }
        cell.selectButton.tag = indexPath.row + 1000;
        
        [cell.selectButton addTarget:self action:@selector(selectBtned:) forControlEvents:UIControlEventTouchUpInside];
        
        if (listFrame.mailModelList.TopTime) {
            cell.topView.hidden = NO;
        } else {
            cell.topView.hidden = YES;
        }
        
        if (listFrame.mailModelList.star) {
            cell.starView.hidden = NO;
        } else {
            cell.starView.hidden = YES;
        }
        
        if (listFrame.mailModelList.redflag) {
            cell.flagView.hidden = NO;
        } else {
            cell.flagView.hidden = YES;
        }
        
        if ([listFrame.mailModelList.AccCount isEqualToString:@"0"]) {
            cell.attachmentView.hidden = YES;
        } else {
            cell.attachmentView.hidden = NO;
        }
        
        cell.delegate = self; //optional
        __weak KWMailHomeViewController *weakSelf = self;
        //configure right buttons
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"删除-2x"] backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
            NSIndexPath * indexPath = [weakSelf.mailTableView indexPathForCell:sender];
            [weakSelf deleteMail:indexPath];
            return NO; }],
                              [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"红旗-2x"]
                                             backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
                                                 [weakSelf flagMail:indexPath];
                                                 cell.flagView.hidden = NO;
                                                 [cell hideSwipeAnimated:YES];
                                                 return NO;
                                             }],
                              [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"标星-2x"]backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsMake(0, 15, 0, 15) callback:^BOOL(MGSwipeTableCell *sender) {
                                  NSIndexPath * indexPath = [weakSelf.mailTableView indexPathForCell:sender];
                                  [weakSelf starMail:indexPath];
                                  cell.starView.hidden = NO;
                                  [cell hideSwipeAnimated:YES];
                                  return NO;
                              }],
                              [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"置顶-2x"] backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
                                  NSIndexPath * indexPath = [weakSelf.mailTableView indexPathForCell:sender];
                                  [weakSelf topMail:indexPath];
                                  cell.topView.hidden = NO;
                                  [cell hideSwipeAnimated:YES];
                                  return NO;
                              }],
                              [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"邮件移动-2x"] backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
                                  NSIndexPath * indexPath = [weakSelf.mailTableView indexPathForCell:sender];
                                  [weakSelf removeMail:indexPath];
                                  [cell hideSwipeAnimated:YES];
                                  return NO; }],
                              [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"已处理-2x"] backgroundColor:RGBCOLOR(242,242,242) insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
                                  NSIndexPath *indexPath = [weakSelf.mailTableView indexPathForCell:sender];
                                  [weakSelf welldone:indexPath];
                                  [cell hideSwipeAnimated:YES];
                                  return NO; }],
                              ];
        cell.rightSwipeSettings.transition = MGSwipeTransitionClipCenter;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
     
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"folder"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"folder"];
        }
        KWMailUserFolder *folder = self.showFolder[indexPath.row];
        cell.textLabel.text = folder.name;
        cell.imageView.image = [UIImage imageNamed:@"大文件夹"];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        return cell;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 10)];
    view.backgroundColor = RGBACOLOR(240, 240, 240, 1);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [self setupBadge];
    if (indexPath.section == 1) {
        
        KWMailListFrame *listFrame = self.mailArray[indexPath.row];
        KWMailListModel *mailModel = listFrame.mailModelList;
        mailModel.Read = @"是";
        
        NSIndexPath *indexPathed = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPathed,nil] withRowAnimation:UITableViewRowAnimationFade];
        
        KWMailDetailViewController *detailVc = [[KWMailDetailViewController alloc] init];
        detailVc.relist = ^(NSIndexPath *sender) {
            [self starMail:sender];
        };
        detailVc.deleteMail = ^(NSIndexPath *sender) {
            [self deleteMail:sender];
        };
        detailVc.topTheMail = ^(NSIndexPath *sender) {
            [self topMail:sender];
        };
        
        detailVc.redTheMail = ^(NSIndexPath *sender) {
            [self flagMail:sender];
        };
        
        
        [detailVc recieveTheMailListArray:mailModel AndAct:self.act];
        [detailVc reciiveTheMailList:self.mailListArray andIndexPath:indexPath];
        [self.navigationController pushViewController:detailVc animated:YES];
        
    } else {
        
        KWMailUserFolder *model = self.showFolder[indexPath.row];
        NSString *idd = model.ID;
        NSString *folderid = [NSString stringWithFormat:@"f_%@_%@", self.mailBaseID, idd];
        NSMutableArray *temp = [NSMutableArray array];
        [self getMailListWithAct:model.ID];
        for (KWMailUserFolder *folder in self.UserFolder) {
            if ([folder.mailboxId isEqualToString:self.mailBaseID] && [folder.parentid isEqualToString:folderid]) {
                [temp addObject:folder];
                NSLog(@"folderBoxid%@",folder.ID);
            }
            self.showFolder = [NSMutableArray array];
            self.showFolder = temp;
        }
        [self.mailTableView reloadData];
        
    }
}

#pragma mark Swipe Delegate

- (void)selectBtned:(UIButton *)sender {
    NSInteger index = sender.tag - 1000;
    
    [self.manageArray addObject:self.mailArray[index]];
    
    KWMailListFrame *frame = self.mailArray[index];
    KWMailListModel *model = frame.mailModelList;

    [self.readArray addObject:model.Id];
    NSLog(@"wwww%@",self.readArray);
    
    
    NSLog(@"%ld",(long)index);
    
    if (sender.selected) {
        sender.selected = NO;
        NSInteger index = sender.tag - 1000;
        KWMailListFrame *frame = self.mailArray[index];
        frame.isSelect = @"no";
    } else {
        sender.selected = YES;
        NSInteger index = sender.tag - 1000;
        KWMailListFrame *frame = self.mailArray[index];
        frame.isSelect = @"yes";
        
    }
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:index inSection:1];
    [_mailTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteMail:(NSIndexPath *)index {
    KWMailListFrame *listFrame = self.mailArray[index.row];
    
    KWMailListModel *model = listFrame.mailModelList;
    NSLog(@"aaaa%@",model.Box);
    
    if ([model.Box isEqualToString:@"LJX"]) {
        NSLog(@"垃圾箱");
        NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/move",USER_SERVERADDRESS];
        KWMailRemoveParam *param = [KWMailRemoveParam param];
        param.mailboxid = model.MailBoxId;
        param.mailid = model.Id;
        param.box = @"YSC";
        [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
        } failure:^(NSError *error) {
            
        }];
        
    }
    
    if ([model.Box isEqualToString:@"YSC"] || [model.Box isEqualToString:@"LJX"]) {
        
        NSLog(@"删除");
        
    } else {
        NSLog(@"aaaa Other");
        NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/move",USER_SERVERADDRESS];
        KWMailRemoveParam *param = [KWMailRemoveParam param];
        param.mailboxid = model.MailBoxId;
        param.mailid = model.Id;
        param.box = @"LJX";
        NSLog(@"aaaaa%@",model.MailBoxId);
        [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
            NSLog(@"%@",json[@"back"]);
        } failure:^(NSError *error) {
            NSLog(@"fail");
        }];
    }
    
    NSMutableArray * temArray = [NSMutableArray arrayWithArray:self.mailArray];
    [temArray removeObjectAtIndex:index.row];
    self.mailArray = [NSMutableArray arrayWithArray:temArray];
    
    [self.mailTableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];

}

- (void)flagMail:(NSIndexPath *)indexPath {
    
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    NSString *flag = listFrame.mailModelList.redflag;
    NSString *mailID = listFrame.mailModelList.Id;
    NSLog(@"%@",flag);
    if (flag) {
        NSLog(@"红旗");
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postFlagTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
            [SVProgressHUD showInfoWithStatus:resultData];
            [SVProgressHUD dismissWithDelay:1];
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postFlagTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
            [SVProgressHUD showInfoWithStatus:resultData];
            [SVProgressHUD dismissWithDelay:1];
        } failure:^(NSError *erroe) {
            
        }];
    }
}

- (void)starMail:(NSIndexPath *)indexPath {
    
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    NSString *star = listFrame.mailModelList.star;
    NSString *mailID = listFrame.mailModelList.Id;
    if (star) {
        NSLog(@"星星");
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postStarTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
//            [self starMailWith:indexPath];
            [SVProgressHUD showInfoWithStatus:resultData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postStarTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
            [SVProgressHUD showInfoWithStatus:resultData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *erroe) {
            
        }];
    }
}

- (void)starMailWith:(NSIndexPath *)index {
//    KWMailListFrame *listFrame = self.mailArray[index.row];
//    listFrame.mailModelList.star = @"";
//    NSLog(@"eeee%@",listFrame.mailModelList.star);
//    [self.mailTableView reloadData];
}

- (void)topMail:(NSIndexPath *)indexPath {
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    NSString *top = listFrame.mailModelList.TopTime;
    NSString *mailID = listFrame.mailModelList.Id;
    if (top) {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postTopTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
            [SVProgressHUD showInfoWithStatus:resultData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postTopTheMailWith:param success:^(NSString *resultData) {
            [self refreshTheTableWith:self.act andBoxId:self.boxId];
            [SVProgressHUD showInfoWithStatus:resultData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } failure:^(NSError *erroe) {
            
        }];
    }
}

- (void)welldone:(NSIndexPath *)indexPath {
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = listFrame.mailModelList.Id;
    NSLog(@"zzz%@",param.MailId);
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/ISCL",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        [SVProgressHUD showInfoWithStatus:@"已处理"];
        [SVProgressHUD dismissWithDelay:0.5];
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
    
    NSLog(@"%@",self.mailACT);
    
}

- (void)removeMail:(NSIndexPath *)index {
    KWMailListFrame *listFrame = self.mailArray[index.row];
    NSString *mailid = listFrame.mailModelList.Id;
    KWMailReMoveView *remailView = [[KWMailReMoveView alloc] initWithFrame:CGRectMake(25, 45, KWContentViewWidth, KWContentViewHeight)];
    [remailView receTheMailId:mailid];
    [remailView showAnimated];
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end









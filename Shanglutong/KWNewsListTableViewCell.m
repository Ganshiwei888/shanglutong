//
//  KWNewsListTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWNewsListTableViewCell.h"
#import "KWNewsListModel.h"
#import "KWNewsListFrame.h"
#import "Utility.h"
#import "NSString+KWCreateTime.h"

@interface KWNewsListTableViewCell ()

@property (weak, nonatomic) UILabel *detailLabel;
@property (weak, nonatomic) UILabel *comeLabel;
@property (weak, nonatomic) UILabel *menuNoLabel;
@property (weak, nonatomic) UILabel *timeLabel;

@end

@implementation KWNewsListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupCellView];
    }
    return self;
}

- (void)setupCellView {

    UIImageView *typeImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:typeImageView];
    self.typeImageView = typeImageView;
    
    UIImageView *readImageView = [[UIImageView alloc] init];
    readImageView.hidden = YES;
    [self.contentView addSubview:readImageView];
    self.readImageView = readImageView;
    
    UILabel *detailLabel = [[UILabel alloc] init];
    detailLabel.font = [UIFont systemFontOfSize:14];
    detailLabel.textColor = [UIColor blackColor];
    detailLabel.numberOfLines = 0;
    
    [self.contentView addSubview:detailLabel];
    self.detailLabel = detailLabel;
    
    UILabel *comeLabel = [[UILabel alloc] init];
    comeLabel.font = [UIFont systemFontOfSize:10];
    comeLabel.textColor = [UIColor darkGrayColor];
    [self.contentView addSubview:comeLabel];
    self.comeLabel = comeLabel;
    
    UILabel *menuNoLabel = [[UILabel alloc] init];
    menuNoLabel.font = [UIFont systemFontOfSize:10];
    menuNoLabel.textColor = [UIColor darkGrayColor];
    [self.contentView addSubview:menuNoLabel];
    self.menuNoLabel = menuNoLabel;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont systemFontOfSize:10];
    timeLabel.textColor = [UIColor darkGrayColor];
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
}

- (void)setListFrame:(KWNewsListFrame *)listFrame {
    
    _listFrame = listFrame;
    KWNewsListModel *listModel = listFrame.listModel;
    
    self.readImageView.image = [UIImage imageNamed:@"未读"];
    
//    self.detailLabel.text = listModel.Html;
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[listModel.Html dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    self.detailLabel.attributedText = attrStr;
    self.detailLabel.text = listModel.Html;

    NSDictionary *attrs = @{NSFontAttributeName : self.detailLabel.font};
    CGSize maxSize = CGSizeMake(310, MAXFLOAT);
    CGSize size = [listModel.Html boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    NSLog(@"%f",size.height);
    self.detailLabel.frame = CGRectMake(60, 12, 290, size.height);

    
    self.comeLabel.text = @"来源";
    
    NSString *menuStr = listModel.MenuNo;
    
    
    if ([menuStr isEqualToString:@"AA"]) {
        menuStr = @"邮件";
        self.typeImageView.image = [UIImage imageNamed:@"邮件-2x"];
    }
    if ([menuStr isEqualToString:@"AB"]) {
        menuStr = @"客户";
        self.typeImageView.image = [UIImage imageNamed:@"客户-2x"];
    }
    if ([menuStr isEqualToString:@"AC"]) {
        menuStr = @"商机";
        self.typeImageView.image = [UIImage imageNamed:@"商机-2x"];
    }
    if ([menuStr isEqualToString:@"AD"]) {
        menuStr = @"产品";
        self.typeImageView.image = [UIImage imageNamed:@"产品-2x"];
    }
    if ([menuStr isEqualToString:@"AF"]) {
        menuStr = @"工作";
    }
    if ([menuStr isEqualToString:@"AG"]) {
        menuStr = @"报价";
        self.typeImageView.image = [UIImage imageNamed:@"询价-2x"];
    }
    if ([menuStr isEqualToString:@"AH"]) {
        menuStr = @"寄样";
    }
    if ([menuStr isEqualToString:@"AI"]) {
        menuStr = @"询价";
        self.typeImageView.image = [UIImage imageNamed:@"询价-2x"];
    }
    if ([menuStr isEqualToString:@"AJ"]) {
        menuStr = @"文档";
        self.typeImageView.image = [UIImage imageNamed:@"文档-2x"];
    }
    if ([menuStr isEqualToString:@"AM"]) {
        menuStr = @"海关";
    }
    if ([menuStr isEqualToString:@"AN"]) {
        menuStr = @"待办";
        self.typeImageView.image = [UIImage imageNamed:@"待办-2x"];
    }
    if ([menuStr isEqualToString:@"AO"]) {
        menuStr = @"BI";
    }
    if ([menuStr isEqualToString:@"ZA"]) {
        menuStr = @"页面";
    }
    if ([menuStr isEqualToString:@"AV"]) {
        menuStr = @"评论";
        self.typeImageView.image = [UIImage imageNamed:@"动态-2x"];
    }
    
    self.menuNoLabel.text = menuStr;
    
    
    NSString *current = [listModel.CreateTm substringToIndex:19];
    
    self.timeLabel.text = current;
    
    
    listFrame.cellHeight = self.detailLabel.frame.size.height + 48;
    
    self.readImageView.frame = CGRectMake(8, listFrame.cellHeight / 2 - 5, 10, 10);
    self.typeImageView.frame = CGRectMake(22, listFrame.cellHeight / 2 - 14, 28, 28);
    self.comeLabel.frame = CGRectMake(60, CGRectGetMaxY(self.detailLabel.frame) + 12, 24, 16);
    self.menuNoLabel.frame = CGRectMake(CGRectGetMaxX(self.comeLabel.frame) + 4, CGRectGetMaxY(self.detailLabel.frame) + 12, 24, 16);
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.menuNoLabel.frame), CGRectGetMaxY(self.detailLabel.frame) + 12, 120, 16);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

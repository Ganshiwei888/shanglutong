//
//  KWCustomer.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "Utility.h"
#import "KWCustomer.h"

@implementation KWCustomer

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _customerNO = checkStringNull(dict[@"No"]);
        _customerSale = checkStringNull(dict[@"Sale"]);
        _customerSName = checkStringNull(dict[@"SName"]);
        _customerType = checkStringNull(dict[@"ClientType"]);
        _customerTime = checkStringNull(dict[@"RDate"]);
        
        if (dict[@"Pf"] == NULL) {
            _customerWarn = @"0";
        } else {
            NSNumber *str = dict[@"Pf"];
            _customerWarn = [NSString stringWithFormat:@"%@", str];
        }
    }
    return self;
}

+ (instancetype)customerWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

@end
/*
 
 Address = "";
 Bak = "";
 BranchId = 0;
 City = "";
 ClientClass = "A\U7ea7";
 ClientLevel = "\U2605\U2605";
 ClientLock = 0;
 ClientSource = "Alibaba\U963f\U91cc\U5df4\U5df4\U8be2\U76d8";
 ClientState = "\U6f5c\U5728\U5ba2\U6237";
 ClientType = "\U4fe1\U4fdd\U5ba2\U6237";
 Country = Afghanistan;
 Credit = "";
 DeptId = 24;
 EDM = "<null>";
 Fax = "";
 Forwarder = "";
 Id = 29230;
 Name = 33afdsf;
 No = AF17070007;
 Pf = 30;
 PostCode = "";
 Province = "";
 RDate = "2017-07-25T05:27:00.087";
 SDate = "<null>";
 SName = 333;
 Sale = "\U7ba1\U7406\U5458";
 Tel = "";
 UserType = 0;
 Web = 33;
 Yj = 0;
 userid = "<null>";
 
 */

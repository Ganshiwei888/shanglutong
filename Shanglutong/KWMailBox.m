//
//  KWMailBox.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailBox.h"
#import "Utility.h"

@implementation KWMailBox

@end

@implementation KWMailBoxModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _email = checkStringNull(dict[@"email"]);
        _mailID = checkStringNull(dict[@"id"]);
        _name = checkStringNull(dict[@"name"]);
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

@end

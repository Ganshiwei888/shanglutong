//
//  KWMailRemoveCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailRemoveCell.h"
#import "Utility.h"

@implementation KWMailRemoveCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupContentView];
    }
    return self;
}

- (void)setupContentView {
    
    UIButton *selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setFrame:CGRectMake(8, 5, 30, 30)];
    [selectBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
    [selectBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
    [selectBtn addTarget:self action:@selector(selectBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:selectBtn];
    self.selectBtn = selectBtn;
    
    UILabel *folderName = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 160, 20)];
    folderName.textColor = [UIColor blackColor];
    folderName.font = [UIFont systemFontOfSize:13];
    folderName.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:folderName];
    self.folderName = folderName;
    
}

- (void)selectBtn:(UIButton *)sender {
    [self.delegate tableCellBtndidSelect:sender];
}

- (void)setMailRemove:(KWMailRemove *)mailRemove {
    
    _mailRemove = mailRemove;
    
    self.selectBtn.hidden = mailRemove.isSelect;
    
    self.folderName.text = mailRemove.folderName;
    
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

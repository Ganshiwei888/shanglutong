//
//  KWAddressPeoViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/7.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressPeoViewController.h"
#import "KWUserAllMenu.h"
#import "KWTalkMessageViewController.h"
#import "Utility.h"
#import "KWMailSendViewController.h"
#import <UIImageView+WebCache.h>

@interface KWAddressPeoViewController ()

@property (nonatomic, strong) UIImageView *userImage;

@property (nonatomic, strong) UILabel *userLable;

@property (nonatomic, strong) UILabel *dutyLabel;

@property (nonatomic, weak) UIButton *chatButton;

@property (nonatomic, weak) UIButton *teleButton;

@property (nonatomic, weak) UIButton *emailButton;

@property (nonatomic, copy) NSString *userName;


@end

@implementation KWAddressPeoViewController

- (UIImageView *)userImage {
    if (!_userImage) {
        _userImage = [[UIImageView alloc] initWithFrame:CGRectMake((KWSCREEN_WIDTH / 2) - 30, 72, 60, 60)];
        _userImage.layer.masksToBounds = YES;
        _userImage.layer.cornerRadius = 30;
        [self.view addSubview:_userImage];
    }
    return _userImage;
}

- (UILabel *)userLable {
    if (!_userLable) {
        _userLable = [[UILabel alloc] initWithFrame:CGRectMake((KWSCREEN_WIDTH / 2) - 50, 140, 100, 20)];
        _userLable.textColor = [UIColor blackColor];
        _userLable.font = [UIFont systemFontOfSize:18];
        _userLable.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_userLable];
    }
    return _userLable;
}

- (UILabel *)dutyLabel {
    if (!_dutyLabel) {
        _dutyLabel = [[UILabel alloc] initWithFrame:CGRectMake((KWSCREEN_WIDTH / 2) - 50, 168, 100, 19)];
        _dutyLabel.textColor = [UIColor grayColor];
        _dutyLabel.font = [UIFont systemFontOfSize:16];
        _dutyLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_dutyLabel];
    }
    return _dutyLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"同事详情";
    self.view.backgroundColor = RGBCOLOR(252, 252, 252);
    
    
}

- (void)setupView {
    
    self.userLable.text = self.allMenu.username;
    
    self.dutyLabel.text = self.allMenu.duty;
    
    NSString *imageURL = [NSString stringWithFormat:@"http://%@/picimage/%@",USER_SERVERADDRESS, _allMenu.PicName];
    
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageURL]];
    
    UIButton *chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [chatButton setFrame:CGRectMake(4, 200, KWSCREEN_WIDTH - 8, 40)];
    [chatButton setBackgroundColor:RGBCOLOR(249, 249, 249)];
    [chatButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [chatButton setTitle:@"聊天" forState:UIControlStateNormal];
    [chatButton addTarget:self action:@selector(chatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    chatButton.layer.masksToBounds = YES;
    chatButton.layer.borderWidth = 2;
    chatButton.layer.borderColor = RGBCOLOR(240, 240, 240).CGColor;
    [self.view addSubview:chatButton];
    self.chatButton = chatButton;
    
    if (self.allMenu.mobile.length > 0) {
        UIButton *teleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [teleButton setFrame:CGRectMake(4, 242, KWSCREEN_WIDTH - 8, 40)];
        [teleButton setTitle:self.allMenu.mobile forState:UIControlStateNormal];
        [teleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [teleButton setBackgroundColor:RGBCOLOR(249, 249, 249)];
        [teleButton addTarget:self action:@selector(teleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        teleButton.layer.masksToBounds = YES;
        teleButton.layer.borderWidth = 2;
        teleButton.layer.borderColor = RGBCOLOR(240, 240, 240).CGColor;
        [self.view addSubview:teleButton];
        self.chatButton = chatButton;
        if (self.allMenu.email.length > 0) {
            UIButton *emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [emailButton setFrame:CGRectMake(4, 284, KWSCREEN_WIDTH - 8, 40)];
            [emailButton setTitle:self.allMenu.email forState:UIControlStateNormal];
            [emailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [emailButton setBackgroundColor:RGBCOLOR(247, 247, 247)];
            [emailButton addTarget:self action:@selector(emailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            emailButton.layer.masksToBounds = YES;
            emailButton.layer.borderWidth = 2;
            emailButton.layer.borderColor = RGBCOLOR(240, 240, 240).CGColor;
            [self.view addSubview:emailButton];
            self.emailButton = emailButton;
        }
    } else {
        if (self.allMenu.email.length > 0) {
            UIButton *emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [emailButton setFrame:CGRectMake(4, 242, KWSCREEN_WIDTH - 8, 40)];
            [emailButton setTitle:self.allMenu.email forState:UIControlStateNormal];
            [emailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [emailButton setBackgroundColor:RGBCOLOR(247, 247, 247)];
            [emailButton addTarget:self action:@selector(emailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            emailButton.layer.masksToBounds = YES;
            emailButton.layer.borderWidth = 2;
            emailButton.layer.borderColor = RGBCOLOR(240, 240, 240).CGColor;
            [self.view addSubview:emailButton];
            self.emailButton = emailButton;
        }
    }
    
}

#pragma mark - Button Click -

- (void)chatButtonClick:(UIButton *)sender  {

    [[EMClient sharedClient].chatManager getConversation:self.allMenu.Hxid type:EMConversationTypeChat createIfNotExist:YES];
    KWTalkMessageViewController *chatController = [[KWTalkMessageViewController alloc] initWithConversationChatter:self.allMenu.Hxid conversationType:EMConversationTypeChat];
    chatController.title = self.allMenu.username;
    [self.navigationController pushViewController:chatController animated:YES];
    
}

- (void)teleButtonClick:(UIButton *)sender {
    if (self.allMenu.mobile.length > 0) {
        NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.allMenu.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
}

- (void)emailButtonClick:(UIButton *)sender {
    KWMailSendViewController *mailSendVC = [[KWMailSendViewController alloc] init];
//    [mailSendVC recieveTheMailDetailModel:nil AndAttachmentArray:nil AndType:MailSendTypeNew AndMailBoxId:@"11" AndHtmlInfo:nil];
    mailSendVC.replyTo = self.allMenu.email;
    UINavigationController *mailSendNavi = [[UINavigationController alloc] initWithRootViewController:mailSendVC];
    
    [self presentViewController:mailSendNavi animated:YES completion:nil];
}

- (void)receTheAddress:(KWUserAllMenu *)allMenu {
    self.allMenu = allMenu;
    self.userName = allMenu.username;
    NSLog(@"%@",_allMenu.username);
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

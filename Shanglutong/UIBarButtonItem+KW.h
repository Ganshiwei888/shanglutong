//
//  UIBarButtonItem+KW.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (KW)

+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon target:(id)target action:(SEL)action;

@end

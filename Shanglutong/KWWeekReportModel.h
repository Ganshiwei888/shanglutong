//
//  KWWeekReportModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
//BranchId = "<null>";
//CommCount = 2;
//CommReadUID = ",1,";
//CreateTm = "2017-08-08T10:47:13.877";
//Creater = admin;
//CreaterName = "\U7ba1\U7406\U5458";
//DeptId = 24;
//Html = "\U597d\U50cf\U6709\U5f88\U591a\U95ee\U9898";
//RepEndDate = "2017-08-13T00:00:00";
//RepStartDate = "2017-08-07T00:00:00";
//Type = 2;
//UpdateTm = "2017-08-09T10:09:24.023";
//Updater = admin;
//UpdaterName = "\U7ba1\U7406\U5458";
//id = 174;
@interface KWWeekReportModel : NSObject

@property (nonatomic, copy) NSString *BranchId;
@property (nonatomic, copy) NSString *CommCount;
@property (nonatomic, copy) NSString *CommReadUID;
@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *DeptId;
@property (nonatomic, copy) NSString *Html;
@property (nonatomic, copy) NSString *RepEndDate;
@property (nonatomic, copy) NSString *RepStartDate;
@property (nonatomic, copy) NSString *Type;
@property (nonatomic, copy) NSString *UpdateTm;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *Updater;
@property (nonatomic, copy) NSString *UpdaterName;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end









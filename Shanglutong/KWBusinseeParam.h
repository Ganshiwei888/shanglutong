//
//  KWBusinseeParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWBusinseeParam : KWBaseParam

@property (nonatomic, copy) NSString *Pageindex;

@property (nonatomic, copy) NSString *Pagemax;

@property (nonatomic, copy) NSString *Act;


@end

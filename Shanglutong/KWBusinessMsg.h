//
//  KWBusinessMsg.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWBusinessMsg : NSObject

@property (nonatomic, assign) NSInteger AutoClose;

@property (nonatomic, copy) NSString *CreatTmN;

@property (nonatomic, copy) NSString *CreateTm;

@property (nonatomic, copy) NSString *Creater;

@property (nonatomic, copy) NSString *Html;

@property (nonatomic, assign) NSInteger IsRead;


@end

//AutoClose = 1;
//CreatTmN = "2017/9/14 20:39:48";
//CreateTm = "2017-09-14T20:39:48.763";
//Creater = "-1";
//CreaterName = "";
//ExId = NEWMAIL;
//ExId1 = "";
//Html = "\U5546\U4e1a\U90ae\U7bb1(sales@salet.net.cn) \U6536\U5230\U65b0\U90ae\U4ef6!";
//Id = 1392;
//IsRead = 1;
//IsShow = 1;
//KeyId = 15;
//MenuNo = AA;
//Type = 2;

//
//  KWMailLeftLabelCell.m
//  Shanglutong
//
//  Created by ganshiwei on 2017/11/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailLeftLabelCell.h"

@implementation KWMailLeftLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.label.layer.cornerRadius = 8.0f;
    self.label.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.label.layer.borderColor = [UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0].CGColor;
        self.label.layer.borderWidth = 2;
        self.label.textColor = [UIColor colorWithRed:0/255.0 green:141/255.0 blue:221/255.0 alpha:1.0];
    } else {
        self.label.layer.borderWidth = 0;
        self.label.textColor = [UIColor lightGrayColor];
    }
    
}

@end

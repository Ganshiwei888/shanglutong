//
//  KWMailBoxTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWMailBoxParam.h"
#import "KWMailBoxResult.h"
#import "KWMailBoxChildResult.h"

@interface KWMailBoxTool : NSObject

/**
 得到用户的邮件文件夹

 @param param post参数
 @param success 请求成功的回调
 @param failure 请求失败的回调
 */
+ (void)postGetUserMailBox:(KWMailBoxParam *)param success:(void(^)(KWMailBoxResult *resultData))success failure:(void(^)(NSError *error))failure;


/**
 得到用户的邮件子文件夹

 @param param post参数
 @param success 请求成功回调
 @param failure 请求失败回调
 */
+ (void)postGetUserMailBoxChild:(KWMailBoxParam *)param success:(void(^)(KWMailBoxChildResult *resultData))success failure:(void(^)(NSError *error))failure;

@end












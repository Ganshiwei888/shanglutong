//
//  TabBarVC.m
//  servuu
//
//  Created by Ming Tian on 2017/3/31.
//  Copyright © 2017年 Ming Tian. All rights reserved.
//

#import "KWHomeViewController.h"
#import "KWMailHomeViewController.h"
#import "KWMailLeftViewController.h"
#import "KWMineViewController.h"
#import "KWAddressViewController.h"
#import "KWCustomerViewController.h"
#import "KWCustomerLeftViewController.h"
#import "KWNavigationController.h"
#import "KWMoreViewController.h"
#import "KWWorkBenchViewController.h"
#import "KWTalkViewController.h"
#import "UIImage+KW.h"
#import "ConversationListController.h"
#import "KWTabBar.h"
#import "KWLoginViewController.h"
#import "AppDelegate.h"
#import "KGModal.h"
#import <MMDrawerController.h>
#import "TabBarVC.h"

@interface TabBarVC ()<UITabBarControllerDelegate,EMClientDelegate>

@property(nonatomic, strong) UIButton *plusButton;

@end

@implementation TabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    [self creatViewControllers];
    [self setupTarBar];
    self.selectedIndex = 1;
    
    EMOptions *options = [EMOptions optionsWithAppkey:@"dandan9609#salet"];
    options.apnsCertName = @"";
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    
    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
}

- (void)autoLoginDidCompleteWithError:(EMError *)error {
    
}

- (void)userAccountDidLoginFromOtherDevice {
    
    NSString *message = @"账号已在其他设备登录";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        [self logout];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    [alert addAction:action];
    [alert addAction:cancel];
    [self showDetailViewController:alert sender:nil];
    
}

- (void)logout {
    
    KWLoginViewController *vc = [[KWLoginViewController alloc] init];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    window.rootViewController = vc;
    [window makeKeyAndVisible];
//    [self presentViewController:vc animated:YES completion:^{
//
//    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)plusButtonClick:(UIButton *)sender {
    KWMoreViewController *moreVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"moreVC"];
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentViewController:moreVC andAnimated:YES];
}

- (void)setupTarBar {
    
    CGFloat centerX;
    CGFloat centerY;
    
    centerX = self.tabBar.frame.size.width / 2.0;
    centerY = self.tabBar.frame.size.height / 2.0;
    
    self.plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.plusButton.backgroundColor = [UIColor redColor];
    [self.plusButton setFrame:CGRectMake(centerX, centerY, 44, 44)];
    [self.plusButton addTarget:self action:@selector(plusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)creatViewControllers {
    
    KWTalkViewController *talk = [[KWTalkViewController alloc] init];
    talk.tabBarItem.tag = 0;
    [self addChildViewController:talk title:@"会话"imageName:@"会话" selectedImageName:@"会话_sel"];
    
    KWWorkBenchViewController *work = [[KWWorkBenchViewController alloc] init];
    work.tabBarItem.tag = 1;
    [self addChildViewController:work title:@"工作台" imageName:@"工作台" selectedImageName:@"工作台_sel"];
    
    UIViewController * addVC = [[UIViewController alloc]init];
    addVC.tabBarItem.tag = 2;
    addVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [self addChildViewController:addVC title:@"" imageName:@"发布" selectedImageName:@"发布_sel"];
    
    
    KWAddressViewController *address = [KWAddressViewController sharedAddressVC];
    address.tabBarItem.tag = 3;
    [self addChildViewController:address title:@"通讯录" imageName:@"通讯录" selectedImageName:@"通讯录_sel"];
    
    KWMineViewController *mine = [[KWMineViewController alloc] init];
    mine.tabBarItem.tag = 4;
    [self addChildViewController:mine title:@"我的" imageName:@"我的" selectedImageName:@"我的_sel"];
}
/**
 *  封装添加vc的方法
 */
- (void)addChildViewController:(UIViewController *)childController title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    childController.tabBarItem.title = title;
    childController.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childController.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:childController];
    [self addChildViewController:nav];
    
    NSDictionary * detdic =@{NSForegroundColorAttributeName:[UIColor lightGrayColor]};
    [nav.tabBarItem setTitleTextAttributes:detdic forState:UIControlStateNormal];
    //改变颜色
    NSDictionary * normaldic =@{NSForegroundColorAttributeName:[UIColor colorWithRed:0/255.0 green:197/255.0 blue:225/255.0 alpha:1]};
    [nav.tabBarItem setTitleTextAttributes:normaldic forState:UIControlStateNormal];
    
    NSDictionary * seledic =@{NSForegroundColorAttributeName:[UIColor colorWithRed:97/255.0 green:93/255.0 blue:92/255.0 alpha:1]};
    [nav.tabBarItem setTitleTextAttributes:seledic forState:UIControlStateSelected];
    
//    if([childController isKindOfClass:[MainVC class]]) {
//        childController.tabBarItem.title = nil;
//        childController.tabBarItem.imageInsets=UIEdgeInsetsMake(-6, 0, 6, 0);//根据需要自动调整
//    }else {
//        childController.tabBarItem.title = nil;
//        childController.tabBarItem.imageInsets=UIEdgeInsetsMake(6, 0,-6, 0);//根据需要自动调整
//    }
}

#pragma tabbarController delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if(viewController == self.viewControllers[2]) {
        // 点击中间按钮后操作
        [self plusButtonClick:nil];
        return NO;
    }
    
//    TMHNavigationController *dnav = (TMHNavigationController *)self.viewControllers[self.selectedIndex];
//    TMHNavigationController *cnav = (TMHNavigationController *)viewController;
//    if([dnav.viewControllers[0] isEqual:cnav.viewControllers[0]]) {
//        [dnav popToRootViewControllerAnimated:YES];
//    }else {
//        [dnav popToRootViewControllerAnimated:NO];
//    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

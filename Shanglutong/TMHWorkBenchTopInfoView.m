//
//  TMHWorkBenchTopInfoView.m
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/19.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "TMHWorkBenchTopInfoView.h"

@implementation TMHWorkBenchTopInfoView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 5;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

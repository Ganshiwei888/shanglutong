//
//  KWTabBarButton.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWTabBarButton.h"
#import "KWBadgeButton.h"
#import "Utility.h"

@interface KWTabBarButton ()

/**
 BadgeValue
 */
@property (nonatomic, weak) KWBadgeButton *badgeButton;

@end

@implementation KWTabBarButton

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:10];
        
        [self setTitleColor: [UIColor grayColor] forState:UIControlStateNormal];
        [self setTitleColor: [UIColor blueColor] forState:UIControlStateSelected];
        
        //添加一个提醒数字
        
        KWBadgeButton *badgeButton = [[KWBadgeButton alloc] init];
        badgeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:badgeButton];
        self.badgeButton = badgeButton;
        
    }
    return self;
}

/**
 重写去掉高亮状态
 
 @param highlighted 高亮
 */
- (void)setHighlighted:(BOOL)highlighted {
    
}

/**
 内部图片的frame
 
 @return 返回图片的frame
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height * KWTabBarButtonImageRatio;
    
    return CGRectMake(0, 5, imageW, imageH);
    
}

/**
 内部文字的frame
 
 @param contentRect 文字的contentRect
 @return 文字的frame
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
    CGFloat titleX = 0;
    CGFloat titleY = contentRect.size.height * KWTabBarButtonImageRatio;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}

/**
 设置item
 
 @param item UITarBarItem
 */
- (void)setItem:(UITabBarItem *)item {
    
    _item = item;
    
    NSLog(@"%@",item.title);
    
    [item addObserver:self forKeyPath:@"badgeValue" options:0 context:nil];
    [item addObserver:self forKeyPath:@"title" options:0 context:nil];
    [item addObserver:self forKeyPath:@"image" options:0 context:nil];
    [item addObserver:self forKeyPath:@"selectedImage" options:0 context:nil];
    
    [self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
}

/**
 释放
 */
- (void)dealloc {
    [self.item removeObserver:self forKeyPath:@"badgeValue"];
    [self.item removeObserver:self forKeyPath:@"title"];
    [self.item removeObserver:self forKeyPath:@"image"];
    [self.item removeObserver:self forKeyPath:@"selectedImage"];
}

/**
 *  监听到某个对象的属性改变了,就会调用
 *
 *  @param keyPath 属性名
 *  @param object  哪个对象的属性被改变
 *  @param change  属性发生的改变
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    //设置标题
    [self setTitle:self.item.title forState:UIControlStateNormal];
    [self setTitle:self.item.title forState:UIControlStateSelected];
    [self setTitleColor:RGBCOLOR(32, 193, 220) forState:UIControlStateNormal];
    [self setTitleColor:RGBCOLOR(22, 57, 138) forState:UIControlStateSelected];
    //设置图片
    [self setImage:self.item.image forState:UIControlStateNormal];
    [self setImage:self.item.selectedImage forState:UIControlStateSelected];
    
    //设置提醒数字
    
    self.badgeButton.badgeValue = self.item.badgeValue;
    
    CGFloat badgeY = 5;
    CGFloat badgeX = self.frame.size.width - self.badgeButton.frame.size.width - 10;
    CGRect badgeF = self.badgeButton.frame;
    
    badgeF.origin.x = badgeX;
    badgeF.origin.y = badgeY;
    self.badgeButton.frame = badgeF;
    
}


@end




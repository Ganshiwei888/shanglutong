//
//  KWPbCustomerViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWPbCustomerViewController.h"
#import "KWPbCustomerLeftViewController.h"
#import "KWPbCustomerParam.h"
#import "KWCustomer.h"
#import "KWCustomerFrame.h"
#import "KWHttpTool.h"
#import "KWCustomerResult.h"
#import "KWCustomerTableViewCell.h"
#import "KWCustomerDetailViewController.h"
#import <MJRefresh.h>
#import <MJExtension.h>
#import <UIViewController+MMDrawerController.h>
#import "Utility.h"

@interface KWPbCustomerViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSArray *customerFrameS;


@end

@implementation KWPbCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"公海客户";
    self.view.backgroundColor = RGBCOLOR(225, 225, 225);
    [self setupNav];
    [self setupTableView];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getThePbCustomer:) name:@"getPbCustomer" object:nil];
    [self getPublicCustomer:@"ALL"];
}

- (void)setupNav {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menud"] style:UIBarButtonItemStylePlain target:self action:@selector(pushTheLeft:)];
    
    
}

- (void)setupTableView {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
}

- (void)getThePbCustomer:(NSNotification *)sender {
    
    NSLog(@"%@",sender.userInfo[@"pubClass"]);
    
    NSString *pubClass = sender.userInfo[@"pubClass"];
    NSLog(@"%@",pubClass);
    [self getPublicCustomer:pubClass];
}

- (void)getPublicCustomer:(NSString *)pubClass {
    
    KWPbCustomerParam *param = [KWPbCustomerParam param];
    
    self.title = @"公海客户";
    
    if ([pubClass isEqualToString:@"全部公海客户"]) {
        pubClass = @"ALL";
    }
    if ([pubClass isEqualToString:@"无分类客户"]) {
        pubClass = @"WFL";
    }
    
    param.pagemax = @"50";
    param.pageIndex = @"1";
    param.pubclass = pubClass;
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/client/Pubclass",USER_SERVERADDRESS];
    __weak KWPbCustomerViewController *weakSelf = self;
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        KWCustomerResult *customerResult =[KWCustomerResult mj_objectWithKeyValues:[json valueForKey:@"back"]];
        weakSelf.customerResult = customerResult;
        
        NSDictionary *dict = json[@"back"];
        NSArray *arrayRes = dict[@"list"];
        NSMutableArray *customerArray = [NSMutableArray array];
        for (NSDictionary *dict in arrayRes) {
            KWCustomer *customer = [[KWCustomer alloc] initWithDict:dict];
            [customerArray addObject:customer];
        }
        NSArray *customerModel = [NSArray array];
        customerModel = customerArray;
        
        NSMutableArray *customerFrameArray = [NSMutableArray array];
        for (KWCustomer *customer in customerModel) {
            KWCustomerFrame *customerFrame = [[KWCustomerFrame alloc] init];
            customerFrame.customer = customer;
            [customerFrameArray addObject:customerFrame];
        }
        self.customerFrameS = customerFrameArray;
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)pushTheLeft:(UIButton *)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource &&  UITableViewdelegete -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.customerFrameS.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KWCustomerTableViewCell *cell = [KWCustomerTableViewCell cellWithCustomer: tableView];
    
    cell.customerFrame = self.customerFrameS[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSUserDefaults standardUserDefaults]setObject:self.customerResult.list[indexPath.row].Id forKey:@"CustomIDNow"];
    
    NSLog(@"%@",self.customerResult.list[indexPath.row].Id);
    
    KWCustomerDetailViewController *detailVc =[[KWCustomerDetailViewController alloc]init];
    detailVc.model = self.customerResult;
    detailVc.num = indexPath.row;
    [detailVc initSetView];
    [self.navigationController pushViewController:detailVc animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

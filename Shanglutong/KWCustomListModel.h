//
//  KWCustomListModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utility.h"

@interface KWCustomListModel : NSObject

@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSString *DataClass;
@property (nonatomic,strong) NSString *DataType;
@property (nonatomic,strong) NSString *DataText;
@property (nonatomic,strong) NSString *DataValue1;
@property (nonatomic,strong) NSString *DataValue2;
@property (nonatomic,strong) NSString *DataValue3;
@property (nonatomic,strong) NSString *DataValue4;
@property (nonatomic,strong) NSString *DataValue5;
@property (nonatomic,strong) NSString *Sort;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)listWithDict:(NSDictionary *)dict;

@end

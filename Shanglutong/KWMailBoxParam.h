//
//  KWMailBoxParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWBaseParam.h"

@interface KWMailBoxParam : KWBaseParam

@property (nonatomic, strong) NSString *parentid;

//@property (nonatomic, copy) NSString *boxid;


@end

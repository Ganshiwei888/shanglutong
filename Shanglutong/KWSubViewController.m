//
//  KWSubViewController.m
//  Shanglutong
//
//  Created by ganshiwei on 2017/11/7.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWSubViewController.h"
#import "KWHttpTool.h"
#import "MBProgressHUD+XQ.h"
#import "KWLoginSub.h"
#import "Utility.h"
#import <MJExtension.h>
#import "KWUserAllMenu.h"
#import "ChineseString.h"
#import "pinyin.h"
#import "KWAddressNumCell.h"

@interface KWSubViewController () <UITableViewDelegate, UITableViewDataSource>
    
    @property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (nonatomic, strong) NSMutableArray *sectionHeadsKeys;
    @property (nonatomic, strong) NSMutableArray *dataArr;
    @property (nonatomic, strong) NSMutableArray *sortedArrForArrays;
    @property (nonatomic, strong) NSMutableArray <KWUserAllMenu *>  *userAddress;
    
@end

@implementation KWSubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"下属管理";
    _dataArr = [[NSMutableArray alloc] init];
    _sortedArrForArrays = [[NSMutableArray alloc] init];
    _sectionHeadsKeys = [[NSMutableArray alloc] init];
    [self setupTableView];
    [self loadSubList];
    
}
    
- (void)setupTableView {
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerClass:[KWAddressNumCell class] forCellReuseIdentifier:@"KWAddressNumCell"];
}
    
- (void)loadSubList {
    MBProgressHUD *HUD = [MBProgressHUD showMessage:@"加载中..." toView:self.view];
    KWBaseParam *param = [KWBaseParam new];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults boolForKey:ISSUBACCOUNT]) {
        NSString *owerUserId = [userDefaults objectForKey:OWERUSERID];
        NSString *owerPassword = [userDefaults objectForKey:OWERPASSWORD];
        param.UserId = owerUserId;
        param.Password =owerPassword;
    };
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/getsub",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        if ([json[@"back"] isKindOfClass:[NSArray class]]) {
         self.userAddress = [KWUserAllMenu mj_objectArrayWithKeyValuesArray:json[@"back"]];
        }
        
        for (KWUserAllMenu *model in self.userAddress) {
            NSLog(@"%@",[model decryptionSubPassword:model.Password]);
            NSString *name = model.userid;
            [_dataArr addObject:name];
        }
        
        self.sortedArrForArrays = [self getChineseStringArr:_dataArr];
        [self.tableView reloadData];
        [HUD hideAnimated:YES];
    } failure:^(NSError *error) {
        [HUD hideAnimated:YES];
    }];
    
}

#pragma mark - Table view data source
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sortedArrForArrays count];
    
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return  [[self.sortedArrForArrays objectAtIndex:section] count];
    
}
    
#pragma mark 设置组标题
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
        return [_sectionHeadsKeys objectAtIndex:section];
    }
    
#pragma mark 右侧索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
        return _sectionHeadsKeys;
 }
    
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KWAddressNumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KWAddressNumCell" forIndexPath:indexPath];
    NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
    if ([arr count] > indexPath.row) {
        ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
        for (int i = 0; i < self.userAddress.count; i++) {
            KWUserAllMenu *model = self.userAddress[i];
            NSString *userId = model.userid;
            if ([str.string isEqualToString:userId]) {
                cell.userPicimage.image = [UIImage imageNamed:@"用户"];
                cell.userid.text = model.username;
                cell.name.text = model.duty;
                cell.callButton.tag = i + 100;
                [cell.callButton setImage:[UIImage new] forState:UIControlStateNormal];
                [cell.callButton setTitle:@"切换账号" forState:UIControlStateNormal];
                [cell.callButton addTarget:self action:@selector(cellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                if (model.PicName.length > 2) {
                    NSString *imageURL = [NSString stringWithFormat:@"http://%@/picimage/%@",USER_SERVERADDRESS,model.PicName];
                    [cell.userPicimage sd_setImageWithURL:[NSURL URLWithString:imageURL]];
                }
            }
        }
        
    } else {
        NSLog(@"arr out of range");
    }
    return cell;
}
    
- (void)cellBtnClick:(UIButton *)sender {
    NSInteger index = sender.tag - 100;
    KWUserAllMenu *model = self.userAddress[index];
    
    NSString *subPassword = [model decryptionSubPassword:model.Password];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:ISSUBACCOUNT]) {
        NSString *owerUserId = [userDefaults objectForKey:USERID];
        NSString *owerPassword = [userDefaults objectForKey:PASSWORD];
        NSString *owerName = [userDefaults objectForKey:NAME];
        NSString *owerDe = [userDefaults objectForKey:DEPART];
        [userDefaults setObject:owerUserId forKey:OWERUSERID];
        [userDefaults setObject:owerPassword forKey:OWERPASSWORD];
        [userDefaults setObject:owerName forKey:OWERNAME];
        [userDefaults setObject:owerDe forKey:OWERDEPART];
    }
    [userDefaults setBool:YES forKey:ISSUBACCOUNT];
    [userDefaults setObject:subPassword forKey:PASSWORD];
    [userDefaults setObject:model.userid forKey:USERID];
    [userDefaults setObject:model.username forKey:NAME];
    [userDefaults setObject:model.duty forKey:DEPART];
    [userDefaults synchronize];
    [MBProgressHUD showSuccess:@"切换成功"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"switchAccount" object:@1];
    [self.navigationController popViewControllerAnimated:YES];
    
}
    
- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort {
        NSMutableArray *chineseStringsArray = [NSMutableArray array];
        for(int i = 0; i < [arrToSort count]; i++) {
            ChineseString *chineseString=[[ChineseString alloc]init];
            chineseString.string=[NSString stringWithString:[arrToSort objectAtIndex:i]];
            
            if(chineseString.string==nil){
                chineseString.string=@"";
            }
            
            if(![chineseString.string isEqualToString:@""]){
                //join the pinYin
                NSString *pinYinResult = [NSString string];
                for(int j = 0;j < chineseString.string.length; j++) {
                    NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                     pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                    
                    pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
                }
                chineseString.pinYin = pinYinResult;
            } else {
                chineseString.pinYin = @"";
            }
            [chineseStringsArray addObject:chineseString];
        }
        
        //sort the ChineseStringArr by pinYin
        NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
        [chineseStringsArray sortUsingDescriptors:sortDescriptors];
        
        
        NSMutableArray *arrayForArrays = [NSMutableArray array];
        BOOL checkValueAtIndex= NO;  //flag to check
        NSMutableArray *TempArrForGrouping = nil;
        
        for(int index = 0; index < [chineseStringsArray count]; index++)
        {
            ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
            NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
            NSString *sr= [strchar substringToIndex:1];
            NSLog(@"%@",sr);        //sr containing here the first character of each string
            if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
            {
                [_sectionHeadsKeys addObject:[sr uppercaseString]];
                TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
                checkValueAtIndex = NO;
            }
            if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
            {
                [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
                if(checkValueAtIndex == NO)
                {
                    [arrayForArrays addObject:TempArrForGrouping];
                    checkValueAtIndex = YES;
                }
            }
        }
        return arrayForArrays;
    }



@end

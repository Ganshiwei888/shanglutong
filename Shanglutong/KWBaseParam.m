//
//  KWBaseParam.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"
#import "KWUserDefaultsManager.h"
#import "Utility.h"

@implementation KWBaseParam

- (id)init {
    if (self = [super init]) {
        self.UserId = USER_ID;
        self.Password = USER_PASSWORD;
        self.Ran = RAN;
        self.Sign = SIGN;
    }
    return self;
}

+ (instancetype)param
{
    return [[self alloc] init];
}

- (id)copyWithZone:(NSZone *)zone {
    return @"copy 好烦，用runtime把-。-";
}


@end

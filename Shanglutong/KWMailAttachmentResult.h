//
//  KWMailAttachmentResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/12.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailAttachmentResult : NSObject

@property (nonatomic, strong) NSString *fileid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *size;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//
//  TMHWorkBenchTopInfoView.h
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/19.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMHWorkBenchTopInfoView : UIView

@property (weak, nonatomic) IBOutlet UILabel *flagLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;


@end

//
//  KWMomentAddViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/30.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMomentAddViewController.h"
#import "Utility.h"

@interface KWMomentAddViewController ()<UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, weak) UITextField *textField;
@property (nonatomic, weak) UITextView *textView;

@end

@implementation KWMomentAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"发布动态";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtn:)];
    
}

- (void)saveBtn:(UIButton *)sender {
    
}

- (void)setupView {
    
    [self setupTextField];
    [self setupTextView];
}

- (void)setupTextField {
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(16, 72, KWSCREEN_WIDTH - 32, 30)];
    textField.delegate = self;
    textField.placeholder = @"请输入标题:";
    [self.view addSubview:textField];
    self.textField = textField;
}

- (void)setupTextView {
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(self.textField.frame) + 8, KWSCREEN_WIDTH - 32, 260)];
    textView.delegate = self;
    textView.text = @"说点什么吧。。。";
    textView.textColor = [UIColor grayColor];
    [self.view addSubview:textView];
    self.textView = textView;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if([self.textView.text isEqualToString:@"说点什么吧。。。"]){
        self.textView.text=@"";
        self.textView.textColor=[UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if(self.textView.text.length < 1){
        self.textView.text = @"说点什么吧。。。";
        self.textView.textColor = [UIColor grayColor];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

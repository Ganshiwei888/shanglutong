//
//  NSString+KWCreateTime.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "NSString+KWCreateTime.h"
@implementation NSString (KWCreateTime)

+ (NSString *)currentDateStringFrom:(NSString *)dateString {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    //    format.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    format.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    //设置地区
    format.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    NSDate *Date = [format dateFromString:dateString];
    
    //获取当前时间
    NSDate *nowDate = [NSDate new];
    
    long nowTime = [nowDate timeIntervalSince1970];
    long weiboTime = [Date timeIntervalSince1970];
    long time = nowTime-weiboTime;
    if (time<60) { //一分钟内 显示刚刚
        return @"刚刚";
    } else if (time > 60 && time <= 3600){
        return [NSString stringWithFormat:@"%d分钟前",(int)time/60];
    } else if (time > 3600 && time < 3600*24){
        return [NSString stringWithFormat:@"%d小时前",(int)time/3600];
    } else if (time >= 3600*24 && time < 3600*24*7) {
        return [NSString stringWithFormat:@"%d天前", (int)time/3600/24];
    } else {//直接显示日期
        format.dateFormat = @"yyyy年MM月dd日";
        format.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh"];
        return  [format stringFromDate:Date];
    }
}

+ (NSString *)createDateStringToCreateTimeFromNow:(NSString *)dateString {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    //    format.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    format.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    //设置地区
    format.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    NSDate *Date = [format dateFromString:dateString];
    
    //获取当前时间
    NSDate *nowDate = [NSDate new];
    
    long nowTime = [nowDate timeIntervalSince1970];
    long weiboTime = [Date timeIntervalSince1970];
    long time = nowTime-weiboTime;
    if (time<60) { //一分钟内 显示刚刚
        return @"刚刚";
    } else if (time > 60 && time <= 3600){
        return [NSString stringWithFormat:@"%d分钟前",(int)time/60];
    } else if (time > 3600 && time < 3600*24){
        return [NSString stringWithFormat:@"%d小时前",(int)time/3600];
    } else if (time >= 3600*24 && time < 3600*24*7) {
        return [NSString stringWithFormat:@"%d天前", (int)time/3600/24];
    } else {//直接显示日期
        format.dateFormat = @"yyyy年MM月dd日";
        format.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh"];
        NSString *dateStr = [format stringFromDate:Date];
        NSString *strUrl1 = [dateStr stringByReplacingOccurrencesOfString:@"年" withString:@"/"];
        NSString *strUrl2 = [strUrl1 stringByReplacingOccurrencesOfString:@"月" withString:@"/"];
        NSString *strUrl3 = [strUrl2 stringByReplacingOccurrencesOfString:@"日" withString:@""];
        NSString *current = [strUrl3 substringFromIndex:2];        
        return current;
    }
}



@end

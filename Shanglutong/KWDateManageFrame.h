//
//  KWDateManageFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWDateManageModel;
@interface KWDateManageFrame : NSObject

@property (strong, nonatomic) KWDateManageModel *manageModel;

@property (assign, readonly, nonatomic) CGRect comeF;
@property (assign, readonly, nonatomic) CGRect comeValueF;

@property (assign, readonly, nonatomic) CGRect noteF;

@property (assign, readonly, nonatomic) CGRect chargeF;
@property (assign, readonly, nonatomic) CGRect timeF;
@property (assign, readonly, nonatomic) CGRect timeOutF;

@property (assign, readonly, nonatomic) CGRect doneF;
@property (assign, readonly, nonatomic) CGRect doneViewF;

@end

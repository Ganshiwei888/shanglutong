//
//  KWDateManageFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDateManageFrame.h"
#import "Utility.h"
#import "KWDateManageModel.h"

//@property (assign, readonly, nonatomic) CGRect comeF;
//@property (assign, readonly, nonatomic) CGRect comeValueF;
//
//@property (assign, readonly, nonatomic) CGRect noteF;
//
//@property (assign, readonly, nonatomic) CGRect chargeF;
//@property (assign, readonly, nonatomic) CGRect timeF;
//@property (assign, readonly, nonatomic) CGRect timeOutF;
//
//@property (assign, readonly, nonatomic) CGRect doneF;
//@property (assign, readonly, nonatomic) CGRect doneViewF;

@implementation KWDateManageFrame

- (void)setManageModel:(KWDateManageModel *)manageModel {
    _manageModel = manageModel;
    
    CGFloat comeX = 32;
    CGFloat comeY = 8;
    CGFloat comeW = 30;
    CGFloat comeH = 18;
    _comeF = CGRectMake(comeX, comeY, comeW, comeH);
    
    CGFloat comeValueX = CGRectGetMaxX(_comeF) + 4;
    CGFloat comeValueY = 8;
    CGFloat comeValueW = 30;
    CGFloat comeValueH = 18;
    _comeValueF = CGRectMake(comeValueX, comeValueY, comeValueW, comeValueH);
    
    CGFloat noteX = 32;
    CGFloat noteY = CGRectGetMaxY(_comeValueF) + 4;
    CGFloat noteW = 260;
    CGFloat noteH = 30;
    _noteF = CGRectMake(noteX, noteY, noteW, noteH);
    
    CGFloat chargeX = 32;
    CGFloat chargeY = CGRectGetMaxY(_noteF) + 4;
    CGFloat chargeW = 45;
    CGFloat chargeH = 18;
    _chargeF = CGRectMake(chargeX, chargeY, chargeW, chargeH);
    
    CGFloat timeX = CGRectGetMaxX(_chargeF) + 4;
    CGFloat timeY = chargeY;
    CGFloat timeW = 120;
    CGFloat timeH = 18;
    _timeF = CGRectMake(timeX, timeY, timeW, timeH);
    
    CGFloat timeOutX = CGRectGetMaxX(_timeF) + 4;
    CGFloat timeOutY = chargeY;
    CGFloat timeOutW = 80;
    CGFloat timeOutH = 20;
    _timeOutF = CGRectMake(timeOutX, timeOutY, timeOutW, timeOutH);
    
    CGFloat doneX = KWSCREEN_WIDTH - 80;
    CGFloat doneY = noteY;
    CGFloat doneW = 40;
    CGFloat doneH = 18;
    _doneF = CGRectMake(doneX, doneY, doneW, doneH);
    
    CGFloat doneViewX = CGRectGetMaxX(_doneF) + 4;
    CGFloat doneViewY = noteY;
    CGFloat doneViewW = 18;
    CGFloat doneViewH = 18;
    _doneViewF = CGRectMake(doneViewX, doneViewY, doneViewW, doneViewH);
    
}



@end

















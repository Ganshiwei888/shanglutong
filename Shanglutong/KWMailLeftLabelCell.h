//
//  KWMailLeftLabelCell.h
//  Shanglutong
//
//  Created by ganshiwei on 2017/11/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWMailLeftLabelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

//
//  KWMailSendViewController.h
//  
//
//  Created by KeWen on 2017/8/8.
//
//

#import <UIKit/UIKit.h>
#import "KWMailDetailFileModel.h"
#import "KWMailDetailResult.h"

typedef NS_ENUM(NSUInteger, MailSendType) {
    MailSendTypeNew,
    MailSendTypeReply,
    MailSendTypeForward
};

@interface KWMailSendViewController : UIViewController

@property (nonatomic,strong)NSString *sendName;
@property (nonatomic,strong)NSString *replyTo;
@property (nonatomic,strong)NSString *subject;


/**
 填充数据

 @param mailDetailModel 邮件详细内容
 @param attachmentArray 邮件的附件
 @param type 回复的模式
 @param mailBoxId 邮件id
 @param htmlInfo html的内容
 */
- (void)recieveTheMailDetailModel:(KWMailDetailResultModel *)mailDetailModel AndAttachmentArray:(NSArray<KWMailDetailFileModel *> *)attachmentArray AndType:(MailSendType)type AndMailBoxId:(NSString *)mailBoxId AndHtmlInfo:(NSMutableDictionary*)htmlInfo;

/*
 填充数据的方法
 */
- (void)setMailSendViewText:(NSString *)fromEmail AndReplyTo:(NSString *)replyTo AndSubject:(NSString *)subject;

- (void)receTheSendMail:(NSString *)sendMail andMailId:(NSString *)mailId;

@end















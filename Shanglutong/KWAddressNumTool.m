//
//  KWAddressNumTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressNumTool.h"
#import "KWBaseParam.h"
#import <MJExtension.h>
#import "Utility.h"
@implementation KWAddressNumTool

+ (void)postGetAddressNumWith:(KWBaseParam *)param success:(void (^)(id))success failure:(void (^)(NSError *))failure {
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/GetAllUser",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        if (success) {
            success(json);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
    
}

@end

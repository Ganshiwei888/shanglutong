//
//  KWMailListFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailListFrame.h"
#import "KWMailList.h"


@implementation KWMailListFrame

- (void)setMailModelList:(KWMailListModel *)mailModelList {
    
    _isEdit = @"0";
    
    _mailModelList = mailModelList;
    
    CGFloat fromNameFrameX = 36;
    CGFloat fromNameFrameY = 8;
    CGFloat fromNameFrameW = 280;
    CGFloat fromNameFrameH = 21;
    _fromNameFrame = CGRectMake(fromNameFrameX, fromNameFrameY, fromNameFrameW, fromNameFrameH);
    
    CGFloat readViewFrameX = 13;
    CGFloat readViewFrameY = 13;
    CGFloat readViewFrameW = 10;
    CGFloat readViewFrameH = 10;
    _readViewFrame = CGRectMake(readViewFrameX, readViewFrameY, readViewFrameW, readViewFrameH);
    
    CGFloat topViewFrameX = CGRectGetMaxX(_fromNameFrame);
    CGFloat topViewFrameY = 11;
    CGFloat topViewFrameW = 14;
    CGFloat topViewFrameH = 14;
    _topViewFrame = CGRectMake(topViewFrameX, topViewFrameY, topViewFrameW, topViewFrameH);
    
    CGFloat starViewFrameX = CGRectGetMaxX(_topViewFrame) + 8;
    CGFloat starViewFrameY = 11;
    CGFloat starViewFrameW = 14;
    CGFloat starViewFrameH = 14;
    _starViewFrame = CGRectMake(starViewFrameX, starViewFrameY, starViewFrameW, starViewFrameH);
    
    CGFloat flagViewFrameX = CGRectGetMaxX(_starViewFrame) + 8;
    CGFloat flagViewFrameY = 11;
    CGFloat flagViewFrameW = 14;
    CGFloat flagViewFrameH = 14;
    _flagViewFrame = CGRectMake(flagViewFrameX, flagViewFrameY, flagViewFrameW, flagViewFrameH);
    
    CGFloat attachmentFrameX = CGRectGetMaxX(_flagViewFrame) + 8;
    CGFloat attachmentFrameY = 11;
    CGFloat attachmentFrameW = 14;
    CGFloat attachmentFrameH = 14;
    _attachmentFrame = CGRectMake(attachmentFrameX, attachmentFrameY, attachmentFrameW, attachmentFrameH);
    
    CGFloat selectViewFrameX = 4;
    CGFloat selectViewFrameY = 35;
    CGFloat selectViewFrameW = 24;
    CGFloat selectViewFrameH = 24;
    _selectViewFrame = CGRectMake(selectViewFrameX, selectViewFrameY, selectViewFrameW, selectViewFrameH);
    
    
    CGFloat subjectFrameX = 36;
    CGFloat subjectFrameY = 32;
    CGFloat subjectFrameW = 320;
    CGFloat subjectFrameH = 21;
    _subjectFrame = CGRectMake(subjectFrameX, subjectFrameY, subjectFrameW, subjectFrameH);
    
    CGFloat mailDateilFrameX = 36;
    CGFloat mailDateilFrameY = 58;
    CGFloat mailDateilFrameW = KWSCREEN_WIDTH - 120;
    CGFloat mailDateilFrameH = 19;
    _mailDateilFrame = CGRectMake(mailDateilFrameX, mailDateilFrameY, mailDateilFrameW, mailDateilFrameH);
    
    CGFloat recDateFrameX = CGRectGetMaxX(_mailDateilFrame) + 30;
    CGFloat recDateFrameY = CGRectGetMinY(_mailDateilFrame);
    CGFloat recDateFrameW = 50;
    CGFloat recDateFrameH = 21;
    _recDateFrame = CGRectMake(recDateFrameX, recDateFrameY, recDateFrameW, recDateFrameH);
    
}

- (void)setIsEdit:(NSString *)isEdit {
    _isEdit = isEdit;
}

- (void)setIsSelect:(NSString *)isSelect {
    _isSelect = isSelect;
}

@end











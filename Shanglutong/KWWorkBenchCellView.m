//
//  KWWorkBenchCellView.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWWorkBenchCellView.h"
#import "KWUserMenuno.h"
#import "Utility.h"

@implementation KWWorkBenchCellView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    
//    UIImageView *backImage = [[UIImageView alloc] initWithFrame:self.bounds];
//    backImage.image = [UIImage imageNamed:@"app_item_bg"];
//    [self.contentView addSubview:backImage];
//    self.layer.borderWidth = 0.5;
//    self.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
//
    UIImageView *workBenchImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - 25, 21, 50, 50)];
    [self.contentView addSubview:workBenchImage];
    self.workBenchImage = workBenchImage;
    
    UILabel * workNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/2 - 42, 82, 84, 20)];
    workNameLabel.textAlignment = NSTextAlignmentCenter;
    workNameLabel.font = [UIFont systemFontOfSize:14];
    workNameLabel.backgroundColor = [UIColor clearColor];
    workNameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:workNameLabel];
    self.workNameLabel = workNameLabel;
    
    
}

- (void)setUserMenuno:(KWUserMenuno *)userMenuno {
    
    _userMenuno = userMenuno;
    NSString *imageName = userMenuno.name;
    self.workBenchImage.image = [UIImage imageNamed:imageName];
    
    NSString *workName = userMenuno.name;
    self.workNameLabel.text = workName;

}


@end









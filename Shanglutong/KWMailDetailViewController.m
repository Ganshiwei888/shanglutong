//
//  KWMailDetailViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailDetailViewController.h"
#import "KWMailListResult.h"
#import "KWMailDetailTool.h"
#import "KWMailDetailParam.h"
#import "KWMailDetailResult.h"
#import "MBProgressHUD+XQ.h"
#import "KWMailToolView.h"
#import "KWMailSendViewController.h"
#import "KWNavigationController.h"
#import "KWAttachButton.h"
#import <AFNetworking.h>
#import "Utility.h"
#import <WebKit/WebKit.h>
#import "MBProgressHUD+XQ.h"
#import <SVProgressHUD.h>
#import "LWActiveIncator.h"
#import "ZoomHeaderView.h"
#import "KWUserDefaultsManager.h"
#import "KWMailAttachmentResult.h"
#import "KWMailAttachmentViewController.h"
#import "KWMailImageAttachmentViewController.h"
#import "KWMailDocAttachmentViewController.h"
#import "KWMailDetailCollectionViewCell.h"
#import "KWMailListTool.h"
#import "KWMailListParam.h"
#import "KWCustomFromViewController.h"
#import "KWHttpTool.h"
#import "KWMailDetailParam.h"
#import "KWHomeViewController.h"
#import <MJExtension.h>
@interface KWMailDetailViewController () <UIWebViewDelegate, WKNavigationDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate>

@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, strong) KWMailListModel *mailListModel;
@property (nonatomic, strong) NSArray *mailListArray;
@property (nonatomic, copy) NSString *act;
@property (nonatomic, weak) WKWebView *mailWebView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIScrollView *mailDetailScrollView;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UILabel *sendNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, assign) NSIndexPath *indexPath;
@property (weak, nonatomic) UIButton *rightBarButton;
@property (weak, nonatomic) UIButton *secondRightBarButton;
@property (nonatomic, strong) UIView *detailView; //底部View
@property (nonatomic, strong) UIView *mailToolView;
@property (nonatomic, strong) UIView *attachmentView;
@property (nonatomic, weak) UICollectionView *attactmentCollection;
@property (nonatomic, strong) UIView *BGView;
@property (nonatomic, copy) NSString *htmlbody;
@property (nonatomic, strong) NSMutableDictionary *htmlBodyDic; //包含html的宽高属性
@property (nonatomic, assign) NSInteger addCount;
@property (nonatomic, assign) NSInteger indexPathRow;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, strong) KWMailDetailResultModel *resultModel;
@property (nonatomic, strong) NSArray *numOfAttach;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, strong) NSString *filePath;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *attachButton;

@property (copy, nonatomic) NSString *firstStr;



@property (nonatomic, weak) UILabel *detailName;

@end

@implementation KWMailDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    
    self.tabBarController.tabBar.hidden = YES;

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.topView.layer.cornerRadius = 10.0;
    self.topView.layer.masksToBounds = YES;
    self.count = 0;
    self.addCount = 0;
    // Do any additional setup after loading the view from its nib.
    [self setupWebView];
    [self setupDetailButton];
    [self setupMailDetail];
    [self setupAttachment];
    [self setupBottomView];
//    self.mailWebView.translatesAutoresizingMaskIntoConstraints = NO;
//    self.mailDetailScrollView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
//    self.mailDetailScrollView.delegate = self;

}

- (void)setupNav {
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"邮件详情";
    [titleLabel setFont:[UIFont fontWithName:@"System Medium" size:12]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(0, 0, 100, 40);
    self.navigationItem.titleView = titleLabel;
    self.titleLabel = titleLabel;
    
    UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarButton.frame = CGRectMake(0, 0, 25, 25);
    [rightBarButton setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
    [rightBarButton setBackgroundColor:[UIColor clearColor]];
    [rightBarButton addTarget:self action:@selector(rightBarButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
    
    UIButton *secondRightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    secondRightBarButton.frame = CGRectMake(0, 0, 25, 25);
    [secondRightBarButton setImage:[UIImage imageNamed:@"向上"] forState:UIControlStateNormal];
    if (self.indexPath.row == 0) {
        NSLog(@"can not get ");
    }
    [secondRightBarButton setBackgroundColor:[UIColor clearColor]];
    [secondRightBarButton addTarget:self action:@selector(secondRightBarButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *secondRightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:secondRightBarButton];
    
    self.navigationItem.rightBarButtonItems = @[rightBarButtonItem, secondRightBarButtonItem];
    self.rightBarButton = rightBarButton;
    self.secondRightBarButton = secondRightBarButton;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    CGFloat y = scrollView.contentOffset.y;
//    NSLog(@"%f",y);
//    
//    if (y < 0) {
//        //_scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        self.mailDetailScrollView.contentSize = CGSizeMake(0, y);
//    } else {
//        self.mailDetailScrollView.contentSize = CGSizeMake(KWSCREEN_WIDTH, 800);
//    }
//    
//}

- (void)setupBottomView {
    
    CGFloat buttonW = 26;
    CGFloat buttonH = 26;
    CGFloat buttonX = (KWSCREEN_WIDTH - (26 * 5)) / 5;
    CGFloat buttonY = 10;
    
    UIButton *starButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [starButton setImage:[UIImage imageNamed:@"标星asd"] forState:UIControlStateNormal];
    [starButton setFrame:CGRectMake(buttonX/2, buttonY, buttonW, buttonH)];
    [starButton addTarget:self action:@selector(starButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:starButton];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton setImage:[UIImage imageNamed:@"删除"] forState:UIControlStateNormal];
    [deleteButton setFrame:CGRectMake(CGRectGetMaxX(starButton.frame) + buttonX, buttonY, buttonW, buttonH)];
    [deleteButton addTarget:self action:@selector(deleteMail:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:deleteButton];
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setImage:[UIImage imageNamed:@"钩-2"] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(CGRectGetMaxX(deleteButton.frame) + buttonX, buttonY, buttonW, buttonH)];
    [doneButton addTarget:self action:@selector(welldid:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:doneButton];
    
    UIButton *replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [replyButton setImage:[UIImage imageNamed:@"回信转发"] forState:UIControlStateNormal];
    [replyButton setFrame:CGRectMake(CGRectGetMaxX(doneButton.frame) + buttonX, buttonY, buttonW, buttonH)];
    [replyButton addTarget:self action:@selector(replyToSomeone:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:replyButton];
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton setImage:[UIImage imageNamed:@"更多detail"] forState:UIControlStateNormal];
    [moreButton setFrame:CGRectMake(CGRectGetMaxX(replyButton.frame) + buttonX, buttonY, buttonW, buttonH)];
    [moreButton addTarget:self action:@selector(mailToolButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:moreButton];
    
    
}

- (void)welldid:(UIButton *)sender {
    
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = self.mailListModel.Id;
    NSLog(@"zzz%@",param.MailId);
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/ISCL",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json);
        [SVProgressHUD showInfoWithStatus:@"已处理"];
        [SVProgressHUD dismissWithDelay:0.5];
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
}

- (void)starButton:(UIButton *)sender {
    NSIndexPath *index = self.indexPath;
    self.relist(index);
    
    NSString *star = self.mailListModel.star;
    NSString *mailID = self.mailListModel.Id;
    if (star) {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postStarTheMailWith:param success:^(NSString *resultData) {
            [SVProgressHUD showInfoWithStatus:resultData];
            [SVProgressHUD dismissWithDelay:1];
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postStarTheMailWith:param success:^(NSString *resultData) {
            [SVProgressHUD showInfoWithStatus:resultData];
            [SVProgressHUD dismissWithDelay:1];
        } failure:^(NSError *erroe) {
            
        }];
    }
}

- (void)deleteMail:(UIButton *)sender {
    
    self.deleteMail(self.indexPath);
    [SVProgressHUD showInfoWithStatus:@"已删除"];
    [SVProgressHUD dismissWithDelay:0.5];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)replyToSomeone:(UIButton *)sender {
    //删除提醒框
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    //添加按钮
    __weak typeof(alter) weakAlter = alter;
    [weakAlter addAction:[UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
        [sendVc recieveTheMailDetailModel:self.resultModel AndAttachmentArray:nil AndType:MailSendTypeReply AndMailBoxId:self.mailListModel.Id AndHtmlInfo:self.htmlBodyDic ];
        [sendVc setMailSendViewText:self.mailListModel.FromEmail AndReplyTo:self.mailListModel.ReplyTo AndSubject:self.mailListModel.Subject];
        KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:sendVc];
        
        [self presentViewController:nav animated:YES completion:nil];
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"转发" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
        [sendVc recieveTheMailDetailModel:self.resultModel AndAttachmentArray:nil AndType:MailSendTypeForward AndMailBoxId:self.mailListModel.Id AndHtmlInfo:self.htmlBodyDic ];
        [sendVc setMailSendViewText:nil AndReplyTo:self.mailListModel.ReplyTo AndSubject:self.mailListModel.Subject];
        KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:sendVc];
        
        [self presentViewController:nav animated:YES completion:nil];    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"全回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //全回复有问题
        KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
        [sendVc recieveTheMailDetailModel:self.resultModel AndAttachmentArray:nil AndType:MailSendTypeForward AndMailBoxId:self.mailListModel.Id AndHtmlInfo:self.htmlBodyDic ];
        [sendVc setMailSendViewText:self.mailListModel.FromEmail AndReplyTo:self.mailListModel.ReplyTo AndSubject:self.mailListModel.Subject];
        KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:sendVc];
        
        [self presentViewController:nav animated:YES completion:nil];
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alter animated:YES completion:nil];
}

#pragma mark - Nav Bar Button -

- (void)rightBarButtonClick:(UIButton *)sender {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.indexPath.row + 1 inSection:self.indexPath.section];
    
    NSInteger maxCount = self.mailListArray.count;
    NSLog(@"+++++%ld",(long)indexPath.row);
    
    if (indexPath.row + 1 <= maxCount ) {
        NSLog(@"有下一封");
        
        KWMailListModel *nextModel = self.mailListArray[indexPath.row];
        
        self.sendNameLabel.text = nextModel.FromName;
        self.subjectLabel.text = nextModel.Subject;
        self.timeLabel.text = nextModel.RecDate;
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = nextModel.Id;
        [LWActiveIncator showInView:self.view];
        [KWMailDetailTool postGetMailDetailWith:param success:^(KWMailDetailResultModel *resultData) {
            
            NSString *htmlBody = resultData.htmlbody;
            self.htmlbody = htmlBody;
            [LWActiveIncator hideInViwe:self.view];
            [self.mailWebView loadHTMLString:htmlBody baseURL:nil];
            
        } failure:^(NSError *error) {
            
        }];

        self.indexPath = indexPath;
    } else {
        [MBProgressHUD showError:@"这是最后一封" toView:self.view];
    }
}

- (void)secondRightBarButtonClick:(UIButton *)sender {
    [self getAboveMail];
}

- (void)getAboveMail {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.indexPath.row - 1 inSection:0];
    if ((self.indexPath.row - 1) >= 0) {
        NSLog(@"有上一封");
        
        KWMailListModel *aboveModel = self.mailListArray[self.indexPath.row - 1];
        
        self.sendNameLabel.text = aboveModel.FromName;
        self.subjectLabel.text = aboveModel.Subject;
        self.timeLabel.text = aboveModel.RecDate;
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = aboveModel.Id;
        [LWActiveIncator showInView:self.view];
        [KWMailDetailTool postGetMailDetailWith:param success:^(KWMailDetailResultModel *resultData) {
            
            NSString *htmlBody = resultData.htmlbody;
            self.htmlbody = htmlBody;
            [LWActiveIncator hideInViwe:self.view];
            [self.mailWebView loadHTMLString:htmlBody baseURL:nil];
            
        } failure:^(NSError *error) {
            
        }];
        self.indexPath = indexPath;
    } else {
        [MBProgressHUD showError:@"这是第一封" toView:self.view];
    }
}

- (void)setupAttachment {
    
}

- (void)setupWebView {
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    //初始化偏好设置属性：preferences
    config.preferences = [WKPreferences new];
    //The minimum font size in points default is 0;
    config.preferences.minimumFontSize = 10;
    //是否支持JavaScript
    config.preferences.javaScriptEnabled = YES;
    //不通过用户交互，是否可以打开窗口
    config.preferences.javaScriptCanOpenWindowsAutomatically = YES;

//    WKWebView *mailWebView = [[WKWebView alloc] initWithFrame:CGRectMake(8, 118, KWSCREEN_WIDTH - 16, 474) configuration:config];
    
    if([[[UIDevice currentDevice] systemVersion] integerValue] >= 8) {
        WKWebView *wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(8, 98, KWSCREEN_WIDTH -16, KWSCREEN_HEIGHT - 102 - 64 - 40)];
//        [self.view addSubview:wkWebView];
        wkWebView.navigationDelegate = self;
        [self.mailDetailScrollView addSubview:wkWebView];
        self.mailWebView = wkWebView;
        //[wkWebView loadHTMLString:htmlStr baseURL:nil];
    } else {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 98, KWSCREEN_WIDTH - 16, KWSCREEN_HEIGHT - 102 -64 - 40)];
//        [self.view addSubview:webView];
        webView.delegate = self;
        [self.mailDetailScrollView addSubview:webView];
//        [webView loadHTMLString:htmlStr baseURL:nil];
    }
//    NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
//    
//    WKUserScript *wkUserScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
//    
    
}

#pragma UIWebView
-(void)webViewDidFinishLoad:(UIWebView*) webView {
    
    CGFloat documentHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"content\").offsetHeight;"] floatValue];
    CGRect frame = webView.frame;
    frame.size.height = documentHeight + 400;
    webView.frame = frame;
}

#pragma WKWebView

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    self.htmlBodyDic = [[NSMutableDictionary alloc]init];
    [webView evaluateJavaScript:@"document.body.scrollWidth" completionHandler:^(id _Nullable width, NSError * _Nullable error) {
        NSInteger with = [width integerValue];
        [self.htmlBodyDic setValue:[NSString stringWithFormat:@"%ld",(long)with] forKey:@"htmlWith"];
    }];
    [webView evaluateJavaScript:@"document.body.scrollHeight" completionHandler:^(id _Nullable heigth,NSError * _Nullable error) {
        [self.htmlBodyDic setValue:[NSString stringWithFormat:@"%@",heigth] forKey:@"htmlHeight"];
    }];
    
        //在这里计算html的实际宽高并且保存
    NSString *heightString = @"document.getElementById('webview_content_wrapper').offsetHeight + parseInt(window.getComputedStyle(document.getElementsByTagName('body')[0]).getPropertyValue('margin-top'))  + parseInt(window.getComputedStyle(document.getElementsByTagName('body')[0]).getPropertyValue('margin-bottom'))";
    
    [webView evaluateJavaScript:heightString completionHandler:^(id _Nullable resultHeight,NSError *_Nullable error) {
        
    }];
    
    if (![self.htmlbody containsString:@"device-width"]) {
        [webView evaluateJavaScript:@"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);" completionHandler:^(id _Nullable resultWith, NSError * _Nullable error) {
            NSLog(@"%@",resultWith);
        }];
    }
    
    NSString *js=@"var script = document.createElement('script');"
        "script.type = 'text/javascript';"
        "script.text = \"function ResizeImages() { "
        "var myimg,oldwidth;"
        "var maxwidth = %f;"
        "for(i=0;i <document.images.length;i++){"
        "myimg = document.images[i];"
        "if(myimg.width > maxwidth){"
        "oldwidth = myimg.width;"
        "myimg.width = %f;"
        "}"
        "}"
        "}\";"
        "document.getElementsByTagName('head')[0].appendChild(script);";
    js=[NSString stringWithFormat:js,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.width];
    [webView evaluateJavaScript:js completionHandler:nil];
    [webView evaluateJavaScript:@"ResizeImages();" completionHandler:nil];
}


//- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
//    
//    [webView evaluateJavaScript:@"document.getElementById(\"content\").offsetHeight;" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
//        //获取页面高度，并重置webview的frame
//        CGFloat documentHeight = [result doubleValue];
//        CGRect frame = webView.frame;
//        frame.size.height = documentHeight + KWSCREEN_HEIGHT - 90 - 64 - 16 - 64/*显示不全*/;
//        webView.frame = frame;
//    }];
//    
//}

- (void)setupDetailButton {
    
    [self.detailButton setTintColor: RGBCOLOR(8, 46, 84)];
    self.detailButton.layer.borderColor = RGBCOLOR(8, 46, 84).CGColor;
    self.detailButton.layer.borderWidth = 1.0;
    self.detailButton.layer.cornerRadius = 6.0;
    self.detailButton.layer.masksToBounds = YES;
    
}

- (void)setupMailDetail {
    
    [self downTheMail];
    
}

- (void)setupRead {
    NSString *mailId = self.mailListModel.Id;
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = mailId;
    param.is = @"true";
    [KWMailListTool postReadTheMailWith:param success:^(NSString *resultData) {
    
    } failure:^(NSError *erroe) {
        
    }];
    
}

- (IBAction)detailButtonClick:(UIButton *)sender {
    
    // ------全屏遮罩
    self.BGView  = [[UIView alloc] init];
    self.BGView.frame           = [[UIScreen mainScreen] bounds];
    self.BGView.tag             = 100;
    self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.BGView.opaque = NO;
    
    //--UIWindow的优先级最高，Window包含了所有视图，在这之上添加视图，可以保证添加在最上面
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    [appWindow addSubview:self.BGView];
    
    // ------给全屏遮罩添加的点击事件
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitClick)];
    gesture.numberOfTapsRequired = 1;
    gesture.cancelsTouchesInView = NO;
    [self.BGView addGestureRecognizer:gesture];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        
    }];
    
    // ------底部弹出的View
    self.detailView = [[UIView alloc] init];
    self.detailView.layer.cornerRadius = 8;
    self.detailView.layer.masksToBounds = YES;
    self.detailView.frame = CGRectMake(8, KWSCREEN_WIDTH + 80, KWSCREEN_WIDTH -16, 200);
    self.detailView.backgroundColor = [UIColor whiteColor];
    [appWindow addSubview:self.detailView];
    
    [self setupDetailView];
    
    
    // ------View出现动画
    self.detailView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
    [UIView animateWithDuration:0.3 animations:^{
        
        self.detailView.transform = CGAffineTransformMakeTranslation(0.01, 0.01);
        
    }];
    
}

- (void)mailToolButton:(UIButton *)sender {
    
    // ------全屏遮罩
    self.BGView  = [[UIView alloc] init];
    self.BGView.frame           = [[UIScreen mainScreen] bounds];
    self.BGView.tag             = 100;
    self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.BGView.opaque = NO;
    
    //--UIWindow的优先级最高，Window包含了所有视图，在这之上添加视图，可以保证添加在最上面
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    [appWindow addSubview:self.BGView];
    
    // ------给全屏遮罩添加的点击事件
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitClickTwo)];
    gesture.numberOfTapsRequired = 1;
    gesture.cancelsTouchesInView = NO;
    [self.BGView addGestureRecognizer:gesture];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        
    }];
    // ------底部弹出的View
    self.mailToolView = [[UIView alloc] init];
    self.mailToolView.layer.cornerRadius = 8;
    self.mailToolView.layer.masksToBounds = YES;
    self.mailToolView.frame = CGRectMake(8, KWSCREEN_WIDTH + 100, KWSCREEN_WIDTH - 16, 160);
    self.mailToolView.backgroundColor = [UIColor whiteColor];
    
    
    [appWindow addSubview: self.mailToolView];
    [self setupToolView];

    // ------View出现动画
    self.mailToolView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
    [UIView animateWithDuration:0.3 animations:^{
        
        self.mailToolView.transform = CGAffineTransformMakeTranslation(0.01, 0.01);
        
    }];
    
}

- (IBAction)attachmentButton:(UIButton *)sender {
    
    NSLog(@"chick");
    
    // ------全屏遮罩
    self.BGView  = [[UIView alloc] init];
    self.BGView.frame = [[UIScreen mainScreen] bounds];
    self.BGView.tag = 100;
    self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    self.BGView.opaque = NO;
    
    //--UIWindow的优先级最高，Window包含了所有视图，在这之上添加视图，可以保证添加在最上面
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    [appWindow addSubview:self.BGView];
    
    // ------给全屏遮罩添加的点击事件
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitClickThree)];
    gesture.numberOfTapsRequired = 1;
    gesture.cancelsTouchesInView = NO;
    [self.BGView addGestureRecognizer:gesture];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.BGView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        
    }];
    
    // ------底部弹出的View
    UIView * attachmentView = [[UIView alloc] init];
    attachmentView.layer.cornerRadius = 8;
    attachmentView.layer.masksToBounds = YES;
    attachmentView.frame = CGRectMake(8, KWSCREEN_WIDTH + 80, KWSCREEN_WIDTH - 16, 200);
    attachmentView.backgroundColor = [UIColor whiteColor];
    [appWindow addSubview:attachmentView];
    self.attachmentView = attachmentView;
    [self setupAttachmentView];
    // ------View出现动画
    attachmentView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
    [UIView animateWithDuration:0.3 animations:^{
        
        attachmentView.transform = CGAffineTransformMakeTranslation(0.01, 0.01);
        
    }];
    
}


- (void)exitClick {
    
    NSLog(@"====");
    [UIView animateWithDuration:0.3 animations:^{
        
        self.detailView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
        self.detailView.alpha = 0.2;
        self.BGView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [self.BGView removeFromSuperview];
        [self.detailView removeFromSuperview];
    }];
    
}

- (void)exitClickTwo {
    
    NSLog(@"====");
    [UIView animateWithDuration:0.3 animations:^{
        
        self.mailToolView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
        self.mailToolView.alpha = 0.2;
        self.BGView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [self.BGView removeFromSuperview];
        [self.mailToolView removeFromSuperview];
    }];
    
}

- (void)exitClickThree {
    
    NSLog(@"====");
    [UIView animateWithDuration:0.3 animations:^{
        
        self.attachmentView.transform = CGAffineTransformMakeTranslation(0.01, KWSCREEN_HEIGHT);
        self.attachmentView.alpha = 0.2;
        self.BGView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [self.BGView removeFromSuperview];
        [self.attachmentView removeFromSuperview];
    }];
    
}


- (void)setupDetailView {
    
    KWMailListModel *mailDetail = self.mailListArray[self.indexPath.row];
    
    UILabel *subjectName = [[UILabel alloc] initWithFrame:CGRectMake(16, 2, KWSCREEN_WIDTH - 32, 42)];
    subjectName.textColor = [UIColor blackColor];
    subjectName.font = [UIFont fontWithName:@"System Heavy" size:14.0f];
    subjectName.numberOfLines = 0;
    subjectName.text = mailDetail.Subject;
    [self.detailView addSubview:subjectName];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 44, KWSCREEN_WIDTH - 32, 18)];
    timeLabel.textColor = RGBCOLOR(41, 36, 33);
    timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f];
    timeLabel.text = mailDetail.RecDate;
    [self.detailView addSubview:timeLabel];
    
    UIView *dividingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timeLabel.frame) + 4, KWSCREEN_WIDTH, 0.5)];
    dividingView1.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.detailView addSubview:dividingView1];
    
    UILabel *headimageLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(dividingView1.frame) + 8, 36, 36)];
    NSString *str = self.sendNameLabel.text;
    if (str.length > 1) {
        headimageLabel.text = [str substringToIndex:1];
    }
    headimageLabel.font = [UIFont systemFontOfSize:18];
    headimageLabel.textColor = [UIColor blueColor];
    headimageLabel.textAlignment = NSTextAlignmentCenter;
    headimageLabel.backgroundColor = [UIColor clearColor];
    headimageLabel.layer.cornerRadius = 18.0;
    headimageLabel.layer.borderColor = [UIColor blueColor].CGColor;
    headimageLabel.layer.borderWidth = 1;
    [self.detailView addSubview:headimageLabel];
    
    
    UILabel *sendName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(headimageLabel.frame) + 16, CGRectGetMaxY(dividingView1.frame) + 8, KWSCREEN_WIDTH - 80, 16)];
    sendName.textColor = RGBCOLOR(11, 23, 70);
    sendName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    sendName.text = mailDetail.FromName;
    [self.detailView addSubview:sendName];
    
    UILabel *sendMail = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(headimageLabel.frame) + 16, CGRectGetMaxY(sendName.frame) + 4, KWSCREEN_WIDTH - 80, 16)];
    sendMail.textColor = RGBCOLOR(11, 23, 70);
    sendMail.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    sendMail.text = mailDetail.FromEmail;
    [self.detailView addSubview:sendMail];
    
    UIView *dividingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(sendMail.frame) + 8, KWSCREEN_WIDTH, 0.5)];
    dividingView2.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.detailView addSubview:dividingView2];
    
    UILabel *addressee = [[UILabel alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(dividingView2.frame) + 4, KWSCREEN_WIDTH - 80, 18)];
    addressee.text = @"收件人";
    addressee.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    addressee.textColor = [UIColor darkGrayColor];
    [self.detailView addSubview:addressee];
    
    UILabel *receName = [[UILabel alloc] initWithFrame:CGRectMake(32, CGRectGetMaxY(addressee.frame), KWSCREEN_WIDTH - 80, 30)];
    receName.text = mailDetail.itTo;
    receName.numberOfLines = 0;
    receName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    receName.textColor = RGBCOLOR(11, 23, 70);
    [self.detailView addSubview:receName];
    
}

- (void)setupToolView {
    
    KWMailToolView *KWMailToolView = [[[NSBundle mainBundle] loadNibNamed:@"KWMailToolView" owner:nil options:nil] lastObject];
    [KWMailToolView.redFlag addTarget:self action:@selector(redFlagBtn:) forControlEvents:UIControlEventTouchUpInside];
    [KWMailToolView.topButton addTarget:self action:@selector(topBtn:) forControlEvents:UIControlEventTouchUpInside];
    NSLog(@"%f",KWMailToolView.frame.size.width);
    [self.mailToolView addSubview: KWMailToolView];
    
}

- (void)redFlagBtn:(UIButton *)sender {
    [SVProgressHUD showInfoWithStatus:@"标记红旗"];
    [SVProgressHUD dismissWithDelay:0.5];
    
    NSIndexPath *index = self.indexPath;
    
    self.redTheMail(index);
    
    NSString *red = self.mailListModel.redflag;
    NSString *mailID = self.mailListModel.Id;
    if (red) {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postFlagTheMailWith:param success:^(NSString *resultData) {
            
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postFlagTheMailWith:param success:^(NSString *resultData) {
            
        } failure:^(NSError *erroe) {
            
        }];
    }
}

- (void)topBtn:(UIButton *)sender {
    [SVProgressHUD showInfoWithStatus:@"已置顶"];
    [SVProgressHUD dismissWithDelay:0.5];
    
    NSIndexPath *index = self.indexPath;
    
    self.topTheMail(index);
    
    NSString *top = self.mailListModel.TopTime;
    NSString *mailID = self.mailListModel.Id;
    if (top) {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"false";
        [KWMailListTool postTopTheMailWith:param success:^(NSString *resultData) {
            NSLog(@"topppp");
        } failure:^(NSError *erroe) {
            
        }];
    } else {
        KWMailDetailParam *param = [KWMailDetailParam param];
        param.MailId = mailID;
        param.is = @"true";
        [KWMailListTool postTopTheMailWith:param success:^(NSString *resultData) {
            NSLog(@"toop");
        } failure:^(NSError *erroe) {
            
        }];
    }
}


- (void)setupAttachmentView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *attactmentCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(8, 5, KWSCREEN_WIDTH - 16, 190) collectionViewLayout:layout];
    [attactmentCollection registerClass:[KWMailDetailCollectionViewCell class] forCellWithReuseIdentifier:@"cellId"];
    attactmentCollection.delegate = self;
    attactmentCollection.dataSource = self;
    attactmentCollection.backgroundColor = [UIColor clearColor];
    [self.attachmentView addSubview:attactmentCollection];
    self.attactmentCollection = attactmentCollection;
 
}

- (void)downTheMail {
    
    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = self.mailListModel.Id;
    
    self.sendNameLabel.text = self.mailListModel.FromName;
    self.subjectLabel.text = self.mailListModel.Subject;
    self.timeLabel.text = self.mailListModel.RecDate;
    
    NSLog(@"%@",self.mailListModel.clientid);
    if (self.mailListModel.clientid.length > 0) {
        self.sendNameLabel.hidden = YES;
        
        UIButton *sendNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendNameBtn setTitle:self.mailListModel.FromName forState:UIControlStateNormal];
        [sendNameBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sendNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        sendNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        sendNameBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        sendNameBtn.layer.borderWidth = 1.0;
        sendNameBtn.layer.masksToBounds = YES;
        sendNameBtn.layer.cornerRadius = 5.0;
        sendNameBtn.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGFloat length = [self.mailListModel.FromName boundingRectWithSize:CGSizeMake(2000, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        [sendNameBtn setFrame:CGRectMake(8, 46, length + 30, 16)];
        [sendNameBtn addTarget:self action:@selector(openTheCustomView:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:sendNameBtn];

    }
    [KWMailDetailTool postGetMailDetailWith:param success:^(KWMailDetailResultModel *resultData) {
        
       
        self.resultModel = resultData;
        NSString *htmlBody = resultData.htmlbody;
        self.htmlbody = htmlBody;
        [self.mailWebView loadHTMLString:htmlBody baseURL:nil];
        
        if (htmlBody.length < 1) {
            NSString *textBody = resultData.textbody;
            
            UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8, 180, KWSCREEN_WIDTH - 16, 300)];
            textView.delegate = self;
            textView.backgroundColor = [UIColor whiteColor];
            textView.font = [UIFont systemFontOfSize:18];
            textView.editable = NO;
//            [self.view addSubview:textView];
            textView.text = textBody;
        }
    } failure:^(NSError *error) {
        
        [SVProgressHUD showInfoWithStatus:@"下载邮件失败"];
        [SVProgressHUD dismissWithDelay:1.0];
        
    }];
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Get"];

    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
       
        NSArray *fileArray = json[@"back"][@"file"];
        if (fileArray.count > 0) {
            [self.attachButton setHidden:NO];
        }
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in fileArray) {
            KWMailAttachmentResult *model = [[KWMailAttachmentResult alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.numOfAttach = temp;
    } failure:^(NSError *error) {
        
    }];
}

- (void)didOpenbyApns:(NSString *)mailID {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClick:)];

    KWMailDetailParam *param = [KWMailDetailParam param];
    param.MailId = mailID;
    
    if (self.mailListModel.clientid.length > 0) {
        self.sendNameLabel.hidden = YES;
        
        UIButton *sendNameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendNameBtn setTitle:self.mailListModel.FromName forState:UIControlStateNormal];
        [sendNameBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sendNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        sendNameBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        sendNameBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        sendNameBtn.layer.borderWidth = 1.0;
        sendNameBtn.layer.masksToBounds = YES;
        sendNameBtn.layer.cornerRadius = 5.0;
        sendNameBtn.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGFloat length = [self.mailListModel.FromName boundingRectWithSize:CGSizeMake(2000, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        [sendNameBtn setFrame:CGRectMake(8, 46, length + 30, 16)];
        [sendNameBtn addTarget:self action:@selector(openTheCustomView:) forControlEvents:UIControlEventTouchUpInside];
        [self.topView addSubview:sendNameBtn];
        
    }
    [KWMailDetailTool postGetMailDetailWith:param success:^(KWMailDetailResultModel *resultData) {
        
        
        self.resultModel = resultData;
        NSString *htmlBody = resultData.htmlbody;
        self.htmlbody = htmlBody;
        
        self.sendNameLabel.text = resultData.FromName;
        self.subjectLabel.text = resultData.Subject;
        self.timeLabel.text = resultData.CreateTime;
        [self.mailWebView loadHTMLString:htmlBody baseURL:nil];
        
        if (htmlBody.length < 1) {
            NSString *textBody = resultData.textbody;
            
            UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8, 180, KWSCREEN_WIDTH - 16, 300)];
            textView.delegate = self;
            textView.backgroundColor = [UIColor whiteColor];
            textView.font = [UIFont systemFontOfSize:18];
            textView.editable = NO;
            [self.view addSubview:textView];
            textView.text = textBody;
        }
    } failure:^(NSError *error) {
        
        [SVProgressHUD showInfoWithStatus:@"下载邮件失败"];
        [SVProgressHUD dismissWithDelay:1.0];
        
    }];
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Get"];
    
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        
        NSArray *fileArray = json[@"back"][@"file"];
        if (fileArray.count > 0) {
            [self.attachButton setHidden:NO];
        }
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in fileArray) {
            KWMailAttachmentResult *model = [[KWMailAttachmentResult alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.numOfAttach = temp;
    } failure:^(NSError *error) {
        
    }];
}

- (void)backBtnClick:(UIButton *)sender {
    
    NSLog(@"click");
    
    KWHomeViewController *tabVC = [[KWHomeViewController alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    //        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self];
    [UIView transitionFromView:self.view
                        toView:tabVC.view
                      duration:0
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [self.navigationController setViewControllers:@[tabVC]];
     }];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (void)openTheCustomView:(UIButton *)sender {
    KWCustomFromViewController *vc = [[KWCustomFromViewController alloc] init];
    
    vc.fromName = self.mailListModel.FromName;
    
    [vc receiveTheMail:self.mailListModel.FromEmail andclientId:self.mailListModel.clientid];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.numOfAttach){
        
        return self.numOfAttach.count;
        
    }else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cellId";
    
    KWMailDetailCollectionViewCell *cell = (KWMailDetailCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    KWMailAttachmentResult *cellModel = self.numOfAttach[indexPath.row];
    cell.attachmentNameLabel.text = cellModel.name;
    cell.attachmentImageView.image = [UIImage imageNamed:@"附件asd"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self exitClickThree];
    
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *userId = [KWUserDefaultsManager getUserId];
    NSString *directoryPath = [cachesPath stringByAppendingPathComponent:userId];
    
    BOOL isExist;
    NSFileManager *manage = [NSFileManager defaultManager];
    BOOL directoryIsExist = [manage fileExistsAtPath:directoryPath isDirectory:&isExist];
    if (!(isExist && directoryIsExist)) {
        BOOL isCreateDir = [manage createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:nil];
        if (!isCreateDir) {
            NSLog(@"创建用户文件夹失败");
        }
    }
    
    KWMailListModel *model = self.mailListArray[self.indexPath.row];
    NSString *boxDirectoryPath = [directoryPath stringByAppendingPathComponent:model.MailBoxId];
    BOOL isboxIdDirExist;
    BOOL boxIdDirectoryIsExist = [manage fileExistsAtPath:boxDirectoryPath isDirectory:&isboxIdDirExist];
    if (!(isboxIdDirExist && boxIdDirectoryIsExist)) {
        BOOL isCreateBoxIdDir = [manage createDirectoryAtPath:boxDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
        if (!isCreateBoxIdDir) {
            NSLog(@"创建邮箱ID文件夹失败");
        }
    }
    
    NSString *mailIdDirPath = [directoryPath stringByAppendingPathComponent:model.Id];
    BOOL ismailIdDirExist;
    BOOL mailIdDirectoryIsExist = [manage fileExistsAtPath:mailIdDirPath isDirectory:&ismailIdDirExist];
    if (!(ismailIdDirExist && mailIdDirectoryIsExist)) {
        BOOL isCreateMailIdDir = [manage createDirectoryAtPath:mailIdDirPath withIntermediateDirectories:YES attributes:nil error:nil];
        if (!isCreateMailIdDir) {
            NSLog(@"创建邮件id文件夹失败");
        }
    }
    KWMailAttachmentResult *result = self.numOfAttach[indexPath.row];
    self.filePath = [mailIdDirPath stringByAppendingPathComponent:result.name];
    static NSString * const storyBoardIdentifier = @"MailAttachmentViewController";
    if ([manage fileExistsAtPath:self.filePath]) {
        NSData *fileData = [NSData dataWithContentsOfFile:self.filePath];
        if (fileData.length == [result.size integerValue]) {
            NSString *lastString = [result.name componentsSeparatedByString:@"."].lastObject;
            NSString *lowerString = [lastString lowercaseString];
            if ([lowerString isEqualToString:@"png"] || [lowerString isEqualToString:@"jpg"] || [lowerString isEqualToString:@"jpeg"] || [lowerString isEqualToString:@"gif"]) {
                KWMailImageAttachmentViewController *imageAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MailImageAttachmentViewController"];
                [imageAttachmentVC recieveTheImage:fileData];
                [self.navigationController pushViewController:imageAttachmentVC animated:YES];
            } else if ([lowerString isEqualToString:@"pdf"] || [lowerString isEqualToString:@"doc"] || [lowerString isEqualToString:@"docx"] || [lowerString isEqualToString:@"xls"] || [lowerString isEqualToString:@"xlsx"] || [lowerString isEqualToString:@"ppt"] || [lowerString isEqualToString:@"pptx"] || [lowerString isEqualToString:@"txt"]) {
                KWMailDocAttachmentViewController *docAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MailDocAttachmentViewController"];
                [docAttachmentVC recieveFilePath:self.filePath];
                [self.navigationController pushViewController:docAttachmentVC animated:YES];
            } else {
                NSString *msg = [NSString stringWithFormat:@"此文件已下载\n但无法打开查看%@格式的文件",lowerString];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:action];
                [self presentViewController:alert animated:YES completion:nil];
            }

        } else {
            [manage removeItemAtPath:self.filePath error:nil];
            KWMailAttachmentViewController *mailAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:storyBoardIdentifier];
            [mailAttachmentVC getAttachmentCellModel:result andMailHomeMailListCell:model AndFilePath:self.filePath];
            [self.navigationController pushViewController:mailAttachmentVC animated:YES];
        }
    } else {
        KWMailAttachmentViewController *mailAttachmentVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:storyBoardIdentifier];
        [mailAttachmentVC getAttachmentCellModel:result andMailHomeMailListCell:model AndFilePath:self.filePath];
        [self.navigationController pushViewController:mailAttachmentVC animated:YES];
    }
}
//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 8;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Receive The Mail Data -

- (void)recieveTheMailListArray:(KWMailListModel *)mailListModel AndAct:(NSString *)act {
    self.mailListModel = mailListModel;
    self.act = act;
    [self setupRead];

}

- (void)reciiveTheMailList:(NSArray *)mailListArray andIndexPath:(NSIndexPath *)index {
    self.mailListArray = mailListArray;
    self.indexPath = index;
}



@end

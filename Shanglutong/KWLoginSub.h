//
//  KWLoginSub.h
//  Shanglutong
//
//  Created by ganshiwei on 2017/11/7.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWLoginSub : KWBaseParam

@property (nonatomic, copy) NSString *parentids;
@property (nonatomic, copy) NSString *userid;

@end

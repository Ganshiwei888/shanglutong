//
//  KWNewsViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWNewsViewController.h"
#import "KWNewsListModel.h"
#import "KWNewsListFrame.h"
#import "Utility.h"
#import "KWNewsListParam.h"
#import "KWNewsListTableViewCell.h"
#import "KWHttpTool.h"
#import "KWNewsReadParam.h"
#import "YCXMenu.h"
#import "LWActiveIncator.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>
#import <MJRefresh.h>
#import <MGSwipeTableCell.h>

@interface KWNewsViewController () <UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate, UIActionSheetDelegate> {
    NSMutableArray *_items;
}

@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *listArray;
@property (copy, nonatomic) NSString *count;
@property (assign, nonatomic) NSInteger Pageindex;
@end

@implementation KWNewsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [LWActiveIncator showInView:self.view];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息中心";
    [self setupData];
    [self setupTableView];
    self.Pageindex = 1;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backBtn:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more2"] style:UIBarButtonItemStylePlain target:self action:@selector(readTheNews:)];
    // Do any additional setup after loading the view.
}

- (void)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)readTheNews:(UIButton *)sender {
    [YCXMenu setTintColor:[UIColor colorWithRed:0.118 green:0.573 blue:0.820 alpha:1]];
    //    [YCXMenu setSelectedColor:[UIColor ]];
    if ([YCXMenu isShow]){
        [YCXMenu dismissMenu];
    } else {
        [YCXMenu showMenuInView:self.view fromRect:CGRectMake(self.view.frame.size.width - 50, 0, 50, 0) menuItems:self.items selected:^(NSInteger index, YCXMenuItem *item) {
            NSLog(@"%@",item);
        }];
    }
}


#pragma mark - setter/getter
- (NSMutableArray *)items {
    if (!_items) {
        
        // set title
        YCXMenuItem *menuTitle = [YCXMenuItem menuTitle:@"更多" WithIcon:nil];
        menuTitle.foreColor = [UIColor whiteColor];
        menuTitle.titleFont = [UIFont boldSystemFontOfSize:20.0f];
        
        //set logout button
        
        YCXMenuItem *readBtn = [YCXMenuItem menuItem:@"全部已读" image:nil target:self action:@selector(readBtn:)];
        readBtn.foreColor = [UIColor whiteColor];
        readBtn.alignment = NSTextAlignmentCenter;
        
        YCXMenuItem *unreadBtn = [YCXMenuItem menuItem:@"全部未读" image:nil target:self action:@selector(unreadBtn:)];
        unreadBtn.foreColor = [UIColor whiteColor];
        unreadBtn.alignment = NSTextAlignmentCenter;
        
        //set item
        _items = [@[menuTitle,readBtn,unreadBtn] mutableCopy];
    }
    return _items;
}

- (void)setItems:(NSMutableArray *)items {
    _items = items;
}

- (void)readBtn:(UIButton *)sender {
    
    for (KWNewsListFrame *frame in self.listArray) {
        KWNewsListModel *model = frame.listModel;
        model.IsRead = @"1";
    }
    [self.tableView reloadData];
    
    KWNewsReadParam *param = [KWNewsReadParam param];
    
    param.Ydwd = @"1";
    
    NSString *newtPath = [NSString stringWithFormat:@"http://%@/api/workdt/UPMSG",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:newtPath params:param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json);
        if (json[@"back"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
            [SVProgressHUD dismissWithDelay:1.0];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
}

- (void)unreadBtn:(UIButton *)sender {
    for (KWNewsListFrame *frame in self.listArray) {
        KWNewsListModel *model = frame.listModel;
        model.IsRead = @"0";
    }
    [self.tableView reloadData];
    KWNewsReadParam *param = [KWNewsReadParam param];
    param.Ydwd = @"0";
    NSString *newtPath = [NSString stringWithFormat:@"http://%@/api/workdt/UPMSG",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:newtPath params:param.mj_keyValues success:^(id json) {
        if (json[@"back"]) {
            [SVProgressHUD showInfoWithStatus:json[@"back"]];
            [SVProgressHUD dismissWithDelay:1.0];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)setupData {
    
    KWNewsListParam *param = [KWNewsListParam param];
    param.Pagemax = @"50";
    param.Pageindex = @"1";
    param.Act = @"ALL";
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getmsg",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        
        NSArray *backArray = json[@"back"][@"msg"];
        NSString *count = json[@"back"][@"count"];
        if (count) {
            self.count = count;
        }
        if (backArray) {
            NSMutableArray *temp = [NSMutableArray array];
            for (NSDictionary *dict in backArray) {
                KWNewsListModel *model = [[KWNewsListModel alloc] initWithDict:dict];
                KWNewsListFrame *frame = [[KWNewsListFrame alloc] init];
                frame.listModel = model;
                [temp addObject:frame];
            }
            self.listArray = temp;
            [self.tableView reloadData];
            [LWActiveIncator hideInViwe:self.view];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)setupTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerViewRefreshed)];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerViewRefresh)];
}

- (void)headerViewRefreshed {
    [self setupData];
    [self.tableView.mj_header endRefreshing];
}

- (void)footerViewRefresh {
    
    NSInteger countInt = [self.count integerValue];
    
    if (self.listArray.count < countInt) {
        self.Pageindex++;
        NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.Pageindex];
        KWNewsListParam *param = [KWNewsListParam param];
        param.Pagemax = @"50";
        param.Pageindex = pageStr;
        param.Act = @"ALL";
        
        NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getmsg",USER_SERVERADDRESS];
        
        [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
            
            NSMutableArray *mutList = [self.listArray mutableCopy];
            NSMutableArray *temp = [NSMutableArray array];
            for (NSDictionary *dict in json[@"back"][@"msg"]) {
                KWNewsListModel *model = [[KWNewsListModel alloc] initWithDict:dict];
                KWNewsListFrame *frame = [[KWNewsListFrame alloc] init];
                frame.listModel = model;
                [temp addObject:frame];
            }
            
            for (int i = 0; i < temp.count; i++) {
                [mutList addObject:temp[i]];
            }
            NSArray *newArray = mutList;
            self.listArray = newArray;
            
            [self.tableView reloadData];
            [self.tableView.mj_footer endRefreshing];
        } failure:^(NSError *error) {
            [self.tableView.mj_footer endRefreshing];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cell";
    KWNewsListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[KWNewsListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    cell.listFrame = self.listArray[indexPath.row];
    KWNewsListFrame *frame = self.listArray[indexPath.row];
    KWNewsListModel *model = frame.listModel;
    NSInteger read = [model.IsRead integerValue];
    if ( read == 0 ) {
        [cell.readImageView setHidden:NO];
    } else {
        [cell.readImageView setHidden:YES];
    }
    
    __weak KWNewsViewController *weakSelf = self;
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"已读" icon:nil
                                         backgroundColor:RGBCOLOR(28, 59, 138) insets:UIEdgeInsetsMake(0, 40, 0, 40) callback:^BOOL(MGSwipeTableCell *sender) {
                                             NSIndexPath * index = [weakSelf.tableView indexPathForCell:sender];
                                             [self readNews:index];
                                             [cell hideSwipeAnimated:YES];
                                             return NO;
                                         }],
                          [MGSwipeButton buttonWithTitle:@"未读" icon:nil
                                         backgroundColor:[UIColor redColor] insets:UIEdgeInsetsZero callback:^BOOL(MGSwipeTableCell *sender) {
                                             NSIndexPath * index = [weakSelf.tableView indexPathForCell:sender];
                                             [self unReadNews:index];
                                             [cell hideSwipeAnimated:YES];
                                             return NO;
                                         }],
                          ];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    KWNewsListFrame *frame = self.listArray[indexPath.row];
    return frame.cellHeight;
}

- (void)readNews:(NSIndexPath *)sender {
    
    KWNewsListFrame *frame = self.listArray[sender.row];
    
    frame.listModel.IsRead = @"1";
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:sender.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void)unReadNews:(NSIndexPath *)sender {
    KWNewsListFrame *frame = self.listArray[sender.row];
    
    frame.listModel.IsRead = @"0";
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:sender.row inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

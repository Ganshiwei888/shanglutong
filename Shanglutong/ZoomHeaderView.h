//
//  ZoomHeaderView.h
//  JXTableViewZoomHeaderImageView
//
//  Created by jiaxin on 15/12/17.
//  Copyright © 2015年 jiaxin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ZoomHeaderViewType) {
    ZoomHeaderViewTypeNoConstraint,
    ZoomHeaderViewTypeCodeConstraint,
    ZoomHeaderViewTypeXibConstraint,
};
@class ZoomHeaderView;

@interface ZoomHeaderView : UIView

//是否需要向上滑动的时候图片收缩，默认YES，具体效果看Demo
@property (nonatomic, assign) BOOL isNeedNarrow;
/**
 用户头像button
 */
@property (weak, nonatomic)  UIButton *userImageButton;
/**
 用户姓名
 */
@property (weak, nonatomic)  UILabel *userName;


@property (weak, nonatomic) UILabel *userDuty;

@property (strong, nonatomic) UIButton *switchToSelf;
@property (strong, nonatomic) UIImageView *subImageView;


- (instancetype)initWithFrame:(CGRect)frame type:(ZoomHeaderViewType)type;
- (void)updateHeaderImageViewFrameWithOffsetY:(CGFloat)offsetY;

@end

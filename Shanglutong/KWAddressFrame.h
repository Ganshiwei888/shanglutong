//
//  KWAddressFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class KWAddress;
@interface KWAddressFrame : NSObject
/**
 内容模型
 */
@property (nonatomic, strong) KWAddress *address;
/**
 联系人图
 */
@property (nonatomic, assign, readonly) CGRect headimageView;
/**
 联系人frame
 */
@property (nonatomic, assign, readonly) CGRect addresNameView;
/**
 sex and age
 */
@property (nonatomic, assign, readonly) CGRect sexAndageView;
/**
 工作性质frame
 */
@property (nonatomic, assign, readonly) CGRect workNameView;
/**
 分割线
 */
@property (nonatomic, assign, readonly) CGRect dividingView;
/**
 phone Number image
 */
@property (nonatomic, assign, readonly) CGRect phoneImage;
/**
 phone Num view
 */
@property (nonatomic, assign, readonly) CGRect phoneNumView;

/**
 Email image
 */
@property (nonatomic, assign, readonly) CGRect emailImage;
/**
 Emial view
 */
@property (nonatomic, assign, readonly) CGRect emailView;
/**
 facebook Number image
 */
@property (nonatomic, assign, readonly) CGRect faceBookImage;
/**
 facebook Num view
 */
@property (nonatomic, assign, readonly) CGRect faceBookView;


@end













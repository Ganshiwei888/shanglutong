//
//  KWComDetailModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWComDetailModel.h"
#import "Utility.h"

@implementation KWComDetailModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _Bak = dict[@"Bak"];
        _Creater = dict[@"Creater"];
        _CreateTm = dict[@"CreateTm"];
        _MenuNo = dict[@"MenuNo"];
    }
    return self;
}

@end

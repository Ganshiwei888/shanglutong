//
//  KWCustomerDetailViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWCustomerResult.h"

@interface KWCustomerDetailViewController : UIViewController
//model的下标 用于取植
@property (nonatomic,assign) NSInteger num;

@property (nonatomic,strong) KWCustomerResult *model;

@property (nonatomic,strong) KWlistCustonModel *detailModel;

- (void)initSetView;

@end

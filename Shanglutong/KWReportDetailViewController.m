//
//  KWReportDetailViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWReportDetailViewController.h"
#import "Utility.h"
#import <WebKit/WebKit.h>


@interface KWReportDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *reporterLabel;
@property (weak, nonatomic) IBOutlet UILabel *departLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *finishTime;
@property (strong, nonatomic) UITextView *textview;

@property (strong, nonatomic) UIWebView *webView;

@property (nonatomic, strong) KWDayReportModel *dayModel;
@property (nonatomic, strong) KWWeekReportModel *weekModel;
@property (nonatomic, copy) NSString *type;

@end

@implementation KWReportDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.textview];
    if ([self.type isEqualToString:@"day"]) {
        [self setupDay];
    }
    if ([self.type isEqualToString:@"week"]) {
        [self setupWeek];
    }
}

-(UITextView *)textview {
    if (!_textview) {
        _textview = [[UITextView alloc]initWithFrame:CGRectMake(8, 230, KWSCREEN_WIDTH - 16, 160)];
        _textview.backgroundColor =[UIColor whiteColor];
        _textview.hidden = YES;
    }
    return _textview;
}

- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(8, 230, KWSCREEN_WIDTH - 16, KWSCREEN_HEIGHT - 240)];
//        _webView.backgroundColor = [UIColor redColor];
        _webView.dataDetectorTypes = UIDataDetectorTypeAll;
//        _webView.scalesPageToFit = YES;
        _webView.opaque = YES;
        [self.view addSubview:_webView];
    }
    return _webView;
}


- (void)setupDay {
    
    self.textview.attributedText = [self attributedStringWithHTMLString:self.dayModel.Html];
    self.textview.font = [UIFont systemFontOfSize:18];
    self.textview.text = self.dayModel.Html;
    
    
    self.reporterLabel.text = self.dayModel.CreaterName;
//    self.departLabel.text = self.dayModel.DeptIdStr;
    self.startTime.text = [self.dayModel.CreateTm substringToIndex:10];
    self.finishTime.text = [self.dayModel.CreateTm substringToIndex:10];
   
    NSString *htmlStr = self.dayModel.Html;
    NSLog(@"zzzz%@", htmlStr);
    
    [self.webView loadHTMLString:htmlStr baseURL:nil];

    
}

- (void)setupWeek {
    
    self.textview.attributedText = [self attributedStringWithHTMLString:self.weekModel.Html];
    self.textview.font = [UIFont systemFontOfSize:18];
    self.textview.text = self.weekModel.Html;
    
    self.reporterLabel.text = self.weekModel.CreaterName;
//    self.departLabel.text = self.weekModel.DeptId;
    self.startTime.text = [self.weekModel.RepStartDate substringToIndex:10];
    if (self.weekModel.RepEndDate.length > 0) {
        self.endTime.text = [self.weekModel.RepEndDate substringToIndex:10];
    }
//    self.endTime.text = [self.weekModel.RepEndDate substringToIndex:10];
    self.finishTime.text = [self.weekModel.CreateTm substringToIndex:10];
    
    NSString *htmlStr = self.weekModel.Html;
    NSLog(@"zzzz%@",htmlStr);
    [self.webView loadHTMLString:htmlStr baseURL:nil];


}

- (NSAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString {
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}

- (void)receiveTheDayModel:(KWDayReportModel *)daymodel type:(NSString *)type {
    self.type = type;
    self.dayModel = daymodel;
    
}

- (void)receiveTheWeekModel:(KWWeekReportModel *)weekModel type:(NSString *)type {
    self.type = type;
    self.weekModel = weekModel;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

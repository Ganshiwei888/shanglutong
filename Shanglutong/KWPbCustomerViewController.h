//
//  KWPbCustomerViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWCustomerResult;
@interface KWPbCustomerViewController : UIViewController

@property (nonatomic, strong) KWCustomerResult *customerResult;


@end

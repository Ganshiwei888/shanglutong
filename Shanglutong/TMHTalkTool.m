//
//  TMHTalkTool.m
//  Shanglutong
//
//  Created by Ming Tian on 2017/9/26.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "TMHTalkTool.h"

@implementation TMHTalkTool

#pragma mark - 自定义方法:获取用户头像和昵称
+ (KWUserAllMenu *)getUserMenuWithHxid:(NSString *)hxid {
    NSArray *dataArray = [KWAddressViewController sharedAddressVC].userAddress;
    for(KWUserAllMenu *menu in dataArray) {
        if([menu.Hxid isEqualToString:hxid]) {
            return menu;
        }
    }
    return nil;
}

@end

//
//  KWMailBoxResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWMailBox.h"
@class KWMailBoxLabelList;
@interface KWMailBoxResult : NSObject

/**
 用户文件夹数量
 */
@property (nonatomic, strong) NSArray <KWMailBoxLabelList *> *back;

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *timestamp;

@end

@interface KWMailBoxLabelList : NSObject

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *name;


@end

//{
//    back =     (
//                {
//                    email = "inquiry@foodchem.cn";
//                    id = 11;
//                    name = "\U63a8\U5e7f\U544a";
//                },
//                {
//                    email = "info@18518.net";
//                    id = 8;
//                    name = "\U4e1a\U52a1";
//                }
//                );
//    code = 0000;
//    timestamp = 1501654184;
//}

//{
//     back =     (
//            {
//                email = "<null>";
//                id = "b_8_SJX";
//                name = "\U6536\U4ef6\U7bb1";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "b_8_CGX";
//                name = "\U8349\U7a3f\U7bb1";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "b_8_YFS";
//                name = "\U5df2\U53d1\U9001\U90ae\U4ef6";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "b_8_LJX";
//                name = "\U5783\U573e\U7bb1";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "b_8_YSC";
//                name = "\U5df2\U5220\U9664\U90ae\U4ef6";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_99";
//                name = "usahost-2334333";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_109";
//                name = "\U534e\U4e3a\U516c\U53f8";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_110";
//                name = "china wps";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_113";
//                name = "\U5404\U56de\U5404\U5bb6";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_119";
//                name = "\U5de5\U5382";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_121";
//                name = USPS;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_122";
//                name = "Novelia Roberta";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_123";
//                name = "Delivery Clerk";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_133";
//                name = 2555;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_134";
//                name = 65;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_135";
//                name = 666;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_138";
//                name = cs;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_139";
//                name = cs;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_140";
//                name = cs333;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_141";
//                name = cs333;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_142";
//                name = "\U6d4b\U8bd5\Uff01";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_145";
//                name = cbb;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_146";
//                name = nnn;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_147";
//                name = nnn;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_148";
//                name = "\U4f60\U4eec";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_150";
//                name = asd;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_153";
//                name = hahahha;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_154";
//                name = 1;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_176";
//                name = CSCSCS;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_189";
//                name = 43;
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_190";
//                name = "\U65b0\U5efa\U6587\U4ef6\U5939";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_192";
//                name = "\U5730\U65b9";
//                next = "<null>";
//            },
//            {
//                email = "<null>";
//                id = "f_8_206";
//                name = "\U6211\U7684";
//                next = "<null>";
//            }
//            );
//code = 0000;
//timestamp = 1501653568;
//}

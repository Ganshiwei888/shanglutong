//
//  KWCutomPageOneView.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCutomPageOneView.h"
#import "Utility.h"

@implementation KWCutomPageOneView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]){
        [self addOneView];
        [self addTwoView];
        [self addThreeView];
        [self addFourView];
    }
    return self;
}

//  第一部分  订单
- (void)addOneView{
    //总
    UIView *viewAllOne = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 140)];
    viewAllOne.backgroundColor = RGBACOLOR(251, 251, 251, 1);
    [self addSubview:viewAllOne];
    //头部标题
    UILabel *headLabelOne =[[UILabel alloc]initWithFrame:CGRectMake(10, 6, KWSCREEN_WIDTH/2, 12)];
    headLabelOne.text = @"订单";
    headLabelOne.textColor =RGBACOLOR(143, 143, 143, 1);
    headLabelOne.textAlignment = NSTextAlignmentLeft;
    headLabelOne.font = [UIFont systemFontOfSize:12];
    [viewAllOne addSubview:headLabelOne];
    //内部第一个
    UIView *viewOneOne = [[UIView alloc]initWithFrame:CGRectMake(2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneOne.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneOne];
    
    UILabel *labelTitleOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneOne.frame.size.width, 14.5)];
    labelTitleOneOne.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneOne.text = @"我司订单额";
    labelTitleOneOne.textAlignment = NSTextAlignmentCenter;
    labelTitleOneOne.font = [UIFont systemFontOfSize:14];
    [viewOneOne addSubview:labelTitleOneOne];
    
    
    UILabel *labelBodyOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneOne.frame.size.width, 25)];
    labelBodyOneOne.textColor = RGBACOLOR(19, 144, 67, 1);
    labelBodyOneOne.text = @"235";
    labelBodyOneOne.textAlignment = NSTextAlignmentCenter;
    labelBodyOneOne.font = [UIFont systemFontOfSize:24.5];
    [viewOneOne addSubview:labelBodyOneOne];
    
    
    //内部第二个
    UIView *viewOneTwo = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneOne.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneTwo.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneTwo];
    
    UILabel *labelTitleOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneTwo.frame.size.width, 14.5)];
    labelTitleOneTwo.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneTwo.text = @"采购数";
    labelTitleOneTwo.textAlignment = NSTextAlignmentCenter;
    labelTitleOneTwo.font = [UIFont systemFontOfSize:14];
    [viewOneTwo addSubview:labelTitleOneTwo];
    
    
    UILabel *labelBodyOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneTwo.frame.size.width, 25)];
    labelBodyOneTwo.textColor = RGBACOLOR(19, 144, 67, 1);
    labelBodyOneTwo.text = @"557";
    labelBodyOneTwo.textAlignment = NSTextAlignmentCenter;
    labelBodyOneTwo.font = [UIFont systemFontOfSize:24.5];
    [viewOneTwo addSubview:labelBodyOneTwo];
    
    
    //内部第三个
    UIView *viewOneThree = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneTwo.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneThree.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneThree];
    
    UILabel *labelTitleOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneThree.frame.size.width, 14.5)];
    labelTitleOneThree.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneThree.text = @"海关占比";
    labelTitleOneThree.textAlignment = NSTextAlignmentCenter;
    labelTitleOneThree.font = [UIFont systemFontOfSize:14];
    [viewOneThree addSubview:labelTitleOneThree];
    
    
    UILabel *labelBodyOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneThree.frame.size.width, 25)];
    labelBodyOneThree.textColor = RGBACOLOR(19, 144, 67, 1);
    labelBodyOneThree.text = @"25%";
    labelBodyOneThree.textAlignment = NSTextAlignmentCenter;
    labelBodyOneThree.font = [UIFont systemFontOfSize:24.5];
    [viewOneThree addSubview:labelBodyOneThree];
    
}

//  第二部分  订单
- (void)addTwoView{
    //总
    UIView *viewAllOne = [[UIView alloc]initWithFrame:CGRectMake(0, 140, KWSCREEN_WIDTH, 140)];
    viewAllOne.backgroundColor = RGBACOLOR(251, 251, 251, 1);
    [self addSubview:viewAllOne];
    //头部标题
    UILabel *headLabelOne =[[UILabel alloc]initWithFrame:CGRectMake(10, 6, KWSCREEN_WIDTH/2, 12)];
    headLabelOne.text = @"商机";
    headLabelOne.textColor =RGBACOLOR(143, 143, 143, 1);
    headLabelOne.textAlignment = NSTextAlignmentLeft;
    headLabelOne.font = [UIFont systemFontOfSize:12];
    [viewAllOne addSubview:headLabelOne];
    //内部第一个
    UIView *viewOneOne = [[UIView alloc]initWithFrame:CGRectMake(2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneOne.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneOne];
    
    UILabel *labelTitleOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneOne.frame.size.width, 14.5)];
    labelTitleOneOne.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneOne.text = @"商机数量";
    labelTitleOneOne.textAlignment = NSTextAlignmentCenter;
    labelTitleOneOne.font = [UIFont systemFontOfSize:14];
    [viewOneOne addSubview:labelTitleOneOne];
    
    
    UILabel *labelBodyOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneOne.frame.size.width, 25)];
    labelBodyOneOne.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneOne.text = @"5";
    labelBodyOneOne.textAlignment = NSTextAlignmentCenter;
    labelBodyOneOne.font = [UIFont systemFontOfSize:24.5];
    [viewOneOne addSubview:labelBodyOneOne];
    
    
    //内部第二个
    UIView *viewOneTwo = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneOne.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneTwo.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneTwo];
    
    UILabel *labelTitleOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneTwo.frame.size.width, 14.5)];
    labelTitleOneTwo.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneTwo.text = @"产品数";
    labelTitleOneTwo.textAlignment = NSTextAlignmentCenter;
    labelTitleOneTwo.font = [UIFont systemFontOfSize:14];
    [viewOneTwo addSubview:labelTitleOneTwo];
    
    
    UILabel *labelBodyOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneTwo.frame.size.width, 25)];
    labelBodyOneTwo.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneTwo.text = @"235";
    labelBodyOneTwo.textAlignment = NSTextAlignmentCenter;
    labelBodyOneTwo.font = [UIFont systemFontOfSize:24.5];
    [viewOneTwo addSubview:labelBodyOneTwo];
    
    
    //内部第三个
    UIView *viewOneThree = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneTwo.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneThree.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneThree];
    
    UILabel *labelTitleOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneThree.frame.size.width, 14.5)];
    labelTitleOneThree.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneThree.text = @"成功率";
    labelTitleOneThree.textAlignment = NSTextAlignmentCenter;
    labelTitleOneThree.font = [UIFont systemFontOfSize:14];
    [viewOneThree addSubview:labelTitleOneThree];
    
    
    UILabel *labelBodyOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneThree.frame.size.width, 25)];
    labelBodyOneThree.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneThree.text = @"15%";
    labelBodyOneThree.textAlignment = NSTextAlignmentCenter;
    labelBodyOneThree.font = [UIFont systemFontOfSize:24.5];
    [viewOneThree addSubview:labelBodyOneThree];
}

//  第三部分  订单
- (void)addThreeView{
    //总
    UIView *viewAllOne = [[UIView alloc]initWithFrame:CGRectMake(0, 280, KWSCREEN_WIDTH, 140)];
    viewAllOne.backgroundColor = RGBACOLOR(251, 251, 251, 1);
    [self addSubview:viewAllOne];
    //头部标题
    UILabel *headLabelOne =[[UILabel alloc]initWithFrame:CGRectMake(10, 6, KWSCREEN_WIDTH/2, 12)];
    headLabelOne.text = @"报价";
    headLabelOne.textColor =RGBACOLOR(143, 143, 143, 1);
    headLabelOne.textAlignment = NSTextAlignmentLeft;
    headLabelOne.font = [UIFont systemFontOfSize:12];
    [viewAllOne addSubview:headLabelOne];
    //内部第一个
    UIView *viewOneOne = [[UIView alloc]initWithFrame:CGRectMake(2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneOne.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneOne];
    
    UILabel *labelTitleOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneOne.frame.size.width, 14.5)];
    labelTitleOneOne.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneOne.text = @"报价数量";
    labelTitleOneOne.textAlignment = NSTextAlignmentCenter;
    labelTitleOneOne.font = [UIFont systemFontOfSize:14];
    [viewOneOne addSubview:labelTitleOneOne];
    
    
    UILabel *labelBodyOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneOne.frame.size.width, 25)];
    labelBodyOneOne.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneOne.text = @"5";
    labelBodyOneOne.textAlignment = NSTextAlignmentCenter;
    labelBodyOneOne.font = [UIFont systemFontOfSize:24.5];
    [viewOneOne addSubview:labelBodyOneOne];
    
    
    //内部第二个
    UIView *viewOneTwo = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneOne.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneTwo.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneTwo];
    
    UILabel *labelTitleOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneTwo.frame.size.width, 14.5)];
    labelTitleOneTwo.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneTwo.text = @"产品数";
    labelTitleOneTwo.textAlignment = NSTextAlignmentCenter;
    labelTitleOneTwo.font = [UIFont systemFontOfSize:14];
    [viewOneTwo addSubview:labelTitleOneTwo];
    
    
    UILabel *labelBodyOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneTwo.frame.size.width, 25)];
    labelBodyOneTwo.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneTwo.text = @"235";
    labelBodyOneTwo.textAlignment = NSTextAlignmentCenter;
    labelBodyOneTwo.font = [UIFont systemFontOfSize:24.5];
    [viewOneTwo addSubview:labelBodyOneTwo];
    
    
    //内部第三个
    UIView *viewOneThree = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneTwo.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneThree.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneThree];
    
    UILabel *labelTitleOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneThree.frame.size.width, 14.5)];
    labelTitleOneThree.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneThree.text = @"成功率";
    labelTitleOneThree.textAlignment = NSTextAlignmentCenter;
    labelTitleOneThree.font = [UIFont systemFontOfSize:14];
    [viewOneThree addSubview:labelTitleOneThree];
    
    
    UILabel *labelBodyOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneThree.frame.size.width, 25)];
    labelBodyOneThree.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneThree.text = @"15%";
    labelBodyOneThree.textAlignment = NSTextAlignmentCenter;
    labelBodyOneThree.font = [UIFont systemFontOfSize:24.5];
    [viewOneThree addSubview:labelBodyOneThree];
}

//  第四部分  订单
- (void)addFourView{
    //总
    UIView *viewAllOne = [[UIView alloc]initWithFrame:CGRectMake(0, 420, KWSCREEN_WIDTH, 254)];
    viewAllOne.backgroundColor = RGBACOLOR(251, 251, 251, 1);
    [self addSubview:viewAllOne];
    //头部标题
    UILabel *headLabelOne =[[UILabel alloc]initWithFrame:CGRectMake(10, 6, KWSCREEN_WIDTH/2, 12)];
    headLabelOne.text = @"关注产品";
    headLabelOne.textColor =RGBACOLOR(143, 143, 143, 1);
    headLabelOne.textAlignment = NSTextAlignmentLeft;
    headLabelOne.font = [UIFont systemFontOfSize:12];
    [viewAllOne addSubview:headLabelOne];
    //内部第一个
    UIView *viewOneOne = [[UIView alloc]initWithFrame:CGRectMake(2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneOne.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneOne];
    
    UILabel *labelTitleOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneOne.frame.size.width, 14.5)];
    labelTitleOneOne.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneOne.text = @"询盘";
    labelTitleOneOne.textAlignment = NSTextAlignmentCenter;
    labelTitleOneOne.font = [UIFont systemFontOfSize:14];
    [viewOneOne addSubview:labelTitleOneOne];
    
    
    UILabel *labelBodyOneOne = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneOne.frame.size.width, 25)];
    labelBodyOneOne.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneOne.text = @"5";
    labelBodyOneOne.textAlignment = NSTextAlignmentCenter;
    labelBodyOneOne.font = [UIFont systemFontOfSize:24.5];
    [viewOneOne addSubview:labelBodyOneOne];
    
    
    //内部第二个
    UIView *viewOneTwo = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneOne.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneTwo.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneTwo];
    
    UILabel *labelTitleOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneTwo.frame.size.width, 14.5)];
    labelTitleOneTwo.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneTwo.text = @"报价";
    labelTitleOneTwo.textAlignment = NSTextAlignmentCenter;
    labelTitleOneTwo.font = [UIFont systemFontOfSize:14];
    [viewOneTwo addSubview:labelTitleOneTwo];
    
    
    UILabel *labelBodyOneTwo = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneTwo.frame.size.width, 25)];
    labelBodyOneTwo.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneTwo.text = @"23";
    labelBodyOneTwo.textAlignment = NSTextAlignmentCenter;
    labelBodyOneTwo.font = [UIFont systemFontOfSize:24.5];
    [viewOneTwo addSubview:labelBodyOneTwo];
    
    
    //内部第三个
    UIView *viewOneThree = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(viewOneTwo.frame)+2, 24+2, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneThree.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneThree];
    
    UILabel *labelTitleOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneThree.frame.size.width, 14.5)];
    labelTitleOneThree.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneThree.text = @"商机";
    labelTitleOneThree.textAlignment = NSTextAlignmentCenter;
    labelTitleOneThree.font = [UIFont systemFontOfSize:14];
    [viewOneThree addSubview:labelTitleOneThree];
    
    
    UILabel *labelBodyOneThree = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneThree.frame.size.width, 25)];
    labelBodyOneThree.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneThree.text = @"17";
    labelBodyOneThree.textAlignment = NSTextAlignmentCenter;
    labelBodyOneThree.font = [UIFont systemFontOfSize:24.5];
    [viewOneThree addSubview:labelBodyOneThree];
    
    //内部第四个
    UIView *viewOneFour = [[UIView alloc]initWithFrame:CGRectMake(2, 24+2+116, (KWSCREEN_WIDTH-8)/3, 114)];
    viewOneFour.backgroundColor = [UIColor whiteColor];
    [viewAllOne addSubview:viewOneFour];
    
    UILabel *labelTitleOneFour = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, viewOneFour.frame.size.width, 14.5)];
    labelTitleOneFour.textColor = RGBACOLOR(143, 143, 143, 1);
    labelTitleOneFour.text = @"海关";
    labelTitleOneFour.textAlignment = NSTextAlignmentCenter;
    labelTitleOneFour.font = [UIFont systemFontOfSize:14];
    [viewOneFour addSubview:labelTitleOneFour];
    
    
    UILabel *labelBodyOneFour = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, viewOneFour.frame.size.width, 25)];
    labelBodyOneFour.textColor = RGBACOLOR(19, 125, 192, 1);
    labelBodyOneFour.text = @"5";
    labelBodyOneFour.textAlignment = NSTextAlignmentCenter;
    labelBodyOneFour.font = [UIFont systemFontOfSize:24.5];
    [viewOneFour addSubview:labelBodyOneFour];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

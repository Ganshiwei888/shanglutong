//
//  KWCommentParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWCommentParam : KWBaseParam

@property (copy, nonatomic) NSString *Pageindex;
@property (copy, nonatomic) NSString *Pagemax;
@property (copy, nonatomic) NSString *Act;

@end

//
//  KWCustomDetailModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWCustomDetailModel : NSObject

@property (nonatomic,strong)NSString *BranchId;
@property (nonatomic,strong)NSString *DeptId;
@property (nonatomic,strong)NSString *No;
@property (nonatomic,strong)NSString *SName;
@property (nonatomic,strong)NSString *Name;
@property (nonatomic,strong)NSString *Address;
@property (nonatomic,strong)NSString *PostCode;
@property (nonatomic,strong)NSString *Country;
@property (nonatomic,strong)NSString *Province;
@property (nonatomic,strong)NSString *City;
@property (nonatomic,strong)NSString *Tel;
@property (nonatomic,strong)NSString *Fax;
@property (nonatomic,strong)NSString *Web;
@property (nonatomic,strong)NSString *Bak;
@property (nonatomic,strong)NSString *ClientClass;
@property (nonatomic,strong)NSString *ClientLevel;
@property (nonatomic,strong)NSString *ClientState;
@property (nonatomic,strong)NSString *ClientSource;
@property (nonatomic,strong)NSString *ClientType;
@property (nonatomic,strong)NSString *ClientLock;
@property (nonatomic,strong)NSString *Aud;
@property (nonatomic,strong)NSString *SDate;
@property (nonatomic,strong)NSString *RDate;
@property (nonatomic,strong)NSString *KfTime;
@property (nonatomic,strong)NSString *Yj;
@property (nonatomic,strong)NSString *Pf;
@property (nonatomic,strong)NSString *EDM;
@property (nonatomic,strong)NSString *Pub;
@property (nonatomic,strong)NSString *PubTime;
@property (nonatomic,strong)NSString *JoinTime;
@property (nonatomic,strong)NSString *IsDel;
@property (nonatomic,strong)NSString *DelTime;
@property (nonatomic,strong)NSString *DelUserId;
@property (nonatomic,strong)NSString *DelUserName;
@property (nonatomic,strong)NSString *DelReason;
@property (nonatomic,strong)NSString *Creater;
@property (nonatomic,strong)NSString *CreaterName;
@property (nonatomic,strong)NSString *CreateTm;
@property (nonatomic,strong)NSString *Updater;
@property (nonatomic,strong)NSString *UpdaterName;
@property (nonatomic,strong)NSString *UpdateTm;
@property (nonatomic,strong)NSString *Star;

@end

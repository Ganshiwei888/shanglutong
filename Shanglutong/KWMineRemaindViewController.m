//
//  KWMineRemaindViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMineRemaindViewController.h"
#import "Utility.h"

@interface KWMineRemaindViewController ()<UITableViewDataSource, UITableViewDelegate> {
    UITableView *_tableView;
    NSArray *_setArray;
}

@end

@implementation KWMineRemaindViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.navigationController.navigationBar.translucent = YES;
    self.title = @"提醒设置";
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    
    self.tabBarController.tabBar.hidden = NO;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createTableView];
}

- (void)createTableView
{
    //设置数据
    _setArray = [NSArray arrayWithObjects:@"声音",@"震动",@"消息免打扰", nil];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}


#pragma mark - UITableViewDelegate  UItableVIewDateSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _setArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 10)];
    view.backgroundColor = RGBACOLOR(240, 240, 240, 1);
    return view;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = _setArray[indexPath.section];
    cell.textLabel.textColor = RGBCOLOR(145, 145, 145);
    if (indexPath.section ==0){
        UISwitch *switchBtn = [[UISwitch alloc]initWithFrame:CGRectMake(0, 0, 70, 40)];
        [switchBtn addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchBtn;
    }else if (indexPath.section == 1){
        UISwitch *switchBtn = [[UISwitch alloc]initWithFrame:CGRectMake(0, 0, 70, 40)];
        [switchBtn addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchBtn;
    }else {
        cell.detailTextLabel.text = @"23:00 - 08:00";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}




#pragma 开关
- (void)switchBtnClick:(UISwitch *)sender
{
    if (sender.isOn){
        //开启手势验证
    }else {
        //关闭
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

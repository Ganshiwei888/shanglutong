//
//  KWMailListParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailListParam : KWBaseParam

@property (nonatomic, copy) NSString *pageindex;

@property (nonatomic, copy) NSString *pagemax;

@property (nonatomic, copy) NSString *act;

@property (nonatomic, copy) NSString *Boxid;

@property (nonatomic, copy) NSString *key;

@property (nonatomic, copy) NSString *where;

@end

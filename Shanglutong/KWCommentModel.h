//
//  KWCommentModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWCommentModel : NSObject

@property (copy, nonatomic) NSString *Bak;
@property (copy, nonatomic) NSString *CreateTm;
@property (copy, nonatomic) NSString *Creater;
@property (copy, nonatomic) NSString *CreaterName;
@property (copy, nonatomic) NSString *Id;
@property (copy, nonatomic) NSString *KeyId;
@property (copy, nonatomic) NSString *MenuNo;
@property (copy, nonatomic) NSString *ShareUID;
@property (copy, nonatomic) NSString *ShareUID1;
@property (copy, nonatomic) NSString *TableName;
@property (copy, nonatomic) NSString *TitleHtml;
@property (copy, nonatomic) NSString *WdId;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//
//Bak = 111;
//CreateTm = "2017-08-19T21:04:07.927";
//Creater = admin;
//CreaterName = "\U7ba1\U7406\U5458";
//Id = 189;
//KeyId = 29229;
//MenuNo = AB;
//ShareUID = ",1,121,126,147,";
//ShareUID1 = "-1";
//TableName = "Client_T";
//TitleHtml = "[DZ17070006]4444we";
//WdId = 0;





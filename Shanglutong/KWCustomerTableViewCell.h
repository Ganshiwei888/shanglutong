//
//  KWCustomerTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWCustomerFrame;
@interface KWCustomerTableViewCell : UITableViewCell

/**
 customer的frame模型
 */
@property (nonatomic, strong) KWCustomerFrame *customerFrame;

+ (instancetype)cellWithCustomer:(UITableView *)tableView;

@end

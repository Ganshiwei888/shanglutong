//
//  KWUserAllMenu.h
//  
//
//  Created by KeWen on 2017/8/14.
//
//

#import <Foundation/Foundation.h>

@interface KWUserAllMenu : NSObject

@property (nonatomic, copy) NSString *BranchId;
@property (nonatomic, copy) NSString *DeptId;
@property (nonatomic, copy) NSString *PicName;
@property (nonatomic, copy) NSString *duty;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *userid;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *Hxid;
@property (nonatomic, copy) NSString *Password;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (NSString *)decryptionSubPassword:(NSString *)password;

@end

//         {
//                BranchId = 0;
//                DeptId = 24;
//                PicName = "201708051738193819admin.jpg";
//                duty = "";
//                email = "<null>";
//                mobile = 18888888888;
//                sex = "\U7537";
//                userid = admin;
//                username = "\U7ba1\U7406\U5458";
//         }








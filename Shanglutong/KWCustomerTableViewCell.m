//
//  KWCustomerTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerTableViewCell.h"
#import "KWCustomerFrame.h"
#import "KWCustomer.h"
#import "KWCustomerContent.h"
#import "Utility.h"

@interface KWCustomerTableViewCell ()

@property (nonatomic, weak) KWCustomerContent *customerContent;

@end

@implementation KWCustomerTableViewCell

+ (instancetype)cellWithCustomer:(UITableView *)tableView {
    static NSString *ID = @"status";
    KWCustomerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[KWCustomerTableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.backgroundColor = RGBCOLOR(239, 239, 244);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupContentView];
    }
    return self;
}

- (void)setupContentView {
    KWCustomerContent *customerContent = [[KWCustomerContent alloc] initWithFrame:CGRectMake(8, 5, KWSCREEN_WIDTH - 16, 150)];
    [self.contentView addSubview:customerContent];
    self.customerContent = customerContent;
}

#pragma mark - 传递数据模型 - 

- (void)setCustomerFrame:(KWCustomerFrame *)customerFrame {
    _customerFrame = customerFrame;
    
    self.customerContent.customerFrame = self.customerFrame;
    
}


//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end

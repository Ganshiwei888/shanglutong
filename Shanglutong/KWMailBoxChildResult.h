//
//  KWMailBoxChildResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWMailBoxChildList.h"

@interface KWMailBoxChildResult : KWMailBoxChildListModel

@property (nonatomic, strong) NSMutableArray *childResult;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *timestamp;

@end

//
//  KWMailRemoveCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWMailRemove.h"

@protocol KWMailRemoveCellDelegate <NSObject>

- (void)tableCellBtndidSelect:(UIButton *)button;

@end

@interface KWMailRemoveCell : UITableViewCell

@property (weak, nonatomic) UIButton *selectBtn;

@property (weak, nonatomic) UILabel *folderName;

@property (strong, nonatomic) KWMailRemove *mailRemove;

@property (weak, nonatomic) id<KWMailRemoveCellDelegate> delegate;

@end

//
//  KWCustomerFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerFrame.h"
#import "KWCustomer.h"
#import "Utility.h"

@implementation KWCustomerFrame

- (void)setCustomer:(KWCustomer *)customer {
    _customer = customer;
    
//    _noLabelView
    CGFloat noLabelViewW = 30;
    CGFloat noLabelViewH = 16;
    CGFloat noLabelViewX = 16;
    CGFloat noLabelViewY = 5;
    _noLabelView = CGRectMake(noLabelViewX, noLabelViewY, noLabelViewW, noLabelViewH);
    
//    _noValueView
    CGFloat noValueViewW = 80;
    CGFloat noValueViewH = 16;
    CGFloat noValueViewX = CGRectGetMaxX(_noLabelView) + 16;
    CGFloat noValueViewY = CGRectGetMinY(_noLabelView);
    _noValueView = CGRectMake(noValueViewX, noValueViewY, noValueViewW, noValueViewH);
    
//    _saleLabelView
    CGFloat saleLabelViewW = 48;
    CGFloat saleLabelViewH = 16;
    CGFloat saleLabelViewX = KWSCREEN_WIDTH - 115;
    CGFloat saleLabelViewY = CGRectGetMinY(_noLabelView);
    _saleLabelView = CGRectMake(saleLabelViewX, saleLabelViewY, saleLabelViewW, saleLabelViewH);
    
//    _saleValueView
    CGFloat saleValueViewW = 50;
    CGFloat saleValueViewH = 16;
    CGFloat saleValueViewX = CGRectGetMaxX(_saleLabelView) + 8;
    CGFloat saleValueViewY = CGRectGetMinY(_noLabelView);
    _saleValueView = CGRectMake(saleValueViewX, saleValueViewY, saleValueViewW, saleValueViewH);
    
//    _customerNameView
    CGFloat customerNameW = KWSCREEN_WIDTH - 36;
    CGFloat customerNameH = 25;
    CGFloat customerNameX = 16;
    CGFloat customerNameY = 29;
    _customerNameView = CGRectMake(customerNameX, customerNameY, customerNameW, customerNameH);
    
//    _dividingImageView
    CGFloat dividingImageViewW = KWSCREEN_WIDTH;
    CGFloat dividingImageViewH = 1;
    CGFloat dividingImageViewX = 0;
    CGFloat dividingImageViewY = 60;
    _dividingImageView = CGRectMake(dividingImageViewX, dividingImageViewY, dividingImageViewW, dividingImageViewH);
    
//    _typeImageView
    CGFloat typeImageViewW = 20;
    CGFloat typeImageViewH = 20;
    CGFloat typeImageViewX = 16;
    CGFloat typeImageViewY = 70;
    _typeImageView = CGRectMake(typeImageViewX, typeImageViewY, typeImageViewW, typeImageViewH);
    
//    _typeLabelView
    CGFloat typeLabelViewW = 40;
    CGFloat typeLabelViewH = 20;
    CGFloat typeLabelViewX = CGRectGetMaxX(_typeImageView) + 8;
    CGFloat typeLabelViewY = CGRectGetMaxY(_dividingImageView) + 8;
    _typeLabelView = CGRectMake(typeLabelViewX, typeLabelViewY, typeLabelViewW, typeLabelViewH);
    
//    _typeValueView
    CGFloat typeValueViewW = 70;
    CGFloat typeValueViewH = 20;
    CGFloat typeValueViewX = CGRectGetMaxX(_typeLabelView) + 8;
    CGFloat typeValueViewY = CGRectGetMaxY(_dividingImageView) + 8;
    _typeValueView = CGRectMake(typeValueViewX, typeValueViewY, typeValueViewW, typeValueViewH);
    
//    _timeImageView
    CGFloat timeImageViewW = 20;
    CGFloat timeImageViewH = 20;
    CGFloat timeImageViewX = 16;
    CGFloat timeImageViewY = 98;
    _timeImageView = CGRectMake(timeImageViewX, timeImageViewY, timeImageViewW, timeImageViewH);
    
//    _timeLabelView
    CGFloat timeLabelViewW = 98;
    CGFloat timeLabelViewH = 20;
    CGFloat timeLabelViewX = 44;
    CGFloat timeLabelViewY = 98;
    _timeLabelView = CGRectMake(timeLabelViewX, timeLabelViewY, timeLabelViewW, timeLabelViewH);
    
//    _timeValueView
    CGFloat timeValueViewW = 209;
    CGFloat timeValueViewH = 20;
    CGFloat timeValueViewX = 150;
    CGFloat timeValueViewY = 98;
    _timeValueView = CGRectMake(timeValueViewX, timeValueViewY, timeValueViewW, timeValueViewH);
    
//    _warnImageView
    CGFloat warnImageViewW = 20;
    CGFloat warnImageViewH = 20;
    CGFloat warnImageViewX = 16;
    CGFloat warnImageViewY = 126;
    _warnImageView = CGRectMake(warnImageViewX, warnImageViewY, warnImageViewW, warnImageViewH);
    
//    _warnLabelView
    CGFloat warnLabelViewW = 65;
    CGFloat warnLabelViewH = 20;
    CGFloat warnLabelViewX = 44;
    CGFloat warnLabelViewY = 126;
    _warnLabelView = CGRectMake(warnLabelViewX, warnLabelViewY, warnLabelViewW, warnLabelViewH);
    
//    _warnValueView
    CGFloat warnValueViewW = 70;
    CGFloat warnValueViewH = 20;
    CGFloat warnValueViewX = 117;
    CGFloat warnValueViewY = 126;
    _warnValueView = CGRectMake(warnValueViewX, warnValueViewY, warnValueViewW, warnValueViewH);
    
}

@end











//
//  KWCustomerViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/26.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWCustomerResult.h"

@interface KWCustomerViewController : UIViewController
/**
 客户模型
 */
@property (nonatomic, strong) KWCustomerResult *customerResult;

@end

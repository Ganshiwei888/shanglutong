//
//  KWAddressViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWAddressViewController : UIViewController

@property (nonatomic, strong) NSArray *userAddress;
@property (nonatomic, weak) UISegmentedControl *segment;
@property (nonatomic, assign) int segmentIndex;
+ (instancetype) sharedAddressVC;


@end

//
//  KWCommaddViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RefreshComm)(NSString *keyid);

@interface KWCommaddViewController : UIViewController

@property (copy, nonatomic) NSString *keyId;

@property (copy, nonatomic) NSString *MenuNo;

@property (copy, nonatomic) RefreshComm refreshComm;

@end

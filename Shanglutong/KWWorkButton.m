//
//  KWWorkButton.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWWorkButton.h"
#import "Utility.h"

@implementation KWWorkButton

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [self setTitleColor: [UIColor grayColor] forState:UIControlStateNormal];
        [self setTitleColor: [UIColor blueColor] forState:UIControlStateSelected];
        
    }
    return self;
}

/**
 重写去掉高亮状态
 
 @param highlighted 高亮
 */
- (void)setHighlighted:(BOOL)highlighted {
    
}

/**
 内部图片的frame
 
 @return 返回图片的frame
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height * KWTabBarButtonImageRatio;
    
    return CGRectMake(0, 5, imageW, imageH);
    
}

/**
 内部文字的frame
 
 @param contentRect 文字的contentRect
 @return 文字的frame
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
    CGFloat titleX = 0;
    CGFloat titleY = contentRect.size.height * KWTabBarButtonImageRatio;
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height - titleY;
    
    return CGRectMake(titleX, titleY, titleW, titleH);
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

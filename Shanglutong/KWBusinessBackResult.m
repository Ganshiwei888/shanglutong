//
//  KWBusinessBackResult.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/17.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBusinessBackResult.h"
#import "NSObject+KWModel.h"

@implementation KWBusinessBackResult

+ (NSDictionary *)arrayContainModelClass {
    return @{@"msg" : @"KWBusinessMsg"};
}

@end

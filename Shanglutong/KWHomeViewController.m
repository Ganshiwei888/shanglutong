//
//  KWHomeViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWHomeViewController.h"
#import "KWMailHomeViewController.h"
#import "KWMailLeftViewController.h"
#import "KWMineViewController.h"
#import "KWAddressViewController.h"
#import "KWCustomerViewController.h"
#import "KWCustomerLeftViewController.h"
#import "KWNavigationController.h"
#import "KWMoreViewController.h"
#import "KWWorkBenchViewController.h"
#import "KWTalkViewController.h"
#import "UIImage+KW.h"
#import "ConversationListController.h"
#import "KWTabBar.h"
#import "KWLoginViewController.h"
#import "AppDelegate.h"
#import "KGModal.h"
#import <MMDrawerController.h>

@interface KWHomeViewController () <KWTabBarDelegate, EMClientDelegate>

@property(nonatomic,strong) MMDrawerController * drawerController;

@end

@implementation KWHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTarBar];
    [self.customTabBar.plusButton addTarget:self action:@selector(plusButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self setupAllChildViewControllers];
    self.selectedIndex = 1;
    
    EMOptions *options = [EMOptions optionsWithAppkey:@"dandan9609#salet"];
    options.apnsCertName = @"";
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];

    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];


    // Do any additional setup after loading the view.
}

- (void)autoLoginDidCompleteWithError:(EMError *)error {

}

- (void)userAccountDidLoginFromOtherDevice {
    
    NSString *message = @"账号已在其他设备登录";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        [self logout];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    [alert addAction:action];
    [alert addAction:cancel];
    [self showDetailViewController:alert sender:nil];

}

- (void)logout {
    
    KWLoginViewController *vc = [[KWLoginViewController alloc] init];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    window.rootViewController = vc;
    [window makeKeyAndVisible];
//    [self presentViewController:vc animated:YES completion:^{
    
//    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 删除系统自动生成的UITabBarButton
    for (UIView *child in self.tabBar.subviews) {
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}

- (void)plusButtonClick:(UIButton *)sender {
    KWMoreViewController *moreVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"moreVC"];
    [KGModal sharedInstance].closeButtonType = KGModalCloseButtonTypeNone;
    [[KGModal sharedInstance] showWithContentViewController:moreVC andAnimated:YES];
}


- (void)setupTarBar {
    KWTabBar *customTabBar = [[KWTabBar alloc] init];
    CGFloat customW = self.view.frame.size.width;
    CGFloat customH = self.tabBar.frame.size.height;
    customTabBar.frame = CGRectMake(0, 0, customW, customH);
    customTabBar.delegate = self;
    self.selectedIndex = 1;

    [self.tabBar addSubview:customTabBar];
    self.customTabBar = customTabBar;
}

- (void)tabBar:(KWTabBar *)tabBar didSelectedButtonFrom:(long)from to:(long)to {
    self.selectedIndex = to;
}

- (void)setupAllChildViewControllers {
    
//    KWMailHomeViewController *home = [[KWMailHomeViewController alloc] init];
//    home.tabBarItem.badgeValue = @"10";
//    [self setupChildViewController:home title:@"会话" imageName:@"会话-1" selectedImageName:@"会话-1"];

    KWTalkViewController *talk = [[KWTalkViewController alloc] init];
    
    [self setupChildViewController:talk title:@"会话" imageName:@"会话-1" selectedImageName:@"会话ed"];
    
//    KWCustomerViewController *customer = [[KWCustomerViewController alloc] init];
//    [self setupChildViewController:customer title:@"工作台" imageName:@"工作台-2" selectedImageName:@"工作台-2"];
    
    KWWorkBenchViewController *work = [[KWWorkBenchViewController alloc] init];
    [self setupChildViewController:work title:@"工作台" imageName:@"工作台-1" selectedImageName:@"工作台ed"];
    
    KWAddressViewController *address = [[KWAddressViewController alloc] init];
    [self setupChildViewController:address title:@"通讯录" imageName:@"通讯录-1" selectedImageName:@"通讯录ed"];
    KWMineViewController *mine = [[KWMineViewController alloc] init];
    [self setupChildViewController:mine title:@"我的" imageName:@"我的-1" selectedImageName:@"我的ed"];
}

/**
 初始化
 
 @param childVc 子控制器
 @param title 标题
 @param imageName 图片名
 @param selectedImageName 选中的图片名
 */
- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName {
    
    // 1.设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
    
    childVc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:childVc];
    
    [self addChildViewController:nav];
    
//    if ([title isEqualToString:@"邮件"]) {
//        KWMailLeftViewController *leftVc = [[KWMailLeftViewController alloc] init];
//        KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:leftVc];
//        MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:nav leftDrawerViewController: leftNav];
//        drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningNavigationBar;
//        drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
//        //    [drawerController.navigationController setNavigationBarHidden:YES];
//        //5、设置左右两边抽屉显示的多少
//        drawerController.maximumLeftDrawerWidth = 280.0;
//        drawerController.maximumRightDrawerWidth = 280.0;
//       
//        
//        [self addChildViewController:drawerController];
//        
//    } else {
//        
//        KWCustomerLeftViewController *leftVc = [[KWCustomerLeftViewController alloc] init];
//        KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:leftVc];
//        MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:nav leftDrawerViewController: leftNav];
//        drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningNavigationBar;
//        drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
//        //    [drawerController.navigationController setNavigationBarHidden:YES];
//        //5、设置左右两边抽屉显示的多少
//        drawerController.maximumLeftDrawerWidth = 280.0;
//        drawerController.maximumRightDrawerWidth = 280.0;
//        
//        [self addChildViewController:drawerController];
//        
//    }
    
    // 3.添加tabbar内部的按钮
    [self.customTabBar addTabBarWithItem:childVc.tabBarItem];
//    NSString *asd = self.tabBarItem.title;
//    NSLog(@"%@",asd);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


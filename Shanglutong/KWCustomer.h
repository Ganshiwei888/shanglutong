//
//  KWCustomer.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWCustomer : NSObject

/**
 用户编号
 */
@property (nonatomic, copy) NSString *customerNO;
/**
 业务员
 */
@property (nonatomic, copy) NSString *customerSale;
/**
 客户姓名
 */
@property (nonatomic, copy) NSString *customerSName;
/**
 客户状态
 */
@property (nonatomic, copy) NSString *customerState;
/**
 客户状态
 */
@property (nonatomic, copy) NSString *customerType;
/**
 上次联系时间
 */
@property (nonatomic, copy) NSString *customerTime;
/**
 销售预警
 */
@property (nonatomic, copy) NSString *customerWarn;
/**
 用户级别
 */
@property (nonatomic, copy) NSString *customerClass;
/**
 用户星级
 */
@property (nonatomic, copy) NSString *customerLevel;

- (instancetype)initWithDict:(NSDictionary *)dict;
+ (instancetype)customerWithDict:(NSDictionary *)dict;

@end


//
//  KWAddressDetailViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWAddressDetailViewController : UIViewController

@property (strong, nonatomic) NSArray *detailArray;

- (void)loadTableView;

@end

//
//  KWMailListResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWMailListModel;
@interface KWMailListResult : NSObject



@property (nonatomic,strong) NSString *count;

@property (nonatomic,strong) NSString *index;

@property (nonatomic,strong) NSString *unread;
/**
 存放邮件模型
 */
@property (nonatomic,strong) NSMutableArray <KWMailListModel *> *list;

@property (nonatomic,strong) NSString *timestamp;


@end

@interface KWMailListModel : NSObject

@property (nonatomic, copy) NSString *AccCount;
@property (nonatomic, copy) NSString *Area;
@property (nonatomic, copy) NSString *Box;
@property (nonatomic, copy) NSString *BoxBase;
@property (nonatomic, assign) NSInteger isSelect;
@property (nonatomic, copy) NSString *CreateTime;
@property (nonatomic, copy) NSString *FromEmail;
@property (nonatomic, copy) NSString *FromName;
@property (nonatomic, copy) NSString *IP;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *MailBoxId;
@property (nonatomic, copy) NSString *MailDate;
@property (nonatomic, copy) NSString *MailLabel;
@property (nonatomic, copy) NSString *Read;
@property (nonatomic, copy) NSString *ReadMode;
@property (nonatomic, copy) NSString *MailType;
@property (nonatomic, copy) NSString *RecDate;
@property (nonatomic, copy) NSString *ReplyTo;
@property (nonatomic, copy) NSString *SendDate;
@property (nonatomic, copy) NSString *Subject;
@property (nonatomic, copy) NSString *TopTime;
@property (nonatomic, copy) NSString *bcc;
@property (nonatomic, copy) NSString *clientid;
@property (nonatomic, copy) NSString *itFrom;
@property (nonatomic, copy) NSString *itTo;
@property (nonatomic, copy) NSString *priority;
@property (nonatomic, copy) NSString *redflag;
@property (nonatomic, copy) NSString *star;
@property (nonatomic, copy) NSString *textbody;

@end

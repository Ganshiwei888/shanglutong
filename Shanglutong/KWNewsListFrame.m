//
//  KWNewsListFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWNewsListFrame.h"
#import "KWNewsListModel.h"
#import "Utility.h"


@implementation KWNewsListFrame

- (void)setListModel:(KWNewsListModel *)listModel {
    
    _listModel = listModel;
    
    CGFloat typeX = 25;
    CGFloat typeY = 46;
    CGFloat typeW = 28;
    CGFloat typeH = 28;
    _typeF = CGRectMake(typeX, typeY, typeW, typeH);
    
    CGFloat readImageX = 10;
    CGFloat readImageY = 55;
    CGFloat readImageW = 10;
    CGFloat readImageH = 10;
    _readImageF = CGRectMake(readImageX, readImageY, readImageW, readImageH);
    
//    CGFloat detailX = CGRectGetMaxX(_typeF) + 8;
//    CGFloat detailY = 18;
//    CGFloat detailW = 310;
//    CGFloat detailH = 80;
//    _detailF = CGRectMake(detailX, detailY, detailW, detailH);
    
    CGFloat detailX = 32;
    CGFloat detailY = 8;
    CGFloat detailW = KWSCREEN_WIDTH - 40;
    CGFloat detailH = 80;
    _detailF = CGRectMake(detailX, detailY, detailW, detailH);
    
    CGFloat comeX = 32;
    CGFloat comeY = CGRectGetMaxY(_detailF) + 4;
    CGFloat comeW = 24;
    CGFloat comeH = 16;
    _comeF = CGRectMake(comeX, comeY, comeW, comeH);
    
    CGFloat menuNoX = CGRectGetMaxX(_comeF) + 2;
    CGFloat menuNoY = comeY;
    CGFloat menuNoW = 30;
    CGFloat menuNoH = 16;
    _menuNoF = CGRectMake(menuNoX, menuNoY, menuNoW, menuNoH);
    
    CGFloat timeX = CGRectGetMaxX(_menuNoF) + 4;
    CGFloat timeY = comeY;
    CGFloat timeW = 120;
    CGFloat timeH = 16;
    _timeF = CGRectMake(timeX, timeY, timeW, timeH);
    
}

@end




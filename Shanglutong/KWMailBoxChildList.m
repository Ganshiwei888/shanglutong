//
//  KWMailBoxChildList.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailBoxChildList.h"
#import "Utility.h"

@implementation KWMailBoxChildList

@end

@implementation KWMailBoxChildListModel

+ (instancetype)modelWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _email = checkStringNull(dict[@"email"]);
        _mailId = checkStringNull(dict[@"id"]);
        _name = checkStringNull(dict[@"name"]);
        _next = checkStringNull(dict[@"next"]);
        _act = checkStringNull(dict[@"act"]);
        _pic = checkStringNull(dict[@"pic"]);
        _boxid = checkStringNull(dict[@"boxid"]);
    }
    return self;
}

@end

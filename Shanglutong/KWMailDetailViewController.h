//
//  KWMailDetailViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWMailListResult.h"

typedef void(^refreshTheList)(NSIndexPath *sender);
typedef void(^deleteMail)(NSIndexPath *sender);
typedef void(^topTheMail)(NSIndexPath *sender);
typedef void(^redTheMail)(NSIndexPath *sender);

@interface KWMailDetailViewController : UIViewController

@property (nonatomic, copy) refreshTheList relist;
@property (nonatomic, copy) deleteMail deleteMail;
@property (nonatomic, copy) topTheMail topTheMail;
@property (nonatomic, copy) redTheMail redTheMail;


- (void)recieveTheMailListArray:(KWMailListModel *)mailListModel AndAct:(NSString *)act;

- (void)reciiveTheMailList:(NSArray *)mailListArray andIndexPath:(NSIndexPath *)index;

- (void)didOpenbyApns:(NSString *)mailID;

@end






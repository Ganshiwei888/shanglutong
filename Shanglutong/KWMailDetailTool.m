//
//  KWMailDetailTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailDetailTool.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import <MJExtension.h>

@implementation KWMailDetailTool

+ (void)postGetMailDetailWith:(KWMailDetailParam *)param success:(void (^)(KWMailDetailResultModel *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mail/Get"];

    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        NSLog(@"okok%@",json);
        KWMailDetailResultModel *resultData = [KWMailDetailResultModel mj_objectWithKeyValues:[json valueForKey:@"back"]];
        success(resultData);
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end






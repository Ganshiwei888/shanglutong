//
//  KWComDetailTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWComDetailTableViewCell.h"
#import "KWComDetailFrame.h"
#import "KWComDetailModel.h"
#import "Utility.h"

@interface KWComDetailTableViewCell ()

@property (weak, nonatomic) UILabel *detailLabel;

@property (weak, nonatomic) UIImageView *userImageView;

@property (weak, nonatomic) UILabel *nameLabel;

@property (weak, nonatomic) UILabel *timeLabel;

@end

@implementation KWComDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setupCellView];
    }
    
    return self;
}

- (void)setupCellView {
    
    UILabel *detailLabel = [[UILabel alloc] init];
    detailLabel.font = [UIFont systemFontOfSize:12];
    detailLabel.textColor = [UIColor blackColor];
    detailLabel.numberOfLines = 0;
    [detailLabel sizeToFit];
    [self.contentView addSubview: detailLabel];
    self.detailLabel = detailLabel;
    
    UIImageView *userImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:userImageView];
    self.userImageView = userImageView;
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:10];
    nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont systemFontOfSize:10];
    timeLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
    
}

- (void)setComDetailFrame:(KWComDetailFrame *)comDetailFrame {
    _comDetailFrame = comDetailFrame;
    
    KWComDetailModel *detailModel = comDetailFrame.comDetailModel;
    
    self.detailLabel.text = detailModel.Bak;
    self.detailLabel.frame = comDetailFrame.detailF;
    
    self.userImageView.image = [UIImage imageNamed:@"user"];
    self.userImageView.frame = comDetailFrame.userImageF;
    
    self.nameLabel.text = detailModel.Creater;
    self.nameLabel.frame = comDetailFrame.nameF;
    
    NSString *timeOne = [detailModel.CreateTm substringToIndex:19];
    NSString *strUrl = [timeOne stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    self.timeLabel.text = strUrl;
    self.timeLabel.frame = comDetailFrame.timeF;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

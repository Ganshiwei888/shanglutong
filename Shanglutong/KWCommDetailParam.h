//
//  KWCommDetailParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWCommDetailParam : KWBaseParam

@property (copy, nonatomic) NSString *KeyId;

@end

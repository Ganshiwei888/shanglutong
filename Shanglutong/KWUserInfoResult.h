//
//  KWUserInfoResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWUserInfoResultModel;
@interface KWUserInfoResult : NSObject

@property (nonatomic, strong) KWUserInfoResultModel *resultModel;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSNumber *timestamp;
@property (nonatomic, strong) NSString *info;

@end

@interface KWUserInfoResultModel : NSObject


//{
//    back =     {
//        branch = "<null>";
//        department = "\U5546\U4e1a\U667a\U80fd\U90e8";
//        id = 219;
//        maillabel =         (
//                             {
//                                 name = "\U5f85\U5904\U7406|maillabel-14";
//                             }
//                             );
//        name = "8130-Andy Chen";
//        phone = "<null>";
//        sex = "\U7537";
//        state = "\U6b63\U5e38";
//        type = 1;
//    };
//    code = "\U5df2\U6ce8\U518c\U7528\U6237";
//    timestamp = 1501573392;
//}


@property (nonatomic, strong) NSString *branch;
@property (nonatomic, strong) NSString *department;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSDictionary *maillabel;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *phone;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSNumber *type;

@end













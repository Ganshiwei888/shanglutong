//
//  KWMoreViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/9.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMoreViewController.h"
#import "KWMoreCollectionViewCell.h"
#import "KGModal.h"
#import "KWMailSendViewController.h"
#import "Utility.h"

@interface KWMoreViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *yearAndMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *weakLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, copy) NSArray *cellNameArray;

@end

@implementation KWMoreViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.cellNameArray = @[@"写邮件", @"写报告", @"写动态", @"建客户", @"建报价", @"建商机"];
    self.cellNameArray = @[@"写邮件", @"写报告", @"写动态"];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.itemSize = CGSizeMake(KWSCREEN_WIDTH / 3.0, KWSCREEN_WIDTH / 3.0);
    self.collectionView.collectionViewLayout = layout;

    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)backButtonClick:(UIButton *)sender {
    [[KGModal sharedInstance] hide];
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item ==0){
        
        KWMailSendViewController *sendVc = [[KWMailSendViewController alloc] init];
        
        [sendVc recieveTheMailDetailModel:nil AndAttachmentArray:nil AndType:MailSendTypeNew AndMailBoxId:@"11" AndHtmlInfo:nil];
        UINavigationController *mailSendNavi = [[UINavigationController alloc] initWithRootViewController:sendVc];
        [self presentViewController:mailSendNavi animated:YES completion:^{
            [[KGModal sharedInstance] hide];
        }];
        
        
    }
}

#pragma mark - UICollectionViewDataSource
//定义展示的Section的个数
-( NSInteger )numberOfSectionsInCollectionView:( UICollectionView *)collectionView{
    return 1 ;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.cellNameArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KWMoreCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"moreCollectionViewCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:self.cellNameArray[indexPath.row]];
    cell.nameLabel.text = self.cellNameArray[indexPath.row];
    return cell;
}

-(BOOL)collectionView:( UICollectionView *)collectionView shouldSelectItemAtIndexPath:( NSIndexPath *)indexPath{
    return YES ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  KWMailReMoveView.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailReMoveView.h"
#import "KWMailRemoveCell.h"
#import "Utility.h"
#import "KWBaseParam.h"
#import "KWMailUserFolder.h"
#import "KWHttpTool.h"
#import "KWMailRemoveParam.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>


@interface KWMailReMoveView ()<UITableViewDataSource, UITableViewDelegate, KWMailRemoveCellDelegate> {
    NSInteger ListCount;
}

@property (strong, nonatomic) UIWindow *alphaWindow;

@property (weak, nonatomic) UITableView *tableView;
@property (weak, nonatomic) UITableView *folderTableView;

@property (assign, nonatomic) NSInteger index;

@property (strong, nonatomic) NSMutableArray *indexArray;
@property (strong, nonatomic) NSArray *baseBoxArray;
@property (strong, nonatomic) NSArray *folderArray;
@property (copy, nonatomic) NSString *mailId;

@property (assign, nonatomic) NSInteger selectIndex;
@property (weak, nonatomic) UIButton *lastSelectedButton;

@property (strong, nonatomic) NSMutableArray *showFolderArray;
@property (strong, nonatomic) NSMutableArray *listArray;

@property (strong, nonatomic) NSMutableDictionary *listDict;


@end

@implementation KWMailReMoveView

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}

- (NSMutableDictionary *)listDict {
    if (!_listDict) {
        _listDict = [NSMutableDictionary dictionary];
    }
    return _listDict;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    [self setuoTopView];
    [self setupBackView];
    [self setupTableView];
    [self setupMailBox];
}

- (void)setuoTopView {
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 49)];
    topView.backgroundColor = RGBCOLOR(22, 59, 138);
    [self addSubview:topView];
    
    UILabel *topLable = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width / 2) - 60 , 15, 120, 20)];
    topLable.text = @"移动到文件夹";
    topLable.textColor = [UIColor whiteColor];
    topLable.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:topLable];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setFrame:CGRectMake(16, 14, 40, 30)];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(disappearFromBottomAnimated) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [topView addSubview:cancelBtn];
    
    UIButton *deterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [deterBtn setFrame:CGRectMake(self.frame.size.width - 56, 14, 40, 30)];
    [deterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deterBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [deterBtn setTitle:@"确定" forState:UIControlStateNormal];
    [deterBtn addTarget:self action:@selector(removeTheMail:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:deterBtn];
    
}

- (void)setupBackView {
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, self.frame.size.width, 24)];
    backView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:backView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame: CGRectMake((self.frame.size.width / 2)- 60, 0, 120, 24)];
    NSString *title = @"返回上级文件夹";
    NSAttributedString *attStr = [[NSAttributedString alloc] initWithString:title attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}];
    [backBtn setAttributedTitle:attStr forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtn:) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:backBtn];
    
}

- (void)setupTableView {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 49 + 24 , self.frame.size.width, self.frame.size.height - 80) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self addSubview:tableView];
    self.tableView = tableView;
    
    UITableView *folderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 49 + 24 , self.frame.size.width, self.frame.size.height - 80) style:UITableViewStylePlain];
    folderTableView.delegate = self;
    folderTableView.dataSource = self;
    [self addSubview:folderTableView];
    self.folderTableView = folderTableView;
    [self.folderTableView setHidden:YES];
    
}

- (void)backBtn:(UIButton *)sender {
    
    if (self.showFolderArray.count > 0) {
        KWMailUserFolder *model = self.showFolderArray[0];
        NSString *name = model.name;
        NSLog(@"%@",name);
        if ([name isEqualToString:@"收件箱"]) {
            [self.folderTableView setHidden:YES];
            [self.tableView setHidden:NO];
        } else {
            ListCount--;
            NSString *strCount = [NSString stringWithFormat:@"%ld",(long)ListCount];
            NSArray *temp = self.listDict[strCount];
            self.showFolderArray = [NSMutableArray array];
            self.showFolderArray = [temp mutableCopy];
            [self.folderTableView reloadData];
        }
    }
}

- (void)setupMailBox {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mailbox/Get",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in json[@"back"]) {
            KWMailRemove *model = [[KWMailRemove alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.baseBoxArray = temp;
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)setupUserFolder:(NSString *)parentid {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/Folder/Get",USER_SERVERADDRESS];

    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json);
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in json[@"back"]) {
            KWMailUserFolder *model = [[KWMailUserFolder alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.folderArray = temp;
        
        NSMutableArray *tempArray = [NSMutableArray array];
        KWMailUserFolder *f1 = [[KWMailUserFolder alloc] init];
        f1.name = @"收件箱";
        f1.ID = [NSString stringWithFormat:@"b_%@_SJX",parentid];
        f1.mailboxId = [NSString stringWithFormat:@"%@",parentid];
        KWMailUserFolder *f2 = [[KWMailUserFolder alloc] init];
        f2.name = @"草稿箱";
        f2.ID = [NSString stringWithFormat:@"b_%@_CGX",parentid];
        f2.mailboxId = [NSString stringWithFormat:@"%@",parentid];
        KWMailUserFolder *f3 = [[KWMailUserFolder alloc] init];
        f3.name = @"已发送邮件";
        f3.ID = [NSString stringWithFormat:@"b_%@_YFS",parentid];
        f3.mailboxId = [NSString stringWithFormat:@"%@",parentid];
        KWMailUserFolder *f4 = [[KWMailUserFolder alloc] init];
        f4.name = @"垃圾箱";
        f4.ID = [NSString stringWithFormat:@"b_%@_LJX",parentid];
        f4.mailboxId = [NSString stringWithFormat:@"%@",parentid];
        KWMailUserFolder *f5 = [[KWMailUserFolder alloc] init];
        f5.name = @"已删除邮件";
        f5.ID = [NSString stringWithFormat:@"b_%@_YSC",parentid];
        f5.mailboxId = [NSString stringWithFormat:@"%@",parentid];
        
        [tempArray addObject:f1];
        [tempArray addObject:f2];
        [tempArray addObject:f3];
        [tempArray addObject:f4];
        [tempArray addObject:f5];
        
        
        for (KWMailUserFolder *folder in self.folderArray) {
            NSString *folderP = [NSString stringWithFormat:@"m_%@",parentid];
            if ([folderP isEqualToString:folder.parentid]) {
                [tempArray addObject:folder];
            }
        }
        self.showFolderArray = tempArray;
        
        
        [self.folderTableView reloadData];
    } failure:^(NSError *error) {
        
    }];
    
    
}

#pragma mark - UITableViewDataSource ** UITbaleViewDelegate - 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return self.baseBoxArray.count;
    } else {
        return self.showFolderArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        static NSString *cellID = @"remove";
        KWMailRemoveCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[KWMailRemoveCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectBtn.tag = indexPath.row + 100;
        cell.selectBtn.hidden = YES;
        KWMailRemove *model = self.baseBoxArray[indexPath.row];
        cell.folderName.text = model.folderName;
        [cell.selectBtn addTarget:self action:@selector(folderSelect:) forControlEvents:UIControlEventTouchUpInside];
        return cell;

    } else {
        
        static NSString *cellID = @"folder";
        KWMailRemoveCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[KWMailRemoveCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectBtn.tag = indexPath.row + 100;
        cell.selectBtn.hidden = NO;
        KWMailUserFolder *model = self.showFolderArray[indexPath.row];
        cell.folderName.text = model.name;
        [cell.selectBtn addTarget:self action:@selector(folderSelect:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        
        KWMailRemove *model = self.baseBoxArray[indexPath.row];
        NSLog(@"zzzz%@",model.Id);
        [self setupUserFolder:model.Id];
        [self.tableView setHidden:YES];
        [self.folderTableView setHidden:NO];
        
    } else {
        
        [self saveTheList];
   
        KWMailUserFolder *folder = self.showFolderArray[indexPath.row];
        NSMutableArray *temp = [NSMutableArray array];
        for (KWMailUserFolder *model in self.folderArray) {
            NSString *fstr = [NSString stringWithFormat:@"f_%@_%@",folder.mailboxId, folder.ID];
            if ([folder.ID isEqualToString:model.parentid] || [model.parentid isEqualToString:fstr]) {
                [temp addObject:model];
                if ([folder.ID isEqualToString:fstr]) {
                    [temp addObject:model];
                }
            }
        }
        if (temp.count > 0) {
            self.showFolderArray = [NSMutableArray array];
            self.showFolderArray = temp;
            
            [self.folderTableView reloadData];
        }
    }
}

- (void)saveTheList {
    
    if (self.showFolderArray.count > 0) {
        NSLog(@"qqq%ld",(long)ListCount);
        NSString *strCount = [NSString stringWithFormat:@"%ld",(long)ListCount];
        NSMutableArray *tempArry = [NSMutableArray array];
        for (KWMailUserFolder *model in self.showFolderArray) {
            [tempArry addObject:model];
        }
        NSArray *newArray = [NSArray array];
        newArray = [tempArry mutableCopy];
        [self.listDict setValue:newArray forKey:strCount];
        ListCount++;
    } else {
        ListCount++;
    }
}

- (void)tableCellBtndidSelect:(UIButton *)button {
    
    if (self.lastSelectedButton != nil) {
        [self.lastSelectedButton setSelected:NO];
    } else {
    
    }
    
}

- (void)folderSelect:(UIButton *)sender {
    
    if (sender.selected) {
        [sender setSelected:NO];
    } else {
        self.lastSelectedButton = sender;
        [sender setSelected:YES];
    }
    
    NSInteger index = sender.tag - 100;
    NSLog(@"%ld",(long)index);
    self.index = index;
    
   
}

- (void)removeTheMail:(UIButton *)sender {
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/move",USER_SERVERADDRESS];
    if (self.showFolderArray.count > 0) {
        KWMailUserFolder *model = self.showFolderArray[self.index];
        
        NSLog(@"%@",model.ID);
        
        if ([model.name isEqualToString:@"收件箱"]) {
            NSLog(@"yes");
            KWMailRemoveParam *param = [KWMailRemoveParam param];
            param.mailboxid = model.mailboxId;
            param.mailid = self.mailId;
            param.box = @"SJX";
            
            [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
                if (json[@"back"]) {
                    NSString *back = json[@"back"];
                    [SVProgressHUD showInfoWithStatus:back];
                    [SVProgressHUD dismissWithDelay:1];
                }
            } failure:^(NSError *error) {
                
            }];
            [self disappearFromBottomAnimated];
        }
        if ([model.name isEqualToString:@"草稿箱"]) {
            NSLog(@"yes");
            KWMailRemoveParam *param = [KWMailRemoveParam param];
            param.mailboxid = model.mailboxId;
            param.mailid = self.mailId;
            param.box = @"CGX";
            
            [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
                NSLog(@"%@",json[@"back"]);
                if (json[@"back"]) {
                    NSString *back = json[@"back"];
                    [SVProgressHUD showInfoWithStatus:back];
                    [SVProgressHUD dismissWithDelay:1];
                }
            } failure:^(NSError *error) {
                
            }];
            [self disappearFromBottomAnimated];
        }
        if ([model.name isEqualToString:@"已发送邮件"]) {
            NSLog(@"yes");
            KWMailRemoveParam *param = [KWMailRemoveParam param];
            param.mailboxid = model.mailboxId;
            param.mailid = self.mailId;
            param.box = @"YFS";
            
            [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
                NSLog(@"%@",json[@"back"]);
                if (json[@"back"]) {
                    NSString *back = json[@"back"];
                    [SVProgressHUD showInfoWithStatus:back];
                    [SVProgressHUD dismissWithDelay:1];
                }
            } failure:^(NSError *error) {
                
            }];
            [self disappearFromBottomAnimated];
        }
        if ([model.name isEqualToString:@"垃圾箱"]) {
            NSLog(@"yes");
            KWMailRemoveParam *param = [KWMailRemoveParam param];
            param.mailboxid = model.mailboxId;
            param.mailid = self.mailId;
            param.box = @"LJX";
            
            [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
                NSLog(@"%@",json);
                if (json[@"back"]) {
                    NSString *back = json[@"back"];
                    [SVProgressHUD showInfoWithStatus:back];
                    [SVProgressHUD dismissWithDelay:1];
                }
            } failure:^(NSError *error) {
                NSLog(@"fail");
            }];
            [self disappearFromBottomAnimated];
        }
        if ([model.name isEqualToString:@"已删除邮件"]) {
            NSLog(@"yes");
            KWMailRemoveParam *param = [KWMailRemoveParam param];
            param.mailboxid = model.mailboxId;
            param.mailid = self.mailId;
            param.box = @"YSC";
            
            [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
                NSLog(@"%@",json[@"back"]);
                if (json[@"back"]) {
                    NSString *back = json[@"back"];
                    [SVProgressHUD showInfoWithStatus:back];
                    [SVProgressHUD dismissWithDelay:1];
                }
            } failure:^(NSError *error) {
                
            }];
            [self disappearFromBottomAnimated];
        }
        
        KWMailRemoveParam *param = [KWMailRemoveParam param];
        param.mailboxid = model.mailboxId;
        param.mailid = self.mailId;
        param.rootid = model.ID;
        
        [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
            NSLog(@"%@",json[@"back"]);
            if (json[@"back"]) {
                NSString *back = json[@"back"];
                [SVProgressHUD showInfoWithStatus:back];
                [SVProgressHUD dismissWithDelay:1];
            }
            [self disappearFromBottomAnimated];
        } failure:^(NSError *error) {
            
        }];
    }
}

- (UIWindow *)alphaWindow {
    if (!_alphaWindow) {
        _alphaWindow = [[UIWindow alloc] init];
        _alphaWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _alphaWindow.windowLevel = 100;
    }
    return _alphaWindow;
}

//#pragma mark - implementaion
- (void)showAnimated {
    self.alphaWindow.frame = [UIScreen mainScreen].bounds;
    [self.alphaWindow makeKeyAndVisible];
    [self.alphaWindow addSubview:self];
    
    CAKeyframeAnimation * animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.25;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [self.layer addAnimation:animation forKey:nil];
}

- (void)disappearFromBottomAnimated {
    [UIView animateWithDuration:0.35 animations:^{
        CGRect rect = self.frame;
        self.frame = CGRectMake(rect.origin.x, rect.origin.y + 642, rect.size.width, rect.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alphaWindow.hidden = YES;
    }];
}

- (void)disappearFromTopAnimated {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = self.frame;
        self.frame = CGRectMake(rect.origin.x, rect.origin.y - 652, rect.size.width, rect.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alphaWindow.hidden = YES;
    }];
}

#pragma mark - ReceTheMailId - 

- (void)receTheMailId:(NSString *)mailId  {
    self.mailId = mailId;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

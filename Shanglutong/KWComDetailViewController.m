//
//  KWComDetailViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWComDetailViewController.h"
#import "KWComDetailModel.h"
#import "KWComDetailTableViewCell.h"
#import "KWComDetailFrame.h"
#import "KWHttpTool.h"
#import "KWCommDetailParam.h"
#import "KWCommaddViewController.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>
#import "Utility.h"

@interface KWComDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) UITableView *tableView;
@property (weak, nonatomic) UIView *topView;
@property (copy, nonatomic) NSString *titleText;
@property (copy, nonatomic) NSString *keyId;
@property (copy, nonatomic) NSString *MenuNo;
@property (weak, nonatomic) UILabel *bakLabel;

@property (strong, nonatomic) NSArray *listArray;


@end

@implementation KWComDetailViewController

- (NSArray *)listArray {
    if (!_listArray) {
        _listArray = [NSArray array];
    }
    return _listArray;
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"相关评论";
    [self setupNav];
    [self setupTopView];
    [self setupTableView];
    [self setUpData];
    // Do any additional setup after loading the view.
}

- (void)setupNav {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addComm:)];
}

- (void)setupTopView {
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, KWSCREEN_WIDTH, 70)];
    [topView setBackgroundColor: RGBCOLOR(22, 59, 138)];
    [self.view addSubview:topView];
    self.topView = topView;
    
    UILabel *bakLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 12, 320, 45)];
    bakLabel.textColor = [UIColor whiteColor];
    bakLabel.font = [UIFont systemFontOfSize:14];
    bakLabel.numberOfLines = 0;
    bakLabel.text = self.titleText;
    [self.topView addSubview:bakLabel];
    self.bakLabel = bakLabel;
}

- (void)setupTableView {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 134, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 134) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
}

- (void)setUpData {
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Getcomm",USER_SERVERADDRESS];
    
    KWCommDetailParam *param = [KWCommDetailParam param];
    param.KeyId = self.keyId;
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSArray *backArray = json[@"back"];
        if (backArray) {
            
            self.listArray = [NSArray array];
            NSMutableArray *temp = [NSMutableArray array];
            for (NSDictionary *dict in backArray) {
                KWComDetailModel *model = [[KWComDetailModel alloc] initWithDict:dict];
                self.MenuNo = model.MenuNo;
                KWComDetailFrame *detailFrame = [[KWComDetailFrame alloc] init];
                detailFrame.comDetailModel = model;
                [temp addObject:detailFrame];
            }
            self.listArray = temp;
            [self.tableView reloadData];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)addComm:(UIButton *)sender {
    KWCommaddViewController *vc = [[KWCommaddViewController alloc] init];
    vc.keyId = self.keyId;
    vc.MenuNo = self.MenuNo;
    NSLog(@"ttt%@",self.MenuNo);
    vc.refreshComm = ^(NSString *keyid) {
        [self setUpData];
        [self.tableView reloadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)recTheBakStr:(NSString *)titleText {
    self.titleText = titleText;
}

- (void)recTheKeyId:(NSString *)keyId {
    self.keyId = keyId;
}

#pragma mark - TableViewDataSource && TableViewDelegate - 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellid";
    KWComDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[KWComDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if ((indexPath.row % 2) == 0) {
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.comDetailFrame = self.listArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

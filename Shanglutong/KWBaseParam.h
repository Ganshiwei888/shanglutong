//
//  KWBaseParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWBaseParam : NSObject

//UserId:用户登录账号，
//Password:用户密码  96E79218965EB72C92A549DD5A330112
//Ran:随机码
//Sign:app签名加密后得出来的签名   4297F44B13955235245B2497399D7A93

@property (nonatomic, copy) NSString *UserId;
@property (nonatomic, copy) NSString *Password;
@property (nonatomic, copy) NSString *Ran;
@property (nonatomic, copy) NSString *Sign;

+ (instancetype)param;

- (id)copyWithZone:(NSZone *)zone;

@end

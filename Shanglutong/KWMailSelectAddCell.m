//
//  KWMailSelectAddCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/6.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailSelectAddCell.h"
#import "KWAddress.h"
#import "Utility.h"

@implementation KWMailSelectAddCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupContentView];
    }
    return self;
}

- (void)setupContentView {
    UIButton *selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setFrame:CGRectMake(8, 5, 30, 30)];
    [selectBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
    [selectBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
    [self.contentView addSubview:selectBtn];
    self.selectBtn = selectBtn;
    
    UILabel *addressName = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 90, 20)];
    addressName.textColor = [UIColor blackColor];
    addressName.font = [UIFont systemFontOfSize:12];
    addressName.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:addressName];
    self.addressName = addressName;
    
    UILabel *addressEmail = [[UILabel alloc] initWithFrame:CGRectMake(144, 10, 160, 20)];
    addressEmail.textColor = [UIColor grayColor];
    addressEmail.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:addressEmail];
    self.addressEmail = addressEmail;
    
}

- (void)setAddress:(KWAddress *)address {
    
    _address = address;
    
    self.addressName.text = address.Name;
    self.addressEmail.text = address.Email;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

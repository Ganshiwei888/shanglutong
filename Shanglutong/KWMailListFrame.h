//
//  KWMailListFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "KWMailListResult.h"

@interface KWMailListFrame : NSObject

@property (nonatomic, assign, readonly) CGRect fromNameFrame;

@property (nonatomic, assign, readonly) CGRect subjectFrame;

@property (nonatomic, assign, readonly) CGRect recDateFrame;

@property (nonatomic, assign, readonly) CGRect mailDateilFrame;

@property (nonatomic, assign, readonly) CGRect topViewFrame;

@property (nonatomic, assign, readonly) CGRect starViewFrame;

@property (nonatomic, assign, readonly) CGRect flagViewFrame;

@property (nonatomic, assign, readonly) CGRect attachmentFrame;

@property (nonatomic, assign, readonly) CGRect selectViewFrame;

@property (nonatomic, assign, readonly) CGRect readViewFrame;

@property (nonatomic, strong) KWMailListModel *mailModelList;

@property (nonatomic, copy) NSString *isSelect;

@property (nonatomic, copy) NSString *isEdit;

@end







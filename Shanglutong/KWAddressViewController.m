//
//  KWAddressViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressViewController.h"
#import "KWAddress.h"
#import "KWAddressFrame.h"
#import "KWAddressBackView.h"
#import "KWAddressCell.h"
#import "NSString+MD5.h"
#import "Utility.h"
#import "UIBarButtonItem+KW.h"
#import "KWUserAllMenu.h"
#import "KWAddressNumViewController.h"
#import "KWAddressDetailViewController.h"
#import "KWAddressPeoViewController.h"
#import <AFNetworking.h>
#import "MBProgressHUD+XQ.h"
#import "KWAddressNumTool.h"
#import "KWAddressNumCell.h"
#import "KWBaseParam.h"
#import "KWMailSendViewController.h"
#import <MJRefresh/MJRefresh.h>
#import <UIImageView+WebCache.h>
#import "ChineseString.h"
#import "LWActiveIncator.h"
#import "pinyin.h"
#import "KWTalkViewController.h"
#import "KWTalkMessageViewController.h"
#import "KWSwitchAccount.h"
#import "KWUserDefaultsManager.h"
#import <MJExtension.h>
#import "KWLoginSub.h"

static KWAddressViewController *sharedAddressVC;

@interface KWAddressViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *_indexArray; //索引数组
}


@property (nonatomic, strong) NSMutableArray *addressFrame;
@property (nonatomic, strong) NSArray<KWAddress*> *addressDetail;
@property (nonatomic, assign) NSInteger pageIndex; //设置当前的pageIndex
@property (nonatomic, strong) UIViewController *currentVC;
@property (nonatomic, strong) KWAddressNumViewController *twoVC;
@property (nonatomic, strong) KWAddressViewController *oneVC;
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, weak) UITableView *rightTableView;
//@property (nonatomic, strong) NSArray *userAddress;
@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
@property (nonatomic, retain) NSMutableArray *dataArr;
@property (nonatomic, retain) NSMutableArray *sortedArrForArrays;


@end

@implementation KWAddressViewController

//单例
+ (instancetype)sharedAddressVC {
    if(!sharedAddressVC) {
        sharedAddressVC = [[KWAddressViewController alloc]init];
    }
    return sharedAddressVC;
}


//懒加载
- (NSMutableArray *)addressFrame {
    if (!_addressFrame) {
        _addressFrame = [NSMutableArray array];
    }
    return _addressFrame;
}

- (NSArray *)addressDetail {
    if (!_addressDetail) {
        _addressDetail = [NSArray array];
    }
    return _addressDetail;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [LWActiveIncator showInView:self.view];
    });
    
    if(self.segmentIndex == 0) {
        self.segmentIndex = 3;
        self.segment.selectedSegmentIndex = 0;
        [self change:self.segment];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //将默认的页面添加进去
    
    _dataArr = [[NSMutableArray alloc] init];
    _sortedArrForArrays = [[NSMutableArray alloc] init];
    _sectionHeadsKeys = [[NSMutableArray alloc] init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupNav];
    //设置tableView
    [self setupIndex];
//    self.pageIndex = 1;
    [self setupTableView];
    [self setupAddressData];
    [self setupRightTableView];
    self.view.backgroundColor = RGBCOLOR(225, 225, 225);
    [self setupAddressNum];
    [self addObserver];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUI:) name:@"switchAccount" object:nil];
}

- (void)reloadUI:(NSNotification *)notification {
    _dataArr = [[NSMutableArray alloc] init];
    _sortedArrForArrays = [[NSMutableArray alloc] init];
    _sectionHeadsKeys = [[NSMutableArray alloc] init];
    [self setupAddressNum];
    [self setupAddressData];
}

- (void)setupIndex {
    _indexArray = [NSMutableArray arrayWithObjects:@"#",nil];
    for (char ch='A'; ch<='Z'; ch++) {
        if (ch=='I' || ch=='O' || ch=='U' || ch=='V')
            continue;
        [_indexArray addObject:[NSString stringWithFormat:@"%c",ch]];
    }
}

- (void)setupTableView {
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT) style:UITableViewStylePlain];
    [self.view addSubview:tableView];
    tableView.delegate = self;
    tableView.dataSource = self;
    self.tableView = tableView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.tableView setHidden:YES];
    self.tableView.estimatedRowHeight = 110;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerViewRefresh)];

    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerViewRefresh)];
}

- (void)setupRightTableView {
    
    UITableView *rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 64 - 49) style:UITableViewStylePlain];
    rightTableView.delegate = self;
    rightTableView.dataSource = self;
    [self.view addSubview:rightTableView];
    self.rightTableView = rightTableView;
    self.rightTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.rightTableView.showsVerticalScrollIndicator = NO;
    self.rightTableView.showsVerticalScrollIndicator = NO;
    [self.rightTableView setHidden:NO];
}

- (void)setupAddressNum {
    
    KWBaseParam *param = [KWBaseParam param];
    
    [KWAddressNumTool postGetAddressNumWith:param success:^(id json) {
        if (![json[@"back"] isKindOfClass:[NSDictionary class]]) {
            return ;
        }
        NSArray *userallmenu = json[@"back"][@"userallmenu"];
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in userallmenu) {
            KWUserAllMenu *model = [[KWUserAllMenu alloc] initWithDict:dict];
            [temp addObject:model];
        }
        self.userAddress = temp;
        
        for (KWUserAllMenu *model in self.userAddress) {
            NSString *name = model.userid;
            [_dataArr addObject:name];
        }
        
        self.sortedArrForArrays = [self getChineseStringArr:_dataArr];
        [self.rightTableView reloadData];
        [LWActiveIncator hideInViwe:self.view];
    } failure:^(NSError *error) {
    }];
}

- (void)headerViewRefresh {
    [self setupAddressData];
}

- (void)footerViewRefresh {
    
    if (self.pageIndex * 50 > (self.addressDetail.count)){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD showError:@"没有更多了"];
            [self endRefrshing];
        });
    } else {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        self.pageIndex ++;
//        NSString *password = @"111111";
//        NSString *password_MD5 = [NSString stringTo32BitMD5UpperStringWithString:password];
        NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
        [requestInfo setValue:USER_ID forKey:@"UserId"];
        [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
        [requestInfo setValue:@"123" forKey:@"Ran"];
        [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
        [requestInfo setValue:[NSString stringWithFormat:@"%ld",(long)_pageIndex] forKey:@"pageindex"];
        [requestInfo setValue:@"50" forKey:@"pagemax"];
        NSString *netPath = [NSString stringWithFormat:@"http://%@/api/contact/Get",USER_SERVERADDRESS];
        __weak KWAddressViewController *weakSelf = self;
        
        [manager POST:netPath parameters:requestInfo progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (![responseObject[@"back"] isKindOfClass:[NSDictionary class]]) {
                return ;
            }
            //取出字典模型
            NSDictionary *dictArray = responseObject[@"back"];
            //取出list数组
            NSArray *arrayRes = dictArray[@"list"];
            //创建address数组
            
            NSMutableArray *addressAdd = [NSMutableArray array];
            for (NSDictionary *dict in arrayRes) {
                KWAddress *address = [[KWAddress alloc] initWithDict:dict];
                [addressAdd addObject:address];
            }
            
            NSMutableArray *addressMutableArrayAdd = [weakSelf.addressDetail mutableCopy];
            
            for (int i = 0; i < addressAdd.count; i++  ) {
                [addressMutableArrayAdd addObject:addressAdd[i]];
            }
            self.addressDetail = addressMutableArrayAdd;
            NSArray *addressData = addressMutableArrayAdd;
            
            // 创建frame模型对象
            NSMutableArray *addressFrameArray = [NSMutableArray array];
            for (KWAddress *address in addressData) {
                KWAddressFrame *addressFrame = [[KWAddressFrame alloc] init];
                // 传递微博模型数据
                addressFrame.address = address;
                [addressFrameArray addObject:addressFrame];
            }
            
            // 赋值
            self.addressFrame = addressFrameArray;
            
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_footer endRefreshing];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"fail");
        }];
    }
}

- (void)setupNav {
    
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithIcon:@"addressButton" target:self action:@selector(addAddress)];
    
//    UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.text = @"联系人";
//    [titleLabel setFont:[UIFont fontWithName:@"System Medium" size:10]];
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.frame = CGRectMake(0, 0, 100, 40);
//    self.navigationItem.titleView = titleLabel;
    
    NSArray *array = [NSArray arrayWithObjects:@"联系人",@"同事", nil];
    //初始化UISegmentedControl
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:array];
    //设置frame
    segment.frame =CGRectMake(60, 100, 160, 30);
    self.navigationItem.titleView = segment;
    //添加事件
    segment.selectedSegmentIndex = 1;
    [segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    self.segment = segment;
    
}

- (void)change:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        NSLog(@"1");
        [self.tableView setHidden:NO];
        [self.rightTableView setHidden:YES];
    }else if (sender.selectedSegmentIndex == 1){
        NSLog(@"2");
        [self.rightTableView setHidden:NO];
        [self.tableView setHidden:YES];
    }
}

- (void)addAddress {
 
    IWLog(@"Button Click");
    
}

- (void)setupAddressData {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionary];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:@"123" forKey:@"Ran"];
    [requestInfo setValue:@"4297F44B13955235245B2497399D7A93" forKey:@"Sign"];
    [requestInfo setValue:@"1" forKey:@"pageindex"];
    [requestInfo setValue:@"50" forKey:@"pagemax"];
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/contact/Get",USER_SERVERADDRESS];
    
    __weak KWAddressViewController *weakSelf = self;
    
    [manager POST:netPath parameters:requestInfo progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //取出字典模型
        NSDictionary *dictArray = responseObject[@"back"];
        //取出list数组
        NSArray *arrayRes = dictArray[@"list"];
        //创建address数组
        NSMutableArray *addressMutableArray = [NSMutableArray array];
        for (NSDictionary *dict in arrayRes) {
            KWAddress *address = [KWAddress addressWithDict:dict];
            [addressMutableArray addObject:address];
        }
        self.addressDetail = addressMutableArray;
        NSArray *addressData = addressMutableArray;
        
        // 创建frame模型对象
        NSMutableArray *addressFrameArray = [NSMutableArray array];
        for (KWAddress *address in addressData) {
            KWAddressFrame *addressFrame = [[KWAddressFrame alloc] init];
            // 传递微博模型数据
            addressFrame.address = address;
            [addressFrameArray addObject:addressFrame];
        }
        
        // 赋值
        self.addressFrame = addressFrameArray;
    
        [self.tableView reloadData];
        [weakSelf endRefrshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail");
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.tableView) {
        return 1;
    } else {
        return [self.sortedArrForArrays count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return self.addressFrame.count;
    } else {
        return  [[self.sortedArrForArrays objectAtIndex:section] count];
    }
}

#pragma mark 设置组标题
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return nil;
    } else {
        return [_sectionHeadsKeys objectAtIndex:section];
    }
    
}

#pragma mark 右侧索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.tableView) {
        return nil;
    } else {
        return _sectionHeadsKeys;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        return 180;
    } else {
        return 66;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        KWAddressCell *cell = [KWAddressCell cellWithTableView:tableView];
        
        cell.addressFrame = self.addressFrame[indexPath.row];
        
        return cell;

    } else {
        
        static NSString *cellID = @"right";
        KWAddressNumCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[KWAddressNumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
            for (int i = 0; i < self.userAddress.count; i++) {
                KWUserAllMenu *model = self.userAddress[i];
                NSString *userId = model.userid;
                if ([str.string isEqualToString:userId]) {
                    cell.userPicimage.image = [UIImage imageNamed:@"用户"];
                    cell.userid.text = model.username;
                    cell.name.text = model.duty;
                    cell.callButton.tag = i + 100;
                    [cell.callButton addTarget:self action:@selector(cellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                    if (model.PicName.length > 2) {
                        NSString *imageURL = [NSString stringWithFormat:@"http://%@/picimage/%@",USER_SERVERADDRESS,model.PicName];
                        [cell.userPicimage sd_setImageWithURL:[NSURL URLWithString:imageURL]];
                    }
                }
            }
            
        } else {
            NSLog(@"arr out of range");
        }
        return cell;
    }
    
}

- (void)cellBtnClick:(UIButton *)sender {
    
    NSInteger index = sender.tag - 100;
    
    KWUserAllMenu *model = self.userAddress[index];
    //删除提醒框
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    //添加按钮
    __weak typeof(alter) weakAlter = alter;
    [weakAlter addAction:[UIAlertAction actionWithTitle:@"聊天" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[EMClient sharedClient].chatManager getConversation:model.username type:EMConversationTypeChat createIfNotExist:YES];
        KWTalkMessageViewController *chatController = [[KWTalkMessageViewController alloc] initWithConversationChatter:model.username conversationType:EMConversationTypeChat];
        chatController.title = model.username;
        [self.navigationController pushViewController:chatController animated:YES];
        
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"打电话" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (model.mobile.length > 0) {
            NSMutableString* str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",model.mobile];
            // NSLog(@"str======%@",str);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"发短信" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *mobile = model.mobile;
        NSString *urlStr = [NSString stringWithFormat:@"sms://%@",mobile];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        [[UIApplication sharedApplication] openURL:url];
        
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"发邮件" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        KWMailSendViewController *mailSendVC = [[KWMailSendViewController alloc] init];
        //    [mailSendVC recieveTheMailDetailModel:nil AndAttachmentArray:nil AndType:MailSendTypeNew AndMailBoxId:@"11" AndHtmlInfo:nil];
        mailSendVC.replyTo = model.email;
        UINavigationController *mailSendNavi = [[UINavigationController alloc] initWithRootViewController:mailSendVC];
        
        [self presentViewController:mailSendNavi animated:YES completion:nil];
        
        
    }]];
    
    [alter addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alter animated:YES completion:nil];
}

#pragma mark - UITableViewdelegate -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        KWAddressDetailViewController *detailVc = [[KWAddressDetailViewController alloc] init];
        
        NSLog(@"%@",self.addressDetail);
        
        NSString *phone = self.addressDetail[indexPath.row].Mobile? self.addressDetail[indexPath.row].Mobile : @"";
        
        NSString *tel = self.addressDetail[indexPath.row].Tel? self.addressDetail[indexPath.row].Tel : @"";
        
        NSString *email =  self.addressDetail[indexPath.row].Email? self.addressDetail[indexPath.row].Email : @"";
        
        NSString *email1 = self.addressDetail[indexPath.row].Email1? self.addressDetail[indexPath.row].Email1 : @"";
        
        NSString *email2 =  self.addressDetail[indexPath.row].Email2? self.addressDetail[indexPath.row].Email2 : @"";
        
        NSString *wechat =  self.addressDetail[indexPath.row].WeChat? self.addressDetail[indexPath.row].WeChat : @"";
        
        NSString *qq =  self.addressDetail[indexPath.row].QQ? self.addressDetail[indexPath.row].QQ : @"";
        
        NSString *faceBook = self.addressDetail[indexPath.row].Facebook? self.addressDetail[indexPath.row].Facebook : @"";
        
        NSString *twitter = self.addressDetail[indexPath.row].Twitter? self.addressDetail[indexPath.row].Twitter : @"";
        
        NSString *sex = self.addressDetail[indexPath.row].Sex? self.addressDetail[indexPath.row].Sex : @"";
        
        NSString *birthday = self.addressDetail[indexPath.row].Birthday? self.addressDetail[indexPath.row].Birthday : @"";
        
        NSString *job =  self.addressDetail[indexPath.row].Job? self.addressDetail[indexPath.row].Job : @"";
        
        NSString *dept = self.addressDetail[indexPath.row].Dept? self.addressDetail[indexPath.row].Dept : @"";
        
        NSString *mainadd = self.addressDetail[indexPath.row].MainAdd? self.addressDetail[indexPath.row].MainAdd : @"";
        
        NSArray *array = [NSArray arrayWithObjects:phone, tel, email, email1, email2, wechat, qq, faceBook, twitter, sex, @"", birthday, job, dept, mainadd, nil];
        
        detailVc.detailArray = array;
        [detailVc loadTableView];
        [self.navigationController pushViewController:detailVc animated:YES];
    } else {
        
        NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
            for (int i = 0; i < self.userAddress.count; i++) {
                KWUserAllMenu *model = self.userAddress[i];
                NSString *userId = model.userid;
                if ([str.string isEqualToString:userId]) {
                    KWAddressPeoViewController *vc = [[KWAddressPeoViewController alloc] init];
                    [vc receTheAddress:model];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
        } else {
            NSLog(@"arr out of range");
        }
    }
}

- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort {
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    for(int i = 0; i < [arrToSort count]; i++) {
        ChineseString *chineseString=[[ChineseString alloc]init];
        chineseString.string=[NSString stringWithString:[arrToSort objectAtIndex:i]];
        
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < chineseString.string.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin = pinYinResult;
        } else {
            chineseString.pinYin = @"";
        }
        [chineseStringsArray addObject:chineseString];
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        NSString *sr= [strchar substringToIndex:1];
        NSLog(@"%@",sr);        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
            }
        }
    }
    return arrayForArrays;
}


- (void)endRefrshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}


@end








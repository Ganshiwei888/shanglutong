//
//  KWSamrtLabelView.h
//  Shanglutong
//
//  Created by 甘世伟 on 2017/10/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickSamrtLabelCellBlock)(NSIndexPath *indexPath);

@class KWMailBoxChildListModel;

@interface KWSamrtLabelView : UIView

@property (nonatomic, strong) NSArray <KWMailBoxChildListModel *> *dataArray;
@property (nonatomic, copy) ClickSamrtLabelCellBlock clickBlock;

@end

//
//  KWCustomerPageTwo.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWCustomDetailModel;
@interface KWCustomerPageTwo : UITableViewController

@property (nonatomic, strong) KWCustomDetailModel *model;

@end

//
//  KWMailDetailCollectionViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/12.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWMailAttachmentResult.h"

@interface KWMailDetailCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic)  UIImageView *attachmentImageView;
@property (weak, nonatomic)  UILabel *attachmentNameLabel;

@property (nonatomic, strong) KWMailAttachmentResult *result;

@end

//
//  KWMailDetailFileModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWMailDetailFileModel : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger size;

@end

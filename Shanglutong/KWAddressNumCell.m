//
//  KWAddressNumCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWAddressNumCell.h"

@implementation KWAddressNumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupInitFrame];
    }
    return self;
}

- (void)setupInitFrame {
    UIImageView *userPicimage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 52, 52)];
    userPicimage.layer.cornerRadius = 26;
    userPicimage.layer.borderColor = [UIColor clearColor].CGColor;
    userPicimage.layer.borderWidth = 0;
    userPicimage.layer.masksToBounds = YES;
    userPicimage.contentMode = UIViewContentModeScaleAspectFill;
    userPicimage.image = [UIImage imageNamed:@"用户"];
    [self.contentView addSubview:userPicimage];
    self.userPicimage = userPicimage;
    
    UILabel *userid = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 120, 18)];
    userid.textColor = [UIColor blackColor];
    userid.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:userid];
    self.userid = userid;
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(70, 36, 120, 18)];
    name.textColor = [UIColor lightGrayColor];
    name.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:name];
    self.name = name;
    
    UIButton *callButton = [UIButton buttonWithType: UIButtonTypeSystem];
    
    [callButton setFrame:CGRectMake(KWSCREEN_WIDTH - 90, 16, 80, 44)];
    [callButton setImage:[UIImage imageNamed:@"call"] forState:UIControlStateNormal];
    [self.contentView addSubview:callButton];
    self.callButton  = callButton;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

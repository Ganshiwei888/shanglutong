//
//  KWMailSelectAddCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/6.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWAddress;
@interface KWMailSelectAddCell : UITableViewCell

@property (weak, nonatomic) UIButton *selectBtn;

@property (weak, nonatomic) UILabel *addressName;
@property (weak, nonatomic) UILabel *addressEmail;

@property (strong, nonatomic) KWAddress *address;

@end

//
//  KWMailAttachmentResult.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/12.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailAttachmentResult.h"
#import "Utility.h"

@implementation KWMailAttachmentResult

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    if (self = [super init]) {
        _fileid = checkStringNull(dict[@"id"]);
        _name = checkStringNull(dict[@"name"]);
        _size = checkStringNull(dict[@"size"]);
    }
    return self;
    
}

@end

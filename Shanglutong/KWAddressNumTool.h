//
//  KWAddressNumTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWHttpTool.h"

@class KWBaseParam;
@interface KWAddressNumTool : KWHttpTool

+ (void)postGetAddressNumWith:(KWBaseParam *)param success:(void(^)(id json))success failure:(void(^)(NSError *error))failure;

@end

//
//  NSString+KWCreateTime.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (KWCreateTime)

+ (NSString *)createDateStringToCreateTimeFromNow:(NSString *)dateString;

+ (NSString *)currentDateStringFrom:(NSString *)dateString;

@end

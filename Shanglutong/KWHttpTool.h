//
//  KWUserHttpTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWHttpTool : NSObject
/**
 发送一个post请求

 @param url 请求的URL
 @param params 请求的参数
 @param success 请求成功
 @param failure 请求失败
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSError *error))failure;


@end

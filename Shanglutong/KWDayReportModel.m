//
//  KWDayReportModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDayReportModel.h"
#import "Utility.h"
//
//@property (nonatomic, copy) NSString *CreateTm;
//@property (nonatomic, copy) NSString *Creater;
//@property (nonatomic, copy) NSString *CreaterName;
//@property (nonatomic, copy) NSString *Def;
//@property (nonatomic, copy) NSString *DeptIdStr;
//@property (nonatomic, copy) NSString *Html;
//@property (nonatomic, copy) NSString *Name;
//@property (nonatomic, copy) NSString *UpdateTm;
//@property (nonatomic, copy) NSString *Updater;
//@property (nonatomic, copy) NSString *UpdaterName;
//@property (nonatomic, copy) NSString *Type;
//@property (nonatomic, copy) NSString *Id;

@implementation KWDayReportModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _CreateTm = checkStringNull(dict[@"CreateTm"]);
        _Creater = checkStringNull(dict[@"Creater"]);
        _CreaterName = checkStringNull(dict[@"CreaterName"]);
        _Html = checkStringNull(dict[@"Html"]);
        _Type = checkStringNull(dict[@"Type"]);
        _Id = checkStringNull(dict[@"Id"]);
        _UpdateTm = checkStringNull(dict[@"UpdateTm"]);
        
    }
    return self;
}

@end

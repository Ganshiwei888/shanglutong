//
//  KWMineMailSetCellTableViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/29.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMineMailSetCellTableViewCell.h"

@implementation KWMineMailSetCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // Initialization code
    self.mailNum.layer.borderWidth = 1;
    self.mailNum.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.mailNum.layer.borderColor = [UIColor clearColor].CGColor;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

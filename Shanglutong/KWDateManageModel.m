//
//  KWDateManageModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDateManageModel.h"
#import "Utility.h"

@implementation KWDateManageModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        
        _menuno = checkStringNull(dict[@"menuno"]);
        _note = checkStringNull(dict[@"note" ]);
        _end_date = dict[@"end_date"];
        _status = checkStringNull(dict[@"status"]);
        _Urgency_level = checkStringNull(dict[@"Urgency_level"]);
        
        
    }
    return self;
}

@end

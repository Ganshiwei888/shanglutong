//
//  KWSamrtLabelView.m
//  Shanglutong
//
//  Created by 甘世伟 on 2017/10/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWSamrtLabelView.h"
#import "KWMailBoxChildList.h"
#import "UIColor+Hex.h"

@interface KWSamrtLabelView () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray <KWMailBoxChildListModel *> *data;
@property (strong, nonatomic) NSDictionary *colorDic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

@implementation KWSamrtLabelView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.tableView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    CGSize size = [UIScreen mainScreen].bounds.size;
    self.frame = CGRectMake(0, 0, size.width, size.height);
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
}

- (IBAction)close:(UIButton *)sender {
    [self removeFromSuperview];
}

- (void)setDataArray:(NSArray<KWMailBoxChildListModel *> *)dataArray {
    _data = dataArray;
    NSInteger count = _data.count;
    if (count > 8) {
        count = 8;
    }
    self.tableViewHeightConstraint.constant = count * 44;
    [self layoutIfNeeded];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    KWMailBoxChildListModel *model = self.data[indexPath.row];
    cell.textLabel.text = model.name;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.contentView.backgroundColor = [UIColor colorWithHexString:self.colorDic[model.Class]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.clickBlock) {
        self.clickBlock(indexPath);
        [self close:nil];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSDictionary *)colorDic{
    if (!_colorDic) {
        _colorDic = @{@"LBL":@"333333",@"maillabel-1":@"D1DADE",@"maillabel-2":@"1ab394",@"maillabel-3":@"1c84c6",@"maillabel-4":@"f8ac59",@"maillabel-5":@"ed5565",@"maillabel-6":@"23c6c8",@"maillabel-7":@"AD33AD",@"maillabel-8":@"2B8787",@"maillabel-9":@"5F27B3",@"maillabel-10":@"666666",@"maillabel-11":@"E3A325",@"maillabel-12":@"CC0000",@"maillabel-13":@"AD855C",@"maillabel-14":@"855C85",@"maillabel-15":@"997799",@"maillabel-16":@"262ED7",@"maillabel-17":@"333333",@"maillabel-18":@"CC6666"};
    }
    return _colorDic;
}



@end

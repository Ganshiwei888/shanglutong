//
//  KWUserInfoParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWBaseParam.h"

@interface KWUserInfoParam : KWBaseParam;

@property (strong, nonatomic) NSString *appid;
///**
// 用户ID
// */
//@property (nonatomic, strong) NSString *UserId;
///**
// 用户密码
// */
//@property (nonatomic, strong) NSString *Password;
///**
// 随机码
// */
//@property (nonatomic, strong) NSString *Ran;
///**
// app签名加密后得出来的签名
// */
//@property (nonatomic, strong) NSString *Sign;

@end

//
//  KWDateManageViewCell.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWDateManageViewCell.h"
#import "KWDateManageModel.h"
#import "KWDateManageFrame.h"
#import "Utility.h"
//
//@property (assign, readonly, nonatomic) CGRect comeF;
//@property (assign, readonly, nonatomic) CGRect comeValueF;
//
//@property (assign, readonly, nonatomic) CGRect noteF;
//
//@property (assign, readonly, nonatomic) CGRect chargeF;
//@property (assign, readonly, nonatomic) CGRect timeF;
//@property (assign, readonly, nonatomic) CGRect timeOutF;
//
//@property (assign, readonly, nonatomic) CGRect doneF;
//@property (assign, readonly, nonatomic) CGRect doneViewF;

@interface KWDateManageViewCell ()

@property (weak, nonatomic) UILabel *comeLabel;
@property (weak, nonatomic) UILabel *comeValue;
@property (weak, nonatomic) UILabel *noteLabel;
@property (weak, nonatomic) UILabel *chargeLabel;
@property (weak, nonatomic) UILabel *timeLabel;
@property (weak, nonatomic) UILabel *timeOutLabel;

@end

@implementation KWDateManageViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupCellView];
    }
    return self;
}

- (void)setupCellView {
    
    UILabel *comeLabel = [[UILabel alloc] init];
    comeLabel.textColor = [UIColor darkGrayColor];
    comeLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:comeLabel];
    self.comeLabel = comeLabel;
    
    UILabel *comeValue = [[UILabel alloc] init];
    comeValue.textColor = [UIColor darkGrayColor];
    comeValue.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:comeValue];
    self.comeValue = comeValue;
    
    UILabel *noteLabel = [[UILabel alloc] init];
    noteLabel.textColor = [UIColor blackColor];
    noteLabel.font = [UIFont systemFontOfSize:13];
    [self.contentView addSubview:noteLabel];
    self.noteLabel = noteLabel;
    
    UILabel *chargeLabel = [[UILabel alloc] init];
    chargeLabel.textColor = [UIColor darkGrayColor];
    chargeLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:chargeLabel];
    self.chargeLabel = chargeLabel;
    
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.textColor = [UIColor darkGrayColor];
    timeLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
    UILabel *timeOutLabel = [[UILabel alloc] init];
    timeOutLabel.textColor = [UIColor redColor];
    timeOutLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:timeOutLabel];
    self.timeOutLabel = timeOutLabel;
    
    UILabel *doneLabel = [[UILabel alloc] init];
    doneLabel.textColor = [UIColor blackColor];
    doneLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:doneLabel];
    self.doneLabel = doneLabel;
    
    UIImageView *doneWellView = [[UIImageView alloc] init];
    doneWellView.backgroundColor = [UIColor greenColor];
    [self.contentView addSubview:doneWellView];
    self.doneWellView = doneWellView;
    
}

- (void)setManageFrame:(KWDateManageFrame *)manageFrame {
    _manageFrame = manageFrame;
    
    KWDateManageModel *manageModel = manageFrame.manageModel;
    
    self.comeLabel.text = @"来源";
    self.comeLabel.frame = manageFrame.comeF;
    
    NSString *menuStr = manageModel.menuno;
    
    if ([menuStr isEqualToString:@"AA"]) {
        menuStr = @"邮件";
    }
    if ([menuStr isEqualToString:@"AB"]) {
        menuStr = @"客户";
    }
    if ([menuStr isEqualToString:@"AC"]) {
        menuStr = @"商机";
    }
    if ([menuStr isEqualToString:@"AD"]) {
        menuStr = @"产品";
    }
    if ([menuStr isEqualToString:@"AF"]) {
        menuStr = @"工作";
    }
    if ([menuStr isEqualToString:@"AG"]) {
        menuStr = @"报价";
    }
    if ([menuStr isEqualToString:@"AH"]) {
        menuStr = @"寄样";
    }
    if ([menuStr isEqualToString:@"AI"]) {
        menuStr = @"询价";
    }
    if ([menuStr isEqualToString:@"AJ"]) {
        menuStr = @"文档";
    }
    if ([menuStr isEqualToString:@"AM"]) {
        menuStr = @"海关";
    }
    if ([menuStr isEqualToString:@"AN"]) {
        menuStr = @"待办";
    }
    if ([menuStr isEqualToString:@"AO"]) {
        menuStr = @"BI";
    }
    if ([menuStr isEqualToString:@"ZA"]) {
        menuStr = @"页面";
    }
    
    self.comeValue.text = menuStr;
    self.comeValue.frame = manageFrame.comeValueF;
    
    self.noteLabel.text = manageModel.note;
    self.noteLabel.frame = manageFrame.noteF;
    
    _chargeLabel.text = @"代办日期";
    _chargeLabel.frame = manageFrame.chargeF;
    
    _timeLabel.text = manageModel.end_date;
    _timeLabel.frame = manageFrame.timeF;
    
    _timeOutLabel.text = @"超出3天";
    _timeOutLabel.frame = manageFrame.timeOutF;
    
    _doneLabel.text = @"未处理";
    _doneLabel.frame = manageFrame.doneF;
    
    _doneWellView.frame = manageFrame.doneViewF;
    NSString *status = manageModel.Urgency_level;
    
    if ([status isEqualToString:@"0"]) {
        _doneWellView.backgroundColor = RGBCOLOR(61, 202, 63);
    }
    if ([status isEqualToString:@"1"]) {
        _doneWellView.backgroundColor = RGBCOLOR(130, 193, 47);
    }
    if ([status isEqualToString:@"2"]) {
        _doneWellView.backgroundColor = RGBCOLOR(255, 253, 56);
    }
    if ([status isEqualToString:@"3"]) {
        _doneWellView.backgroundColor = RGBCOLOR(253, 191, 45);
    }
    if ([status isEqualToString:@"4"]) {
        _doneWellView.backgroundColor = RGBCOLOR(253, 127, 35);
    }
    if ([status isEqualToString:@"5"]) {
        _doneWellView.backgroundColor = RGBCOLOR(251, 13, 27);
    }
    

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

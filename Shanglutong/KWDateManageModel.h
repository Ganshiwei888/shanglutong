//
//  KWDateManageModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/28.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWDateManageModel : NSObject

@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *IsEnd;
@property (nonatomic, copy) NSString *Reminder;
@property (nonatomic, copy) NSString *Reminder_State;
@property (nonatomic, copy) NSString *clientid;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *menuno;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *note;
@property (nonatomic, copy) NSString *Urgency_level;



- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//{
//    CreateTm = "2017-05-09T10:57:08.227";
//    Creater = admin;
//    CreaterName = "\U7ba1\U7406\U5458";
//    IsEnd = 1;
//    Reminder = 0;
//    "Reminder_State" = 1;
//    StartTimeEx = "2017-08-29T10:54:23";
//    "Urgency_level" = 1;
//    Url = "<null>";
//    UrlTitle = "<null>";
//    clientid = 0;
//    "end_date" = "2017-05-11T10:54:23";
//    id = 297;
//    "is_repeat" = 1;
//    menuno = AN;
//    note = "\U518d\U4e0d\U4ea4\U6ca1\U7684\U6bd5\U4e1a\U4e86";
//    "start_date" = "2017-05-10T10:54:23";
//    status = 2;
//},

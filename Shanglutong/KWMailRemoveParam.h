//
//  KWMailRemoveParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailRemoveParam : KWBaseParam

@property (strong, nonatomic) NSString *mailboxid;
@property (strong, nonatomic) NSString *mailid;
@property (strong, nonatomic) NSString *rootid;
@property (strong, nonatomic) NSString *box;

@end

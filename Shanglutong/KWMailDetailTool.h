//
//  KWMailDetailTool.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWMailDetailParam.h"
#import "KWMailDetailResult.h"

@interface KWMailDetailTool : NSObject

/**
 请求邮件详情

 @param param 请求参数
 @param success 请求成功
 @param failure 请求失败
 */
+ (void)postGetMailDetailWith:(KWMailDetailParam *)param success:(void(^)(KWMailDetailResultModel *resultData))success failure:(void(^)(NSError *error))failure;

@end

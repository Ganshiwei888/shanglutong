//
//  UIImage+KW.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KW)

+ (UIImage *)resizedImageWithName:(NSString *)name;

@end

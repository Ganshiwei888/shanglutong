//
//  KWUserDefaultsManager.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWUserDefaultsManager : NSObject

+ (void)setUserAccount:(NSString *_Nonnull)account;
+ (NSString *_Nullable)getUserAccount;

+ (void)setUserPassword:(NSString *_Nonnull)password;
+ (NSString *_Nullable)getUserPassword;

+ (void)setUserServerAddress:(NSString *_Nonnull)serverAddress;
+ (NSString *_Nullable)getUserServerAddress;

+ (void)setUserId:(NSString * _Nonnull)userId;
+ (NSString *_Nullable)getUserId;

@end

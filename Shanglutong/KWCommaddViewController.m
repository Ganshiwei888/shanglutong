//
//  KWCommaddViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCommaddViewController.h"
#import "Utility.h"
#import "KWCommAddParam.h"
#import "KWHttpTool.h"
#import <MJExtension.h>
#import <SVProgressHUD.h>

@interface KWCommaddViewController () <UITextViewDelegate>

@property (weak, nonatomic) UITextView *textView;

@end

@implementation KWCommaddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评论";
    [self setupNav];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    
    UITextView *textView =[[UITextView alloc]initWithFrame:CGRectMake(20,20,KWSCREEN_WIDTH - 40, 320)];
    textView.backgroundColor= [UIColor whiteColor];
    textView.text = @"添加评论";
    textView.textColor = [UIColor grayColor];
    textView.delegate = self;
    [self.view addSubview:textView];
    self.textView = textView;
    
}

- (void)setupNav {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtn:)];
}

- (void)saveBtn:(UIButton *)sender {
    NSLog(@"%@",self.textView.text);
    
    KWCommAddParam *param = [KWCommAddParam param];
    param.BAK = self.textView.text;
    param.KeyId = self.keyId;
    param.MenuNo = self.MenuNo;
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/workdt/Setcomm", USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        NSLog(@"%@",json[@"back"]);
        [SVProgressHUD showInfoWithStatus:@"评论成功"];
        [SVProgressHUD dismissWithDelay:1.0];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    self.refreshComm(@"start");
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(self.textView.text.length < 1){
        self.textView.text = @"添加评论";
        self.textView.textColor = [UIColor grayColor];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([self.textView.text isEqualToString:@"添加评论"]){
        self.textView.text=@"";
        self.textView.textColor=[UIColor blackColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

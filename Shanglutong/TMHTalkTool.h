//
//  TMHTalkTool.h
//  Shanglutong
//
//  Created by Ming Tian on 2017/9/26.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KWUserAllMenu.h"  // 自定用户数据模型
#import "KWAddressViewController.h"

@interface TMHTalkTool : NSObject

+ (KWUserAllMenu *)getUserMenuWithHxid:(NSString *)hxid;

@end

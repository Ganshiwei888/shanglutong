//
//  KWTabBar.m
//  Shanglutong
//
//  Created by KeWen on 2017/7/21.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWTabBar.h"
#import "KWTabBarButton.h"
@interface KWTabBar ()

@property (nonatomic, strong) NSMutableArray *tabBarButtons;
@property (nonatomic, weak) KWTabBarButton *selectedButton;

@end

@implementation KWTabBar

- (NSMutableArray *)tabBarButtons {
    if (!_tabBarButtons) {
        _tabBarButtons = [NSMutableArray array];
    }
    return _tabBarButtons;
}

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor whiteColor]];
        // 添加一个加号按钮
        UIButton *plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [plusButton setBackgroundImage:[UIImage imageNamed:@"发布-1"] forState:UIControlStateNormal];
        //        [plusButton setBackgroundImage:[UIImage imageWithName:@"plusButton"] forState:UIControlStateHighlighted];
        [plusButton setImage:[UIImage imageNamed:@"发布ed"] forState:UIControlStateNormal];
        //        [plusButton setImage:[UIImage imageWithName:@"plusButton"] forState:UIControlStateHighlighted];
        plusButton.bounds = CGRectMake(0, 0, plusButton.currentBackgroundImage.size.width, plusButton.currentBackgroundImage.size.height);
        [self addSubview:plusButton];
        self.plusButton = plusButton;
    }
    return self;
}

- (void)addTabBarWithItem:(UITabBarItem *)item {
    // 1.创建按钮
    KWTabBarButton *button = [[KWTabBarButton alloc] init];
    [self addSubview:button];
    // 添加按钮到数组中
    [self.tabBarButtons addObject:button];
    
    // 2.设置数据
    button.item = item;
    
    // 3.监听按钮点击
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchDown];
    
    // 4.默认选中第0个按钮
    if (self.tabBarButtons.count == 2) {
        [self buttonClick:button];
    }
}

- (void)buttonClick:(KWTabBarButton *)button {
    //通知代理
    if ([self.delegate respondsToSelector:@selector(tabBar:didSelectedButtonFrom:to:)]) {
        [self.delegate tabBar:self didSelectedButtonFrom:self.selectedButton.tag to: button.tag];
    }
    //设置按钮的状态
    self.selectedButton.selected = NO;
    button.selected = YES;
    self.selectedButton = button;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    // 调整加号按钮的位置
    CGFloat h = self.frame.size.height;
    CGFloat w = self.frame.size.width;
    self.plusButton.center = CGPointMake(w * 0.5, h * 0.5);
    
    // 按钮的frame数据
    CGFloat buttonH = h;
    CGFloat buttonW = w / self.subviews.count;
    CGFloat buttonY = 0;
    
    for (int index = 0; index<self.tabBarButtons.count; index++) {
        // 1.取出按钮
        KWTabBarButton *button = self.tabBarButtons[index];
        
        // 2.设置按钮的frame
        CGFloat buttonX = index * buttonW;
        if (index > 1) {
            buttonX += buttonW;
        }
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        
        // 3.绑定tag
        button.tag = index;
    }
    
}


@end




















//
//  KWReportDetailViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWDayReportModel.h"
#import "KWWeekReportModel.h"

@interface KWReportDetailViewController : UIViewController

- (void)receiveTheDayModel:(KWDayReportModel *)daymodel type:(NSString *)type;

- (void)receiveTheWeekModel:(KWWeekReportModel *)weekModel type:(NSString *)type;


@end

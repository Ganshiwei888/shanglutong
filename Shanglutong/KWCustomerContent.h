//
//  KWCustomerContent.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWCustomerFrame;
@interface KWCustomerContent : UIView

@property (nonatomic, strong) KWCustomerFrame *customerFrame;

@end

//
//  KWAddressBackView.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWAddressFrame;
@interface KWAddressBackView : UIView
/**
 address frame
 */
@property (nonatomic, strong) KWAddressFrame *addressFrame;
@end

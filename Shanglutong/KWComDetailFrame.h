//
//  KWComDetailFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWComDetailModel;
@interface KWComDetailFrame : NSObject

@property (strong, nonatomic) KWComDetailModel *comDetailModel;

@property (assign, readonly, nonatomic) CGRect detailF;

@property (assign, readonly, nonatomic) CGRect userImageF;

@property (assign, readonly, nonatomic) CGRect nameF;

@property (assign, readonly, nonatomic) CGRect timeF;



@end


//
//  KWMailBoxTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/2.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailBoxTool.h"
#import "KWHttpTool.h"
#import "KWMailBox.h"
#import <MJExtension.h>
#import "Utility.h"

@implementation KWMailBoxTool

+ (void)postGetUserMailBox:(KWMailBoxParam *)param success:(void (^)(KWMailBoxResult *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mailbox/Get"];
    
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
 
            KWMailBoxResult *resultData = [KWMailBoxResult mj_objectWithKeyValues:json];
            success(resultData);
        }
        
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)postGetUserMailBoxChild:(KWMailBoxParam *)param success:(void (^)(KWMailBoxChildResult *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/mailbox/Getmenu"];
    [KWHttpTool postWithURL:URL params:param.mj_keyValues success:^(id json) {
        if (success) {
            
            NSLog(@"%@",json);
            
            NSArray *mailChildBox = [json valueForKey:@"back"];
            KWMailBoxChildResult *childResult = [[KWMailBoxChildResult alloc] init];
            NSMutableArray *array = [NSMutableArray array];
            for (NSDictionary *dict in mailChildBox) {
                KWMailBoxChildListModel *listModel = [[KWMailBoxChildListModel alloc] initWithDict:dict];
                [array addObject:listModel];
            }
            childResult.childResult = array;
            childResult.code = [json valueForKey:@"code"];
            childResult.timestamp = [json valueForKey:@"timestamp"];
            
            success(childResult);
        }
    } failure:^(NSError *error) {
        
        if (failure) {
            failure(error);
        }
        
    }];
    
}


@end







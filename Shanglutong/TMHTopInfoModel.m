//
//  TMHTopInfoModel.m
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/19.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "TMHTopInfoModel.h"

@implementation TMHTopInfoModel

- (instancetype)initWithName:(NSString *)name content:(double)content timestamp:(NSTimeInterval)timestamp {
    NSDate *dateObj = [NSDate dateWithTimeIntervalSince1970:timestamp];
    self.date = [NSString stringWithFormat:@"%ld年%ld月",(long)dateObj.year, (long)dateObj.month];
    if([name isEqualToString:@"xse"]) {
        self.name = @"销售额";
        self.content = [NSString stringWithFormat:@"%.2f",content];
        self.unit = @"单位:万元";
    }
    if([name isEqualToString:@"bj"]) {
        self.name = @"报价数";
        self.content = [NSString stringWithFormat:@"%.0f",content];
        self.unit = @"单位:个";
    }
    if([name isEqualToString:@"sj"]) {
        self.name = @"商机数";
        self.content = [NSString stringWithFormat:@"%.0f",content];
        self.unit = @"单位:个";
    }
    if([name isEqualToString:@"xzkh"]) {
        self.name = @"新增客户";
        self.content = [NSString stringWithFormat:@"%.0f",content];
        self.unit = @"单位:个";
    }
    if([name isEqualToString:@"zkh"]) {
        self.name = @"转成功客户";
        self.content = [NSString stringWithFormat:@"%.0f",content];
        self.unit = @"单位:个";
    }
    return self;
}

@end

//
//  KWMomentModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

//                              Bak = "\U6d4b\U8bd5";
//                              CommCount = 3;
//                              CreateTm = "2017-08-21T16:36:34.65";
//                              Creater = admin;
//                              CreaterName = "\U7ba1\U7406\U5458";
//                              Id = 51;
//                              KeyId = "-1";
//                              MenuNo = AT;
//                              ShareUID = ",1,121,126,147,";
//                              ShareUID1 = "-1";
//                              TableName = "WorkDynamic_T";
//                              TitleHtml = 6666;
@class KWMomentCommlistModel;
@interface KWMomentWorkdtModel : NSObject
@property (nonatomic, copy) NSString *Bak;
@property (nonatomic, copy) NSString *CommCount;
@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *KeyId;
@property (nonatomic, copy) NSString *MenuNo;
@property (nonatomic, copy) NSString *ShareUID;
@property (nonatomic, copy) NSString *ShareUID1;
@property (nonatomic, copy) NSString *TableName;
@property (nonatomic, copy) NSString *TitleHtml;
@property (nonatomic, strong) NSArray <KWMomentCommlistModel *> *commlist;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

//{
//    back =     {
//        commlist =         (
//                            (
//                            ),
//                            (
//                             {
//                                 Bak = 66666;
//                                 CreateTm = "2017-08-21T16:36:51.677";
//                                 Creater = admin;
//                                 CreaterName = "\U7ba1\U7406\U5458";
//                                 Id = 191;
//                                 KeyId = 51;
//                                 MenuNo = AT;
//                                 ShareUID = ",1,121,126,147,";
//                                 ShareUID1 = "";
//                                 TableName = "WorkDynamic_T";
//                                 TitleHtml = 6666;
//                                 WdId = 51;
//                             },
//                             {
//                                 Bak = 123;
//                                 CreateTm = "2017-08-21T16:36:58.123";
//                                 Creater = admin;
//                                 CreaterName = "\U7ba1\U7406\U5458";
//                                 Id = 192;
//                                 KeyId = 51;
//                                 MenuNo = AT;
//                                 ShareUID = ",1,121,126,147,";
//                                 ShareUID1 = "";
//                                 TableName = "WorkDynamic_T";
//                                 TitleHtml = 6666;
//                                 WdId = 51;
//                             },
//                             {
//                                 Bak = "\U54c8\U54c8";
//                                 CreateTm = "2017-08-21T16:37:02.83";
//                                 Creater = admin;
//                                 CreaterName = "\U7ba1\U7406\U5458";
//                                 Id = 193;
//                                 KeyId = 51;
//                                 MenuNo = AT;
//                                 ShareUID = ",1,121,126,147,";
//                                 ShareUID1 = "";
//                                 TableName = "WorkDynamic_T";
//                                 TitleHtml = 6666;
//                                 WdId = 51;
//                             }
//                             )
//                            );
//        count = 7;
//        index = 0;
//        list = "<null>";
//        unread = 0;
//        workdt =         (
//                          {
//                              Bak = xddddd;
//                              CommCount = 0;
//                              CreateTm = "2017-08-22T23:04:07.82";
//                              Creater = test;
//                              CreaterName = "\U5218\U5fb7\U534e";
//                              Id = 52;
//                              KeyId = "-1";
//                              MenuNo = AT;
//                              ShareUID = ",1,121,126,147,118,";
//                              ShareUID1 = "-1";
//                              TableName = "WorkDynamic_T";
//                              TitleHtml = dddsdsds;
//                          },
//                          {
//                              Bak = "\U6d4b\U8bd5";
//                              CommCount = 3;
//                              CreateTm = "2017-08-21T16:36:34.65";
//                              Creater = admin;
//                              CreaterName = "\U7ba1\U7406\U5458";
//                              Id = 51;
//                              KeyId = "-1";
//                              MenuNo = AT;
//                              ShareUID = ",1,121,126,147,";
//                              ShareUID1 = "-1";
//                              TableName = "WorkDynamic_T";
//                              TitleHtml = 6666;
//                          }
//                          );
//    };
//    code = 0000;
//    timestamp = 1503640250;
//}
//





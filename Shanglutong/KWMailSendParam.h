//
//  KWMailSendParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/8.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailSendParam : KWBaseParam

@property (nonatomic, strong) NSNumber *size;
@property (nonatomic, strong) NSString *name;

@end

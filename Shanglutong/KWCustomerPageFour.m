//
//  KWCustomerPageFour.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerPageFour.h"
#import "KWAddressCell.h"
#import "KWAddress.h"
#import "KWAddressFrame.h"
#import "Utility.h"
#import "KWHttpTool.h"
#import "KWAddressDetailViewController.h"
#import <SVProgressHUD.h>

@interface KWCustomerPageFour () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSArray<KWAddress*> *addressDetail;
@property (nonatomic, strong) NSMutableArray *addressFrame;

@end

@implementation KWCustomerPageFour

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createTableView];
    
    [self getAddress];
}

- (void)createTableView {
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 220) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    _tableView.separatorStyle = UITableViewCellAccessoryNone;
}

- (void)getAddress {
    //设置常用参数
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:@"1" forKey:@"pageindex"];
    [requestInfo setValue:@"20" forKey:@"pagemax"];
    [requestInfo setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomIDNow"] forKey:@"clientid"];
    
    NSLog(@"******%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomIDNow"] );
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/contact/Get"];
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        NSString *code = json[@"code"];
        if (![code isEqualToString:@"0000"]) {
            [SVProgressHUD showErrorWithStatus:@"没有联系人"];
            [SVProgressHUD dismissWithDelay:1];
        } else {
            NSDictionary *dictArray = json[@"back"];
            //取出list数组
            NSArray *arrayRes = dictArray[@"list"];
            NSLog(@"%@",arrayRes);
            //创建address数组
            NSMutableArray *addressMutableArray = [NSMutableArray array];
            for (NSDictionary *dict in arrayRes) {
                KWAddress *address = [KWAddress addressWithDict:dict];
                [addressMutableArray addObject:address];
            }
            
            self.addressDetail = addressMutableArray;
            NSArray *addressData = addressMutableArray;
            
            // 创建frame模型对象
            NSMutableArray *addressFrameArray = [NSMutableArray array];
            for (KWAddress *address in addressData) {
                KWAddressFrame *addressFrame = [[KWAddressFrame alloc] init];
                // 传递微博模型数据
                addressFrame.address = address;
                NSLog(@"%@", address.Name);
                [addressFrameArray addObject:addressFrame];
            }
            
            // 赋值
            self.addressFrame = addressFrameArray;
            
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - UITableViewDataSource && UITableViewDelegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.addressFrame.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KWAddressCell *cell = [KWAddressCell cellWithTableView:tableView];
    
    cell.addressFrame = self.addressFrame[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KWAddressDetailViewController *detailVc = [[KWAddressDetailViewController alloc] init];
    
    NSLog(@"%@",self.addressDetail);
    
    NSString *phone = self.addressDetail[indexPath.row].Mobile? self.addressDetail[indexPath.row].Mobile : @"";
    
    NSString *tel = self.addressDetail[indexPath.row].Tel? self.addressDetail[indexPath.row].Tel : @"";
    
    NSString *email =  self.addressDetail[indexPath.row].Email? self.addressDetail[indexPath.row].Email : @"";
    
    NSString *email1 = self.addressDetail[indexPath.row].Email1? self.addressDetail[indexPath.row].Email1 : @"";
    
    NSString *email2 =  self.addressDetail[indexPath.row].Email2? self.addressDetail[indexPath.row].Email2 : @"";
    
    NSString *wechat =  self.addressDetail[indexPath.row].WeChat? self.addressDetail[indexPath.row].WeChat : @"";
    
    NSString *qq =  self.addressDetail[indexPath.row].QQ? self.addressDetail[indexPath.row].QQ : @"";
    
    NSString *faceBook = self.addressDetail[indexPath.row].Facebook? self.addressDetail[indexPath.row].Facebook : @"";
    
    NSString *twitter = self.addressDetail[indexPath.row].Twitter? self.addressDetail[indexPath.row].Twitter : @"";
    
    NSString *sex = self.addressDetail[indexPath.row].Sex? self.addressDetail[indexPath.row].Sex : @"";
    
    NSString *birthday = self.addressDetail[indexPath.row].Birthday? self.addressDetail[indexPath.row].Birthday : @"";
    
    NSString *job =  self.addressDetail[indexPath.row].Job? self.addressDetail[indexPath.row].Job : @"";
    
    NSString *dept = self.addressDetail[indexPath.row].Dept? self.addressDetail[indexPath.row].Dept : @"";
    
    NSString *mainadd = self.addressDetail[indexPath.row].MainAdd? self.addressDetail[indexPath.row].MainAdd : @"";
    
    NSArray *array = [NSArray arrayWithObjects:phone, tel, email, email1, email2, wechat, qq, faceBook, twitter, sex, @"", birthday, job, dept, mainadd, nil];
    
    detailVc.detailArray = array;
    [detailVc loadTableView];
    [self.navigationController pushViewController:detailVc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

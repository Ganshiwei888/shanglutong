//
//  KWCommentTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWCommentFrame;
@interface KWCommentTableViewCell : UITableViewCell

@property (nonatomic, strong) KWCommentFrame *commentFrame;

@property (weak, nonatomic) UIImageView *userImageView;

@end

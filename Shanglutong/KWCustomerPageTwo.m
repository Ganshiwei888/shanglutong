//
//  KWCustomerPageTwo.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerPageTwo.h"
#import "KWCustomDetailModel.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import <MJExtension.h>

@interface KWCustomerPageTwo () {
    NSArray *_array;
}

@end

@implementation KWCustomerPageTwo

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //增加数据
    [self createDate];
    
    [self addModel];
}

- (void)footView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 150)];
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(53, 15, 100, 14)];
    lable.textColor = [UIColor grayColor];
    lable.text = @"编        辑";
    lable.font = [UIFont systemFontOfSize:14];
    [view addSubview:lable];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 0, KWSCREEN_WIDTH-20, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    line.alpha = 0.3;
    [view addSubview:line];
    
    UILabel *textLable = [[UILabel alloc]initWithFrame:CGRectMake(53, CGRectGetMaxY(lable.frame)+10, KWSCREEN_WIDTH-106, 75)];
    textLable.layer.borderWidth = 1;
    textLable.layer.borderColor = [UIColor grayColor].CGColor;
    textLable.numberOfLines = 0;
    textLable.text = _model.Bak;
    textLable.textAlignment = NSTextAlignmentCenter;
    textLable.font = [UIFont systemFontOfSize:12];
    [view addSubview:textLable];
    
    self.tableView.tableFooterView = view;
    
    
}

- (void)addModel{
    
    //设置常用参数
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomIDNow"] forKey:@"clientid"];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS, @"/api/client/get"];
    
    __weak KWCustomerPageTwo *weakSelf = self;
    
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        weakSelf.model = [KWCustomDetailModel mj_objectWithKeyValues:[json valueForKey:@"back"]];
        //footView
        [self footView];
        [self.tableView reloadData];

    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
    
}

- (void)createDate
{
    _array = [NSArray arrayWithObjects:@"业  务  员",@"客户简称",@"客户全称",@"客户分类",@"客户国家",@"客户星级",@"客户来源",@"客户状态",@"客户类型",@"创建时间",@"最后联系",@"电        话",@"传        真",@"网        址",@"特殊要求", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
    }
    cell.textLabel.text = _array[indexPath.row];
    cell.textLabel.textColor = [UIColor grayColor];
    switch (indexPath.row) {
        case 0:
            cell.detailTextLabel.text = _model.CreaterName;
            break;
        case 1:
            cell.detailTextLabel.text = _model.SName;
            break;
        case 2:
            cell.detailTextLabel.text = _model.Name;
            break;
        case 3:
            cell.detailTextLabel.text = _model.ClientType;
            break;
        case 4:
            cell.detailTextLabel.text = _model.Country;
            break;
        case 5:
            cell.detailTextLabel.text = _model.ClientLevel;
            break;
        case 6:
            cell.detailTextLabel.text = _model.ClientSource;
            break;
        case 7:
            cell.detailTextLabel.text = _model.ClientState;
            break;
        case 8:
            cell.detailTextLabel.text = _model.ClientType;
            break;
        case 9:
            cell.detailTextLabel.text = _model.JoinTime;
            break;
        case 10:
            cell.detailTextLabel.text = _model.KfTime;
            break;
        case 11:
            cell.detailTextLabel.text = _model.Tel;
            break;
        case 12:
            cell.detailTextLabel.text = _model.Fax;
            break;
        case 13:
            cell.detailTextLabel.text = _model.Web;
            break;
        case 14:
            cell.detailTextLabel.text = _model.RDate;
            break;
        default:
            break;
    }
    cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

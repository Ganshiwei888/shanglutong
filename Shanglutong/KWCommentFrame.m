//
//  KWCommentFrame.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCommentFrame.h"
#import "Utility.h"

@implementation KWCommentFrame

- (void)setCommentModel:(KWCommentModel *)commentModel {
    _commentModel = commentModel;
    
    CGFloat comTitleW = 290;
    CGFloat comTitleH = 20;
    CGFloat comTitleX = 16;
    CGFloat comTitleY = 9;
    _comTitleF = CGRectMake(comTitleX, comTitleY, comTitleW, comTitleH);
    
    CGFloat comImageX = KWSCREEN_WIDTH - 50;
    CGFloat comImageY = 9;
    CGFloat comImageW = 24;
    CGFloat comImageH = 24;
    _comImageF = CGRectMake(comImageX, comImageY, comImageW, comImageH);
    
    CGFloat divImageX = 2;
    CGFloat divImageY = CGRectGetMaxY(_comImageF) + 4;
    CGFloat divImageW = KWSCREEN_WIDTH - 4;
    CGFloat divImageH = 1;
    _divImageF = CGRectMake(divImageX, divImageY, divImageW, divImageH);
    
    CGFloat comDetailX = 16;
    CGFloat comDetailY = CGRectGetMaxY(_divImageF) + 4;
    CGFloat comDetailW = KWSCREEN_WIDTH - 32;
    CGFloat comDetailH = 55;
    _comDetailF = CGRectMake(comDetailX, comDetailY, comDetailW, comDetailH);
    
    CGFloat userImageX = 0;
    CGFloat userImageY = CGRectGetMaxY(_comDetailF) + 4;
    CGFloat userImageW = 15;
    CGFloat userImageH = 15;
    _userImageF = CGRectMake(userImageX, userImageY, userImageW, userImageH);
    
    CGFloat userNameX = CGRectGetMaxX(_userImageF) + 4;
    CGFloat userNameY = CGRectGetMaxY(_comDetailF) + 2;
    CGFloat userNameW = 40;
    CGFloat userNameH = 20;
    _userNameF = CGRectMake(userNameX, userNameY, userNameW, userNameH);
    
    CGFloat timeX = CGRectGetMaxX(_userNameF) + 8;
    CGFloat timeY = userNameY;
    CGFloat timeW = 120;
    CGFloat timeH = 20;
    _timeF = CGRectMake(timeX, timeY, timeW, timeH);

}

@end









//
//  KWCustomerFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class KWCustomer;
@interface KWCustomerFrame : NSObject

/**
 customer
 */
@property (nonatomic, strong) KWCustomer *customer;
/**
 编号
 */
@property (nonatomic, assign, readonly) CGRect noLabelView;
/**
 no
 */
@property (nonatomic, assign, readonly) CGRect noValueView;
/**
 业务
 */
@property (nonatomic, assign, readonly) CGRect saleLabelView;
/**
 业务员
 */
@property (nonatomic, assign, readonly) CGRect saleValueView;
/**
 客户姓名
 */
@property (nonatomic, assign, readonly) CGRect customerNameView;
/**
 分割线
 */
@property (nonatomic, assign, readonly) CGRect dividingImageView;
/**
 type image
 */
@property (nonatomic, assign, readonly) CGRect typeImageView;
/**
 type label
 */
@property (nonatomic, assign, readonly) CGRect typeLabelView;
/**
 typeValueView
 */
@property (nonatomic, assign, readonly) CGRect typeValueView;
/**
 time image
 */
@property (nonatomic, assign, readonly) CGRect timeImageView;
/**
 time Label
 */
@property (nonatomic, assign, readonly) CGRect timeLabelView;
/**
 time Value
 */
@property (nonatomic, assign, readonly) CGRect timeValueView;
/**
 warn image
 */
@property (nonatomic, assign, readonly) CGRect warnImageView;
/**
 warn Label
 */
@property (nonatomic, assign, readonly) CGRect warnLabelView;
/**
 warn Value
 */
@property (nonatomic, assign, readonly) CGRect warnValueView;


@end



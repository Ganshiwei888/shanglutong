//
//  KWNewsListFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWNewsListModel;
@interface KWNewsListFrame : NSObject

@property (strong, nonatomic) KWNewsListModel *listModel;

@property (assign, readonly, nonatomic) CGRect readImageF;
@property (assign, readonly, nonatomic) CGRect detailF;
@property (assign, readonly, nonatomic) CGRect comeF;
@property (assign, readonly, nonatomic) CGRect menuNoF;
@property (assign, readonly, nonatomic) CGRect timeF;
@property (assign, readonly, nonatomic) CGRect typeF;

@property (assign, nonatomic) CGFloat cellHeight;

@end

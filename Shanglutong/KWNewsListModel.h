//
//  KWNewsListModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/24.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWNewsListModel : NSObject

@property (nonatomic, copy) NSString *AutoClose;
@property (nonatomic, copy) NSString *CreateTm;
@property (nonatomic, copy) NSString *Creater;
@property (nonatomic, copy) NSString *CreaterName;
@property (nonatomic, copy) NSString *ExId;
@property (nonatomic, copy) NSString *ExId1;
@property (nonatomic, copy) NSString *Html;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *IsRead;
@property (nonatomic, copy) NSString *IsShow;
@property (nonatomic, copy) NSString *KeyId;
@property (nonatomic, copy) NSString *MenuNo;
@property (nonatomic, copy) NSString *Type;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end


//AutoClose = 0;
//CreateTm = "2017-08-24T08:41:56.803";
//Creater = "";
//CreaterName = System;
//ExId = "";
//ExId1 = "";
//Html = "Fwd: Order Status (#272750948)";
//Id = 811;
//IsRead = 0;
//IsShow = 1;
//KeyId = 323;
//MenuNo = AN;
//Type = 2;


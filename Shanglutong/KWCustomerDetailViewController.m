//
//  KWCustomerDetailViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomerDetailViewController.h"
#import "KWCustomerPageOne.h"
#import "KWCustomerPageTwo.h"
#import "KWCustomerPageThree.h"
#import "KWCustomerPageFour.h"
#import "Utility.h"

#import <WMPageController.h>

@interface KWCustomerDetailViewController ()

@property (nonatomic,strong)WMPageController *page;

@end

@implementation KWCustomerDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    
//    [self initSetView];
    
    [self createNavigationBar];
    //创建分页控制器
    [self createPageController];
    
    [self getNotifacation];
    
}

#pragma mark  initView
- (void) createNavigationBar {
    
    
    
}

- (void)initSetView {
    
    //标题
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 10, 18)];
    titleLabel.text = [NSString stringWithFormat:@"%@",self.model.list[_num].No];
    titleLabel.font =[UIFont systemFontOfSize:16];
    titleLabel.textColor = [UIColor blackColor];
    self.navigationItem.titleView = titleLabel;
    
    //下部头部试图
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, KWSCREEN_WIDTH, 125)];
    headView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:headView];
    
    //labelone
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 17, KWSCREEN_WIDTH - 40, 30)];
//    label.center = CGPointMake(headView.center.x, 17+30/2);
    label.font = [UIFont systemFontOfSize:22];
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    label.text = self.model.list[_num].SName;
    [headView addSubview:label];
    
    
    //label状态
    UILabel *zhuangtaiLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(label.frame),  CGRectGetMaxY(label.frame)+20, 50, 13)];
    zhuangtaiLabel.textColor = [UIColor whiteColor];
    zhuangtaiLabel.text = @"状态:";
    zhuangtaiLabel.font = [UIFont systemFontOfSize:13];
    [zhuangtaiLabel sizeToFit];
    [headView addSubview:zhuangtaiLabel];
    
    UILabel *zhuangtaiLabelNext = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(zhuangtaiLabel.frame)+5, CGRectGetMaxY(label.frame)+20, 100, 13)];
    zhuangtaiLabelNext.textColor = [UIColor yellowColor];
    zhuangtaiLabelNext.text = self.model.list[_num].ClientState;
    zhuangtaiLabelNext.font = [UIFont systemFontOfSize:13];
    zhuangtaiLabelNext.textAlignment = NSTextAlignmentLeft;
    [zhuangtaiLabelNext sizeToFit];
    [headView addSubview:zhuangtaiLabelNext];
    
    //等级
    UILabel *rankLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(zhuangtaiLabel.frame)+100, CGRectGetMaxY(label.frame)+20, 50, 13)];
    rankLabel.textColor = [UIColor whiteColor];
    rankLabel.text = @"等级";
    rankLabel.font = [UIFont systemFontOfSize:13];
    [rankLabel sizeToFit];
    [headView addSubview:rankLabel];
    
    
    UILabel *rankLabelNext = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(rankLabel.frame)+5, CGRectGetMaxY(label.frame)+20, 50, 13)];
    rankLabelNext.textColor = [UIColor yellowColor];
    rankLabelNext.text = self.model.list[_num].ClientLevel;
    rankLabelNext.font = [UIFont systemFontOfSize:13];
    [rankLabelNext sizeToFit];
    [headView addSubview:rankLabelNext];
    
    
    //业务员
    UILabel *salesmanLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(label.frame), CGRectGetMaxY(label.frame)+50, 50, 13)];
    salesmanLabel.textColor = [UIColor whiteColor];
    salesmanLabel.text = @"业务员";
    salesmanLabel.font = [UIFont systemFontOfSize:13];
    [salesmanLabel sizeToFit];
    [headView addSubview:salesmanLabel];
    
    UILabel *salesmanLabelNext = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(salesmanLabel.frame)+5, CGRectGetMaxY(label.frame)+50, 50, 13)];
    salesmanLabelNext.textColor = [UIColor yellowColor];
    salesmanLabelNext.text = self.model.list[_num].Sale;
    salesmanLabelNext.font = [UIFont systemFontOfSize:13];
    salesmanLabelNext.textAlignment = NSTextAlignmentLeft;
    [salesmanLabelNext sizeToFit];
    [headView addSubview:salesmanLabelNext];
    
    UILabel *score = [[UILabel alloc]initWithFrame:CGRectMake(KWSCREEN_WIDTH-25-50, 75, 50, 30)];
    score.backgroundColor = RGBACOLOR(19, 145, 67, 1);
    score.textColor = [UIColor whiteColor];
    score.text = [NSString stringWithFormat:@"%@分",self.model.list[_num].Pf];
    score.font = [UIFont systemFontOfSize:18];
    score.layer.cornerRadius = 15;
    score.layer.masksToBounds = YES;
    score.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:score];

}

- (void)createPageController {
    
    _page = [[WMPageController alloc] initWithViewControllerClasses:@[[KWCustomerPageOne class],[KWCustomerPageTwo class],[KWCustomerPageThree class], [KWCustomerPageFour class]] andTheirTitles:@[@"概况",@"详情",@"邮件",@"联系人"]];
    
    //设置WMPageController每个标题的宽度
    _page.menuItemWidth = KWSCREEN_WIDTH/4;
    
//    _page.menuViewStyle = WMMenuViewStyleLine;
    _page.titleColorSelected = [UIColor blackColor];
    _page.titleColorNormal = [UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1];
    _page.progressColor = [UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1];
    //设置WMPageController标题栏的高度
//    _page.menuHeight = 35;
    //设置WMPageController选中的标题的颜色
    
    
    [self addChildViewController:_page];
    _page.view.frame = CGRectMake(0, 125, self.view.frame.size.width, KWSCREEN_HEIGHT - 190);
    [self.view addSubview:_page.view];
    
}

#pragma mark - notifacationCenter
- (void)getNotifacation {
    //pageTwo
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(postID) name:@"KWCustomerPageTwo" object:nil];
}

- (void)postID {
    id str = self.model.list[_num];
    //发送通知  Id发送出去
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"CustomID" object:self.model.list[_num].id];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CustomID" object:nil userInfo:@{@"customID":str}];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

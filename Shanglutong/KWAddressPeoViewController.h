//
//  KWAddressPeoViewController.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/7.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KWUserAllMenu;
@interface KWAddressPeoViewController : UIViewController

@property (nonatomic, strong) KWUserAllMenu *allMenu;

- (void)receTheAddress:(KWUserAllMenu *)allMenu;

@end

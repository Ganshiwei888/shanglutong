//
//  KWMomentModel.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMomentWorkdtModel.h"
#import "KWMomentCommlistModel.h"
#import "Utility.h"

@implementation KWMomentWorkdtModel

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        _Bak = checkStringNull(dict[@"Bak"]);
        _CommCount = checkStringNull(dict[@"CommCount"]);
        _CreateTm = checkStringNull(dict[@"CreateTm"]);
        _Creater = checkStringNull(dict[@"Creater"]);
        _KeyId = checkStringNull(dict[@"KeyId"]);
        _TitleHtml = checkStringNull(dict[@"TitleHtml"]);
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *nsdict in dict[@"commlist"]) {
            KWMomentCommlistModel *model = [[KWMomentCommlistModel alloc] initWithDict:nsdict];
            [temp addObject:model];
        }
        _commlist = temp;
    }
    return self;
}

@end

//
//  TMHWorkBenchTopView.h
//  Shanglutong
//
//  Created by Ming Tian on 2017/10/18.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMHWorkBenchTopView : UIView<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *noticeButton;
@property (weak, nonatomic) IBOutlet UIScrollView *infoSrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (nonatomic, strong) NSTimer *timer;

@end

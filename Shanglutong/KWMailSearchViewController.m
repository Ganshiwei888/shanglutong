//
//  KWMailSearchViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/9/11.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWMailSearchViewController.h"
#import "KWMailListTableViewCell.h"
#import "KWMailListFrame.h"
#import "KWMailListResult.h"
#import "KWMailDetailViewController.h"
#import "Utility.h"
#import "KWHttpTool.h"
#import <MGSwipeTableCell.h>
#import <MJExtension.h>


@interface KWMailSearchViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) UITableView *mailTableView;

@property (nonatomic, strong) UISearchBar *search;

@property (nonatomic,strong) UISegmentedControl *segment;

//搜索关键字
@property (nonatomic,strong)NSString *strKey;
//搜索的内容
@property (nonatomic,strong)NSString *strDetail;

@property (nonatomic, strong) NSArray *mailArray;

@end

@implementation KWMailSearchViewController

#pragma mark - lazyload

- (UITableView *)tableView {
    if (!_mailTableView){
        _mailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 46, KWSCREEN_WIDTH, KWSCREEN_HEIGHT - 46) style:UITableViewStylePlain];
        _mailTableView.delegate = self;
        _mailTableView.dataSource = self;
        _mailTableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _mailTableView;
}

- (UISegmentedControl *)segment {
    if (!_segment) {
        NSArray *segmentedArray = [[NSArray alloc]initWithObjects:@"发件人",@"收件人",@"主题",@"全部",nil];
        //初始化UISegmentedControl
        _segment = [[UISegmentedControl alloc]initWithItems:segmentedArray];
        _segment.frame = CGRectMake(10, 72, KWSCREEN_WIDTH-20, 30);
        // 设置默认选择项索引
        _segment.selectedSegmentIndex = 2;
        _segment.tintColor = RGBACOLOR(17, 91, 162, 1);
        // 设置在点击后是否恢复原样
        _segment.momentary = NO;
        _segment.hidden = NO;
        _segment.backgroundColor = [UIColor whiteColor];
        //添加事件
        [_segment addTarget:self action:@selector(changeSeg:) forControlEvents:UIControlEventValueChanged];
    }
    return _segment;
}

- (void)viewWillAppear:(BOOL)animated {

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _search = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, 44)];
    _search.barStyle = UIBarStyleDefault;
    _search.showsCancelButton = YES;
    _search.delegate = self;
    _search.placeholder = @"请输入搜索内容";
    self.navigationItem.titleView = _search;
    
 
    [self.view addSubview:self.tableView];
    
}

#pragma mark - searchBarDelegate
/*
 在输入后出现多选择框
 */
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.view addSubview:self.segment];
}

//搜索按钮点击的回调
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"搜索");
}

//书本按钮点击的回调
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    
}


//取消按钮点击的回调
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//搜索结果按钮点击的回调
- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"搜索");
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (!self.strKey) {
        self.strKey = @"htmlbody";
    }
    self.strDetail = searchText;
    [self showSuccese:self.strKey PeopleText:searchText];
}


- (void)changeSeg:(UISegmentedControl *)sender {
    NSLog(@"测试");
    if (sender.selectedSegmentIndex == 0) {
        self.strKey = @"fromname";
        [self showSuccese:self.strKey PeopleText:self.strDetail];
    }else if (sender.selectedSegmentIndex == 1){
        self.strKey = @"subject";
        [self showSuccese:self.strKey PeopleText:self.strDetail];
    }else if (sender.selectedSegmentIndex == 2){
        self.strKey = @"htmlbody";
        [self showSuccese:self.strKey PeopleText:self.strDetail];
    }else if (sender.selectedSegmentIndex == 3){
        self.strKey = @"textbody";
        [self showSuccese:self.strKey PeopleText:self.strDetail];
    }
}

#pragma mark - Search -

- (void)showSuccese:(NSString *)itemName PeopleText:(NSString *)peopleText {
    //设置常用参数
    NSMutableDictionary *requestInfo = [[NSMutableDictionary alloc]init];
    
    [requestInfo setValue:USER_ID forKey:@"UserId"];
    [requestInfo setValue:USER_PASSWORD forKey:@"Password"];
    [requestInfo setValue:RAN forKey:@"Ran"];
    [requestInfo setValue:SIGN forKey:@"Sign"];
    [requestInfo setValue:@"1" forKey:@"pageindex"];
    [requestInfo setValue:@"100" forKey:@"pagemax"];
    [requestInfo setValue:@"0" forKey:@"Boxid"];
    [requestInfo setValue:peopleText forKey:@"key"];
    [requestInfo setValue:@"all" forKey:@"Act"];
    NSString *like = [NSString stringWithFormat:@"%@ like '%%%@%%'",itemName,peopleText];
    [requestInfo setValue:like forKey:@"where"];
    NSLog(@"%@",requestInfo);
   
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/mail/Getlist", USER_SERVERADDRESS];
    __weak KWMailSearchViewController *weakSelf = self;
    [KWHttpTool postWithURL:netPath params:requestInfo success:^(id json) {
        
        NSLog(@"%@",json);
        
        KWMailListResult *listResult = [KWMailListResult mj_objectWithKeyValues:[json valueForKey:@"back"]];
        NSMutableArray *modelArray = [NSMutableArray array];
        for (int i = 0; i < listResult.list.count; i++) {
            KWMailListFrame *listFrame = [[KWMailListFrame alloc] init];
            listFrame.isSelect = @"no";
            listFrame.mailModelList = listResult.list[i];
            [modelArray addObject:listFrame];
        }
        weakSelf.mailArray = modelArray;
        [weakSelf.mailTableView reloadData];
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];


}

#pragma mark - UITableViewDataSource && UITableViewdelegate -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mailArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"search";
    
    KWMailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[KWMailListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    cell.mailListFrame = listFrame;
    
    if ([listFrame.mailModelList.Read isEqualToString:@"否"]) {
        cell.readView.hidden = NO;
    } else {
        cell.readView.hidden = YES;
    }
    
    if ([listFrame.isEdit isEqualToString:@"0"]) {
        cell.selectButton.hidden = YES;
    }
    
    if ([listFrame.isEdit isEqualToString:@"1"]) {
        cell.selectButton.hidden = NO;
    }
    
    if ([listFrame.isSelect  isEqualToString: @"yes"]) {
        cell.selectButton.selected = YES;
    } else if ([listFrame.isSelect isEqualToString:@"no"]) {
        cell.selectButton.selected = NO;
    }
    cell.selectButton.tag = indexPath.row + 1000;
    
//    [cell.selectButton addTarget:self action:@selector(selectBtned:) forControlEvents:UIControlEventTouchUpInside];
    
    if (listFrame.mailModelList.TopTime) {
        cell.topView.hidden = NO;
    } else {
        cell.topView.hidden = YES;
    }
    
    if (listFrame.mailModelList.star) {
        cell.starView.hidden = NO;
    } else {
        cell.starView.hidden = YES;
    }
    
    if (listFrame.mailModelList.redflag) {
        cell.flagView.hidden = NO;
    } else {
        cell.flagView.hidden = YES;
    }
    
    if ([listFrame.mailModelList.AccCount isEqualToString:@"0"]) {
        cell.attachmentView.hidden = YES;
    } else {
        cell.attachmentView.hidden = NO;
    }
    
    //configure right buttons
    
    return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KWMailListFrame *listFrame = self.mailArray[indexPath.row];
    KWMailListModel *mailModel = listFrame.mailModelList;
    mailModel.Read = @"是";
    
//    NSIndexPath *indexPathed = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
//    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPathed,nil] withRowAnimation:UITableViewRowAnimationFade];
    
    KWMailDetailViewController *detailVc = [[KWMailDetailViewController alloc] init];
  
    
    [detailVc recieveTheMailListArray:mailModel AndAct:@"all"];
//    [detailVc reciiveTheMailList:self.mailListArray andIndexPath:indexPath];
    [self.navigationController pushViewController:detailVc animated:YES];
    
    
}

// 将点击tableviewcell的时候收回 searchBar 键盘
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.search resignFirstResponder];
    return indexPath;
}
// 滑动的时候 searchBar 回收键盘
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.search resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

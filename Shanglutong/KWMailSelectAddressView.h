//
//  KWMailSelectAddressView.h
//  Shanglutong
//
//  Created by KeWen on 2017/9/4.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectAdd)(NSString *Email);

@interface KWMailSelectAddressView : UIView

- (void)showAnimated;

@property (copy, nonatomic) SelectAdd selectAdd;

@end

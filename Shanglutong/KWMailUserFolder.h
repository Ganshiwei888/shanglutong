//
//  KWMailUserFolder.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/11.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMailUserFolder : KWBaseParam

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *mailboxId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *parentid;
@property (nonatomic, copy) NSString *sort;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end

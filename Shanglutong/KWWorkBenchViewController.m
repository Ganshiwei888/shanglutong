//
//  KWWorkBenchViewController.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/9.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWWorkBenchViewController.h"
#import "KWMailHomeViewController.h"
#import "KWCustomerViewController.h"
#import "KWMailLeftViewController.h"
#import "KWNavigationController.h"
#import "KWCustomerViewController.h"
#import "KWCustomerLeftViewController.h"
#import "KWPbCustomerViewController.h"
#import "KWPbCustomerLeftViewController.h"
#import "KWReportViewController.h"
#import "KWWorkBenchCellView.h"
#import "KWCustomGrid.h"
#import "KWBaseParam.h"
#import "KWHttpTool.h"
#import "KWUserMenuno.h"
#import "MomentsViewController.h"
#import "KWCommentViewController.h"
#import "KWNewsViewController.h"
#import "KWDateManageViewController.h"
#import "KWBusinessTableViewController.h"
#import "JSBadgeView.h"
#import "KWMailListTool.h"
#import "KWMailListParam.h"
#import <MMDrawerController.h>
#import <MJExtension.h>
#import <SVProgressHUD.h>
#import "Utility.h"
#import "KWWorkButton.h"
#import "TMHWorkBenchTopView.h"
#import "TMHWorkBenchTopInfoView.h"
#import "TMHTopInfoModel.h"
#import "KWAddressViewController.h"

@interface KWWorkBenchViewController () <CustomGridDelegate, UICollectionViewDataSource, UICollectionViewDelegate> {
    
    BOOL isSelected;
    BOOL contain;
    //是否可跳转应用对应的详细页面
    BOOL isSkip;
    //选中格子的起始位置
    CGPoint startPoint;
    //选中格子的起始坐标位置
    CGPoint originPoint;
    
    UIImage *normalImage;
    UIImage *highlightedImage;
    
}

@property (nonatomic, strong) NSMutableArray *topInfoArr;
@property (nonatomic, strong) NSMutableArray *baseFunction;
@property (nonatomic, strong) NSArray *secondFunction;
@property (nonatomic, weak) UICollectionView *workBenchCollection;
@property (nonatomic, weak) UIView *topView;
@property (nonatomic, weak) UIButton *mailButton;
@property (nonatomic, strong) JSBadgeView *badgeView;


@end

@implementation KWWorkBenchViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    CGFloat topViewHeight = (KWSCREEN_HEIGHT == 480) ? KWSCREEN_HEIGHT*0.32 : KWSCREEN_HEIGHT*0.25;
    
    [self.topView setFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, topViewHeight)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self getTopInfo];
    [self getUserAuthority];
    // Do any additional setup after loading the view from its nib.
    [self addObserver];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUI:) name:@"switchAccount" object:nil];
}

- (void)reloadUI:(NSNotification *)notification {
    [self setupUnread];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"topView");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:YES];

}

- (void)setupView {
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWSCREEN_WIDTH, KWSCREEN_HEIGHT/4)];
    topView.backgroundColor = RGBCOLOR(25, 60, 138);
//    [self.view addSubview:topView];
//    self.topView = topView;
    
    TMHWorkBenchTopView *aTopView = [[NSBundle mainBundle]loadNibNamed:@"TMHWorkBenchTopView" owner:self options:nil].lastObject;
    [aTopView.noticeButton addTarget:self action:@selector(noticeBtnCiick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:aTopView];
    self.topView = aTopView;
    /*
    UIButton *noticeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [noticeBtn setFrame:CGRectMake(16, 28, 45, 45)];
    [noticeBtn setImage:[UIImage imageNamed:@"闹钟"] forState:UIControlStateNormal];
    [noticeBtn addTarget:self action:@selector(noticeBtnCiick:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:noticeBtn];
    
    UIButton *diaryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [diaryBtn setFrame:CGRectMake(KWSCREEN_WIDTH - 16 - 45, 28, 45, 45)];
    [diaryBtn setImage:[UIImage imageNamed:@"日历"] forState:UIControlStateNormal];
    [diaryBtn addTarget:self action:@selector(diaryBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:diaryBtn];
    
    UILabel *workBenchLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.topView.frame.size.width/2 - 60, 35, 120, 25)];
    workBenchLabel.font = [UIFont systemFontOfSize:18];
    workBenchLabel.textColor = [UIColor whiteColor];
    workBenchLabel.text = @"工作平台";
    workBenchLabel.textAlignment = NSTextAlignmentCenter;
    [self.topView addSubview:workBenchLabel];
    
    CGFloat ButtonW = 80;
    CGFloat ButtonH = 80;
    
    CGFloat buttonX = (KWSCREEN_WIDTH - (80 * 4))/ 4;
    CGFloat buttonY = (KWSCREEN_HEIGHT/4)/2;
    
    KWWorkButton *mailButton = [[KWWorkButton alloc] initWithFrame:CGRectMake(buttonX / 2, buttonY, ButtonW, ButtonH)];
    [mailButton setImage:[UIImage imageNamed:@"邮件管理"] forState:UIControlStateNormal];
    [mailButton setBackgroundColor:[UIColor clearColor]];
    [mailButton setTitle:@"邮件管理" forState:UIControlStateNormal];
    [mailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mailButton addTarget:self action:@selector(mailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:mailButton];
    self.mailButton = mailButton;
    self.badgeView = [[JSBadgeView alloc]initWithParentView:mailButton alignment:JSBadgeViewAlignmentTopRight];
    self.badgeView.badgePositionAdjustment = CGPointMake(-15, 10);
    //1、背景色
    self.badgeView.badgeBackgroundColor = [UIColor redColor];
    //2、没有反光面
    self.badgeView.badgeOverlayColor = [UIColor clearColor];
    //3、外圈的颜色，默认是白色
    self.badgeView.badgeStrokeColor = [UIColor redColor];
    [self setupUnread];
//    self.badgeView.badgeText = @"999";
    
    KWWorkButton *dailyButton = [[KWWorkButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(mailButton.frame) + buttonX, buttonY, ButtonW, ButtonH)];
    [dailyButton setImage:[UIImage imageNamed:@"工作日报"] forState:UIControlStateNormal];
    [dailyButton setBackgroundColor:[UIColor clearColor]];
    [dailyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dailyButton setTitle:@"工作报告" forState:UIControlStateNormal];
    [dailyButton addTarget:self action:@selector(reportBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:dailyButton];
    
    KWWorkButton *momentButton = [[KWWorkButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(dailyButton.frame) + buttonX, buttonY, ButtonW, ButtonH)];
    [momentButton setImage:[UIImage imageNamed:@"工作动态"] forState:UIControlStateNormal];
    [momentButton setBackgroundColor:[UIColor clearColor]];
    [momentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [momentButton setTitle:@"工作动态" forState:UIControlStateNormal];
    [momentButton addTarget:self action:@selector(momentshow:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:momentButton];
    
    KWWorkButton *commentButton = [[KWWorkButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(momentButton.frame) + buttonX, buttonY, ButtonW, ButtonH)];
    [commentButton setImage:[UIImage imageNamed:@"评论"] forState:UIControlStateNormal];
    [commentButton setBackgroundColor:[UIColor clearColor]];
    [commentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [commentButton setTitle:@"评论中心" forState:UIControlStateNormal];
    [commentButton addTarget:self action:@selector(commentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:commentButton];
    */
}

- (void)noticeBtnCiick:(UIButton *)sender {
    KWNewsViewController *vc = [[KWNewsViewController alloc] init];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:vc];
    [nav setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)diaryBtnClick:(UIButton *)sender {
    KWDateManageViewController *vc = [[KWDateManageViewController alloc] init];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:vc];
    [nav setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)momentshow:(UIButton *)sender {
    MomentsViewController* vc = [[MomentsViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)commentBtn:(UIButton *)sender {
    KWCommentViewController *vc = [[KWCommentViewController alloc] init];
    KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:vc];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)reportBtnClick:(UIButton *)sender {
    KWReportViewController *vc = [[KWReportViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupUnread {
    
    KWMailListParam *param = [KWMailListParam param];
    param.pagemax = @"50";
    param.pageindex = @"1";
    param.Boxid = @"0";
    param.act = @"ALL";
    __weak KWWorkBenchViewController *weakSelf = self;
    [KWMailListTool postGetMailListWith:param success:^(KWMailListResult *resultData) {
        
        NSLog(@"aaa%@",resultData.unread);
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = [resultData.unread intValue];
        
        weakSelf.badgeView.badgeText = resultData.unread;
        
    } failure:^(NSError *error) {
        NSLog(@"can not load data");
        //            [LWActiveIncator hideInViwe:self.view];
        [SVProgressHUD showErrorWithStatus:@"加载邮件失败"];
        [SVProgressHUD dismissWithDelay:1];
    }];

}

- (void)setupTopInfoView {
    if([self.topView isKindOfClass:[TMHWorkBenchTopView class]]) {
        
        TMHWorkBenchTopView *tmhTopView = (TMHWorkBenchTopView *)self.topView;
        CGFloat x = 25.0f;
        CGFloat y = 0.0f;
        CGFloat width = KWSCREEN_WIDTH - x * 2;
        CGFloat height = tmhTopView.infoSrollView.frame.size.height;
        
        [tmhTopView.infoSrollView setContentSize:CGSizeMake(KWSCREEN_WIDTH * self.topInfoArr.count, height)];
        tmhTopView.infoSrollView.pagingEnabled = YES;
        
        tmhTopView.pageControl.numberOfPages = self.topInfoArr.count;
        
        for(int i = 0; i < self.topInfoArr.count; i++) {
            TMHTopInfoModel *model = self.topInfoArr[i];
            TMHWorkBenchTopInfoView *infoView = [[NSBundle mainBundle]loadNibNamed:@"TMHWorkBenchTopInfoView" owner:self options:nil].firstObject;
            infoView.flagLabel.text = model.name;
            infoView.mainLabel.attributedText = [[NSAttributedString alloc]initWithString:model.content attributes:@{NSForegroundColorAttributeName:[UIColor redColor], NSFontAttributeName:[UIFont boldSystemFontOfSize:27]}];
            infoView.timeLabel.text = model.date;
            infoView.unitLabel.text = model.unit;
            
            [infoView setFrame:CGRectMake(x, y, width, height)];
            [tmhTopView.infoSrollView addSubview:infoView];
            
            // 计算坐标;
            x += KWSCREEN_WIDTH;
            
        }
        
        
    }
}

- (void)getTopInfo {
    KWBaseParam *param = [KWBaseParam param];
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/system/getmb",USER_SERVERADDRESS];
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
        if([json[@"code"] isEqualToString:@"0000"]) {
            NSDictionary *back = json[@"back"];
            NSTimeInterval timestamp = [json[@"timestamp"]doubleValue];
            NSMutableArray *tmpArr = [NSMutableArray array];
            for(NSString *key in back.allKeys) {
                TMHTopInfoModel *model = [[TMHTopInfoModel alloc] initWithName:key content:[[back valueForKey:key]doubleValue] timestamp:timestamp];
                [tmpArr addObject:model];
            }
            self.topInfoArr = tmpArr;
            [self setupTopInfoView];
            
        }else {
            NSLog(@"请求错误");
        }
    } failure:^(NSError *error) {
        NSLog(@"请求topinfo失败");
    }];
}

- (void)getUserAuthority {
    
    KWBaseParam *param = [KWBaseParam param];
    
    NSString *netPath = [NSString stringWithFormat:@"http://%@/api/user/getmenu",USER_SERVERADDRESS];
    
    [KWHttpTool postWithURL:netPath params:param.mj_keyValues success:^(id json) {
//        NSLog(@"%@",json);
        NSMutableArray *temp = [NSMutableArray array];
        NSMutableArray *tempTwo = [NSMutableArray array];
        id back = json[@"back"];
        if ([back isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dict in back) {
                KWUserMenuno *model = [[KWUserMenuno alloc] initWithDict:dict];
                [temp addObject:model];
                for (NSDictionary *dicter in model.next) {
                    KWUserMenuno *modeler = [[KWUserMenuno alloc] initWithDict:dicter];
                    [tempTwo addObject:modeler];
                }
            }
        }
        self.baseFunction = temp;
        self.secondFunction = tempTwo;
        
        for (int index = 0; index < self.secondFunction.count; index ++) {
            KWUserMenuno *model = self.secondFunction[index];
            NSLog(@"%@",model.name);
        }
        
        [self setupIndexPathCell];
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)setupIndexPathCell {
    
    NSArray *temp1 = [self.baseFunction copy];
    NSInteger count = temp1.count;
    for (NSInteger index = 0; index < count; index++) {
        
        KWUserMenuno *model = temp1[index];
        NSLog(@"xxx%@",model.name);

        if ([model.name isEqualToString:@"邮件营销"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"系统管理"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"工作报告"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"寄样管理"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"工作报告"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"海关数据"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"销售合同"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"知识中心"]) {
            [self.baseFunction removeObject:model];
        }
        if ([model.name isEqualToString:@"采购订单"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"商机中心"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"报价管理"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"询价管理"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"产品管理"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"供应商管理"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name containsString:@"寄样管理"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"寄样管理"]) {
            [self.baseFunction removeObject:model];
        }

        if ([model.name isEqualToString:@"BI商业智能"]) {
            [self.baseFunction removeObject:model];
        }
    }
    
    [self.baseFunction removeLastObject];
    
    for (int j = 0; j < self.secondFunction.count; j++) {
        KWUserMenuno *second = self.secondFunction[j];
        if ([second.name isEqualToString:@"采购询价"]) {
            [self.baseFunction addObject:self.secondFunction[j]];
        }
        if ([second.name isEqualToString:@"客户公海"]) {
            [self.baseFunction addObject:self.secondFunction[j]];
        }
        if ([second.name isEqualToString:@"客户名片"]) {
            [self.baseFunction addObject:self.secondFunction[j]];
        }
        if ([second.name isEqualToString:@"已删客户"]) {
            [self.baseFunction addObject:self.secondFunction[j]];
        }
        
    }
    
    NSLog(@"xxx%lu",(unsigned long)self.baseFunction.count);
    NSArray *temp2 = [self.baseFunction copy];
    NSInteger count1 = temp2.count;
    for (NSInteger i = 0; i < count1; i++) {
        KWUserMenuno *model = temp2[i];
        NSLog(@"xxxx%@",model.name);
        if ([model.name isEqualToString:@"工作报告"]) {
            [self.baseFunction removeObject:model];
        }
        
        if ([model.name isEqualToString:@"供应商"]) {
            [self.baseFunction removeObject:model];
        }

        if ([model.name isEqualToString:@"BI商业智能"]) {
            [self.baseFunction removeObject:model];
        }
    }
    
    [self setupWorkCell];
}

- (void)setupWorkCell {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat topViewHeight = (KWSCREEN_HEIGHT == 480) ? KWSCREEN_HEIGHT*0.32 : KWSCREEN_HEIGHT*0.25;
    CGFloat collectionViewHeight = KWSCREEN_HEIGHT - topViewHeight - 49;
    
    UICollectionView *workBenchCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, topViewHeight, KWSCREEN_WIDTH, collectionViewHeight) collectionViewLayout:layout];
    [workBenchCollection registerClass:[KWWorkBenchCellView class] forCellWithReuseIdentifier:@"cellId"];
    workBenchCollection.delegate = self;
    workBenchCollection.dataSource = self;
    workBenchCollection.backgroundColor = [UIColor clearColor];
    [self.view addSubview:workBenchCollection];
    self.workBenchCollection = workBenchCollection;

}

#pragma mark - UICollectionDataSource && UICollectionDelegate - 

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.baseFunction){
        
        return self.baseFunction.count + 5;
        
    }else {
        return 0;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(KWSCREEN_WIDTH/4, 120);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cellId";
    
    KWWorkBenchCellView *cell = (KWWorkBenchCellView *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    if(indexPath.row == 0) {
        KWUserMenuno *cellModel = [[KWUserMenuno alloc]init];
        cellModel.name = @"评论中心";
        cell.userMenuno = cellModel;
        return cell;
    }
    if(indexPath.row == 1) {
        KWUserMenuno *cellModel = [[KWUserMenuno alloc]init];
        cellModel.name = @"工作动态";
        cell.userMenuno = cellModel;
        return cell;
    }
    if(indexPath.row == 2) {
        KWUserMenuno *cellModel = [[KWUserMenuno alloc]init];
        cellModel.name = @"待办日程";
        cell.userMenuno = cellModel;
        return cell;
    }
    if(indexPath.row == 3) {
        KWUserMenuno *cellModel = [[KWUserMenuno alloc]init];
        cellModel.name = @"工作日报";
        cell.userMenuno = cellModel;
        return cell;
    }
    if(indexPath.row == 4) {
        KWUserMenuno *cellModel = [[KWUserMenuno alloc]init];
        cellModel.name = @"邮件管理";
        cell.userMenuno = cellModel;
        
        self.badgeView = [[JSBadgeView alloc]initWithParentView:cell alignment:JSBadgeViewAlignmentTopRight];
        self.badgeView.badgePositionAdjustment = CGPointMake(-15, 10);
        //1、背景色
        self.badgeView.badgeBackgroundColor = [UIColor redColor];
        //2、没有反光面
        self.badgeView.badgeOverlayColor = [UIColor clearColor];
        //3、外圈的颜色，默认是白色
        self.badgeView.badgeStrokeColor = [UIColor redColor];
        [self setupUnread];
        
        return cell;
    }
    
    
    KWUserMenuno *cellModel = self.baseFunction[indexPath.row-5];
    cell.userMenuno = cellModel;
    
    
    return cell;
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0) {
        [self commentBtn:nil];
        return;
    }
    if(indexPath.row == 1) {
        [self momentshow:nil];
        return;
    }
    if(indexPath.row == 2) {
        [self diaryBtnClick:nil];
        return;
    }
    if(indexPath.row == 3) {
        [self reportBtnClick:nil];
        return;
    }
    if(indexPath.row == 4) {
        [self mailButtonClick:nil];
        return;
    }
    
    KWUserMenuno *model = self.baseFunction[indexPath.row-5];
    
    if ([model.name isEqualToString:@"客户名片"]) {
        KWNavigationController *nav = self.tabBarController.viewControllers[3];
        [nav popToRootViewControllerAnimated:NO];
        KWAddressViewController *addressVC = [KWAddressViewController sharedAddressVC];
        addressVC.segmentIndex = 0;
        self.tabBarController.selectedIndex = 3;
    }
    
    if ([model.name isEqualToString:@"客户管理"]) {
        KWCustomerViewController *custom = [[KWCustomerViewController alloc] init];
        
        KWCustomerLeftViewController *leftVc = [[KWCustomerLeftViewController alloc] init];
        KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:custom];
        MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:leftNav leftDrawerViewController:leftVc];
        drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
        //    [drawerController.navigationController setNavigationBarHidden:YES];
        //5、设置左右两边抽屉显示的多少
        drawerController.maximumLeftDrawerWidth = 280.0;
        drawerController.maximumRightDrawerWidth = 280.0;
        [drawerController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:drawerController animated:YES completion:^{
            
        }];
    }
    
    if ([model.name isEqualToString:@"客户公海"]) {
        KWPbCustomerViewController *pbCustomer = [[KWPbCustomerViewController alloc] init];
        KWPbCustomerLeftViewController *leftVc = [[KWPbCustomerLeftViewController alloc] init];
        KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:pbCustomer];
        MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:leftNav leftDrawerViewController:leftVc];
        drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
        //    [drawerController.navigationController setNavigationBarHidden:YES];
        //5、设置左右两边抽屉显示的多少
        drawerController.maximumLeftDrawerWidth = 200.0;
        drawerController.maximumRightDrawerWidth = 200.0;
        [drawerController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:drawerController animated:YES completion:^{
            
        }];
    }
    
//    if ([model.name isEqualToString:@"商机中心"]) {
//
//        KWBusinessTableViewController *vc = [[KWBusinessTableViewController alloc] init];
//        KWNavigationController *nav = [[KWNavigationController alloc] initWithRootViewController:vc];
//        [nav setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//        [self presentViewController:nav animated:YES completion:^{
//
//        }];
//
//    }
    
}

- (void)mailButtonClick:(UIButton *)sender {
    
    KWMailHomeViewController *mailHome = [[KWMailHomeViewController alloc] init];
    
    KWMailLeftViewController *leftVc = [[KWMailLeftViewController alloc] init];
    
    KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:mailHome];
    
    MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:leftNav leftDrawerViewController: leftVc];
    drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningNavigationBar;
    drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
            //    [drawerController.navigationController setNavigationBarHidden:YES];
            //5、设置左右两边抽屉显示的多少
    drawerController.maximumLeftDrawerWidth = 280.0;
    drawerController.maximumRightDrawerWidth = 280.0;
    [drawerController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:drawerController animated:YES completion:^{
        
    }];
    
}

- (void)gridItemDidClicked:(KWCustomGrid *)gridItem {
    NSLog(@"您点击的格子Tag是：%ld", (long)gridItem.gridId);
    
    NSString *num = [NSString stringWithFormat:@"%ld",(long)gridItem.gridId];
    
    if ([num isEqualToString:@"0"]) {
        
        KWCustomerViewController *custom = [[KWCustomerViewController alloc] init];
        
        KWCustomerLeftViewController *leftVc = [[KWCustomerLeftViewController alloc] init];
        KWNavigationController *leftNav = [[KWNavigationController alloc] initWithRootViewController:custom];
        MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:leftNav leftDrawerViewController:leftVc];
        drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
        drawerController.closeDrawerGestureModeMask =MMCloseDrawerGestureModeAll;
        //    [drawerController.navigationController setNavigationBarHidden:YES];
        //5、设置左右两边抽屉显示的多少
        drawerController.maximumLeftDrawerWidth = 280.0;
        drawerController.maximumRightDrawerWidth = 280.0;
        
        [self presentViewController:drawerController animated:YES completion:^{
            
        }];
        
    }
    
}

@end

//
//  KWUserTool.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/1.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWUserTool.h"
#import "KWHttpTool.h"
#import "Utility.h"
#import <MJExtension/MJExtension.h>

@implementation KWUserTool

+ (void)userInfoWithParam:(KWUserInfoParam *)param success:(void (^)(KWUserInfoResult *))success failure:(void (^)(NSError *))failure {
    
    NSString *URL = [NSString stringWithFormat:@"http://%@%@", USER_SERVERADDRESS,@"/api/user/get"];
    
    [KWHttpTool postWithURL:URL params: param.mj_keyValues success:^(id json) {
        
        NSLog(@"%@",json);
        
        if([json[@"code"]intValue] != 0001) {
//            [[NSUserDefaults standardUserDefaults]setValue:json[@"back"][@"department"] forKey:@"USER_DEPART"];
        }

        if (success) {
            KWUserInfoResult *model = [[KWUserInfoResult alloc] init];
            model.code = checkStringNull(json[@"code"]);
            model.info = checkStringNull(json[@"back"]);
            
            KWUserInfoResultModel *resultModel = [KWUserInfoResultModel mj_objectWithKeyValues:json[@"back"]];
            
            NSLog(@"%@",resultModel.name);
            
            model.resultModel = resultModel;
            
            success(model);
        }
        
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end









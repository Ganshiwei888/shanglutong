//
//  KWCustomGrid.m
//  Shanglutong
//
//  Created by KeWen on 2017/8/9.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWCustomGrid.h"


@implementation KWCustomGrid

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
              title:(NSString *)title
        normalImage:(UIImage *)normalImage
   highlightedImage:(UIImage *)highlightedImage
             gridId:(NSInteger)gridId
            atIndex:(NSInteger)index
      withIconImage:(NSString *)imageString {
    
    self = [super initWithFrame:frame];
    //计算每个格子的X坐标
    CGFloat pointX = (index % PerRowGridCount) * (GridWidth + PaddingX) + PaddingX;
    //计算每个格子的Y坐标
    CGFloat pointY = (index / PerRowGridCount) * (GridHeight + PaddingY) + PaddingY;
    
    [self setFrame:CGRectMake(pointX, pointY, GridWidth+1, GridHeight+1)];
    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self addTarget:self action:@selector(gridClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 图片icon
    UIImageView * imageIcon = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width/2-18, 24, 36, 36)];
    imageIcon.image = [UIImage imageNamed:imageString];
    imageIcon.tag = self.gridId;
    [self addSubview:imageIcon];
    
    // 标题
    UILabel * title_label = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/2-42, 65, 84, 20)];
    title_label.text = title;
    title_label.textAlignment = NSTextAlignmentCenter;
    title_label.font = [UIFont systemFontOfSize:14];
    title_label.backgroundColor = [UIColor clearColor];
    title_label.textColor = UIColorFromRGB(0x3c454c);
    [self addSubview:title_label];
    
    //////////
    [self setGridId:gridId];
    [self setGridIndex:index];
    [self setGridCenterPoint:self.center];
    
    return self;
}

//响应格子点击事件
- (void)gridClick:(KWCustomGrid *)clickItem
{
    [self.delegate gridItemDidClicked:clickItem];
}

//根据格子的坐标计算格子的索引位置
+ (NSInteger)indexOfPoint:(CGPoint)point
               withButton:(UIButton *)btn
                gridArray:(NSMutableArray *)gridListArray
{
    for (NSInteger i = 0;i< gridListArray.count;i++)
    {
        UIButton *appButton = gridListArray[i];
        if (appButton != btn)
        {
            if (CGRectContainsPoint(appButton.frame, point))
            {
                return i;
            }
        }
    }
    return -1;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

//
//  KWMailListTableViewCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/3.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell.h>

@protocol KWMailListTableViewCellDelegate <NSObject>


@end


@interface MailIndicatorView : UIView
@property (nonatomic, strong) UIColor * indicatorColor;
@property (nonatomic, strong) UIColor * innerColor;
@end

@class KWMailListFrame;
@interface KWMailListTableViewCell : MGSwipeTableCell

@property (nonatomic, weak) UILabel *fromNameLabel;

@property (nonatomic, weak) UILabel *subjectLabel;

@property (nonatomic, weak) UILabel *recDateLabel;

@property (nonatomic, weak) UILabel *mailDateilLabel;

@property (nonatomic, weak) UIImageView *topView;

@property (nonatomic, weak) UIImageView *starView;

@property (nonatomic, weak) UIImageView *flagView;

@property (nonatomic, weak) UIImageView *attachmentView;

@property (nonatomic, weak) UIButton *selectButton;

@property (nonatomic, weak) UIImageView *readView;

@property (nonatomic, strong) KWMailListFrame *mailListFrame;

@property (nonatomic, strong) MailIndicatorView * indicatorView;

@property (nonatomic, assign) BOOL isRead;

-(instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;


@end

//
//  KWComDetailModel.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/23.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWComDetailModel : NSObject

@property (copy, nonatomic) NSString *Bak;
@property (copy, nonatomic) NSString *CreateTm;
@property (copy, nonatomic) NSString *Creater;
@property (copy, nonatomic) NSString *CreaterName;
@property (copy, nonatomic) NSString *Id;
@property (copy, nonatomic) NSString *KeyId;
@property (copy, nonatomic) NSString *MenuNo;
@property (copy, nonatomic) NSString *ShareUID;
@property (copy, nonatomic) NSString *ShareUID1;
@property (nonatomic, copy) NSString *TableName;
@property (nonatomic, copy) NSString *TitleHtml;
@property (nonatomic, copy) NSString *WdId;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
//
//Bak = 0000;
//CreateTm = "2017-07-27T09:55:52.67";
//Creater = admin;
//CreaterName = "\U7ba1\U7406\U5458";
//Id = 112;
//KeyId = 29229;
//MenuNo = AB;
//ShareUID = ",1,121,126,";
//ShareUID1 = "-1";
//TableName = "Client_T";
//TitleHtml = "[DZ17070006]4444we";
//WdId = 0;

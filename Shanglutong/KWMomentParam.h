//
//  KWMomentParam.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/25.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWMomentParam : KWBaseParam

@property (copy, nonatomic) NSString *Pageindex;

@property (copy, nonatomic) NSString *Pagemax;

@end

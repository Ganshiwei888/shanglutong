//
//  Utility.h
//  Shanglutong
//
//  Created by KeWen on 2017/7/20.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#ifndef Utility_h
#define Utility_h

#ifdef DEBUG
#define DMLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#else
#define DMLog(...) do { } while (0)
#endif

//******登录模块******

#define GetUserInfoSuccess  @"getUserInfoSuccess"
#define GetUserInfoFailed   @"getUserInfoFailed"
#define GetUserInfoError    @"getUserInfoError"
#define USERID @"USER_ID"
#define PASSWORD @"USER_PASSWORD"
#define OWERUSERID @"OWERUSERID"
#define OWERPASSWORD @"OWERPASSWORD"
#define ISSUBACCOUNT @"ISSUBACCOUNT"
#define NAME @"userName"
#define DEPART @"USER_DEPART"
#define OWERNAME @"OWERNAME"
#define OWERDEPART @"OWERDEPART"


#define USER_ID       [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_ID"]
#define USER_PASSWORD [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_PASSWORD"]
#define USER_IMAGE       [[NSUserDefaults standardUserDefaults] objectForKey:@"userImage"]
#define RAN           @"123"
#define SIGN          @"4297F44B13955235245B2497399D7A93"
#define USER_SERVERADDRESS  [[NSUserDefaults standardUserDefaults] objectForKey:@"userServerAddress"]
#define USER_NAME  [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]
#define CLIENTID [[NSUserDefaults standardUserDefaults] objectForKey:@"CLIENTID"]
#define USER_MailBoxID  [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_MailBoxID"]
#define USER_DEPART [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_DEPART"]
//邮箱的id


//******判断字符串是否为空******
#define checkStringNull(Str) (Str) == [NSNull null] || (Str) == nil ? @"" : [NSString stringWithFormat:@"%@", (Str)]

//DEBUG模式下打印
#ifdef DEBUG
#define NSLog(...) NSLog(@"\n ---- \n [FUNC:%s 第%d行] \n [LOG:%@] \n ---- \n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define NSLog(...)
#endif

//******颜色******
#pragma mark - color functions

#define UIColorFromHex(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0]
#define RGBACOLOR(r,g,b,a)  [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define BACKGROUND_COLOR [UIColor colorWithRed:242.0/255.0 green:236.0/255.0 blue:231.0/255.0 alpha:1.0]
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]
#define ALPHARGBCOLOR(r,g,b,a) [[UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]colorWithAlphaComponent:a]
#define KWColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

// 按钮的默认文字颜色
#define  KWTabBarButtonTitleColor  [UIColor blackColor]
// 按钮的选中文字颜色
#define  KWTabBarButtonTitleSelectedColor  KWColor(234, 103, 7)

// 图标的比例
#define KWTabBarButtonImageRatio 0.6

//尺寸


#define KWAddressTableBorder 5

//Buttom
#define BOXMAIL_BUTTONWIDTH [UIScreen mainScreen].bounds.size.width * 0.25 * 0.7 * 0.8
#define BOXMAIL_BUTTON_BETWEEN_SPACE [UIScreen mainScreen].bounds.size.width * 0.25 * 0.15 * 0.8

//手机型号
#define iphone5s  ([UIScreen mainScreen].bounds.size.height == 568)
#define iphone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iphone6p  ([UIScreen mainScreen].bounds.size.height == 736)
#define iphone4  ([UIScreen mainScreen].bounds.size.height == 480)
#define kDeviceModel  ([UIDevice currentDevice].model)
#define iPhone ([kDeviceModel hasPrefix:@"iPhone"])
#define iPad ([kDeviceModel hasPrefix:@"iPad"])
#define IOS_SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]


#define KWContentViewWidth (KWSCREEN_WIDTH*325/375)
#define KWContentViewHeight (KWSCREEN_HEIGHT*580/667)



#endif /* Utility_h */

// 3.自定义Log
#ifdef DEBUG
#define IWLog(...) NSLog(__VA_ARGS__)
#else
#define IWLog(...)
#endif



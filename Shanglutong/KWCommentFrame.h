//
//  KWCommentFrame.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/22.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWCommentModel;
@interface KWCommentFrame : NSObject

@property (nonatomic, strong) KWCommentModel *commentModel;

/**
 评论内容
 */
@property (assign, readonly, nonatomic) CGRect comTitleF;

@property (assign, readonly, nonatomic) CGRect comImageF;

@property (assign, readonly, nonatomic) CGRect divImageF;

@property (assign, readonly, nonatomic) CGRect comDetailF;

@property (assign, readonly, nonatomic) CGRect userImageF;

@property (assign, readonly, nonatomic) CGRect userNameF;

@property (assign, readonly, nonatomic) CGRect timeF;



@end

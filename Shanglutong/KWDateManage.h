//
//  KWDateManage.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/31.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import "KWBaseParam.h"

@interface KWDateManage : KWBaseParam

@property (nonatomic, copy) NSString *Act;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *fh;

@end

//
//  KWAddressNumCell.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/14.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWAddressNumCell : UITableViewCell

@property (nonatomic, weak) UIImageView *userPicimage;
@property (nonatomic, weak) UILabel *userid;
@property (nonatomic, weak) UILabel *name;
@property (nonatomic, weak) UIButton *callButton;

@end

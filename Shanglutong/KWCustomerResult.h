//
//  KWCustomerResult.h
//  Shanglutong
//
//  Created by KeWen on 2017/8/10.
//  Copyright © 2017年 KeWen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KWlistCustonModel;
@interface KWCustomerResult : NSObject

@property (nonatomic,strong)NSString *count;
@property (nonatomic,strong)NSString *index;
@property (nonatomic,strong)NSString *unread;
@property (nonatomic,strong)NSArray<KWlistCustonModel* > *list;

@end

@interface KWlistCustonModel : NSObject

@property (nonatomic,strong)NSString *Id;
@property (nonatomic,strong)NSString *No;
@property (nonatomic,strong)NSString *SName;
@property (nonatomic,strong)NSString *Name;
@property (nonatomic,strong)NSString *Address;
@property (nonatomic,strong)NSString *PostCode;
@property (nonatomic,strong)NSString *Country;
@property (nonatomic,strong)NSString *Province;
@property (nonatomic,strong)NSString *City;
@property (nonatomic,strong)NSString *Tel;
@property (nonatomic,strong)NSString *Fax;
@property (nonatomic,strong)NSString *Web;
@property (nonatomic,strong)NSString *Bak;
@property (nonatomic,strong)NSString *ClientClass;
@property (nonatomic,strong)NSString *ClientLevel;
@property (nonatomic,strong)NSString *ClientState;
@property (nonatomic,strong)NSString *ClientSource;
@property (nonatomic,strong)NSString *ClientType;
@property (nonatomic,strong)NSString *ClientLock;
@property (nonatomic,strong)NSString *Credit;
@property (nonatomic,strong)NSString *Sale;
@property (nonatomic,strong)NSString *Forwarder;
@property (nonatomic,strong)NSString *EDM;
@property (nonatomic,strong)NSString *SDate;
@property (nonatomic,strong)NSString *RDate;
@property (nonatomic,strong)NSString *Yj;
@property (nonatomic,strong)NSString *Pf;

@end
